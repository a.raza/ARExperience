﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2847414787_0_0_0;
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Types[] = { &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0 = { 1, GenInst_Int32_t2847414787_0_0_0_Types };
extern const Il2CppType Char_t2778706699_0_0_0;
static const Il2CppType* GenInst_Char_t2778706699_0_0_0_Types[] = { &Char_t2778706699_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t2778706699_0_0_0 = { 1, GenInst_Char_t2778706699_0_0_0_Types };
extern const Il2CppType IConvertible_t4194222097_0_0_0;
static const Il2CppType* GenInst_IConvertible_t4194222097_0_0_0_Types[] = { &IConvertible_t4194222097_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t4194222097_0_0_0 = { 1, GenInst_IConvertible_t4194222097_0_0_0_Types };
extern const Il2CppType IComparable_t1596950936_0_0_0;
static const Il2CppType* GenInst_IComparable_t1596950936_0_0_0_Types[] = { &IComparable_t1596950936_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1596950936_0_0_0 = { 1, GenInst_IComparable_t1596950936_0_0_0_Types };
extern const Il2CppType IComparable_1_t1741309265_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1741309265_0_0_0_Types[] = { &IComparable_1_t1741309265_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1741309265_0_0_0 = { 1, GenInst_IComparable_1_t1741309265_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2020331659_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2020331659_0_0_0_Types[] = { &IEquatable_1_t2020331659_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2020331659_0_0_0 = { 1, GenInst_IEquatable_1_t2020331659_0_0_0_Types };
extern const Il2CppType ValueType_t4014882752_0_0_0;
static const Il2CppType* GenInst_ValueType_t4014882752_0_0_0_Types[] = { &ValueType_t4014882752_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t4014882752_0_0_0 = { 1, GenInst_ValueType_t4014882752_0_0_0_Types };
extern const Il2CppType Int64_t2847414882_0_0_0;
static const Il2CppType* GenInst_Int64_t2847414882_0_0_0_Types[] = { &Int64_t2847414882_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t2847414882_0_0_0 = { 1, GenInst_Int64_t2847414882_0_0_0_Types };
extern const Il2CppType UInt32_t985925326_0_0_0;
static const Il2CppType* GenInst_UInt32_t985925326_0_0_0_Types[] = { &UInt32_t985925326_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t985925326_0_0_0 = { 1, GenInst_UInt32_t985925326_0_0_0_Types };
extern const Il2CppType UInt64_t985925421_0_0_0;
static const Il2CppType* GenInst_UInt64_t985925421_0_0_0_Types[] = { &UInt64_t985925421_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t985925421_0_0_0 = { 1, GenInst_UInt64_t985925421_0_0_0_Types };
extern const Il2CppType Byte_t2778693821_0_0_0;
static const Il2CppType* GenInst_Byte_t2778693821_0_0_0_Types[] = { &Byte_t2778693821_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t2778693821_0_0_0 = { 1, GenInst_Byte_t2778693821_0_0_0_Types };
extern const Il2CppType SByte_t2855346064_0_0_0;
static const Il2CppType* GenInst_SByte_t2855346064_0_0_0_Types[] = { &SByte_t2855346064_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t2855346064_0_0_0 = { 1, GenInst_SByte_t2855346064_0_0_0_Types };
extern const Il2CppType Int16_t2847414729_0_0_0;
static const Il2CppType* GenInst_Int16_t2847414729_0_0_0_Types[] = { &Int16_t2847414729_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t2847414729_0_0_0 = { 1, GenInst_Int16_t2847414729_0_0_0_Types };
extern const Il2CppType UInt16_t985925268_0_0_0;
static const Il2CppType* GenInst_UInt16_t985925268_0_0_0_Types[] = { &UInt16_t985925268_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t985925268_0_0_0 = { 1, GenInst_UInt16_t985925268_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t287189635_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t287189635_0_0_0_Types[] = { &IEnumerable_t287189635_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t287189635_0_0_0 = { 1, GenInst_IEnumerable_t287189635_0_0_0_Types };
extern const Il2CppType ICloneable_t2694744451_0_0_0;
static const Il2CppType* GenInst_ICloneable_t2694744451_0_0_0_Types[] = { &ICloneable_t2694744451_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t2694744451_0_0_0 = { 1, GenInst_ICloneable_t2694744451_0_0_0_Types };
extern const Il2CppType IComparable_1_t4226058764_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4226058764_0_0_0_Types[] = { &IComparable_1_t4226058764_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4226058764_0_0_0 = { 1, GenInst_IComparable_1_t4226058764_0_0_0_Types };
extern const Il2CppType IEquatable_1_t210113862_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t210113862_0_0_0_Types[] = { &IEquatable_1_t210113862_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t210113862_0_0_0 = { 1, GenInst_IEquatable_1_t210113862_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t3317110914_0_0_0;
static const Il2CppType* GenInst_IReflect_t3317110914_0_0_0_Types[] = { &IReflect_t3317110914_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3317110914_0_0_0 = { 1, GenInst_IReflect_t3317110914_0_0_0_Types };
extern const Il2CppType _Type_t1563844123_0_0_0;
static const Il2CppType* GenInst__Type_t1563844123_0_0_0_Types[] = { &_Type_t1563844123_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t1563844123_0_0_0 = { 1, GenInst__Type_t1563844123_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t2334200065_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t2334200065_0_0_0_Types[] = { &ICustomAttributeProvider_t2334200065_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2334200065_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2334200065_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3213777417_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3213777417_0_0_0_Types[] = { &_MemberInfo_t3213777417_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3213777417_0_0_0 = { 1, GenInst__MemberInfo_t3213777417_0_0_0_Types };
extern const Il2CppType IFormattable_t2460033475_0_0_0;
static const Il2CppType* GenInst_IFormattable_t2460033475_0_0_0_Types[] = { &IFormattable_t2460033475_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t2460033475_0_0_0 = { 1, GenInst_IFormattable_t2460033475_0_0_0_Types };
extern const Il2CppType IComparable_1_t1741296387_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1741296387_0_0_0_Types[] = { &IComparable_1_t1741296387_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1741296387_0_0_0 = { 1, GenInst_IComparable_1_t1741296387_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2020318781_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2020318781_0_0_0_Types[] = { &IEquatable_1_t2020318781_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2020318781_0_0_0 = { 1, GenInst_IEquatable_1_t2020318781_0_0_0_Types };
extern const Il2CppType Single_t958209021_0_0_0;
static const Il2CppType* GenInst_Single_t958209021_0_0_0_Types[] = { &Single_t958209021_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t958209021_0_0_0 = { 1, GenInst_Single_t958209021_0_0_0_Types };
extern const Il2CppType IComparable_1_t4215778883_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4215778883_0_0_0_Types[] = { &IComparable_1_t4215778883_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4215778883_0_0_0 = { 1, GenInst_IComparable_1_t4215778883_0_0_0_Types };
extern const Il2CppType IEquatable_1_t199833981_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t199833981_0_0_0_Types[] = { &IEquatable_1_t199833981_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t199833981_0_0_0 = { 1, GenInst_IEquatable_1_t199833981_0_0_0_Types };
extern const Il2CppType Double_t534516614_0_0_0;
static const Il2CppType* GenInst_Double_t534516614_0_0_0_Types[] = { &Double_t534516614_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t534516614_0_0_0 = { 1, GenInst_Double_t534516614_0_0_0_Types };
extern const Il2CppType Decimal_t1688557254_0_0_0;
static const Il2CppType* GenInst_Decimal_t1688557254_0_0_0_Types[] = { &Decimal_t1688557254_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1688557254_0_0_0 = { 1, GenInst_Decimal_t1688557254_0_0_0_Types };
extern const Il2CppType IComparable_1_t1810017353_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1810017353_0_0_0_Types[] = { &IComparable_1_t1810017353_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1810017353_0_0_0 = { 1, GenInst_IComparable_1_t1810017353_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2089039747_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2089039747_0_0_0_Types[] = { &IEquatable_1_t2089039747_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2089039747_0_0_0 = { 1, GenInst_IEquatable_1_t2089039747_0_0_0_Types };
extern const Il2CppType Boolean_t211005341_0_0_0;
static const Il2CppType* GenInst_Boolean_t211005341_0_0_0_Types[] = { &Boolean_t211005341_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t211005341_0_0_0 = { 1, GenInst_Boolean_t211005341_0_0_0_Types };
extern const Il2CppType Delegate_t3660574010_0_0_0;
static const Il2CppType* GenInst_Delegate_t3660574010_0_0_0_Types[] = { &Delegate_t3660574010_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t3660574010_0_0_0 = { 1, GenInst_Delegate_t3660574010_0_0_0_Types };
extern const Il2CppType ISerializable_t1415126241_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1415126241_0_0_0_Types[] = { &ISerializable_t1415126241_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1415126241_0_0_0 = { 1, GenInst_ISerializable_t1415126241_0_0_0_Types };
extern const Il2CppType ParameterInfo_t2610273829_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t2610273829_0_0_0_Types[] = { &ParameterInfo_t2610273829_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2610273829_0_0_0 = { 1, GenInst_ParameterInfo_t2610273829_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t1109275578_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t1109275578_0_0_0_Types[] = { &_ParameterInfo_t1109275578_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t1109275578_0_0_0 = { 1, GenInst__ParameterInfo_t1109275578_0_0_0_Types };
extern const Il2CppType ParameterModifier_t500203470_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t500203470_0_0_0_Types[] = { &ParameterModifier_t500203470_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t500203470_0_0_0 = { 1, GenInst_ParameterModifier_t500203470_0_0_0_Types };
extern const Il2CppType IComparable_1_t4243495130_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4243495130_0_0_0_Types[] = { &IComparable_1_t4243495130_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4243495130_0_0_0 = { 1, GenInst_IComparable_1_t4243495130_0_0_0_Types };
extern const Il2CppType IEquatable_1_t227550228_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t227550228_0_0_0_Types[] = { &IEquatable_1_t227550228_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t227550228_0_0_0 = { 1, GenInst_IEquatable_1_t227550228_0_0_0_Types };
extern const Il2CppType IComparable_1_t4243495188_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4243495188_0_0_0_Types[] = { &IComparable_1_t4243495188_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4243495188_0_0_0 = { 1, GenInst_IComparable_1_t4243495188_0_0_0_Types };
extern const Il2CppType IEquatable_1_t227550286_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t227550286_0_0_0_Types[] = { &IEquatable_1_t227550286_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t227550286_0_0_0 = { 1, GenInst_IEquatable_1_t227550286_0_0_0_Types };
extern const Il2CppType IComparable_1_t4243495283_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4243495283_0_0_0_Types[] = { &IComparable_1_t4243495283_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4243495283_0_0_0 = { 1, GenInst_IComparable_1_t4243495283_0_0_0_Types };
extern const Il2CppType IEquatable_1_t227550381_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t227550381_0_0_0_Types[] = { &IEquatable_1_t227550381_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t227550381_0_0_0 = { 1, GenInst_IEquatable_1_t227550381_0_0_0_Types };
extern const Il2CppType IComparable_1_t1810017295_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1810017295_0_0_0_Types[] = { &IComparable_1_t1810017295_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1810017295_0_0_0 = { 1, GenInst_IComparable_1_t1810017295_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2089039689_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2089039689_0_0_0_Types[] = { &IEquatable_1_t2089039689_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2089039689_0_0_0 = { 1, GenInst_IEquatable_1_t2089039689_0_0_0_Types };
extern const Il2CppType IComparable_1_t1817948630_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1817948630_0_0_0_Types[] = { &IComparable_1_t1817948630_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1817948630_0_0_0 = { 1, GenInst_IComparable_1_t1817948630_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2096971024_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2096971024_0_0_0_Types[] = { &IEquatable_1_t2096971024_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2096971024_0_0_0 = { 1, GenInst_IEquatable_1_t2096971024_0_0_0_Types };
extern const Il2CppType IComparable_1_t1810017448_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1810017448_0_0_0_Types[] = { &IComparable_1_t1810017448_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1810017448_0_0_0 = { 1, GenInst_IComparable_1_t1810017448_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2089039842_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2089039842_0_0_0_Types[] = { &IEquatable_1_t2089039842_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2089039842_0_0_0 = { 1, GenInst_IEquatable_1_t2089039842_0_0_0_Types };
extern const Il2CppType IComparable_1_t3792086476_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3792086476_0_0_0_Types[] = { &IComparable_1_t3792086476_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3792086476_0_0_0 = { 1, GenInst_IComparable_1_t3792086476_0_0_0_Types };
extern const Il2CppType IEquatable_1_t4071108870_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t4071108870_0_0_0_Types[] = { &IEquatable_1_t4071108870_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4071108870_0_0_0 = { 1, GenInst_IEquatable_1_t4071108870_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t3253414155_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t3253414155_0_0_0_Types[] = { &_FieldInfo_t3253414155_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t3253414155_0_0_0 = { 1, GenInst__FieldInfo_t3253414155_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3831964880_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3831964880_0_0_0_Types[] = { &_MethodInfo_t3831964880_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3831964880_0_0_0 = { 1, GenInst__MethodInfo_t3831964880_0_0_0_Types };
extern const Il2CppType MethodBase_t3461000640_0_0_0;
static const Il2CppType* GenInst_MethodBase_t3461000640_0_0_0_Types[] = { &MethodBase_t3461000640_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t3461000640_0_0_0 = { 1, GenInst_MethodBase_t3461000640_0_0_0_Types };
extern const Il2CppType _MethodBase_t3831744243_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3831744243_0_0_0_Types[] = { &_MethodBase_t3831744243_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3831744243_0_0_0 = { 1, GenInst__MethodBase_t3831744243_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t3542137334_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t3542137334_0_0_0_Types[] = { &ConstructorInfo_t3542137334_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t3542137334_0_0_0 = { 1, GenInst_ConstructorInfo_t3542137334_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t1568461643_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t1568461643_0_0_0_Types[] = { &_ConstructorInfo_t1568461643_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1568461643_0_0_0 = { 1, GenInst__ConstructorInfo_t1568461643_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t476453423_0_0_0;
static const Il2CppType* GenInst_TableRange_t476453423_0_0_0_Types[] = { &TableRange_t476453423_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t476453423_0_0_0 = { 1, GenInst_TableRange_t476453423_0_0_0_Types };
extern const Il2CppType TailoringInfo_t3819293284_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t3819293284_0_0_0_Types[] = { &TailoringInfo_t3819293284_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t3819293284_0_0_0 = { 1, GenInst_TailoringInfo_t3819293284_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1028297519_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1028297519_0_0_0_Types[] = { &KeyValuePair_2_t1028297519_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1028297519_0_0_0 = { 1, GenInst_KeyValuePair_2_t1028297519_0_0_0_Types };
extern const Il2CppType Link_t2496691359_0_0_0;
static const Il2CppType* GenInst_Link_t2496691359_0_0_0_Types[] = { &Link_t2496691359_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2496691359_0_0_0 = { 1, GenInst_Link_t2496691359_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2847414787_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2847414787_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t130027246_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2847414787_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t130027246_0_0_0_Types[] = { &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t130027246_0_0_0 = { 1, GenInst_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_KeyValuePair_2_t1028297519_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2847414787_0_0_0, &KeyValuePair_2_t1028297519_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_KeyValuePair_2_t1028297519_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_KeyValuePair_2_t1028297519_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2847414787_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3973643989_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3973643989_0_0_0_Types[] = { &KeyValuePair_2_t3973643989_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3973643989_0_0_0 = { 1, GenInst_KeyValuePair_2_t3973643989_0_0_0_Types };
extern const Il2CppType Contraction_t2055464445_0_0_0;
static const Il2CppType* GenInst_Contraction_t2055464445_0_0_0_Types[] = { &Contraction_t2055464445_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t2055464445_0_0_0 = { 1, GenInst_Contraction_t2055464445_0_0_0_Types };
extern const Il2CppType Level2Map_t2857724309_0_0_0;
static const Il2CppType* GenInst_Level2Map_t2857724309_0_0_0_Types[] = { &Level2Map_t2857724309_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t2857724309_0_0_0 = { 1, GenInst_Level2Map_t2857724309_0_0_0_Types };
extern const Il2CppType BigInteger_t1694088927_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1694088927_0_0_0_Types[] = { &BigInteger_t1694088927_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1694088927_0_0_0 = { 1, GenInst_BigInteger_t1694088927_0_0_0_Types };
extern const Il2CppType KeySizes_t2111859404_0_0_0;
static const Il2CppType* GenInst_KeySizes_t2111859404_0_0_0_Types[] = { &KeySizes_t2111859404_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t2111859404_0_0_0 = { 1, GenInst_KeySizes_t2111859404_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3312956448_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3312956448_0_0_0_Types[] = { &KeyValuePair_2_t3312956448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3312956448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3312956448_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3312956448_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3312956448_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3312956448_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3312956448_0_0_0_Types };
extern const Il2CppType IComparable_1_t3468575203_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3468575203_0_0_0_Types[] = { &IComparable_1_t3468575203_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3468575203_0_0_0 = { 1, GenInst_IComparable_1_t3468575203_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3747597597_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3747597597_0_0_0_Types[] = { &IEquatable_1_t3747597597_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3747597597_0_0_0 = { 1, GenInst_IEquatable_1_t3747597597_0_0_0_Types };
extern const Il2CppType Slot_t2579998_0_0_0;
static const Il2CppType* GenInst_Slot_t2579998_0_0_0_Types[] = { &Slot_t2579998_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2579998_0_0_0 = { 1, GenInst_Slot_t2579998_0_0_0_Types };
extern const Il2CppType Slot_t2579999_0_0_0;
static const Il2CppType* GenInst_Slot_t2579999_0_0_0_Types[] = { &Slot_t2579999_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t2579999_0_0_0 = { 1, GenInst_Slot_t2579999_0_0_0_Types };
extern const Il2CppType StackFrame_t2860697380_0_0_0;
static const Il2CppType* GenInst_StackFrame_t2860697380_0_0_0_Types[] = { &StackFrame_t2860697380_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t2860697380_0_0_0 = { 1, GenInst_StackFrame_t2860697380_0_0_0_Types };
extern const Il2CppType Calendar_t2654956468_0_0_0;
static const Il2CppType* GenInst_Calendar_t2654956468_0_0_0_Types[] = { &Calendar_t2654956468_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t2654956468_0_0_0 = { 1, GenInst_Calendar_t2654956468_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1058295580_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1058295580_0_0_0_Types[] = { &ModuleBuilder_t1058295580_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1058295580_0_0_0 = { 1, GenInst_ModuleBuilder_t1058295580_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t86618962_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t86618962_0_0_0_Types[] = { &_ModuleBuilder_t86618962_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t86618962_0_0_0 = { 1, GenInst__ModuleBuilder_t86618962_0_0_0_Types };
extern const Il2CppType Module_t206139610_0_0_0;
static const Il2CppType* GenInst_Module_t206139610_0_0_0_Types[] = { &Module_t206139610_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t206139610_0_0_0 = { 1, GenInst_Module_t206139610_0_0_0_Types };
extern const Il2CppType _Module_t2197041549_0_0_0;
static const Il2CppType* GenInst__Module_t2197041549_0_0_0_Types[] = { &_Module_t2197041549_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2197041549_0_0_0 = { 1, GenInst__Module_t2197041549_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t3382011775_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t3382011775_0_0_0_Types[] = { &ParameterBuilder_t3382011775_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3382011775_0_0_0 = { 1, GenInst_ParameterBuilder_t3382011775_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2909166611_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2909166611_0_0_0_Types[] = { &_ParameterBuilder_t2909166611_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2909166611_0_0_0 = { 1, GenInst__ParameterBuilder_t2909166611_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t3431720054_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t3431720054_0_0_0_Types[] = { &TypeU5BU5D_t3431720054_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t3431720054_0_0_0 = { 1, GenInst_TypeU5BU5D_t3431720054_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t3761522009_0_0_0;
static const Il2CppType* GenInst_ICollection_t3761522009_0_0_0_Types[] = { &ICollection_t3761522009_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t3761522009_0_0_0 = { 1, GenInst_ICollection_t3761522009_0_0_0_Types };
extern const Il2CppType IList_t1612618265_0_0_0;
static const Il2CppType* GenInst_IList_t1612618265_0_0_0_Types[] = { &IList_t1612618265_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1612618265_0_0_0 = { 1, GenInst_IList_t1612618265_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t3723275281_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t3723275281_0_0_0_Types[] = { &ILTokenInfo_t3723275281_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t3723275281_0_0_0 = { 1, GenInst_ILTokenInfo_t3723275281_0_0_0_Types };
extern const Il2CppType LabelData_t1395746974_0_0_0;
static const Il2CppType* GenInst_LabelData_t1395746974_0_0_0_Types[] = { &LabelData_t1395746974_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1395746974_0_0_0 = { 1, GenInst_LabelData_t1395746974_0_0_0_Types };
extern const Il2CppType LabelFixup_t320573180_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t320573180_0_0_0_Types[] = { &LabelFixup_t320573180_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t320573180_0_0_0 = { 1, GenInst_LabelFixup_t320573180_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t3267237648_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t3267237648_0_0_0_Types[] = { &GenericTypeParameterBuilder_t3267237648_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t3267237648_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t3267237648_0_0_0_Types };
extern const Il2CppType TypeBuilder_t4287691406_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t4287691406_0_0_0_Types[] = { &TypeBuilder_t4287691406_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t4287691406_0_0_0 = { 1, GenInst_TypeBuilder_t4287691406_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3477400324_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3477400324_0_0_0_Types[] = { &_TypeBuilder_t3477400324_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3477400324_0_0_0 = { 1, GenInst__TypeBuilder_t3477400324_0_0_0_Types };
extern const Il2CppType MethodBuilder_t765486855_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t765486855_0_0_0_Types[] = { &MethodBuilder_t765486855_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t765486855_0_0_0 = { 1, GenInst_MethodBuilder_t765486855_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t4088777533_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t4088777533_0_0_0_Types[] = { &_MethodBuilder_t4088777533_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t4088777533_0_0_0 = { 1, GenInst__MethodBuilder_t4088777533_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1859087886_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1859087886_0_0_0_Types[] = { &ConstructorBuilder_t1859087886_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1859087886_0_0_0 = { 1, GenInst_ConstructorBuilder_t1859087886_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t3050391266_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t3050391266_0_0_0_Types[] = { &_ConstructorBuilder_t3050391266_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t3050391266_0_0_0 = { 1, GenInst__ConstructorBuilder_t3050391266_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2184649998_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2184649998_0_0_0_Types[] = { &FieldBuilder_t2184649998_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2184649998_0_0_0 = { 1, GenInst_FieldBuilder_t2184649998_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t4187887906_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t4187887906_0_0_0_Types[] = { &_FieldBuilder_t4187887906_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t4187887906_0_0_0 = { 1, GenInst__FieldBuilder_t4187887906_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t2964464644_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t2964464644_0_0_0_Types[] = { &_PropertyInfo_t2964464644_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2964464644_0_0_0 = { 1, GenInst__PropertyInfo_t2964464644_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t560415562_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t560415562_0_0_0_Types[] = { &CustomAttributeTypedArgument_t560415562_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t560415562_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t560415562_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t318735129_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t318735129_0_0_0_Types[] = { &CustomAttributeNamedArgument_t318735129_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t318735129_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t318735129_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t2584644259_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t2584644259_0_0_0_Types[] = { &CustomAttributeData_t2584644259_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t2584644259_0_0_0 = { 1, GenInst_CustomAttributeData_t2584644259_0_0_0_Types };
extern const Il2CppType ResourceInfo_t4074584572_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t4074584572_0_0_0_Types[] = { &ResourceInfo_t4074584572_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t4074584572_0_0_0 = { 1, GenInst_ResourceInfo_t4074584572_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t3699857703_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t3699857703_0_0_0_Types[] = { &ResourceCacheItem_t3699857703_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t3699857703_0_0_0 = { 1, GenInst_ResourceCacheItem_t3699857703_0_0_0_Types };
extern const Il2CppType IContextProperty_t1838133337_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1838133337_0_0_0_Types[] = { &IContextProperty_t1838133337_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1838133337_0_0_0 = { 1, GenInst_IContextProperty_t1838133337_0_0_0_Types };
extern const Il2CppType Header_t1412152935_0_0_0;
static const Il2CppType* GenInst_Header_t1412152935_0_0_0_Types[] = { &Header_t1412152935_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1412152935_0_0_0 = { 1, GenInst_Header_t1412152935_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t663364710_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t663364710_0_0_0_Types[] = { &ITrackingHandler_t663364710_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t663364710_0_0_0 = { 1, GenInst_ITrackingHandler_t663364710_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2490988372_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2490988372_0_0_0_Types[] = { &IContextAttribute_t2490988372_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2490988372_0_0_0 = { 1, GenInst_IContextAttribute_t2490988372_0_0_0_Types };
extern const Il2CppType DateTime_t339033936_0_0_0;
static const Il2CppType* GenInst_DateTime_t339033936_0_0_0_Types[] = { &DateTime_t339033936_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t339033936_0_0_0 = { 1, GenInst_DateTime_t339033936_0_0_0_Types };
extern const Il2CppType IComparable_1_t3596603798_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3596603798_0_0_0_Types[] = { &IComparable_1_t3596603798_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3596603798_0_0_0 = { 1, GenInst_IComparable_1_t3596603798_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3875626192_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3875626192_0_0_0_Types[] = { &IEquatable_1_t3875626192_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3875626192_0_0_0 = { 1, GenInst_IEquatable_1_t3875626192_0_0_0_Types };
extern const Il2CppType IComparable_1_t651159820_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t651159820_0_0_0_Types[] = { &IComparable_1_t651159820_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t651159820_0_0_0 = { 1, GenInst_IComparable_1_t651159820_0_0_0_Types };
extern const Il2CppType IEquatable_1_t930182214_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t930182214_0_0_0_Types[] = { &IEquatable_1_t930182214_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t930182214_0_0_0 = { 1, GenInst_IEquatable_1_t930182214_0_0_0_Types };
extern const Il2CppType TimeSpan_t763862892_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t763862892_0_0_0_Types[] = { &TimeSpan_t763862892_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t763862892_0_0_0 = { 1, GenInst_TimeSpan_t763862892_0_0_0_Types };
extern const Il2CppType IComparable_1_t4021432754_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t4021432754_0_0_0_Types[] = { &IComparable_1_t4021432754_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t4021432754_0_0_0 = { 1, GenInst_IComparable_1_t4021432754_0_0_0_Types };
extern const Il2CppType IEquatable_1_t5487852_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t5487852_0_0_0_Types[] = { &IEquatable_1_t5487852_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t5487852_0_0_0 = { 1, GenInst_IEquatable_1_t5487852_0_0_0_Types };
extern const Il2CppType TypeTag_t1738289281_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1738289281_0_0_0_Types[] = { &TypeTag_t1738289281_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1738289281_0_0_0 = { 1, GenInst_TypeTag_t1738289281_0_0_0_Types };
extern const Il2CppType Enum_t2778772662_0_0_0;
static const Il2CppType* GenInst_Enum_t2778772662_0_0_0_Types[] = { &Enum_t2778772662_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t2778772662_0_0_0 = { 1, GenInst_Enum_t2778772662_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t3441834685_0_0_0;
static const Il2CppType* GenInst_StrongName_t3441834685_0_0_0_Types[] = { &StrongName_t3441834685_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t3441834685_0_0_0 = { 1, GenInst_StrongName_t3441834685_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t3712260035_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t3712260035_0_0_0_Types[] = { &DateTimeOffset_t3712260035_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t3712260035_0_0_0 = { 1, GenInst_DateTimeOffset_t3712260035_0_0_0_Types };
extern const Il2CppType Guid_t2778838590_0_0_0;
static const Il2CppType* GenInst_Guid_t2778838590_0_0_0_Types[] = { &Guid_t2778838590_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t2778838590_0_0_0 = { 1, GenInst_Guid_t2778838590_0_0_0_Types };
extern const Il2CppType Version_t497901645_0_0_0;
static const Il2CppType* GenInst_Version_t497901645_0_0_0_Types[] = { &Version_t497901645_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t497901645_0_0_0 = { 1, GenInst_Version_t497901645_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t211005341_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t211005341_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2686855369_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2686855369_0_0_0_Types[] = { &KeyValuePair_2_t2686855369_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2686855369_0_0_0 = { 1, GenInst_KeyValuePair_2_t2686855369_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t211005341_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t211005341_0_0_0, &Boolean_t211005341_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t211005341_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_KeyValuePair_2_t2686855369_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t211005341_0_0_0, &KeyValuePair_2_t2686855369_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_KeyValuePair_2_t2686855369_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_KeyValuePair_2_t2686855369_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t211005341_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1337234543_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1337234543_0_0_0_Types[] = { &KeyValuePair_2_t1337234543_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1337234543_0_0_0 = { 1, GenInst_KeyValuePair_2_t1337234543_0_0_0_Types };
extern const Il2CppType X509Certificate_t3432067208_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t3432067208_0_0_0_Types[] = { &X509Certificate_t3432067208_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t3432067208_0_0_0 = { 1, GenInst_X509Certificate_t3432067208_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t3135514852_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t3135514852_0_0_0_Types[] = { &IDeserializationCallback_t3135514852_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t3135514852_0_0_0 = { 1, GenInst_IDeserializationCallback_t3135514852_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t1122151684_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t1122151684_0_0_0_Types[] = { &X509ChainStatus_t1122151684_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1122151684_0_0_0 = { 1, GenInst_X509ChainStatus_t1122151684_0_0_0_Types };
extern const Il2CppType Capture_t1645813025_0_0_0;
static const Il2CppType* GenInst_Capture_t1645813025_0_0_0_Types[] = { &Capture_t1645813025_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t1645813025_0_0_0 = { 1, GenInst_Capture_t1645813025_0_0_0_Types };
extern const Il2CppType Group_t3792618586_0_0_0;
static const Il2CppType* GenInst_Group_t3792618586_0_0_0_Types[] = { &Group_t3792618586_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t3792618586_0_0_0 = { 1, GenInst_Group_t3792618586_0_0_0_Types };
extern const Il2CppType Mark_t3725932776_0_0_0;
static const Il2CppType* GenInst_Mark_t3725932776_0_0_0_Types[] = { &Mark_t3725932776_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3725932776_0_0_0 = { 1, GenInst_Mark_t3725932776_0_0_0_Types };
extern const Il2CppType UriScheme_t3266528785_0_0_0;
static const Il2CppType* GenInst_UriScheme_t3266528785_0_0_0_Types[] = { &UriScheme_t3266528785_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t3266528785_0_0_0 = { 1, GenInst_UriScheme_t3266528785_0_0_0_Types };
extern const Il2CppType BigInteger_t1694088928_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1694088928_0_0_0_Types[] = { &BigInteger_t1694088928_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1694088928_0_0_0 = { 1, GenInst_BigInteger_t1694088928_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t58506160_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t58506160_0_0_0_Types[] = { &ByteU5BU5D_t58506160_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t58506160_0_0_0 = { 1, GenInst_ByteU5BU5D_t58506160_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t2725032177_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t2725032177_0_0_0_Types[] = { &ClientCertificateType_t2725032177_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t2725032177_0_0_0 = { 1, GenInst_ClientCertificateType_t2725032177_0_0_0_Types };
extern const Il2CppType Link_t1745155715_0_0_0;
static const Il2CppType* GenInst_Link_t1745155715_0_0_0_Types[] = { &Link_t1745155715_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1745155715_0_0_0 = { 1, GenInst_Link_t1745155715_0_0_0_Types };
extern const Il2CppType Object_t3878351788_0_0_0;
static const Il2CppType* GenInst_Object_t3878351788_0_0_0_Types[] = { &Object_t3878351788_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t3878351788_0_0_0 = { 1, GenInst_Object_t3878351788_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t1533365615_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t1533365615_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t1533365615_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1533365615_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t1533365615_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3856922794_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3856922794_0_0_0_Types[] = { &IAchievementDescription_t3856922794_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3856922794_0_0_0 = { 1, GenInst_IAchievementDescription_t3856922794_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t851122303_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t851122303_0_0_0_Types[] = { &IAchievementU5BU5D_t851122303_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t851122303_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t851122303_0_0_0_Types };
extern const Il2CppType IAchievement_t813719258_0_0_0;
static const Il2CppType* GenInst_IAchievement_t813719258_0_0_0_Types[] = { &IAchievement_t813719258_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t813719258_0_0_0 = { 1, GenInst_IAchievement_t813719258_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t684312048_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t684312048_0_0_0_Types[] = { &IScoreU5BU5D_t684312048_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t684312048_0_0_0 = { 1, GenInst_IScoreU5BU5D_t684312048_0_0_0_Types };
extern const Il2CppType IScore_t3029734269_0_0_0;
static const Il2CppType* GenInst_IScore_t3029734269_0_0_0_Types[] = { &IScore_t3029734269_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t3029734269_0_0_0 = { 1, GenInst_IScore_t3029734269_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t2316972724_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t2316972724_0_0_0_Types[] = { &IUserProfileU5BU5D_t2316972724_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t2316972724_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t2316972724_0_0_0_Types };
extern const Il2CppType IUserProfile_t2749774601_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t2749774601_0_0_0_Types[] = { &IUserProfile_t2749774601_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t2749774601_0_0_0 = { 1, GenInst_IUserProfile_t2749774601_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3784099155_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3784099155_0_0_0_Types[] = { &AchievementDescription_t3784099155_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3784099155_0_0_0 = { 1, GenInst_AchievementDescription_t3784099155_0_0_0_Types };
extern const Il2CppType UserProfile_t2517340964_0_0_0;
static const Il2CppType* GenInst_UserProfile_t2517340964_0_0_0_Types[] = { &UserProfile_t2517340964_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t2517340964_0_0_0 = { 1, GenInst_UserProfile_t2517340964_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t3894999172_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t3894999172_0_0_0_Types[] = { &GcLeaderboard_t3894999172_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t3894999172_0_0_0 = { 1, GenInst_GcLeaderboard_t3894999172_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1317012096_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1317012096_0_0_0_Types[] = { &GcAchievementData_t1317012096_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1317012096_0_0_0 = { 1, GenInst_GcAchievementData_t1317012096_0_0_0_Types };
extern const Il2CppType Achievement_t581285621_0_0_0;
static const Il2CppType* GenInst_Achievement_t581285621_0_0_0_Types[] = { &Achievement_t581285621_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t581285621_0_0_0 = { 1, GenInst_Achievement_t581285621_0_0_0_Types };
extern const Il2CppType GcScoreData_t2223678307_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t2223678307_0_0_0_Types[] = { &GcScoreData_t2223678307_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t2223678307_0_0_0 = { 1, GenInst_GcScoreData_t2223678307_0_0_0_Types };
extern const Il2CppType Score_t1540476504_0_0_0;
static const Il2CppType* GenInst_Score_t1540476504_0_0_0_Types[] = { &Score_t1540476504_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t1540476504_0_0_0 = { 1, GenInst_Score_t1540476504_0_0_0_Types };
extern const Il2CppType Vector3_t3525329789_0_0_0;
static const Il2CppType* GenInst_Vector3_t3525329789_0_0_0_Types[] = { &Vector3_t3525329789_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3525329789_0_0_0 = { 1, GenInst_Vector3_t3525329789_0_0_0_Types };
extern const Il2CppType Vector2_t3525329788_0_0_0;
static const Il2CppType* GenInst_Vector2_t3525329788_0_0_0_Types[] = { &Vector2_t3525329788_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t3525329788_0_0_0 = { 1, GenInst_Vector2_t3525329788_0_0_0_Types };
extern const Il2CppType Material_t1886596500_0_0_0;
static const Il2CppType* GenInst_Material_t1886596500_0_0_0_Types[] = { &Material_t1886596500_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t1886596500_0_0_0 = { 1, GenInst_Material_t1886596500_0_0_0_Types };
extern const Il2CppType Color_t1588175760_0_0_0;
static const Il2CppType* GenInst_Color_t1588175760_0_0_0_Types[] = { &Color_t1588175760_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t1588175760_0_0_0 = { 1, GenInst_Color_t1588175760_0_0_0_Types };
extern const Il2CppType Color32_t4137084207_0_0_0;
static const Il2CppType* GenInst_Color32_t4137084207_0_0_0_Types[] = { &Color32_t4137084207_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t4137084207_0_0_0 = { 1, GenInst_Color32_t4137084207_0_0_0_Types };
extern const Il2CppType Keyframe_t2095052507_0_0_0;
static const Il2CppType* GenInst_Keyframe_t2095052507_0_0_0_Types[] = { &Keyframe_t2095052507_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t2095052507_0_0_0 = { 1, GenInst_Keyframe_t2095052507_0_0_0_Types };
extern const Il2CppType Camera_t3533968274_0_0_0;
static const Il2CppType* GenInst_Camera_t3533968274_0_0_0_Types[] = { &Camera_t3533968274_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t3533968274_0_0_0 = { 1, GenInst_Camera_t3533968274_0_0_0_Types };
extern const Il2CppType Behaviour_t3120504042_0_0_0;
static const Il2CppType* GenInst_Behaviour_t3120504042_0_0_0_Types[] = { &Behaviour_t3120504042_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t3120504042_0_0_0 = { 1, GenInst_Behaviour_t3120504042_0_0_0_Types };
extern const Il2CppType Component_t2126946602_0_0_0;
static const Il2CppType* GenInst_Component_t2126946602_0_0_0_Types[] = { &Component_t2126946602_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t2126946602_0_0_0 = { 1, GenInst_Component_t2126946602_0_0_0_Types };
extern const Il2CppType Display_t564335855_0_0_0;
static const Il2CppType* GenInst_Display_t564335855_0_0_0_Types[] = { &Display_t564335855_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t564335855_0_0_0 = { 1, GenInst_Display_t564335855_0_0_0_Types };
extern const Il2CppType Playable_t3404354247_0_0_0;
static const Il2CppType* GenInst_Playable_t3404354247_0_0_0_Types[] = { &Playable_t3404354247_0_0_0 };
extern const Il2CppGenericInst GenInst_Playable_t3404354247_0_0_0 = { 1, GenInst_Playable_t3404354247_0_0_0_Types };
extern const Il2CppType IDisposable_t1628921374_0_0_0;
static const Il2CppType* GenInst_IDisposable_t1628921374_0_0_0_Types[] = { &IDisposable_t1628921374_0_0_0 };
extern const Il2CppGenericInst GenInst_IDisposable_t1628921374_0_0_0 = { 1, GenInst_IDisposable_t1628921374_0_0_0_Types };
extern const Il2CppType ContactPoint_t2951122365_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t2951122365_0_0_0_Types[] = { &ContactPoint_t2951122365_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t2951122365_0_0_0 = { 1, GenInst_ContactPoint_t2951122365_0_0_0_Types };
extern const Il2CppType WebCamDevice_t1687076478_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t1687076478_0_0_0_Types[] = { &WebCamDevice_t1687076478_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t1687076478_0_0_0 = { 1, GenInst_WebCamDevice_t1687076478_0_0_0_Types };
extern const Il2CppType Font_t1525081276_0_0_0;
static const Il2CppType* GenInst_Font_t1525081276_0_0_0_Types[] = { &Font_t1525081276_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1525081276_0_0_0 = { 1, GenInst_Font_t1525081276_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t3151226183_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t3151226183_0_0_0_Types[] = { &GUILayoutOption_t3151226183_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t3151226183_0_0_0 = { 1, GenInst_GUILayoutOption_t3151226183_0_0_0_Types };
extern const Il2CppType LayoutCache_t3653031512_0_0_0;
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &LayoutCache_t3653031512_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t816448501_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t816448501_0_0_0_Types[] = { &KeyValuePair_2_t816448501_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t816448501_0_0_0 = { 1, GenInst_KeyValuePair_2_t816448501_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Il2CppObject_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t816448501_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t816448501_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t816448501_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t816448501_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &LayoutCache_t3653031512_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3632373593_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3632373593_0_0_0_Types[] = { &KeyValuePair_2_t3632373593_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3632373593_0_0_0 = { 1, GenInst_KeyValuePair_2_t3632373593_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t1011928986_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t1011928986_0_0_0_Types[] = { &GUILayoutEntry_t1011928986_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t1011928986_0_0_0 = { 1, GenInst_GUILayoutEntry_t1011928986_0_0_0_Types };
extern const Il2CppType GUIStyle_t1006925219_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t1006925219_0_0_0_Types[] = { &GUIStyle_t1006925219_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t1006925219_0_0_0 = { 1, GenInst_GUIStyle_t1006925219_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1006925219_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t1006925219_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2133154421_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2133154421_0_0_0_Types[] = { &KeyValuePair_2_t2133154421_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2133154421_0_0_0 = { 1, GenInst_KeyValuePair_2_t2133154421_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t3647875775_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t3647875775_0_0_0_Types[] = { &DisallowMultipleComponent_t3647875775_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t3647875775_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t3647875775_0_0_0_Types };
extern const Il2CppType Attribute_t498693649_0_0_0;
static const Il2CppType* GenInst_Attribute_t498693649_0_0_0_Types[] = { &Attribute_t498693649_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t498693649_0_0_0 = { 1, GenInst_Attribute_t498693649_0_0_0_Types };
extern const Il2CppType _Attribute_t2001626847_0_0_0;
static const Il2CppType* GenInst__Attribute_t2001626847_0_0_0_Types[] = { &_Attribute_t2001626847_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t2001626847_0_0_0 = { 1, GenInst__Attribute_t2001626847_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t2676812948_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t2676812948_0_0_0_Types[] = { &ExecuteInEditMode_t2676812948_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t2676812948_0_0_0 = { 1, GenInst_ExecuteInEditMode_t2676812948_0_0_0_Types };
extern const Il2CppType RequireComponent_t3196495237_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t3196495237_0_0_0_Types[] = { &RequireComponent_t3196495237_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t3196495237_0_0_0 = { 1, GenInst_RequireComponent_t3196495237_0_0_0_Types };
extern const Il2CppType HitInfo_t2591228609_0_0_0;
static const Il2CppType* GenInst_HitInfo_t2591228609_0_0_0_Types[] = { &HitInfo_t2591228609_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t2591228609_0_0_0 = { 1, GenInst_HitInfo_t2591228609_0_0_0_Types };
extern const Il2CppType PersistentCall_t4127144549_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t4127144549_0_0_0_Types[] = { &PersistentCall_t4127144549_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t4127144549_0_0_0 = { 1, GenInst_PersistentCall_t4127144549_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t1733537956_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t1733537956_0_0_0_Types[] = { &BaseInvokableCall_t1733537956_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1733537956_0_0_0 = { 1, GenInst_BaseInvokableCall_t1733537956_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType GameObject_t4012695102_0_0_0;
static const Il2CppType* GenInst_GameObject_t4012695102_0_0_0_Types[] = { &GameObject_t4012695102_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t4012695102_0_0_0 = { 1, GenInst_GameObject_t4012695102_0_0_0_Types };
extern const Il2CppType RenderTexture_t12905170_0_0_0;
static const Il2CppType* GenInst_RenderTexture_t12905170_0_0_0_Types[] = { &RenderTexture_t12905170_0_0_0 };
extern const Il2CppGenericInst GenInst_RenderTexture_t12905170_0_0_0 = { 1, GenInst_RenderTexture_t12905170_0_0_0_Types };
extern const Il2CppType Texture_t1769722184_0_0_0;
static const Il2CppType* GenInst_Texture_t1769722184_0_0_0_Types[] = { &Texture_t1769722184_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture_t1769722184_0_0_0 = { 1, GenInst_Texture_t1769722184_0_0_0_Types };
extern const Il2CppType EyewearCalibrationReading_t3061222002_0_0_0;
static const Il2CppType* GenInst_EyewearCalibrationReading_t3061222002_0_0_0_Types[] = { &EyewearCalibrationReading_t3061222002_0_0_0 };
extern const Il2CppGenericInst GenInst_EyewearCalibrationReading_t3061222002_0_0_0 = { 1, GenInst_EyewearCalibrationReading_t3061222002_0_0_0_Types };
extern const Il2CppType VideoBackgroundAbstractBehaviour_t1820506856_0_0_0;
static const Il2CppType* GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_Types[] = { &Camera_t3533968274_0_0_0, &VideoBackgroundAbstractBehaviour_t1820506856_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0 = { 2, GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_Types };
static const Il2CppType* GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Camera_t3533968274_0_0_0, &VideoBackgroundAbstractBehaviour_t1820506856_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t448298814_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t448298814_0_0_0_Types[] = { &KeyValuePair_2_t448298814_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t448298814_0_0_0 = { 1, GenInst_KeyValuePair_2_t448298814_0_0_0_Types };
static const Il2CppType* GenInst_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_Types[] = { &VideoBackgroundAbstractBehaviour_t1820506856_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0 = { 1, GenInst_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_Types };
extern const Il2CppType Renderer_t1092684080_0_0_0;
static const Il2CppType* GenInst_Renderer_t1092684080_0_0_0_Types[] = { &Renderer_t1092684080_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t1092684080_0_0_0 = { 1, GenInst_Renderer_t1092684080_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t2427445838_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t2427445838_0_0_0_Types[] = { &TrackableBehaviour_t2427445838_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t2427445838_0_0_0 = { 1, GenInst_TrackableBehaviour_t2427445838_0_0_0_Types };
extern const Il2CppType TrackableResultData_t2490169420_0_0_0;
static const Il2CppType* GenInst_TrackableResultData_t2490169420_0_0_0_Types[] = { &TrackableResultData_t2490169420_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t2490169420_0_0_0 = { 1, GenInst_TrackableResultData_t2490169420_0_0_0_Types };
extern const Il2CppType HideExcessAreaAbstractBehaviour_t3630507405_0_0_0;
static const Il2CppType* GenInst_HideExcessAreaAbstractBehaviour_t3630507405_0_0_0_Types[] = { &HideExcessAreaAbstractBehaviour_t3630507405_0_0_0 };
extern const Il2CppGenericInst GenInst_HideExcessAreaAbstractBehaviour_t3630507405_0_0_0 = { 1, GenInst_HideExcessAreaAbstractBehaviour_t3630507405_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t3012272455_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t3012272455_0_0_0_Types[] = { &MonoBehaviour_t3012272455_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3012272455_0_0_0 = { 1, GenInst_MonoBehaviour_t3012272455_0_0_0_Types };
extern const Il2CppType IViewerParameters_t703877403_0_0_0;
static const Il2CppType* GenInst_IViewerParameters_t703877403_0_0_0_Types[] = { &IViewerParameters_t703877403_0_0_0 };
extern const Il2CppGenericInst GenInst_IViewerParameters_t703877403_0_0_0 = { 1, GenInst_IViewerParameters_t703877403_0_0_0_Types };
extern const Il2CppType ITrackableEventHandler_t2602716514_0_0_0;
static const Il2CppType* GenInst_ITrackableEventHandler_t2602716514_0_0_0_Types[] = { &ITrackableEventHandler_t2602716514_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackableEventHandler_t2602716514_0_0_0 = { 1, GenInst_ITrackableEventHandler_t2602716514_0_0_0_Types };
extern const Il2CppType ReconstructionAbstractBehaviour_t730550669_0_0_0;
static const Il2CppType* GenInst_ReconstructionAbstractBehaviour_t730550669_0_0_0_Types[] = { &ReconstructionAbstractBehaviour_t730550669_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionAbstractBehaviour_t730550669_0_0_0 = { 1, GenInst_ReconstructionAbstractBehaviour_t730550669_0_0_0_Types };
extern const Il2CppType ICloudRecoEventHandler_t1786089329_0_0_0;
static const Il2CppType* GenInst_ICloudRecoEventHandler_t1786089329_0_0_0_Types[] = { &ICloudRecoEventHandler_t1786089329_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloudRecoEventHandler_t1786089329_0_0_0 = { 1, GenInst_ICloudRecoEventHandler_t1786089329_0_0_0_Types };
extern const Il2CppType TargetSearchResult_t3905350710_0_0_0;
static const Il2CppType* GenInst_TargetSearchResult_t3905350710_0_0_0_Types[] = { &TargetSearchResult_t3905350710_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSearchResult_t3905350710_0_0_0 = { 1, GenInst_TargetSearchResult_t3905350710_0_0_0_Types };
extern const Il2CppType Trackable_t1174201883_0_0_0;
static const Il2CppType* GenInst_Trackable_t1174201883_0_0_0_Types[] = { &Trackable_t1174201883_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t1174201883_0_0_0 = { 1, GenInst_Trackable_t1174201883_0_0_0_Types };
extern const Il2CppType VirtualButton_t3523578643_0_0_0;
static const Il2CppType* GenInst_VirtualButton_t3523578643_0_0_0_Types[] = { &VirtualButton_t3523578643_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButton_t3523578643_0_0_0 = { 1, GenInst_VirtualButton_t3523578643_0_0_0_Types };
extern const Il2CppType PIXEL_FORMAT_t3232215024_0_0_0;
extern const Il2CppType Image_t2805765713_0_0_0;
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Image_t2805765713_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3253813172_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3253813172_0_0_0_Types[] = { &KeyValuePair_2_t3253813172_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253813172_0_0_0 = { 1, GenInst_KeyValuePair_2_t3253813172_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0 = { 1, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Il2CppObject_0_0_0, &PIXEL_FORMAT_t3232215024_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3253813172_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t3253813172_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3253813172_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3253813172_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &Image_t2805765713_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t927505169_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t927505169_0_0_0_Types[] = { &KeyValuePair_2_t927505169_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t927505169_0_0_0 = { 1, GenInst_KeyValuePair_2_t927505169_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Trackable_t1174201883_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Trackable_t1174201883_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1153543964_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1153543964_0_0_0_Types[] = { &KeyValuePair_2_t1153543964_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1153543964_0_0_0 = { 1, GenInst_KeyValuePair_2_t1153543964_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButton_t3523578643_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButton_t3523578643_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3502920724_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3502920724_0_0_0_Types[] = { &KeyValuePair_2_t3502920724_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3502920724_0_0_0 = { 1, GenInst_KeyValuePair_2_t3502920724_0_0_0_Types };
extern const Il2CppType DataSet_t1547874638_0_0_0;
static const Il2CppType* GenInst_DataSet_t1547874638_0_0_0_Types[] = { &DataSet_t1547874638_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSet_t1547874638_0_0_0 = { 1, GenInst_DataSet_t1547874638_0_0_0_Types };
extern const Il2CppType DataSetImpl_t611363726_0_0_0;
static const Il2CppType* GenInst_DataSetImpl_t611363726_0_0_0_Types[] = { &DataSetImpl_t611363726_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetImpl_t611363726_0_0_0 = { 1, GenInst_DataSetImpl_t611363726_0_0_0_Types };
extern const Il2CppType Marker_t737566064_0_0_0;
static const Il2CppType* GenInst_Marker_t737566064_0_0_0_Types[] = { &Marker_t737566064_0_0_0 };
extern const Il2CppGenericInst GenInst_Marker_t737566064_0_0_0 = { 1, GenInst_Marker_t737566064_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Marker_t737566064_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Marker_t737566064_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t716908145_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t716908145_0_0_0_Types[] = { &KeyValuePair_2_t716908145_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t716908145_0_0_0 = { 1, GenInst_KeyValuePair_2_t716908145_0_0_0_Types };
extern const Il2CppType WordData_t92535284_0_0_0;
static const Il2CppType* GenInst_WordData_t92535284_0_0_0_Types[] = { &WordData_t92535284_0_0_0 };
extern const Il2CppGenericInst GenInst_WordData_t92535284_0_0_0 = { 1, GenInst_WordData_t92535284_0_0_0_Types };
extern const Il2CppType WordResultData_t408256561_0_0_0;
static const Il2CppType* GenInst_WordResultData_t408256561_0_0_0_Types[] = { &WordResultData_t408256561_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResultData_t408256561_0_0_0 = { 1, GenInst_WordResultData_t408256561_0_0_0_Types };
extern const Il2CppType SmartTerrainRevisionData_t695238033_0_0_0;
static const Il2CppType* GenInst_SmartTerrainRevisionData_t695238033_0_0_0_Types[] = { &SmartTerrainRevisionData_t695238033_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainRevisionData_t695238033_0_0_0 = { 1, GenInst_SmartTerrainRevisionData_t695238033_0_0_0_Types };
extern const Il2CppType SurfaceData_t264493527_0_0_0;
static const Il2CppType* GenInst_SurfaceData_t264493527_0_0_0_Types[] = { &SurfaceData_t264493527_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceData_t264493527_0_0_0 = { 1, GenInst_SurfaceData_t264493527_0_0_0_Types };
extern const Il2CppType PropData_t3365470669_0_0_0;
static const Il2CppType* GenInst_PropData_t3365470669_0_0_0_Types[] = { &PropData_t3365470669_0_0_0 };
extern const Il2CppGenericInst GenInst_PropData_t3365470669_0_0_0 = { 1, GenInst_PropData_t3365470669_0_0_0_Types };
extern const Il2CppType IEditorTrackableBehaviour_t292912100_0_0_0;
static const Il2CppType* GenInst_IEditorTrackableBehaviour_t292912100_0_0_0_Types[] = { &IEditorTrackableBehaviour_t292912100_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorTrackableBehaviour_t292912100_0_0_0 = { 1, GenInst_IEditorTrackableBehaviour_t292912100_0_0_0_Types };
static const Il2CppType* GenInst_Image_t2805765713_0_0_0_Types[] = { &Image_t2805765713_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2805765713_0_0_0 = { 1, GenInst_Image_t2805765713_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackable_t892409423_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackable_t892409423_0_0_0_Types[] = { &SmartTerrainTrackable_t892409423_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackable_t892409423_0_0_0 = { 1, GenInst_SmartTerrainTrackable_t892409423_0_0_0_Types };
extern const Il2CppType SmartTerrainTrackableBehaviour_t3759975578_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTrackableBehaviour_t3759975578_0_0_0_Types[] = { &SmartTerrainTrackableBehaviour_t3759975578_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTrackableBehaviour_t3759975578_0_0_0 = { 1, GenInst_SmartTerrainTrackableBehaviour_t3759975578_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t985925268_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0 = { 2, GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t985925268_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3461775296_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3461775296_0_0_0_Types[] = { &KeyValuePair_2_t3461775296_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3461775296_0_0_0 = { 1, GenInst_KeyValuePair_2_t3461775296_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t985925268_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t985925268_0_0_0, &UInt16_t985925268_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t985925268_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_KeyValuePair_2_t3461775296_0_0_0_Types[] = { &Il2CppObject_0_0_0, &UInt16_t985925268_0_0_0, &KeyValuePair_2_t3461775296_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_KeyValuePair_2_t3461775296_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_KeyValuePair_2_t3461775296_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Type_t_0_0_0, &UInt16_t985925268_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2587004281_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2587004281_0_0_0_Types[] = { &KeyValuePair_2_t2587004281_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2587004281_0_0_0 = { 1, GenInst_KeyValuePair_2_t2587004281_0_0_0_Types };
extern const Il2CppType RectangleData_t790089391_0_0_0;
static const Il2CppType* GenInst_RectangleData_t790089391_0_0_0_Types[] = { &RectangleData_t790089391_0_0_0 };
extern const Il2CppGenericInst GenInst_RectangleData_t790089391_0_0_0 = { 1, GenInst_RectangleData_t790089391_0_0_0_Types };
extern const Il2CppType WordResult_t1871594749_0_0_0;
static const Il2CppType* GenInst_WordResult_t1871594749_0_0_0_Types[] = { &WordResult_t1871594749_0_0_0 };
extern const Il2CppGenericInst GenInst_WordResult_t1871594749_0_0_0 = { 1, GenInst_WordResult_t1871594749_0_0_0_Types };
extern const Il2CppType Word_t2737707072_0_0_0;
static const Il2CppType* GenInst_Word_t2737707072_0_0_0_Types[] = { &Word_t2737707072_0_0_0 };
extern const Il2CppGenericInst GenInst_Word_t2737707072_0_0_0 = { 1, GenInst_Word_t2737707072_0_0_0_Types };
extern const Il2CppType WordAbstractBehaviour_t162212295_0_0_0;
static const Il2CppType* GenInst_WordAbstractBehaviour_t162212295_0_0_0_Types[] = { &WordAbstractBehaviour_t162212295_0_0_0 };
extern const Il2CppGenericInst GenInst_WordAbstractBehaviour_t162212295_0_0_0 = { 1, GenInst_WordAbstractBehaviour_t162212295_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &WordResult_t1871594749_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &WordResult_t1871594749_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1850936830_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1850936830_0_0_0_Types[] = { &KeyValuePair_2_t1850936830_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1850936830_0_0_0 = { 1, GenInst_KeyValuePair_2_t1850936830_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &WordAbstractBehaviour_t162212295_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &WordAbstractBehaviour_t162212295_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t141554376_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t141554376_0_0_0_Types[] = { &KeyValuePair_2_t141554376_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t141554376_0_0_0 = { 1, GenInst_KeyValuePair_2_t141554376_0_0_0_Types };
extern const Il2CppType List_1_t959171264_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t959171264_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t959171264_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t959171264_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t959171264_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_List_1_t959171264_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &String_t_0_0_0, &List_1_t959171264_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t959171264_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t959171264_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2085400466_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2085400466_0_0_0_Types[] = { &KeyValuePair_2_t2085400466_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2085400466_0_0_0 = { 1, GenInst_KeyValuePair_2_t2085400466_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t959171264_0_0_0_Types[] = { &List_1_t959171264_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t959171264_0_0_0 = { 1, GenInst_List_1_t959171264_0_0_0_Types };
extern const Il2CppType ILoadLevelEventHandler_t1667599611_0_0_0;
static const Il2CppType* GenInst_ILoadLevelEventHandler_t1667599611_0_0_0_Types[] = { &ILoadLevelEventHandler_t1667599611_0_0_0 };
extern const Il2CppGenericInst GenInst_ILoadLevelEventHandler_t1667599611_0_0_0 = { 1, GenInst_ILoadLevelEventHandler_t1667599611_0_0_0_Types };
extern const Il2CppType ISmartTerrainEventHandler_t674878747_0_0_0;
static const Il2CppType* GenInst_ISmartTerrainEventHandler_t674878747_0_0_0_Types[] = { &ISmartTerrainEventHandler_t674878747_0_0_0 };
extern const Il2CppGenericInst GenInst_ISmartTerrainEventHandler_t674878747_0_0_0 = { 1, GenInst_ISmartTerrainEventHandler_t674878747_0_0_0_Types };
extern const Il2CppType SmartTerrainInitializationInfo_t1520723184_0_0_0;
static const Il2CppType* GenInst_SmartTerrainInitializationInfo_t1520723184_0_0_0_Types[] = { &SmartTerrainInitializationInfo_t1520723184_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainInitializationInfo_t1520723184_0_0_0 = { 1, GenInst_SmartTerrainInitializationInfo_t1520723184_0_0_0_Types };
extern const Il2CppType Prop_t2737501337_0_0_0;
static const Il2CppType* GenInst_Prop_t2737501337_0_0_0_Types[] = { &Prop_t2737501337_0_0_0 };
extern const Il2CppGenericInst GenInst_Prop_t2737501337_0_0_0 = { 1, GenInst_Prop_t2737501337_0_0_0_Types };
extern const Il2CppType Surface_t2546426275_0_0_0;
static const Il2CppType* GenInst_Surface_t2546426275_0_0_0_Types[] = { &Surface_t2546426275_0_0_0 };
extern const Il2CppGenericInst GenInst_Surface_t2546426275_0_0_0 = { 1, GenInst_Surface_t2546426275_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Surface_t2546426275_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Surface_t2546426275_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2525768356_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2525768356_0_0_0_Types[] = { &KeyValuePair_2_t2525768356_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2525768356_0_0_0 = { 1, GenInst_KeyValuePair_2_t2525768356_0_0_0_Types };
extern const Il2CppType SurfaceAbstractBehaviour_t1477931716_0_0_0;
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &SurfaceAbstractBehaviour_t1477931716_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &SurfaceAbstractBehaviour_t1477931716_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1457273797_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1457273797_0_0_0_Types[] = { &KeyValuePair_2_t1457273797_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1457273797_0_0_0 = { 1, GenInst_KeyValuePair_2_t1457273797_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Prop_t2737501337_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Prop_t2737501337_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2716843418_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2716843418_0_0_0_Types[] = { &KeyValuePair_2_t2716843418_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2716843418_0_0_0 = { 1, GenInst_KeyValuePair_2_t2716843418_0_0_0_Types };
extern const Il2CppType PropAbstractBehaviour_t909732494_0_0_0;
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &PropAbstractBehaviour_t909732494_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &PropAbstractBehaviour_t909732494_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t889074575_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t889074575_0_0_0_Types[] = { &KeyValuePair_2_t889074575_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t889074575_0_0_0 = { 1, GenInst_KeyValuePair_2_t889074575_0_0_0_Types };
static const Il2CppType* GenInst_PropAbstractBehaviour_t909732494_0_0_0_Types[] = { &PropAbstractBehaviour_t909732494_0_0_0 };
extern const Il2CppGenericInst GenInst_PropAbstractBehaviour_t909732494_0_0_0 = { 1, GenInst_PropAbstractBehaviour_t909732494_0_0_0_Types };
static const Il2CppType* GenInst_SurfaceAbstractBehaviour_t1477931716_0_0_0_Types[] = { &SurfaceAbstractBehaviour_t1477931716_0_0_0 };
extern const Il2CppGenericInst GenInst_SurfaceAbstractBehaviour_t1477931716_0_0_0 = { 1, GenInst_SurfaceAbstractBehaviour_t1477931716_0_0_0_Types };
extern const Il2CppType MeshFilter_t4177078322_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t4177078322_0_0_0_Types[] = { &MeshFilter_t4177078322_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t4177078322_0_0_0 = { 1, GenInst_MeshFilter_t4177078322_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableBehaviour_t2427445838_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableBehaviour_t2427445838_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2406787919_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2406787919_0_0_0_Types[] = { &KeyValuePair_2_t2406787919_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2406787919_0_0_0 = { 1, GenInst_KeyValuePair_2_t2406787919_0_0_0_Types };
extern const Il2CppType MarkerAbstractBehaviour_t1462758039_0_0_0;
static const Il2CppType* GenInst_MarkerAbstractBehaviour_t1462758039_0_0_0_Types[] = { &MarkerAbstractBehaviour_t1462758039_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerAbstractBehaviour_t1462758039_0_0_0 = { 1, GenInst_MarkerAbstractBehaviour_t1462758039_0_0_0_Types };
extern const Il2CppType IEditorMarkerBehaviour_t2835060931_0_0_0;
static const Il2CppType* GenInst_IEditorMarkerBehaviour_t2835060931_0_0_0_Types[] = { &IEditorMarkerBehaviour_t2835060931_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorMarkerBehaviour_t2835060931_0_0_0 = { 1, GenInst_IEditorMarkerBehaviour_t2835060931_0_0_0_Types };
extern const Il2CppType WorldCenterTrackableBehaviour_t2878848245_0_0_0;
static const Il2CppType* GenInst_WorldCenterTrackableBehaviour_t2878848245_0_0_0_Types[] = { &WorldCenterTrackableBehaviour_t2878848245_0_0_0 };
extern const Il2CppGenericInst GenInst_WorldCenterTrackableBehaviour_t2878848245_0_0_0 = { 1, GenInst_WorldCenterTrackableBehaviour_t2878848245_0_0_0_Types };
extern const Il2CppType DataSetTrackableBehaviour_t1793464454_0_0_0;
static const Il2CppType* GenInst_DataSetTrackableBehaviour_t1793464454_0_0_0_Types[] = { &DataSetTrackableBehaviour_t1793464454_0_0_0 };
extern const Il2CppGenericInst GenInst_DataSetTrackableBehaviour_t1793464454_0_0_0 = { 1, GenInst_DataSetTrackableBehaviour_t1793464454_0_0_0_Types };
extern const Il2CppType IEditorDataSetTrackableBehaviour_t3829574192_0_0_0;
static const Il2CppType* GenInst_IEditorDataSetTrackableBehaviour_t3829574192_0_0_0_Types[] = { &IEditorDataSetTrackableBehaviour_t3829574192_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorDataSetTrackableBehaviour_t3829574192_0_0_0 = { 1, GenInst_IEditorDataSetTrackableBehaviour_t3829574192_0_0_0_Types };
extern const Il2CppType VirtualButtonAbstractBehaviour_t2124633940_0_0_0;
static const Il2CppType* GenInst_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_Types[] = { &VirtualButtonAbstractBehaviour_t2124633940_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonAbstractBehaviour_t2124633940_0_0_0 = { 1, GenInst_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_Types };
extern const Il2CppType IEditorVirtualButtonBehaviour_t3419943660_0_0_0;
static const Il2CppType* GenInst_IEditorVirtualButtonBehaviour_t3419943660_0_0_0_Types[] = { &IEditorVirtualButtonBehaviour_t3419943660_0_0_0 };
extern const Il2CppGenericInst GenInst_IEditorVirtualButtonBehaviour_t3419943660_0_0_0 = { 1, GenInst_IEditorVirtualButtonBehaviour_t3419943660_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableResultData_t2490169420_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2469511501_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2469511501_0_0_0_Types[] = { &KeyValuePair_2_t2469511501_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2469511501_0_0_0 = { 1, GenInst_KeyValuePair_2_t2469511501_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableResultData_t2490169420_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_Int32_t2847414787_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_Int32_t2847414787_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableResultData_t2490169420_0_0_0, &TrackableResultData_t2490169420_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableResultData_t2490169420_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_KeyValuePair_2_t2469511501_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &TrackableResultData_t2490169420_0_0_0, &KeyValuePair_2_t2469511501_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_KeyValuePair_2_t2469511501_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_KeyValuePair_2_t2469511501_0_0_0_Types };
extern const Il2CppType VirtualButtonData_t1123011399_0_0_0;
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonData_t1123011399_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1102353480_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1102353480_0_0_0_Types[] = { &KeyValuePair_2_t1102353480_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1102353480_0_0_0 = { 1, GenInst_KeyValuePair_2_t1102353480_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t1123011399_0_0_0_Types[] = { &VirtualButtonData_t1123011399_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t1123011399_0_0_0 = { 1, GenInst_VirtualButtonData_t1123011399_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonData_t1123011399_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_Int32_t2847414787_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_Int32_t2847414787_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonData_t1123011399_0_0_0, &VirtualButtonData_t1123011399_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonData_t1123011399_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_KeyValuePair_2_t1102353480_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonData_t1123011399_0_0_0, &KeyValuePair_2_t1102353480_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_KeyValuePair_2_t1102353480_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_KeyValuePair_2_t1102353480_0_0_0_Types };
extern const Il2CppType ImageTarget_t2294340546_0_0_0;
static const Il2CppType* GenInst_ImageTarget_t2294340546_0_0_0_Types[] = { &ImageTarget_t2294340546_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t2294340546_0_0_0 = { 1, GenInst_ImageTarget_t2294340546_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &ImageTarget_t2294340546_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &ImageTarget_t2294340546_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2273682627_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2273682627_0_0_0_Types[] = { &KeyValuePair_2_t2273682627_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2273682627_0_0_0 = { 1, GenInst_KeyValuePair_2_t2273682627_0_0_0_Types };
extern const Il2CppType ProfileData_t1845074131_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1845074131_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0 = { 2, GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1845074131_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t25956863_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t25956863_0_0_0_Types[] = { &KeyValuePair_2_t25956863_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t25956863_0_0_0 = { 1, GenInst_KeyValuePair_2_t25956863_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1845074131_0_0_0_Types[] = { &ProfileData_t1845074131_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1845074131_0_0_0 = { 1, GenInst_ProfileData_t1845074131_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1845074131_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1845074131_0_0_0, &ProfileData_t1845074131_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1845074131_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_KeyValuePair_2_t25956863_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ProfileData_t1845074131_0_0_0, &KeyValuePair_2_t25956863_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_KeyValuePair_2_t25956863_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_KeyValuePair_2_t25956863_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &String_t_0_0_0, &ProfileData_t1845074131_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2971303333_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2971303333_0_0_0_Types[] = { &KeyValuePair_2_t2971303333_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2971303333_0_0_0 = { 1, GenInst_KeyValuePair_2_t2971303333_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonAbstractBehaviour_t2124633940_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &VirtualButtonAbstractBehaviour_t2124633940_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2103976021_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2103976021_0_0_0_Types[] = { &KeyValuePair_2_t2103976021_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2103976021_0_0_0 = { 1, GenInst_KeyValuePair_2_t2103976021_0_0_0_Types };
extern const Il2CppType ITrackerEventHandler_t3415742645_0_0_0;
static const Il2CppType* GenInst_ITrackerEventHandler_t3415742645_0_0_0_Types[] = { &ITrackerEventHandler_t3415742645_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackerEventHandler_t3415742645_0_0_0 = { 1, GenInst_ITrackerEventHandler_t3415742645_0_0_0_Types };
extern const Il2CppType IVideoBackgroundEventHandler_t248107270_0_0_0;
static const Il2CppType* GenInst_IVideoBackgroundEventHandler_t248107270_0_0_0_Types[] = { &IVideoBackgroundEventHandler_t248107270_0_0_0 };
extern const Il2CppGenericInst GenInst_IVideoBackgroundEventHandler_t248107270_0_0_0 = { 1, GenInst_IVideoBackgroundEventHandler_t248107270_0_0_0_Types };
extern const Il2CppType InitError_t1722238648_0_0_0;
static const Il2CppType* GenInst_InitError_t1722238648_0_0_0_Types[] = { &InitError_t1722238648_0_0_0 };
extern const Il2CppGenericInst GenInst_InitError_t1722238648_0_0_0 = { 1, GenInst_InitError_t1722238648_0_0_0_Types };
extern const Il2CppType BackgroundPlaneAbstractBehaviour_t1953260483_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneAbstractBehaviour_t1953260483_0_0_0_Types[] = { &BackgroundPlaneAbstractBehaviour_t1953260483_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneAbstractBehaviour_t1953260483_0_0_0 = { 1, GenInst_BackgroundPlaneAbstractBehaviour_t1953260483_0_0_0_Types };
extern const Il2CppType ITextRecoEventHandler_t4066948379_0_0_0;
static const Il2CppType* GenInst_ITextRecoEventHandler_t4066948379_0_0_0_Types[] = { &ITextRecoEventHandler_t4066948379_0_0_0 };
extern const Il2CppGenericInst GenInst_ITextRecoEventHandler_t4066948379_0_0_0 = { 1, GenInst_ITextRecoEventHandler_t4066948379_0_0_0_Types };
extern const Il2CppType IUserDefinedTargetEventHandler_t3593614636_0_0_0;
static const Il2CppType* GenInst_IUserDefinedTargetEventHandler_t3593614636_0_0_0_Types[] = { &IUserDefinedTargetEventHandler_t3593614636_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserDefinedTargetEventHandler_t3593614636_0_0_0 = { 1, GenInst_IUserDefinedTargetEventHandler_t3593614636_0_0_0_Types };
extern const Il2CppType MeshRenderer_t1217738301_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t1217738301_0_0_0_Types[] = { &MeshRenderer_t1217738301_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1217738301_0_0_0 = { 1, GenInst_MeshRenderer_t1217738301_0_0_0_Types };
extern const Il2CppType IVirtualButtonEventHandler_t2233055962_0_0_0;
static const Il2CppType* GenInst_IVirtualButtonEventHandler_t2233055962_0_0_0_Types[] = { &IVirtualButtonEventHandler_t2233055962_0_0_0 };
extern const Il2CppGenericInst GenInst_IVirtualButtonEventHandler_t2233055962_0_0_0 = { 1, GenInst_IVirtualButtonEventHandler_t2233055962_0_0_0_Types };
extern const Il2CppType Collider_t955670625_0_0_0;
static const Il2CppType* GenInst_Collider_t955670625_0_0_0_Types[] = { &Collider_t955670625_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t955670625_0_0_0 = { 1, GenInst_Collider_t955670625_0_0_0_Types };
extern const Il2CppType WireframeBehaviour_t2976175819_0_0_0;
static const Il2CppType* GenInst_WireframeBehaviour_t2976175819_0_0_0_Types[] = { &WireframeBehaviour_t2976175819_0_0_0 };
extern const Il2CppGenericInst GenInst_WireframeBehaviour_t2976175819_0_0_0 = { 1, GenInst_WireframeBehaviour_t2976175819_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2707313927_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2707313927_gp_0_0_0_0_Types[] = { &IEnumerable_1_t2707313927_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2707313927_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2707313927_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1181603442_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types[] = { &Array_Sort_m1181603442_gp_0_0_0_0, &Array_Sort_m1181603442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3908760906_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m3908760906_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types[] = { &Array_Sort_m3908760906_gp_0_0_0_0, &Array_Sort_m3908760906_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m646233104_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types[] = { &Array_Sort_m646233104_gp_0_0_0_0, &Array_Sort_m646233104_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2404937677_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types[] = { &Array_Sort_m2404937677_gp_0_0_0_0, &Array_Sort_m2404937677_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m377069906_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types[] = { &Array_Sort_m377069906_gp_0_0_0_0, &Array_Sort_m377069906_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1327718954_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1327718954_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types[] = { &Array_Sort_m1327718954_gp_0_0_0_0, &Array_Sort_m1327718954_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m4084526832_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types[] = { &Array_Sort_m4084526832_gp_0_0_0_0, &Array_Sort_m4084526832_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3263917805_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m3263917805_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types[] = { &Array_Sort_m3263917805_gp_0_0_0_0, &Array_Sort_m3263917805_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m3566161319_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types[] = { &Array_Sort_m3566161319_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3566161319_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3566161319_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1767877396_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types[] = { &Array_Sort_m1767877396_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1767877396_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1767877396_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m785378185_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types[] = { &Array_qsort_m785378185_gp_0_0_0_0, &Array_qsort_m785378185_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m3718693973_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types[] = { &Array_compare_m3718693973_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m3718693973_gp_0_0_0_0 = { 1, GenInst_Array_compare_m3718693973_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m3270161954_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types[] = { &Array_qsort_m3270161954_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m3270161954_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3270161954_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m2347367271_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types[] = { &Array_Resize_m2347367271_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m2347367271_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2347367271_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m123384663_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m123384663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1326446122_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types[] = { &Array_ForEach_m1326446122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1326446122_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1326446122_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m1697145810_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1697145810_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m1697145810_gp_0_0_0_0, &Array_ConvertAll_m1697145810_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m614217902_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m614217902_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m653822311_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m653822311_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m1650781518_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m1650781518_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m308210168_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types[] = { &Array_FindIndex_m308210168_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m308210168_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m308210168_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m3338256477_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types[] = { &Array_FindIndex_m3338256477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m1605056024_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types[] = { &Array_FindIndex_m1605056024_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3507857443_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3507857443_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1578426497_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1578426497_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m3933814211_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m3933814211_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1779658273_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1779658273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3823835921_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3823835921_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1695958374_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1695958374_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3280162097_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3280162097_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m921616327_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m921616327_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m4157241456_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m4157241456_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m925096551_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m925096551_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1181152586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types[] = { &Array_FindAll_m1181152586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1181152586_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1181152586_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m2312185345_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types[] = { &Array_Exists_m2312185345_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m2312185345_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m2312185345_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m4233614634_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m4233614634_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m3878993575_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types[] = { &Array_Find_m3878993575_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m3878993575_gp_0_0_0_0 = { 1, GenInst_Array_Find_m3878993575_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m3671273393_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types[] = { &Array_FindLast_m3671273393_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m3671273393_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m3671273393_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2673614354_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2673614354_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2673614354_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2673614354_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2673614354_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2546651274_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2546651274_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2546651274_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2546651274_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2546651274_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t490656863_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t490656863_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t490656863_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t490656863_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t490656863_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t838940445_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t838940445_gp_0_0_0_0_Types[] = { &IList_1_t838940445_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t838940445_gp_0_0_0_0 = { 1, GenInst_IList_1_t838940445_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t56169053_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t56169053_gp_0_0_0_0_Types[] = { &ICollection_1_t56169053_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t56169053_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t56169053_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t2791603879_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t2791603879_gp_0_0_0_0_Types[] = { &Nullable_1_t2791603879_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2791603879_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2791603879_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t3643230563_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t3643230563_gp_0_0_0_0_Types[] = { &Comparer_1_t3643230563_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t3643230563_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t3643230563_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t401310062_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t401310062_gp_0_0_0_0_Types[] = { &DefaultComparer_t401310062_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t401310062_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t401310062_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3795808858_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3795808858_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3795808858_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3795808858_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3795808858_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2776849293_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2776849293_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_0_0_0_0, &Dictionary_2_t2776849293_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2897654481_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2897654481_0_0_0_Types[] = { &KeyValuePair_2_t2897654481_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2897654481_0_0_0 = { 1, GenInst_KeyValuePair_2_t2897654481_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_0_0_0_0, &Dictionary_2_t2776849293_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_0_0_0_0, &Dictionary_2_t2776849293_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_0_0_0_0, &Dictionary_2_t2776849293_gp_1_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 3, GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3981551357_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3981551357_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3981551357_gp_0_0_0_0_ShimEnumerator_t3981551357_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3981551357_gp_0_0_0_0, &ShimEnumerator_t3981551357_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3981551357_gp_0_0_0_0_ShimEnumerator_t3981551357_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3981551357_gp_0_0_0_0_ShimEnumerator_t3981551357_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804842_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1072804842_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804842_gp_0_0_0_0_Enumerator_t1072804842_gp_1_0_0_0_Types[] = { &Enumerator_t1072804842_gp_0_0_0_0, &Enumerator_t1072804842_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804842_gp_0_0_0_0_Enumerator_t1072804842_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1072804842_gp_0_0_0_0_Enumerator_t1072804842_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t6794133_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t6794133_0_0_0_Types[] = { &KeyValuePair_2_t6794133_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t6794133_0_0_0 = { 1, GenInst_KeyValuePair_2_t6794133_0_0_0_Types };
extern const Il2CppType KeyCollection_t1486433853_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1486433853_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0_Types[] = { &KeyCollection_t1486433853_gp_0_0_0_0, &KeyCollection_t1486433853_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1486433853_gp_0_0_0_0_Types[] = { &KeyCollection_t1486433853_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1486433853_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1486433853_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804843_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1072804843_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804843_gp_0_0_0_0_Enumerator_t1072804843_gp_1_0_0_0_Types[] = { &Enumerator_t1072804843_gp_0_0_0_0, &Enumerator_t1072804843_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804843_gp_0_0_0_0_Enumerator_t1072804843_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1072804843_gp_0_0_0_0_Enumerator_t1072804843_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1072804843_gp_0_0_0_0_Types[] = { &Enumerator_t1072804843_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1072804843_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0_Types[] = { &KeyCollection_t1486433853_gp_0_0_0_0, &KeyCollection_t1486433853_gp_1_0_0_0, &KeyCollection_t1486433853_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0_Types[] = { &KeyCollection_t1486433853_gp_0_0_0_0, &KeyCollection_t1486433853_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t3044571855_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3044571855_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_Types[] = { &ValueCollection_t3044571855_gp_0_0_0_0, &ValueCollection_t3044571855_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3044571855_gp_1_0_0_0_Types[] = { &ValueCollection_t3044571855_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3044571855_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3044571855_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804844_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1072804844_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804844_gp_0_0_0_0_Enumerator_t1072804844_gp_1_0_0_0_Types[] = { &Enumerator_t1072804844_gp_0_0_0_0, &Enumerator_t1072804844_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804844_gp_0_0_0_0_Enumerator_t1072804844_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1072804844_gp_0_0_0_0_Enumerator_t1072804844_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1072804844_gp_1_0_0_0_Types[] = { &Enumerator_t1072804844_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804844_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1072804844_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_Types[] = { &ValueCollection_t3044571855_gp_0_0_0_0, &ValueCollection_t3044571855_gp_1_0_0_0, &ValueCollection_t3044571855_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_Types[] = { &ValueCollection_t3044571855_gp_1_0_0_0, &ValueCollection_t3044571855_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t130027246_0_0_0_DictionaryEntry_t130027246_0_0_0_Types[] = { &DictionaryEntry_t130027246_0_0_0, &DictionaryEntry_t130027246_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t130027246_0_0_0_DictionaryEntry_t130027246_0_0_0 = { 2, GenInst_DictionaryEntry_t130027246_0_0_0_DictionaryEntry_t130027246_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_KeyValuePair_2_t2897654481_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_0_0_0_0, &Dictionary_2_t2776849293_gp_1_0_0_0, &KeyValuePair_2_t2897654481_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_KeyValuePair_2_t2897654481_0_0_0 = { 3, GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_KeyValuePair_2_t2897654481_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2897654481_0_0_0_KeyValuePair_2_t2897654481_0_0_0_Types[] = { &KeyValuePair_2_t2897654481_0_0_0, &KeyValuePair_2_t2897654481_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2897654481_0_0_0_KeyValuePair_2_t2897654481_0_0_0 = { 2, GenInst_KeyValuePair_2_t2897654481_0_0_0_KeyValuePair_2_t2897654481_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2776849293_gp_1_0_0_0_Types[] = { &Dictionary_2_t2776849293_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2776849293_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2776849293_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2368211453_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2368211453_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2368211453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2368211453_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2368211453_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t401310063_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t401310063_gp_0_0_0_0_Types[] = { &DefaultComparer_t401310063_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t401310063_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t401310063_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t3181333492_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t3181333492_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t3181333492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3181333492_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3181333492_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4148160909_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4148160909_0_0_0_Types[] = { &KeyValuePair_2_t4148160909_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4148160909_0_0_0 = { 1, GenInst_KeyValuePair_2_t4148160909_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2833266262_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t2833266262_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2833266262_gp_0_0_0_0_IDictionary_2_t2833266262_gp_1_0_0_0_Types[] = { &IDictionary_2_t2833266262_gp_0_0_0_0, &IDictionary_2_t2833266262_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2833266262_gp_0_0_0_0_IDictionary_2_t2833266262_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t2833266262_gp_0_0_0_0_IDictionary_2_t2833266262_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2241909187_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2241909187_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2241909187_gp_0_0_0_0_KeyValuePair_2_t2241909187_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t2241909187_gp_0_0_0_0, &KeyValuePair_2_t2241909187_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2241909187_gp_0_0_0_0_KeyValuePair_2_t2241909187_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t2241909187_gp_0_0_0_0_KeyValuePair_2_t2241909187_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t475681172_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t475681172_gp_0_0_0_0_Types[] = { &List_1_t475681172_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t475681172_gp_0_0_0_0 = { 1, GenInst_List_1_t475681172_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804845_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804845_gp_0_0_0_0_Types[] = { &Enumerator_t1072804845_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804845_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1072804845_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t248125333_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t248125333_gp_0_0_0_0_Types[] = { &Collection_1_t248125333_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t248125333_gp_0_0_0_0 = { 1, GenInst_Collection_1_t248125333_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2688875287_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2688875287_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2688875287_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2688875287_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2688875287_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t215192845_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t215192845_gp_0_0_0_0_Types[] = { &LinkedList_1_t215192845_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t215192845_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t215192845_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804846_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804846_gp_0_0_0_0_Types[] = { &Enumerator_t1072804846_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804846_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1072804846_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3677586223_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3677586223_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3677586223_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3677586223_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3677586223_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t2261335230_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t2261335230_gp_0_0_0_0_Types[] = { &Stack_1_t2261335230_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t2261335230_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2261335230_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804847_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804847_gp_0_0_0_0_Types[] = { &Enumerator_t1072804847_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804847_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1072804847_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t501446570_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t501446570_gp_0_0_0_0_Types[] = { &HashSet_1_t501446570_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t501446570_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t501446570_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1072804848_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1072804848_gp_0_0_0_0_Types[] = { &Enumerator_t1072804848_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1072804848_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1072804848_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t3130145517_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t3130145517_gp_0_0_0_0_Types[] = { &PrimeHelper_t3130145517_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3130145517_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3130145517_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m1224968048_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types[] = { &Enumerable_Any_m1224968048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Cast_m3421076499_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types[] = { &Enumerable_Cast_m3421076499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0 = { 1, GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types[] = { &Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2837618484_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2837618484_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m2710245799_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m2710245799_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m1340241468_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types[] = { &Enumerable_First_m1340241468_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m1340241468_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m1340241468_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m600216364_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m600216364_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m3865828353_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m3865828353_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1248874793_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1248874793_gp_0_0_0_0_Types[] = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t1248874793_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1248874793_gp_0_0_0_0 = { 1, GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1248874793_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m2068134811_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m2548367508_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m885533373_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m290105122_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2190484233_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2983504498_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m228373778_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m228373778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m2204190206_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types[] = { &Component_GetComponents_m2204190206_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m482375811_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types[] = { &Component_GetComponents_m482375811_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m482375811_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m482375811_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0_Types };
extern const Il2CppType SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0;
static const Il2CppType* GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0_Types[] = { &SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0 = { 1, GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0_Types };
extern const Il2CppType GUILayer_t999894339_0_0_0;
static const Il2CppType* GenInst_GUILayer_t999894339_0_0_0_Types[] = { &GUILayer_t999894339_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t999894339_0_0_0 = { 1, GenInst_GUILayer_t999894339_0_0_0_Types };
extern const Il2CppType VideoBackgroundManagerAbstractBehaviour_t1915856045_0_0_0;
static const Il2CppType* GenInst_VideoBackgroundManagerAbstractBehaviour_t1915856045_0_0_0_Types[] = { &VideoBackgroundManagerAbstractBehaviour_t1915856045_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundManagerAbstractBehaviour_t1915856045_0_0_0 = { 1, GenInst_VideoBackgroundManagerAbstractBehaviour_t1915856045_0_0_0_Types };
extern const Il2CppType ObjectTracker_t3275326447_0_0_0;
static const Il2CppType* GenInst_ObjectTracker_t3275326447_0_0_0_Types[] = { &ObjectTracker_t3275326447_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTracker_t3275326447_0_0_0 = { 1, GenInst_ObjectTracker_t3275326447_0_0_0_Types };
extern const Il2CppType SmartTerrainTracker_t3586718050_0_0_0;
static const Il2CppType* GenInst_SmartTerrainTracker_t3586718050_0_0_0_Types[] = { &SmartTerrainTracker_t3586718050_0_0_0 };
extern const Il2CppGenericInst GenInst_SmartTerrainTracker_t3586718050_0_0_0 = { 1, GenInst_SmartTerrainTracker_t3586718050_0_0_0_Types };
extern const Il2CppType VuforiaAbstractBehaviour_t2427322319_0_0_0;
static const Il2CppType* GenInst_VuforiaAbstractBehaviour_t2427322319_0_0_0_Types[] = { &VuforiaAbstractBehaviour_t2427322319_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaAbstractBehaviour_t2427322319_0_0_0 = { 1, GenInst_VuforiaAbstractBehaviour_t2427322319_0_0_0_Types };
extern const Il2CppType DeviceTracker_t1348055288_0_0_0;
static const Il2CppType* GenInst_DeviceTracker_t1348055288_0_0_0_Types[] = { &DeviceTracker_t1348055288_0_0_0 };
extern const Il2CppGenericInst GenInst_DeviceTracker_t1348055288_0_0_0 = { 1, GenInst_DeviceTracker_t1348055288_0_0_0_Types };
extern const Il2CppType RotationalDeviceTracker_t4008517455_0_0_0;
static const Il2CppType* GenInst_RotationalDeviceTracker_t4008517455_0_0_0_Types[] = { &RotationalDeviceTracker_t4008517455_0_0_0 };
extern const Il2CppGenericInst GenInst_RotationalDeviceTracker_t4008517455_0_0_0 = { 1, GenInst_RotationalDeviceTracker_t4008517455_0_0_0_Types };
extern const Il2CppType DistortionRenderingBehaviour_t28016708_0_0_0;
static const Il2CppType* GenInst_DistortionRenderingBehaviour_t28016708_0_0_0_Types[] = { &DistortionRenderingBehaviour_t28016708_0_0_0 };
extern const Il2CppGenericInst GenInst_DistortionRenderingBehaviour_t28016708_0_0_0 = { 1, GenInst_DistortionRenderingBehaviour_t28016708_0_0_0_Types };
extern const Il2CppType ImageTargetAbstractBehaviour_t2880150149_0_0_0;
static const Il2CppType* GenInst_ImageTargetAbstractBehaviour_t2880150149_0_0_0_Types[] = { &ImageTargetAbstractBehaviour_t2880150149_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetAbstractBehaviour_t2880150149_0_0_0 = { 1, GenInst_ImageTargetAbstractBehaviour_t2880150149_0_0_0_Types };
extern const Il2CppType DigitalEyewearAbstractBehaviour_t1656621721_0_0_0;
static const Il2CppType* GenInst_DigitalEyewearAbstractBehaviour_t1656621721_0_0_0_Types[] = { &DigitalEyewearAbstractBehaviour_t1656621721_0_0_0 };
extern const Il2CppGenericInst GenInst_DigitalEyewearAbstractBehaviour_t1656621721_0_0_0 = { 1, GenInst_DigitalEyewearAbstractBehaviour_t1656621721_0_0_0_Types };
extern const Il2CppType MarkerTracker_t2552664724_0_0_0;
static const Il2CppType* GenInst_MarkerTracker_t2552664724_0_0_0_Types[] = { &MarkerTracker_t2552664724_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerTracker_t2552664724_0_0_0 = { 1, GenInst_MarkerTracker_t2552664724_0_0_0_Types };
extern const Il2CppType Mesh_t1525280346_0_0_0;
static const Il2CppType* GenInst_Mesh_t1525280346_0_0_0_Types[] = { &Mesh_t1525280346_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t1525280346_0_0_0 = { 1, GenInst_Mesh_t1525280346_0_0_0_Types };
extern const Il2CppType ReconstructionFromTarget_t1377534133_0_0_0;
static const Il2CppType* GenInst_ReconstructionFromTarget_t1377534133_0_0_0_Types[] = { &ReconstructionFromTarget_t1377534133_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionFromTarget_t1377534133_0_0_0 = { 1, GenInst_ReconstructionFromTarget_t1377534133_0_0_0_Types };
extern const Il2CppType TextTracker_t2495541825_0_0_0;
static const Il2CppType* GenInst_TextTracker_t2495541825_0_0_0_Types[] = { &TextTracker_t2495541825_0_0_0 };
extern const Il2CppGenericInst GenInst_TextTracker_t2495541825_0_0_0 = { 1, GenInst_TextTracker_t2495541825_0_0_0_Types };
extern const Il2CppType BackgroundPlaneBehaviour_t3262988229_0_0_0;
static const Il2CppType* GenInst_BackgroundPlaneBehaviour_t3262988229_0_0_0_Types[] = { &BackgroundPlaneBehaviour_t3262988229_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundPlaneBehaviour_t3262988229_0_0_0 = { 1, GenInst_BackgroundPlaneBehaviour_t3262988229_0_0_0_Types };
extern const Il2CppType ReconstructionBehaviour_t4293105551_0_0_0;
static const Il2CppType* GenInst_ReconstructionBehaviour_t4293105551_0_0_0_Types[] = { &ReconstructionBehaviour_t4293105551_0_0_0 };
extern const Il2CppGenericInst GenInst_ReconstructionBehaviour_t4293105551_0_0_0 = { 1, GenInst_ReconstructionBehaviour_t4293105551_0_0_0_Types };
extern const Il2CppType DigitalEyewearBehaviour_t674634907_0_0_0;
static const Il2CppType* GenInst_DigitalEyewearBehaviour_t674634907_0_0_0_Types[] = { &DigitalEyewearBehaviour_t674634907_0_0_0 };
extern const Il2CppGenericInst GenInst_DigitalEyewearBehaviour_t674634907_0_0_0 = { 1, GenInst_DigitalEyewearBehaviour_t674634907_0_0_0_Types };
extern const Il2CppType VideoBackgroundManager_t3152649370_0_0_0;
static const Il2CppType* GenInst_VideoBackgroundManager_t3152649370_0_0_0_Types[] = { &VideoBackgroundManager_t3152649370_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoBackgroundManager_t3152649370_0_0_0 = { 1, GenInst_VideoBackgroundManager_t3152649370_0_0_0_Types };
extern const Il2CppType ComponentFactoryStarterBehaviour_t1834097041_0_0_0;
static const Il2CppType* GenInst_ComponentFactoryStarterBehaviour_t1834097041_0_0_0_Types[] = { &ComponentFactoryStarterBehaviour_t1834097041_0_0_0 };
extern const Il2CppGenericInst GenInst_ComponentFactoryStarterBehaviour_t1834097041_0_0_0 = { 1, GenInst_ComponentFactoryStarterBehaviour_t1834097041_0_0_0_Types };
extern const Il2CppType VuforiaBehaviour_t1383180241_0_0_0;
static const Il2CppType* GenInst_VuforiaBehaviour_t1383180241_0_0_0_Types[] = { &VuforiaBehaviour_t1383180241_0_0_0 };
extern const Il2CppGenericInst GenInst_VuforiaBehaviour_t1383180241_0_0_0 = { 1, GenInst_VuforiaBehaviour_t1383180241_0_0_0_Types };
extern const Il2CppType MaskOutBehaviour_t742775633_0_0_0;
static const Il2CppType* GenInst_MaskOutBehaviour_t742775633_0_0_0_Types[] = { &MaskOutBehaviour_t742775633_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskOutBehaviour_t742775633_0_0_0 = { 1, GenInst_MaskOutBehaviour_t742775633_0_0_0_Types };
extern const Il2CppType VirtualButtonBehaviour_t3242546262_0_0_0;
static const Il2CppType* GenInst_VirtualButtonBehaviour_t3242546262_0_0_0_Types[] = { &VirtualButtonBehaviour_t3242546262_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonBehaviour_t3242546262_0_0_0 = { 1, GenInst_VirtualButtonBehaviour_t3242546262_0_0_0_Types };
extern const Il2CppType TurnOffBehaviour_t816306785_0_0_0;
static const Il2CppType* GenInst_TurnOffBehaviour_t816306785_0_0_0_Types[] = { &TurnOffBehaviour_t816306785_0_0_0 };
extern const Il2CppGenericInst GenInst_TurnOffBehaviour_t816306785_0_0_0 = { 1, GenInst_TurnOffBehaviour_t816306785_0_0_0_Types };
extern const Il2CppType ImageTargetBehaviour_t1584945287_0_0_0;
static const Il2CppType* GenInst_ImageTargetBehaviour_t1584945287_0_0_0_Types[] = { &ImageTargetBehaviour_t1584945287_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTargetBehaviour_t1584945287_0_0_0 = { 1, GenInst_ImageTargetBehaviour_t1584945287_0_0_0_Types };
extern const Il2CppType MarkerBehaviour_t2463029913_0_0_0;
static const Il2CppType* GenInst_MarkerBehaviour_t2463029913_0_0_0_Types[] = { &MarkerBehaviour_t2463029913_0_0_0 };
extern const Il2CppGenericInst GenInst_MarkerBehaviour_t2463029913_0_0_0 = { 1, GenInst_MarkerBehaviour_t2463029913_0_0_0_Types };
extern const Il2CppType MultiTargetBehaviour_t2071934185_0_0_0;
static const Il2CppType* GenInst_MultiTargetBehaviour_t2071934185_0_0_0_Types[] = { &MultiTargetBehaviour_t2071934185_0_0_0 };
extern const Il2CppGenericInst GenInst_MultiTargetBehaviour_t2071934185_0_0_0 = { 1, GenInst_MultiTargetBehaviour_t2071934185_0_0_0_Types };
extern const Il2CppType CylinderTargetBehaviour_t2447349868_0_0_0;
static const Il2CppType* GenInst_CylinderTargetBehaviour_t2447349868_0_0_0_Types[] = { &CylinderTargetBehaviour_t2447349868_0_0_0 };
extern const Il2CppGenericInst GenInst_CylinderTargetBehaviour_t2447349868_0_0_0 = { 1, GenInst_CylinderTargetBehaviour_t2447349868_0_0_0_Types };
extern const Il2CppType WordBehaviour_t559172041_0_0_0;
static const Il2CppType* GenInst_WordBehaviour_t559172041_0_0_0_Types[] = { &WordBehaviour_t559172041_0_0_0 };
extern const Il2CppGenericInst GenInst_WordBehaviour_t559172041_0_0_0 = { 1, GenInst_WordBehaviour_t559172041_0_0_0_Types };
extern const Il2CppType TextRecoBehaviour_t3745030759_0_0_0;
static const Il2CppType* GenInst_TextRecoBehaviour_t3745030759_0_0_0_Types[] = { &TextRecoBehaviour_t3745030759_0_0_0 };
extern const Il2CppGenericInst GenInst_TextRecoBehaviour_t3745030759_0_0_0 = { 1, GenInst_TextRecoBehaviour_t3745030759_0_0_0_Types };
extern const Il2CppType ObjectTargetBehaviour_t2124871139_0_0_0;
static const Il2CppType* GenInst_ObjectTargetBehaviour_t2124871139_0_0_0_Types[] = { &ObjectTargetBehaviour_t2124871139_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTargetBehaviour_t2124871139_0_0_0 = { 1, GenInst_ObjectTargetBehaviour_t2124871139_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t816448501_0_0_0_KeyValuePair_2_t816448501_0_0_0_Types[] = { &KeyValuePair_2_t816448501_0_0_0, &KeyValuePair_2_t816448501_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t816448501_0_0_0_KeyValuePair_2_t816448501_0_0_0 = { 2, GenInst_KeyValuePair_2_t816448501_0_0_0_KeyValuePair_2_t816448501_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t816448501_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t816448501_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t816448501_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t816448501_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0_Types[] = { &Int32_t2847414787_0_0_0, &Int32_t2847414787_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0 = { 2, GenInst_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2469511501_0_0_0_KeyValuePair_2_t2469511501_0_0_0_Types[] = { &KeyValuePair_2_t2469511501_0_0_0, &KeyValuePair_2_t2469511501_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2469511501_0_0_0_KeyValuePair_2_t2469511501_0_0_0 = { 2, GenInst_KeyValuePair_2_t2469511501_0_0_0_KeyValuePair_2_t2469511501_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2469511501_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2469511501_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2469511501_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2469511501_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t2490169420_0_0_0_Il2CppObject_0_0_0_Types[] = { &TrackableResultData_t2490169420_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t2490169420_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_TrackableResultData_t2490169420_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0_Types[] = { &TrackableResultData_t2490169420_0_0_0, &TrackableResultData_t2490169420_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0 = { 2, GenInst_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1102353480_0_0_0_KeyValuePair_2_t1102353480_0_0_0_Types[] = { &KeyValuePair_2_t1102353480_0_0_0, &KeyValuePair_2_t1102353480_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1102353480_0_0_0_KeyValuePair_2_t1102353480_0_0_0 = { 2, GenInst_KeyValuePair_2_t1102353480_0_0_0_KeyValuePair_2_t1102353480_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1102353480_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1102353480_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1102353480_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1102353480_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t1123011399_0_0_0_Il2CppObject_0_0_0_Types[] = { &VirtualButtonData_t1123011399_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t1123011399_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_VirtualButtonData_t1123011399_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0_Types[] = { &VirtualButtonData_t1123011399_0_0_0, &VirtualButtonData_t1123011399_0_0_0 };
extern const Il2CppGenericInst GenInst_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0 = { 2, GenInst_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0_Types[] = { &Boolean_t211005341_0_0_0, &Boolean_t211005341_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0 = { 2, GenInst_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t211005341_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2686855369_0_0_0_KeyValuePair_2_t2686855369_0_0_0_Types[] = { &KeyValuePair_2_t2686855369_0_0_0, &KeyValuePair_2_t2686855369_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2686855369_0_0_0_KeyValuePair_2_t2686855369_0_0_0 = { 2, GenInst_KeyValuePair_2_t2686855369_0_0_0_KeyValuePair_2_t2686855369_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2686855369_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2686855369_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2686855369_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2686855369_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1028297519_0_0_0_KeyValuePair_2_t1028297519_0_0_0_Types[] = { &KeyValuePair_2_t1028297519_0_0_0, &KeyValuePair_2_t1028297519_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1028297519_0_0_0_KeyValuePair_2_t1028297519_0_0_0 = { 2, GenInst_KeyValuePair_2_t1028297519_0_0_0_KeyValuePair_2_t1028297519_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1028297519_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1028297519_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1028297519_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1028297519_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3312956448_0_0_0_KeyValuePair_2_t3312956448_0_0_0_Types[] = { &KeyValuePair_2_t3312956448_0_0_0, &KeyValuePair_2_t3312956448_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3312956448_0_0_0_KeyValuePair_2_t3312956448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3312956448_0_0_0_KeyValuePair_2_t3312956448_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3312956448_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3312956448_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3312956448_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3312956448_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3461775296_0_0_0_KeyValuePair_2_t3461775296_0_0_0_Types[] = { &KeyValuePair_2_t3461775296_0_0_0, &KeyValuePair_2_t3461775296_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3461775296_0_0_0_KeyValuePair_2_t3461775296_0_0_0 = { 2, GenInst_KeyValuePair_2_t3461775296_0_0_0_KeyValuePair_2_t3461775296_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3461775296_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3461775296_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3461775296_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3461775296_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0_Types[] = { &UInt16_t985925268_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0_Types[] = { &UInt16_t985925268_0_0_0, &UInt16_t985925268_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0 = { 2, GenInst_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t25956863_0_0_0_KeyValuePair_2_t25956863_0_0_0_Types[] = { &KeyValuePair_2_t25956863_0_0_0, &KeyValuePair_2_t25956863_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t25956863_0_0_0_KeyValuePair_2_t25956863_0_0_0 = { 2, GenInst_KeyValuePair_2_t25956863_0_0_0_KeyValuePair_2_t25956863_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t25956863_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t25956863_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t25956863_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t25956863_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0_Types[] = { &ProfileData_t1845074131_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0_Types[] = { &ProfileData_t1845074131_0_0_0, &ProfileData_t1845074131_0_0_0 };
extern const Il2CppGenericInst GenInst_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0 = { 2, GenInst_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3253813172_0_0_0_KeyValuePair_2_t3253813172_0_0_0_Types[] = { &KeyValuePair_2_t3253813172_0_0_0, &KeyValuePair_2_t3253813172_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253813172_0_0_0_KeyValuePair_2_t3253813172_0_0_0 = { 2, GenInst_KeyValuePair_2_t3253813172_0_0_0_KeyValuePair_2_t3253813172_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3253813172_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3253813172_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3253813172_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3253813172_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_PIXEL_FORMAT_t3232215024_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0_Types[] = { &PIXEL_FORMAT_t3232215024_0_0_0, &PIXEL_FORMAT_t3232215024_0_0_0 };
extern const Il2CppGenericInst GenInst_PIXEL_FORMAT_t3232215024_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0 = { 2, GenInst_PIXEL_FORMAT_t3232215024_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[546] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0,
	&GenInst_Char_t2778706699_0_0_0,
	&GenInst_IConvertible_t4194222097_0_0_0,
	&GenInst_IComparable_t1596950936_0_0_0,
	&GenInst_IComparable_1_t1741309265_0_0_0,
	&GenInst_IEquatable_1_t2020331659_0_0_0,
	&GenInst_ValueType_t4014882752_0_0_0,
	&GenInst_Int64_t2847414882_0_0_0,
	&GenInst_UInt32_t985925326_0_0_0,
	&GenInst_UInt64_t985925421_0_0_0,
	&GenInst_Byte_t2778693821_0_0_0,
	&GenInst_SByte_t2855346064_0_0_0,
	&GenInst_Int16_t2847414729_0_0_0,
	&GenInst_UInt16_t985925268_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IEnumerable_t287189635_0_0_0,
	&GenInst_ICloneable_t2694744451_0_0_0,
	&GenInst_IComparable_1_t4226058764_0_0_0,
	&GenInst_IEquatable_1_t210113862_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3317110914_0_0_0,
	&GenInst__Type_t1563844123_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2334200065_0_0_0,
	&GenInst__MemberInfo_t3213777417_0_0_0,
	&GenInst_IFormattable_t2460033475_0_0_0,
	&GenInst_IComparable_1_t1741296387_0_0_0,
	&GenInst_IEquatable_1_t2020318781_0_0_0,
	&GenInst_Single_t958209021_0_0_0,
	&GenInst_IComparable_1_t4215778883_0_0_0,
	&GenInst_IEquatable_1_t199833981_0_0_0,
	&GenInst_Double_t534516614_0_0_0,
	&GenInst_Decimal_t1688557254_0_0_0,
	&GenInst_IComparable_1_t1810017353_0_0_0,
	&GenInst_IEquatable_1_t2089039747_0_0_0,
	&GenInst_Boolean_t211005341_0_0_0,
	&GenInst_Delegate_t3660574010_0_0_0,
	&GenInst_ISerializable_t1415126241_0_0_0,
	&GenInst_ParameterInfo_t2610273829_0_0_0,
	&GenInst__ParameterInfo_t1109275578_0_0_0,
	&GenInst_ParameterModifier_t500203470_0_0_0,
	&GenInst_IComparable_1_t4243495130_0_0_0,
	&GenInst_IEquatable_1_t227550228_0_0_0,
	&GenInst_IComparable_1_t4243495188_0_0_0,
	&GenInst_IEquatable_1_t227550286_0_0_0,
	&GenInst_IComparable_1_t4243495283_0_0_0,
	&GenInst_IEquatable_1_t227550381_0_0_0,
	&GenInst_IComparable_1_t1810017295_0_0_0,
	&GenInst_IEquatable_1_t2089039689_0_0_0,
	&GenInst_IComparable_1_t1817948630_0_0_0,
	&GenInst_IEquatable_1_t2096971024_0_0_0,
	&GenInst_IComparable_1_t1810017448_0_0_0,
	&GenInst_IEquatable_1_t2089039842_0_0_0,
	&GenInst_IComparable_1_t3792086476_0_0_0,
	&GenInst_IEquatable_1_t4071108870_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t3253414155_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3831964880_0_0_0,
	&GenInst_MethodBase_t3461000640_0_0_0,
	&GenInst__MethodBase_t3831744243_0_0_0,
	&GenInst_ConstructorInfo_t3542137334_0_0_0,
	&GenInst__ConstructorInfo_t1568461643_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t476453423_0_0_0,
	&GenInst_TailoringInfo_t3819293284_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_KeyValuePair_2_t1028297519_0_0_0,
	&GenInst_Link_t2496691359_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0_KeyValuePair_2_t1028297519_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2847414787_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t3973643989_0_0_0,
	&GenInst_Contraction_t2055464445_0_0_0,
	&GenInst_Level2Map_t2857724309_0_0_0,
	&GenInst_BigInteger_t1694088927_0_0_0,
	&GenInst_KeySizes_t2111859404_0_0_0,
	&GenInst_KeyValuePair_2_t3312956448_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3312956448_0_0_0,
	&GenInst_IComparable_1_t3468575203_0_0_0,
	&GenInst_IEquatable_1_t3747597597_0_0_0,
	&GenInst_Slot_t2579998_0_0_0,
	&GenInst_Slot_t2579999_0_0_0,
	&GenInst_StackFrame_t2860697380_0_0_0,
	&GenInst_Calendar_t2654956468_0_0_0,
	&GenInst_ModuleBuilder_t1058295580_0_0_0,
	&GenInst__ModuleBuilder_t86618962_0_0_0,
	&GenInst_Module_t206139610_0_0_0,
	&GenInst__Module_t2197041549_0_0_0,
	&GenInst_ParameterBuilder_t3382011775_0_0_0,
	&GenInst__ParameterBuilder_t2909166611_0_0_0,
	&GenInst_TypeU5BU5D_t3431720054_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t3761522009_0_0_0,
	&GenInst_IList_t1612618265_0_0_0,
	&GenInst_ILTokenInfo_t3723275281_0_0_0,
	&GenInst_LabelData_t1395746974_0_0_0,
	&GenInst_LabelFixup_t320573180_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t3267237648_0_0_0,
	&GenInst_TypeBuilder_t4287691406_0_0_0,
	&GenInst__TypeBuilder_t3477400324_0_0_0,
	&GenInst_MethodBuilder_t765486855_0_0_0,
	&GenInst__MethodBuilder_t4088777533_0_0_0,
	&GenInst_ConstructorBuilder_t1859087886_0_0_0,
	&GenInst__ConstructorBuilder_t3050391266_0_0_0,
	&GenInst_FieldBuilder_t2184649998_0_0_0,
	&GenInst__FieldBuilder_t4187887906_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t2964464644_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t560415562_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t318735129_0_0_0,
	&GenInst_CustomAttributeData_t2584644259_0_0_0,
	&GenInst_ResourceInfo_t4074584572_0_0_0,
	&GenInst_ResourceCacheItem_t3699857703_0_0_0,
	&GenInst_IContextProperty_t1838133337_0_0_0,
	&GenInst_Header_t1412152935_0_0_0,
	&GenInst_ITrackingHandler_t663364710_0_0_0,
	&GenInst_IContextAttribute_t2490988372_0_0_0,
	&GenInst_DateTime_t339033936_0_0_0,
	&GenInst_IComparable_1_t3596603798_0_0_0,
	&GenInst_IEquatable_1_t3875626192_0_0_0,
	&GenInst_IComparable_1_t651159820_0_0_0,
	&GenInst_IEquatable_1_t930182214_0_0_0,
	&GenInst_TimeSpan_t763862892_0_0_0,
	&GenInst_IComparable_1_t4021432754_0_0_0,
	&GenInst_IEquatable_1_t5487852_0_0_0,
	&GenInst_TypeTag_t1738289281_0_0_0,
	&GenInst_Enum_t2778772662_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t3441834685_0_0_0,
	&GenInst_DateTimeOffset_t3712260035_0_0_0,
	&GenInst_Guid_t2778838590_0_0_0,
	&GenInst_Version_t497901645_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0,
	&GenInst_KeyValuePair_2_t2686855369_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t211005341_0_0_0_KeyValuePair_2_t2686855369_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t211005341_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t1337234543_0_0_0,
	&GenInst_X509Certificate_t3432067208_0_0_0,
	&GenInst_IDeserializationCallback_t3135514852_0_0_0,
	&GenInst_X509ChainStatus_t1122151684_0_0_0,
	&GenInst_Capture_t1645813025_0_0_0,
	&GenInst_Group_t3792618586_0_0_0,
	&GenInst_Mark_t3725932776_0_0_0,
	&GenInst_UriScheme_t3266528785_0_0_0,
	&GenInst_BigInteger_t1694088928_0_0_0,
	&GenInst_ByteU5BU5D_t58506160_0_0_0,
	&GenInst_ClientCertificateType_t2725032177_0_0_0,
	&GenInst_Link_t1745155715_0_0_0,
	&GenInst_Object_t3878351788_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t1533365615_0_0_0,
	&GenInst_IAchievementDescription_t3856922794_0_0_0,
	&GenInst_IAchievementU5BU5D_t851122303_0_0_0,
	&GenInst_IAchievement_t813719258_0_0_0,
	&GenInst_IScoreU5BU5D_t684312048_0_0_0,
	&GenInst_IScore_t3029734269_0_0_0,
	&GenInst_IUserProfileU5BU5D_t2316972724_0_0_0,
	&GenInst_IUserProfile_t2749774601_0_0_0,
	&GenInst_AchievementDescription_t3784099155_0_0_0,
	&GenInst_UserProfile_t2517340964_0_0_0,
	&GenInst_GcLeaderboard_t3894999172_0_0_0,
	&GenInst_GcAchievementData_t1317012096_0_0_0,
	&GenInst_Achievement_t581285621_0_0_0,
	&GenInst_GcScoreData_t2223678307_0_0_0,
	&GenInst_Score_t1540476504_0_0_0,
	&GenInst_Vector3_t3525329789_0_0_0,
	&GenInst_Vector2_t3525329788_0_0_0,
	&GenInst_Material_t1886596500_0_0_0,
	&GenInst_Color_t1588175760_0_0_0,
	&GenInst_Color32_t4137084207_0_0_0,
	&GenInst_Keyframe_t2095052507_0_0_0,
	&GenInst_Camera_t3533968274_0_0_0,
	&GenInst_Behaviour_t3120504042_0_0_0,
	&GenInst_Component_t2126946602_0_0_0,
	&GenInst_Display_t564335855_0_0_0,
	&GenInst_Playable_t3404354247_0_0_0,
	&GenInst_IDisposable_t1628921374_0_0_0,
	&GenInst_ContactPoint_t2951122365_0_0_0,
	&GenInst_WebCamDevice_t1687076478_0_0_0,
	&GenInst_Font_t1525081276_0_0_0,
	&GenInst_GUILayoutOption_t3151226183_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t816448501_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t816448501_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_LayoutCache_t3653031512_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t3632373593_0_0_0,
	&GenInst_GUILayoutEntry_t1011928986_0_0_0,
	&GenInst_GUIStyle_t1006925219_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1006925219_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2133154421_0_0_0,
	&GenInst_DisallowMultipleComponent_t3647875775_0_0_0,
	&GenInst_Attribute_t498693649_0_0_0,
	&GenInst__Attribute_t2001626847_0_0_0,
	&GenInst_ExecuteInEditMode_t2676812948_0_0_0,
	&GenInst_RequireComponent_t3196495237_0_0_0,
	&GenInst_HitInfo_t2591228609_0_0_0,
	&GenInst_PersistentCall_t4127144549_0_0_0,
	&GenInst_BaseInvokableCall_t1733537956_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_GameObject_t4012695102_0_0_0,
	&GenInst_RenderTexture_t12905170_0_0_0,
	&GenInst_Texture_t1769722184_0_0_0,
	&GenInst_EyewearCalibrationReading_t3061222002_0_0_0,
	&GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0,
	&GenInst_Camera_t3533968274_0_0_0_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t448298814_0_0_0,
	&GenInst_VideoBackgroundAbstractBehaviour_t1820506856_0_0_0,
	&GenInst_Renderer_t1092684080_0_0_0,
	&GenInst_TrackableBehaviour_t2427445838_0_0_0,
	&GenInst_TrackableResultData_t2490169420_0_0_0,
	&GenInst_HideExcessAreaAbstractBehaviour_t3630507405_0_0_0,
	&GenInst_MonoBehaviour_t3012272455_0_0_0,
	&GenInst_IViewerParameters_t703877403_0_0_0,
	&GenInst_ITrackableEventHandler_t2602716514_0_0_0,
	&GenInst_ReconstructionAbstractBehaviour_t730550669_0_0_0,
	&GenInst_ICloudRecoEventHandler_t1786089329_0_0_0,
	&GenInst_TargetSearchResult_t3905350710_0_0_0,
	&GenInst_Trackable_t1174201883_0_0_0,
	&GenInst_VirtualButton_t3523578643_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3253813172_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t3253813172_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_Image_t2805765713_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t927505169_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Trackable_t1174201883_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t1153543964_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButton_t3523578643_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t3502920724_0_0_0,
	&GenInst_DataSet_t1547874638_0_0_0,
	&GenInst_DataSetImpl_t611363726_0_0_0,
	&GenInst_Marker_t737566064_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Marker_t737566064_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t716908145_0_0_0,
	&GenInst_WordData_t92535284_0_0_0,
	&GenInst_WordResultData_t408256561_0_0_0,
	&GenInst_SmartTerrainRevisionData_t695238033_0_0_0,
	&GenInst_SurfaceData_t264493527_0_0_0,
	&GenInst_PropData_t3365470669_0_0_0,
	&GenInst_IEditorTrackableBehaviour_t292912100_0_0_0,
	&GenInst_Image_t2805765713_0_0_0,
	&GenInst_SmartTerrainTrackable_t892409423_0_0_0,
	&GenInst_SmartTerrainTrackableBehaviour_t3759975578_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0,
	&GenInst_KeyValuePair_2_t3461775296_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Il2CppObject_0_0_0_UInt16_t985925268_0_0_0_KeyValuePair_2_t3461775296_0_0_0,
	&GenInst_Type_t_0_0_0_UInt16_t985925268_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2587004281_0_0_0,
	&GenInst_RectangleData_t790089391_0_0_0,
	&GenInst_WordResult_t1871594749_0_0_0,
	&GenInst_Word_t2737707072_0_0_0,
	&GenInst_WordAbstractBehaviour_t162212295_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_WordResult_t1871594749_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t1850936830_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_WordAbstractBehaviour_t162212295_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t141554376_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t959171264_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t959171264_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2085400466_0_0_0,
	&GenInst_List_1_t959171264_0_0_0,
	&GenInst_ILoadLevelEventHandler_t1667599611_0_0_0,
	&GenInst_ISmartTerrainEventHandler_t674878747_0_0_0,
	&GenInst_SmartTerrainInitializationInfo_t1520723184_0_0_0,
	&GenInst_Prop_t2737501337_0_0_0,
	&GenInst_Surface_t2546426275_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Surface_t2546426275_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2525768356_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_SurfaceAbstractBehaviour_t1477931716_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t1457273797_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Prop_t2737501337_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2716843418_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_PropAbstractBehaviour_t909732494_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t889074575_0_0_0,
	&GenInst_PropAbstractBehaviour_t909732494_0_0_0,
	&GenInst_SurfaceAbstractBehaviour_t1477931716_0_0_0,
	&GenInst_MeshFilter_t4177078322_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableBehaviour_t2427445838_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2406787919_0_0_0,
	&GenInst_MarkerAbstractBehaviour_t1462758039_0_0_0,
	&GenInst_IEditorMarkerBehaviour_t2835060931_0_0_0,
	&GenInst_WorldCenterTrackableBehaviour_t2878848245_0_0_0,
	&GenInst_DataSetTrackableBehaviour_t1793464454_0_0_0,
	&GenInst_IEditorDataSetTrackableBehaviour_t3829574192_0_0_0,
	&GenInst_VirtualButtonAbstractBehaviour_t2124633940_0_0_0,
	&GenInst_IEditorVirtualButtonBehaviour_t3419943660_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0,
	&GenInst_KeyValuePair_2_t2469511501_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_TrackableResultData_t2490169420_0_0_0_KeyValuePair_2_t2469511501_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0,
	&GenInst_KeyValuePair_2_t1102353480_0_0_0,
	&GenInst_VirtualButtonData_t1123011399_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonData_t1123011399_0_0_0_KeyValuePair_2_t1102353480_0_0_0,
	&GenInst_ImageTarget_t2294340546_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_ImageTarget_t2294340546_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2273682627_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0,
	&GenInst_KeyValuePair_2_t25956863_0_0_0,
	&GenInst_ProfileData_t1845074131_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ProfileData_t1845074131_0_0_0_KeyValuePair_2_t25956863_0_0_0,
	&GenInst_String_t_0_0_0_ProfileData_t1845074131_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2971303333_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_VirtualButtonAbstractBehaviour_t2124633940_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_KeyValuePair_2_t2103976021_0_0_0,
	&GenInst_ITrackerEventHandler_t3415742645_0_0_0,
	&GenInst_IVideoBackgroundEventHandler_t248107270_0_0_0,
	&GenInst_InitError_t1722238648_0_0_0,
	&GenInst_BackgroundPlaneAbstractBehaviour_t1953260483_0_0_0,
	&GenInst_ITextRecoEventHandler_t4066948379_0_0_0,
	&GenInst_IUserDefinedTargetEventHandler_t3593614636_0_0_0,
	&GenInst_MeshRenderer_t1217738301_0_0_0,
	&GenInst_IVirtualButtonEventHandler_t2233055962_0_0_0,
	&GenInst_Collider_t955670625_0_0_0,
	&GenInst_WireframeBehaviour_t2976175819_0_0_0,
	&GenInst_IEnumerable_1_t2707313927_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m850548632_gp_0_0_0_0,
	&GenInst_Array_Sort_m1181603442_gp_0_0_0_0_Array_Sort_m1181603442_gp_0_0_0_0,
	&GenInst_Array_Sort_m3908760906_gp_0_0_0_0_Array_Sort_m3908760906_gp_1_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m646233104_gp_0_0_0_0_Array_Sort_m646233104_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0,
	&GenInst_Array_Sort_m2404937677_gp_0_0_0_0_Array_Sort_m2404937677_gp_1_0_0_0,
	&GenInst_Array_Sort_m377069906_gp_0_0_0_0_Array_Sort_m377069906_gp_0_0_0_0,
	&GenInst_Array_Sort_m1327718954_gp_0_0_0_0_Array_Sort_m1327718954_gp_1_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m4084526832_gp_0_0_0_0_Array_Sort_m4084526832_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3263917805_gp_0_0_0_0_Array_Sort_m3263917805_gp_1_0_0_0,
	&GenInst_Array_Sort_m3566161319_gp_0_0_0_0,
	&GenInst_Array_Sort_m1767877396_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0,
	&GenInst_Array_qsort_m785378185_gp_0_0_0_0_Array_qsort_m785378185_gp_1_0_0_0,
	&GenInst_Array_compare_m3718693973_gp_0_0_0_0,
	&GenInst_Array_qsort_m3270161954_gp_0_0_0_0,
	&GenInst_Array_Resize_m2347367271_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m123384663_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1326446122_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1697145810_gp_0_0_0_0_Array_ConvertAll_m1697145810_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m614217902_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m653822311_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1650781518_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m308210168_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3338256477_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1605056024_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3507857443_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1578426497_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3933814211_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1779658273_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3823835921_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1695958374_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3280162097_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m921616327_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m4157241456_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m925096551_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1181152586_gp_0_0_0_0,
	&GenInst_Array_Exists_m2312185345_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m4233614634_gp_0_0_0_0,
	&GenInst_Array_Find_m3878993575_gp_0_0_0_0,
	&GenInst_Array_FindLast_m3671273393_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2673614354_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2546651274_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t490656863_gp_0_0_0_0,
	&GenInst_IList_1_t838940445_gp_0_0_0_0,
	&GenInst_ICollection_1_t56169053_gp_0_0_0_0,
	&GenInst_Nullable_1_t2791603879_gp_0_0_0_0,
	&GenInst_Comparer_1_t3643230563_gp_0_0_0_0,
	&GenInst_DefaultComparer_t401310062_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3795808858_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2897654481_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m913868521_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3591673703_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_ShimEnumerator_t3981551357_gp_0_0_0_0_ShimEnumerator_t3981551357_gp_1_0_0_0,
	&GenInst_Enumerator_t1072804842_gp_0_0_0_0_Enumerator_t1072804842_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t6794133_0_0_0,
	&GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0,
	&GenInst_KeyCollection_t1486433853_gp_0_0_0_0,
	&GenInst_Enumerator_t1072804843_gp_0_0_0_0_Enumerator_t1072804843_gp_1_0_0_0,
	&GenInst_Enumerator_t1072804843_gp_0_0_0_0,
	&GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_1_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0,
	&GenInst_KeyCollection_t1486433853_gp_0_0_0_0_KeyCollection_t1486433853_gp_0_0_0_0,
	&GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0,
	&GenInst_ValueCollection_t3044571855_gp_1_0_0_0,
	&GenInst_Enumerator_t1072804844_gp_0_0_0_0_Enumerator_t1072804844_gp_1_0_0_0,
	&GenInst_Enumerator_t1072804844_gp_1_0_0_0,
	&GenInst_ValueCollection_t3044571855_gp_0_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0,
	&GenInst_ValueCollection_t3044571855_gp_1_0_0_0_ValueCollection_t3044571855_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t130027246_0_0_0_DictionaryEntry_t130027246_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_0_0_0_0_Dictionary_2_t2776849293_gp_1_0_0_0_KeyValuePair_2_t2897654481_0_0_0,
	&GenInst_KeyValuePair_2_t2897654481_0_0_0_KeyValuePair_2_t2897654481_0_0_0,
	&GenInst_Dictionary_2_t2776849293_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2368211453_gp_0_0_0_0,
	&GenInst_DefaultComparer_t401310063_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3181333492_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4148160909_0_0_0,
	&GenInst_IDictionary_2_t2833266262_gp_0_0_0_0_IDictionary_2_t2833266262_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2241909187_gp_0_0_0_0_KeyValuePair_2_t2241909187_gp_1_0_0_0,
	&GenInst_List_1_t475681172_gp_0_0_0_0,
	&GenInst_Enumerator_t1072804845_gp_0_0_0_0,
	&GenInst_Collection_1_t248125333_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2688875287_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m406542986_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m406542986_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1626564605_gp_0_0_0_0,
	&GenInst_LinkedList_1_t215192845_gp_0_0_0_0,
	&GenInst_Enumerator_t1072804846_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3677586223_gp_0_0_0_0,
	&GenInst_Stack_1_t2261335230_gp_0_0_0_0,
	&GenInst_Enumerator_t1072804847_gp_0_0_0_0,
	&GenInst_HashSet_1_t501446570_gp_0_0_0_0,
	&GenInst_Enumerator_t1072804848_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3130145517_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1224968048_gp_0_0_0_0,
	&GenInst_Enumerable_Cast_m3421076499_gp_0_0_0_0,
	&GenInst_Enumerable_CreateCastIterator_m203024317_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2837618484_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m2710245799_gp_0_0_0_0,
	&GenInst_Enumerable_First_m1340241468_gp_0_0_0_0,
	&GenInst_Enumerable_ToArray_m600216364_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m3865828353_gp_0_0_0_0,
	&GenInst_U3CCreateCastIteratorU3Ec__Iterator0_1_t1248874793_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m2068134811_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m2548367508_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2256560286_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m885533373_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2042178407_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m290105122_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2190484233_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2983504498_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m228373778_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2204190206_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m482375811_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m426455346_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m861782781_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m17928077_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m494097074_gp_0_0_0_0,
	&GenInst_SmartTerrainBuilderImpl_CreateReconstruction_m4181641612_gp_0_0_0_0,
	&GenInst_GUILayer_t999894339_0_0_0,
	&GenInst_VideoBackgroundManagerAbstractBehaviour_t1915856045_0_0_0,
	&GenInst_ObjectTracker_t3275326447_0_0_0,
	&GenInst_SmartTerrainTracker_t3586718050_0_0_0,
	&GenInst_VuforiaAbstractBehaviour_t2427322319_0_0_0,
	&GenInst_DeviceTracker_t1348055288_0_0_0,
	&GenInst_RotationalDeviceTracker_t4008517455_0_0_0,
	&GenInst_DistortionRenderingBehaviour_t28016708_0_0_0,
	&GenInst_ImageTargetAbstractBehaviour_t2880150149_0_0_0,
	&GenInst_DigitalEyewearAbstractBehaviour_t1656621721_0_0_0,
	&GenInst_MarkerTracker_t2552664724_0_0_0,
	&GenInst_Mesh_t1525280346_0_0_0,
	&GenInst_ReconstructionFromTarget_t1377534133_0_0_0,
	&GenInst_TextTracker_t2495541825_0_0_0,
	&GenInst_BackgroundPlaneBehaviour_t3262988229_0_0_0,
	&GenInst_ReconstructionBehaviour_t4293105551_0_0_0,
	&GenInst_DigitalEyewearBehaviour_t674634907_0_0_0,
	&GenInst_VideoBackgroundManager_t3152649370_0_0_0,
	&GenInst_ComponentFactoryStarterBehaviour_t1834097041_0_0_0,
	&GenInst_VuforiaBehaviour_t1383180241_0_0_0,
	&GenInst_MaskOutBehaviour_t742775633_0_0_0,
	&GenInst_VirtualButtonBehaviour_t3242546262_0_0_0,
	&GenInst_TurnOffBehaviour_t816306785_0_0_0,
	&GenInst_ImageTargetBehaviour_t1584945287_0_0_0,
	&GenInst_MarkerBehaviour_t2463029913_0_0_0,
	&GenInst_MultiTargetBehaviour_t2071934185_0_0_0,
	&GenInst_CylinderTargetBehaviour_t2447349868_0_0_0,
	&GenInst_WordBehaviour_t559172041_0_0_0,
	&GenInst_TextRecoBehaviour_t3745030759_0_0_0,
	&GenInst_ObjectTargetBehaviour_t2124871139_0_0_0,
	&GenInst_KeyValuePair_2_t816448501_0_0_0_KeyValuePair_2_t816448501_0_0_0,
	&GenInst_KeyValuePair_2_t816448501_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2847414787_0_0_0_Int32_t2847414787_0_0_0,
	&GenInst_KeyValuePair_2_t2469511501_0_0_0_KeyValuePair_2_t2469511501_0_0_0,
	&GenInst_KeyValuePair_2_t2469511501_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TrackableResultData_t2490169420_0_0_0_Il2CppObject_0_0_0,
	&GenInst_TrackableResultData_t2490169420_0_0_0_TrackableResultData_t2490169420_0_0_0,
	&GenInst_KeyValuePair_2_t1102353480_0_0_0_KeyValuePair_2_t1102353480_0_0_0,
	&GenInst_KeyValuePair_2_t1102353480_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t1123011399_0_0_0_Il2CppObject_0_0_0,
	&GenInst_VirtualButtonData_t1123011399_0_0_0_VirtualButtonData_t1123011399_0_0_0,
	&GenInst_Boolean_t211005341_0_0_0_Boolean_t211005341_0_0_0,
	&GenInst_Boolean_t211005341_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2686855369_0_0_0_KeyValuePair_2_t2686855369_0_0_0,
	&GenInst_KeyValuePair_2_t2686855369_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t1028297519_0_0_0_KeyValuePair_2_t1028297519_0_0_0,
	&GenInst_KeyValuePair_2_t1028297519_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3312956448_0_0_0_KeyValuePair_2_t3312956448_0_0_0,
	&GenInst_KeyValuePair_2_t3312956448_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3461775296_0_0_0_KeyValuePair_2_t3461775296_0_0_0,
	&GenInst_KeyValuePair_2_t3461775296_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t985925268_0_0_0_Il2CppObject_0_0_0,
	&GenInst_UInt16_t985925268_0_0_0_UInt16_t985925268_0_0_0,
	&GenInst_KeyValuePair_2_t25956863_0_0_0_KeyValuePair_2_t25956863_0_0_0,
	&GenInst_KeyValuePair_2_t25956863_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t1845074131_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ProfileData_t1845074131_0_0_0_ProfileData_t1845074131_0_0_0,
	&GenInst_KeyValuePair_2_t3253813172_0_0_0_KeyValuePair_2_t3253813172_0_0_0,
	&GenInst_KeyValuePair_2_t3253813172_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PIXEL_FORMAT_t3232215024_0_0_0_PIXEL_FORMAT_t3232215024_0_0_0,
};
