﻿using UnityEngine;
using System.Collections;

public class CaveOfWonder : MonoBehaviour 
{
    public Animator animator;

	void OnGUI()
	{	
		GUIStyle style = GUI.skin.GetStyle("window");
		style.fontSize = (int)(25.0f);
		
        if( GUI.Button (new Rect ( 10,Screen.height- 40,120 * 2, 80), "Animate Cave", style))
		{
			GameObject a_child = this.gameObject.transform.GetChild (0).gameObject;

            if (animator.GetInteger ("AnimCave") == 1) 
			{
                animator.SetInteger ("AnimCave", 0);
			} 
			else
			{
                animator.SetInteger ("AnimCave", 1);
			}
		}
	}
}
