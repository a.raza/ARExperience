﻿using UnityEngine;
using System.Collections;

public class Village : MonoBehaviour 
{
    public Animator animator;
    
	void OnGUI()
	{	
		GUIStyle style = GUI.skin.GetStyle("window");
		style.fontSize = (int)(25.0f);

        if( GUI.Button (new Rect ( 500,Screen.height- 40,120 * 2, 80), "Animate Village", style))
		{   
            if (animator.GetBool ("AnimVillage") == true) 
			{
                animator.SetBool ("AnimVillage", false);
			}
			else
			{
                animator.SetBool ("AnimVillage", true);
			}
		}
	}
}
