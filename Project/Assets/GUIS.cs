﻿using UnityEngine;
using System.Collections;

public class GUIS : MonoBehaviour 
{
	public Component[] m_buttons;

	void Start()
	{
				
	}

	void OnGUI()
	{
//		GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));

		// All rectangles are now adjusted to the group. (0,0) is the topleft corner of the group.

		GUIStyle style = GUI.skin.GetStyle("window");
		style.fontSize = (int)(25.0f);

		if( GUI.Button (new Rect (Screen.width /2,Screen.height /2,120*2,80), "Start",style))
		{
			m_buttons [0].gameObject.GetComponent<CaveOfWonder> ().enabled = true;
			m_buttons [1].gameObject.GetComponent<Village> ().enabled = true;

			this.gameObject.GetComponent<GUIS> ().enabled = false;
		}
	}

	void WindowFunction (int windowID) {
		// Draw any Controls inside the window here
	}

}
