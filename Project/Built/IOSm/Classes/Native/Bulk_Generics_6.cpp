﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t292577037;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2577235966;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t2549405407;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t539097040;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_t4109915417;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3644373756;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1634065389;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t1115694098;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t1357374531;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t4029173993;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t407342383;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t1890143508;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t501095600;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t3778787834;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t346249057;
// System.Collections.Generic.IComparer`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IComparer_1_t1717696561;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3313246173;
// System.Collections.Generic.IComparer`1<System.Int32>
struct IComparer_1_t1252154900;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1302937806;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t3536813829;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerable_1_t3190889485;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1801841577;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ICollection_1_t784566515;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3019176036;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeNamedArgument>
struct IComparer_1_t3018442538;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t3432569918;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2043522010;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t1026246948;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3123668047;
// System.Collections.Generic.IComparer`1<System.Reflection.CustomAttributeTypedArgument>
struct IComparer_1_t3260122971;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t1809402084;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t420354176;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t3698046410;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t4247347793;
// System.Collections.Generic.IComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct IComparer_1_t1636955137;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t2482537770;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t1093489862;
// System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ICollection_1_t76214800;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t2556575283;
// System.Collections.Generic.IComparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct IComparer_1_t2310090823;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3407512455;
// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2806094150;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t3003598734;
// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t2287722859;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t2485227443;
// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t2529403292;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t2726907876;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t4000251768;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>
struct ReadOnlyCollection_1_t3481880477;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t3723560910;
// System.Comparison`1<System.Object>
struct Comparison_1_t3540781296;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t113996300;
// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586;
// System.Predicate`1<System.Object>
struct Predicate_1_t1408070318;
// System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Predicate_1_t3061133318;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t539570894;
// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t171826611;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t4074528527;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t3775219180;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
struct UnityEvent_3_t1749754057;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
struct UnityEvent_4_t1065917714;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22469511501.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22469511501MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2490169420.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21102353480.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21102353480MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1123011399.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22686855369.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22686855369MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Boolean211005341MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297519.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297519MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23461775296.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23461775296MethodDeclarations.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt16985925268MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25956863.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25956863MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1845074131.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23253813172.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23253813172MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3232215024.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1730156748.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1730156748MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "System_System_Collections_Generic_LinkedList_1_gen292577037.h"
#include "mscorlib_System_UInt32985925326.h"
#include "System_System_Collections_Generic_LinkedListNode_12549405407.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "System_System_Collections_Generic_LinkedList_1_gen292577037MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedListNode_12549405407MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu4014815677.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu4014815677MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2577235966MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedListNode_1_539097040MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2195698409.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2195698409MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4109915417.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3496444386.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3496444386MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1115694098.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3738124819.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3738124819MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1357374531.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2114956985.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2114956985MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4029173993.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2788092671.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2788092671MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen407342383.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3905350710.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4109915417MethodDeclarations.h"
#include "mscorlib_System_NullReferenceException3216235232.h"
#include "mscorlib_System_InvalidCastException922874574.h"
#include "mscorlib_System_Math2778998461MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1115694098MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1357374531MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4029173993MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen407342383MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat4014815677.h"
#include "System_System_Collections_Generic_Stack_1_Enumerat4014815677MethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen3407512455.h"
#include "System_System_Collections_Generic_Stack_1_gen3407512455MethodDeclarations.h"
#include "mscorlib_System_ArrayTypeMismatchException3670186211.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2806094150MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2287722859.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2287722859MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2529403292.h"
#include "mscorlib_System_Collections_ObjectModel_Collection2529403292MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo4000251768MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo3481880477.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo3481880477MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo3723560910.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo3723560910MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen3540781296.h"
#include "mscorlib_System_Comparison_1_gen3540781296MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Converter_2_gen113996300.h"
#include "mscorlib_System_Converter_2_gen113996300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt3156486586.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateCastIt3156486586MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Nullable_1_gen3649900800.h"
#include "mscorlib_System_Nullable_1_gen3649900800MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_TimeSpan763862892MethodDeclarations.h"
#include "mscorlib_System_ValueType4014882752MethodDeclarations.h"
#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_Predicate_1_gen1408070318.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"
#include "mscorlib_System_Predicate_1_gen3061133318.h"
#include "mscorlib_System_Predicate_1_gen3061133318MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_ge539570894.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_ge539570894MethodDeclarations.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGette171826611.h"
#include "mscorlib_System_Reflection_MonoProperty_StaticGette171826611MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen4244616972.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen4244616972MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen4074528527.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen4074528527MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase2174897510MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen3775219180.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen3775219180MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1749754057.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1749754057MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen1065917714.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen1065917714MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisKeyValuePair_2_t3312956448_m19109324_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t346249057* p0, KeyValuePair_2_t3312956448  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisKeyValuePair_2_t3312956448_m19109324(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057*, KeyValuePair_2_t3312956448 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisKeyValuePair_2_t3312956448_m19109324_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisKeyValuePair_2_t3312956448_m216248203_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t346249057* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisKeyValuePair_2_t3312956448_m216248203(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisKeyValuePair_2_t3312956448_m216248203_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisKeyValuePair_2_t3312956448_m3402855490_gshared (Il2CppObject * __this /* static, unused */, KeyValuePair_2U5BU5D_t346249057** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisKeyValuePair_2_t3312956448_m3402855490(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057**, int32_t, const MethodInfo*))Array_Resize_TisKeyValuePair_2_t3312956448_m3402855490_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Int32>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisInt32_t2847414787_m877823422_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t1809983122* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisInt32_t2847414787_m877823422(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122*, int32_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisInt32_t2847414787_m877823422_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Int32>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisInt32_t2847414787_m1377894077_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t1809983122* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisInt32_t2847414787_m1377894077(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisInt32_t2847414787_m1377894077_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Int32>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisInt32_t2847414787_m3594335604_gshared (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t1809983122** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisInt32_t2847414787_m3594335604(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122**, int32_t, const MethodInfo*))Array_Resize_TisInt32_t2847414787_m3594335604_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2704617185_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* p0, Il2CppObject * p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2704617185(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2704617185_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisIl2CppObject_m1856111878_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisIl2CppObject_m1856111878(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisIl2CppObject_m1856111878_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m4097160425_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m4097160425(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m4097160425_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m2624070686_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036* p0, CustomAttributeNamedArgument_t318735129  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m2624070686(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m2624070686_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeNamedArgument>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisCustomAttributeNamedArgument_t318735129_m2207964893_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeNamedArgument_t318735129_m2207964893(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisCustomAttributeNamedArgument_t318735129_m2207964893_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeNamedArgument>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCustomAttributeNamedArgument_t318735129_m3334689556_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeNamedArgument_t318735129_m3334689556(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeNamedArgument_t318735129_m3334689556_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m2421987343_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047* p0, CustomAttributeTypedArgument_t560415562  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m2421987343(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m2421987343_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<System.Reflection.CustomAttributeTypedArgument>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisCustomAttributeTypedArgument_t560415562_m3383283150_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisCustomAttributeTypedArgument_t560415562_m3383283150(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisCustomAttributeTypedArgument_t560415562_m3383283150_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<System.Reflection.CustomAttributeTypedArgument>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisCustomAttributeTypedArgument_t560415562_m3172077765_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisCustomAttributeTypedArgument_t560415562_m3172077765(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047**, int32_t, const MethodInfo*))Array_Resize_TisCustomAttributeTypedArgument_t560415562_m3172077765_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<Vuforia.Image/PIXEL_FORMAT>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisPIXEL_FORMAT_t3232215024_m1758937983_gshared (Il2CppObject * __this /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793* p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisPIXEL_FORMAT_t3232215024_m1758937983(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793*, int32_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisPIXEL_FORMAT_t3232215024_m1758937983_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<Vuforia.Image/PIXEL_FORMAT>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisPIXEL_FORMAT_t3232215024_m1930878910_gshared (Il2CppObject * __this /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisPIXEL_FORMAT_t3232215024_m1930878910(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisPIXEL_FORMAT_t3232215024_m1930878910_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<Vuforia.Image/PIXEL_FORMAT>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisPIXEL_FORMAT_t3232215024_m749834165_gshared (Il2CppObject * __this /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisPIXEL_FORMAT_t3232215024_m749834165(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793**, int32_t, const MethodInfo*))Array_Resize_TisPIXEL_FORMAT_t3232215024_m749834165_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<Vuforia.TargetFinder/TargetSearchResult>(!!0[],!!0,System.Int32,System.Int32)
extern "C"  int32_t Array_IndexOf_TisTargetSearchResult_t3905350710_m3197609661_gshared (Il2CppObject * __this /* static, unused */, TargetSearchResultU5BU5D_t2556575283* p0, TargetSearchResult_t3905350710  p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisTargetSearchResult_t3905350710_m3197609661(__this /* static, unused */, p0, p1, p2, p3, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283*, TargetSearchResult_t3905350710 , int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisTargetSearchResult_t3905350710_m3197609661_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Sort<Vuforia.TargetFinder/TargetSearchResult>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C"  void Array_Sort_TisTargetSearchResult_t3905350710_m2348215338_gshared (Il2CppObject * __this /* static, unused */, TargetSearchResultU5BU5D_t2556575283* p0, int32_t p1, int32_t p2, Il2CppObject* p3, const MethodInfo* method);
#define Array_Sort_TisTargetSearchResult_t3905350710_m2348215338(__this /* static, unused */, p0, p1, p2, p3, method) ((  void (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))Array_Sort_TisTargetSearchResult_t3905350710_m2348215338_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
// System.Void System.Array::Resize<Vuforia.TargetFinder/TargetSearchResult>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisTargetSearchResult_t3905350710_m4076314509_gshared (Il2CppObject * __this /* static, unused */, TargetSearchResultU5BU5D_t2556575283** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisTargetSearchResult_t3905350710_m4076314509(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283**, int32_t, const MethodInfo*))Array_Resize_TisTargetSearchResult_t3905350710_m4076314509_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1266893246_gshared (KeyValuePair_2_t2469511501 * __this, int32_t ___key0, TrackableResultData_t2490169420  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t2469511501 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t2469511501 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TrackableResultData_t2490169420  L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t2469511501 *, TrackableResultData_t2490169420 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t2469511501 *)__this, (TrackableResultData_t2490169420 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2157360106_gshared (KeyValuePair_2_t2469511501 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3105716651_gshared (KeyValuePair_2_t2469511501 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Value()
extern "C"  TrackableResultData_t2490169420  KeyValuePair_2_get_Value_m2398953998_gshared (KeyValuePair_2_t2469511501 * __this, const MethodInfo* method)
{
	{
		TrackableResultData_t2490169420  L_0 = (TrackableResultData_t2490169420 )__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2306736171_gshared (KeyValuePair_2_t2469511501 * __this, TrackableResultData_t2490169420  ___value0, const MethodInfo* method)
{
	{
		TrackableResultData_t2490169420  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1148788029_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1148788029_gshared (KeyValuePair_2_t2469511501 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1148788029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	TrackableResultData_t2490169420  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		int32_t L_2 = ((  int32_t (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2469511501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2469511501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		TrackableResultData_t2490169420  L_8 = ((  TrackableResultData_t2490169420  (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2469511501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		TrackableResultData_t2490169420  L_9 = ((  TrackableResultData_t2490169420  (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2469511501 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (TrackableResultData_t2490169420 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2676204643_gshared (KeyValuePair_2_t1102353480 * __this, int32_t ___key0, VirtualButtonData_t1123011399  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t1102353480 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t1102353480 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		VirtualButtonData_t1123011399  L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t1102353480 *, VirtualButtonData_t1123011399 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1102353480 *)__this, (VirtualButtonData_t1123011399 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m4182360997_gshared (KeyValuePair_2_t1102353480 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2442740710_gshared (KeyValuePair_2_t1102353480 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  VirtualButtonData_t1123011399  KeyValuePair_2_get_Value_m1627425673_gshared (KeyValuePair_2_t1102353480 * __this, const MethodInfo* method)
{
	{
		VirtualButtonData_t1123011399  L_0 = (VirtualButtonData_t1123011399 )__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3460576486_gshared (KeyValuePair_2_t1102353480 * __this, VirtualButtonData_t1123011399  ___value0, const MethodInfo* method)
{
	{
		VirtualButtonData_t1123011399  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3554271650_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3554271650_gshared (KeyValuePair_2_t1102353480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3554271650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	VirtualButtonData_t1123011399  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		int32_t L_2 = ((  int32_t (*) (KeyValuePair_2_t1102353480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1102353480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (KeyValuePair_2_t1102353480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1102353480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		VirtualButtonData_t1123011399  L_8 = ((  VirtualButtonData_t1123011399  (*) (KeyValuePair_2_t1102353480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1102353480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		VirtualButtonData_t1123011399  L_9 = ((  VirtualButtonData_t1123011399  (*) (KeyValuePair_2_t1102353480 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1102353480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (VirtualButtonData_t1123011399 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2040323320_gshared (KeyValuePair_2_t2686855369 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t2686855369 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t2686855369 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t2686855369 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t2686855369 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_gshared (KeyValuePair_2_t2686855369 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1751794225_gshared (KeyValuePair_2_t2686855369 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_gshared (KeyValuePair_2_t2686855369 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3162969521_gshared (KeyValuePair_2_t2686855369 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3396952209_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_gshared (KeyValuePair_2_t2686855369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3396952209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (KeyValuePair_2_t2686855369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2686855369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (KeyValuePair_2_t2686855369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t2686855369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		bool L_8 = ((  bool (*) (KeyValuePair_2_t2686855369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2686855369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = ((  bool (*) (KeyValuePair_2_t2686855369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t2686855369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_12 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2730552978_gshared (KeyValuePair_2_t1028297519 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t1028297519 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t1028297519 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t1028297519 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t1028297519 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_gshared (KeyValuePair_2_t1028297519 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1188304983_gshared (KeyValuePair_2_t1028297519 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_gshared (KeyValuePair_2_t1028297519 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m137193687_gshared (KeyValuePair_2_t1028297519 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2052282219_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_gshared (KeyValuePair_2_t1028297519 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2052282219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (KeyValuePair_2_t1028297519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1028297519 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (KeyValuePair_2_t1028297519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t1028297519 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		int32_t L_8 = ((  int32_t (*) (KeyValuePair_2_t1028297519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1028297519 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (KeyValuePair_2_t1028297519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t1028297519 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_12 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4168265535_gshared (KeyValuePair_2_t3312956448 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t3312956448 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t3312956448 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t3312956448 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t3312956448 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3256475977_gshared (KeyValuePair_2_t3312956448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1278074762_gshared (KeyValuePair_2_t3312956448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3899079597_gshared (KeyValuePair_2_t3312956448 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2954518154_gshared (KeyValuePair_2_t3312956448 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1313859518_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_gshared (KeyValuePair_2_t3312956448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1313859518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3312956448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3312956448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3312956448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (KeyValuePair_2_t3312956448 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3312956448 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_12 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3840239519_gshared (KeyValuePair_2_t3461775296 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t3461775296 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t3461775296 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t3461775296 *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t3461775296 *)__this, (uint16_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1711553769_gshared (KeyValuePair_2_t3461775296 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4131482410_gshared (KeyValuePair_2_t3461775296 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m992554829_gshared (KeyValuePair_2_t3461775296 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3359578666_gshared (KeyValuePair_2_t3461775296 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m665911326_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m665911326_gshared (KeyValuePair_2_t3461775296 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m665911326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	uint16_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (KeyValuePair_2_t3461775296 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3461775296 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (KeyValuePair_2_t3461775296 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3461775296 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		uint16_t L_8 = ((  uint16_t (*) (KeyValuePair_2_t3461775296 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3461775296 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		uint16_t L_9 = ((  uint16_t (*) (KeyValuePair_2_t3461775296 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3461775296 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (uint16_t)L_9;
		String_t* L_10 = UInt16_ToString_m741885559((uint16_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_12 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2890662519_gshared (KeyValuePair_2_t25956863 * __this, Il2CppObject * ___key0, ProfileData_t1845074131  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t25956863 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t25956863 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProfileData_t1845074131  L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t25956863 *, ProfileData_t1845074131 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t25956863 *)__this, (ProfileData_t1845074131 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2269785873_gshared (KeyValuePair_2_t25956863 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3685443922_gshared (KeyValuePair_2_t25956863 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  ProfileData_t1845074131  KeyValuePair_2_get_Value_m2743302773_gshared (KeyValuePair_2_t25956863 * __this, const MethodInfo* method)
{
	{
		ProfileData_t1845074131  L_0 = (ProfileData_t1845074131 )__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3467494482_gshared (KeyValuePair_2_t25956863 * __this, ProfileData_t1845074131  ___value0, const MethodInfo* method)
{
	{
		ProfileData_t1845074131  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2516779894_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2516779894_gshared (KeyValuePair_2_t25956863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2516779894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ProfileData_t1845074131  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (KeyValuePair_2_t25956863 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t25956863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (KeyValuePair_2_t25956863 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t25956863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_6 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)L_6;
		ProfileData_t1845074131  L_8 = ((  ProfileData_t1845074131  (*) (KeyValuePair_2_t25956863 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t25956863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ProfileData_t1845074131  L_9 = ((  ProfileData_t1845074131  (*) (KeyValuePair_2_t25956863 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t25956863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ProfileData_t1845074131 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m663570445_gshared (KeyValuePair_2_t3253813172 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (KeyValuePair_2_t3253813172 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((KeyValuePair_2_t3253813172 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (KeyValuePair_2_t3253813172 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((KeyValuePair_2_t3253813172 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2589995323_gshared (KeyValuePair_2_t3253813172 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2974711292_gshared (KeyValuePair_2_t3253813172 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1871736891_gshared (KeyValuePair_2_t3253813172 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1040348156_gshared (KeyValuePair_2_t3253813172 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2117123366_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2117123366_gshared (KeyValuePair_2_t3253813172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2117123366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t2956870243* G_B2_1 = NULL;
	StringU5BU5D_t2956870243* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t2956870243* G_B1_1 = NULL;
	StringU5BU5D_t2956870243* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t2956870243* G_B3_2 = NULL;
	StringU5BU5D_t2956870243* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t2956870243* G_B5_1 = NULL;
	StringU5BU5D_t2956870243* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t2956870243* G_B4_1 = NULL;
	StringU5BU5D_t2956870243* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t2956870243* G_B6_2 = NULL;
	StringU5BU5D_t2956870243* G_B6_3 = NULL;
	{
		StringU5BU5D_t2956870243* L_0 = (StringU5BU5D_t2956870243*)((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t2956870243* L_1 = (StringU5BU5D_t2956870243*)L_0;
		int32_t L_2 = ((  int32_t (*) (KeyValuePair_2_t3253813172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3253813172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (KeyValuePair_2_t3253813172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3253813172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t2956870243* L_7 = (StringU5BU5D_t2956870243*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t2956870243* L_8 = (StringU5BU5D_t2956870243*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (KeyValuePair_2_t3253813172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3253813172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (KeyValuePair_2_t3253813172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3253813172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t2956870243* L_13 = (StringU5BU5D_t2956870243*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t2956870243*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t Enumerator__ctor_m21100526_MetadataUsageId;
extern "C"  void Enumerator__ctor_m21100526_gshared (Enumerator_t1730156749 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator__ctor_m21100526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		__this->set_si_4(L_0);
		SerializationInfo_t2995724695 * L_1 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2995724695 *)L_1, (String_t*)_stringLiteral3322014, (Type_t *)L_2, /*hidden argument*/NULL);
		__this->set_list_0(((LinkedList_1_t292577037 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		SerializationInfo_t2995724695 * L_4 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2995724695 *)L_4);
		int32_t L_5 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2995724695 *)L_4, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		__this->set_index_2(L_5);
		SerializationInfo_t2995724695 * L_6 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2995724695 *)L_6);
		uint32_t L_7 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2995724695 *)L_6, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_3(L_7);
		__this->set_current_1((LinkedListNode_1_t2549405407 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m3775551166_gshared (Enumerator_t1730156749 * __this, LinkedList_1_t292577037 * ___parent0, const MethodInfo* method)
{
	{
		__this->set_si_4((SerializationInfo_t2995724695 *)NULL);
		LinkedList_1_t292577037 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t2549405407 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t292577037 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3113396643_gshared (Enumerator_t1730156749 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Enumerator_t1730156749 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1730156749 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m3278322477_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3278322477_gshared (Enumerator_t1730156749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m3278322477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t292577037 * L_3 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t2549405407 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m4245765310_MetadataUsageId;
extern "C"  void Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m4245765310_gshared (Enumerator_t1730156749 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m4245765310_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		SerializationInfo_t2995724695 * L_2 = ___info0;
		uint32_t L_3 = (uint32_t)__this->get_version_3();
		NullCheck((SerializationInfo_t2995724695 *)L_2);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2995724695 *)L_2, (String_t*)_stringLiteral351608024, (uint32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_4 = ___info0;
		int32_t L_5 = (int32_t)__this->get_index_2();
		NullCheck((SerializationInfo_t2995724695 *)L_4);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2995724695 *)L_4, (String_t*)_stringLiteral100346066, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern Il2CppClass* IDeserializationCallback_t3135514852_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m98136233_MetadataUsageId;
extern "C"  void Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m98136233_gshared (Enumerator_t1730156749 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m98136233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		SerializationInfo_t2995724695 * L_0 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		LinkedList_1_t292577037 * L_1 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck(L_1);
		SerializationInfo_t2995724695 * L_2 = (SerializationInfo_t2995724695 *)L_1->get_si_4();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		LinkedList_1_t292577037 * L_3 = (LinkedList_1_t292577037 *)__this->get_list_0();
		Enumerator_t1730156749  L_4 = (*(Enumerator_t1730156749 *)__this);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_4);
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t3135514852_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
	}

IL_0032:
	{
		__this->set_si_4((SerializationInfo_t2995724695 *)NULL);
		uint32_t L_6 = (uint32_t)__this->get_version_3();
		LinkedList_1_t292577037 * L_7 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck(L_7);
		uint32_t L_8 = (uint32_t)L_7->get_version_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_index_2();
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		LinkedList_1_t292577037 * L_10 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck((LinkedList_1_t292577037 *)L_10);
		LinkedListNode_1_t2549405407 * L_11 = ((  LinkedListNode_1_t2549405407 * (*) (LinkedList_1_t292577037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((LinkedList_1_t292577037 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (LinkedListNode_1_t2549405407 *)L_11;
		V_1 = (int32_t)0;
		goto IL_0079;
	}

IL_006e:
	{
		LinkedListNode_1_t2549405407 * L_12 = V_0;
		NullCheck(L_12);
		LinkedListNode_1_t2549405407 * L_13 = (LinkedListNode_1_t2549405407 *)L_12->get_forward_2();
		V_0 = (LinkedListNode_1_t2549405407 *)L_13;
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_index_2();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_006e;
		}
	}
	{
		LinkedListNode_1_t2549405407 * L_17 = V_0;
		__this->set_current_1(L_17);
	}

IL_008c:
	{
		return;
	}
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m1721921340_MetadataUsageId;
extern "C"  int32_t Enumerator_get_Current_m1721921340_gshared (Enumerator_t1730156749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1721921340_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t2549405407 * L_2 = (LinkedListNode_1_t2549405407 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t2549405407 * L_4 = (LinkedListNode_1_t2549405407 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t2549405407 *)L_4);
		int32_t L_5 = ((  int32_t (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((LinkedListNode_1_t2549405407 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_MoveNext_m211074417_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m211074417_gshared (Enumerator_t1730156749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m211074417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t292577037 * L_3 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t2549405407 * L_6 = (LinkedListNode_1_t2549405407 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t292577037 * L_7 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t2549405407 * L_8 = (LinkedListNode_1_t2549405407 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t2549405407 * L_9 = (LinkedListNode_1_t2549405407 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t2549405407 * L_10 = (LinkedListNode_1_t2549405407 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t2549405407 * L_11 = (LinkedListNode_1_t2549405407 *)__this->get_current_1();
		LinkedList_1_t292577037 * L_12 = (LinkedList_1_t292577037 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t2549405407 * L_13 = (LinkedListNode_1_t2549405407 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t2549405407 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t2549405407 * L_14 = (LinkedListNode_1_t2549405407 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m4050946346_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m4050946346_gshared (Enumerator_t1730156749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m4050946346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t2549405407 *)NULL);
		__this->set_list_0((LinkedList_1_t292577037 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t Enumerator__ctor_m2035556075_MetadataUsageId;
extern "C"  void Enumerator__ctor_m2035556075_gshared (Enumerator_t4014815678 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator__ctor_m2035556075_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t2995724695 * L_0 = ___info0;
		__this->set_si_4(L_0);
		SerializationInfo_t2995724695 * L_1 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2995724695 *)L_1, (String_t*)_stringLiteral3322014, (Type_t *)L_2, /*hidden argument*/NULL);
		__this->set_list_0(((LinkedList_1_t2577235966 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))));
		SerializationInfo_t2995724695 * L_4 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2995724695 *)L_4);
		int32_t L_5 = SerializationInfo_GetInt32_m4048035953((SerializationInfo_t2995724695 *)L_4, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		__this->set_index_2(L_5);
		SerializationInfo_t2995724695 * L_6 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2995724695 *)L_6);
		uint32_t L_7 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2995724695 *)L_6, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_3(L_7);
		__this->set_current_1((LinkedListNode_1_t539097040 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m857368315_gshared (Enumerator_t4014815678 * __this, LinkedList_1_t2577235966 * ___parent0, const MethodInfo* method)
{
	{
		__this->set_si_4((SerializationInfo_t2995724695 *)NULL);
		LinkedList_1_t2577235966 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t539097040 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2577235966 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared (Enumerator_t4014815678 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Enumerator_t4014815678 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t4014815678 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared (Enumerator_t4014815678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2577235966 * L_3 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t539097040 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3585730209_MetadataUsageId;
extern "C"  void Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3585730209_gshared (Enumerator_t4014815678 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Runtime_Serialization_ISerializable_GetObjectData_m3585730209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		SerializationInfo_t2995724695 * L_2 = ___info0;
		uint32_t L_3 = (uint32_t)__this->get_version_3();
		NullCheck((SerializationInfo_t2995724695 *)L_2);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2995724695 *)L_2, (String_t*)_stringLiteral351608024, (uint32_t)L_3, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_4 = ___info0;
		int32_t L_5 = (int32_t)__this->get_index_2();
		NullCheck((SerializationInfo_t2995724695 *)L_4);
		SerializationInfo_AddValue_m2348540514((SerializationInfo_t2995724695 *)L_4, (String_t*)_stringLiteral100346066, (int32_t)L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern Il2CppClass* IDeserializationCallback_t3135514852_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m407024268_MetadataUsageId;
extern "C"  void Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m407024268_gshared (Enumerator_t4014815678 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m407024268_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t539097040 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		SerializationInfo_t2995724695 * L_0 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		LinkedList_1_t2577235966 * L_1 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck(L_1);
		SerializationInfo_t2995724695 * L_2 = (SerializationInfo_t2995724695 *)L_1->get_si_4();
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		LinkedList_1_t2577235966 * L_3 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		Enumerator_t4014815678  L_4 = (*(Enumerator_t4014815678 *)__this);
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_4);
		NullCheck((Il2CppObject *)L_3);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(0 /* System.Void System.Runtime.Serialization.IDeserializationCallback::OnDeserialization(System.Object) */, IDeserializationCallback_t3135514852_il2cpp_TypeInfo_var, (Il2CppObject *)L_3, (Il2CppObject *)L_5);
	}

IL_0032:
	{
		__this->set_si_4((SerializationInfo_t2995724695 *)NULL);
		uint32_t L_6 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2577235966 * L_7 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck(L_7);
		uint32_t L_8 = (uint32_t)L_7->get_version_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_9 = (int32_t)__this->get_index_2();
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		LinkedList_1_t2577235966 * L_10 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck((LinkedList_1_t2577235966 *)L_10);
		LinkedListNode_1_t539097040 * L_11 = ((  LinkedListNode_1_t539097040 * (*) (LinkedList_1_t2577235966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((LinkedList_1_t2577235966 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (LinkedListNode_1_t539097040 *)L_11;
		V_1 = (int32_t)0;
		goto IL_0079;
	}

IL_006e:
	{
		LinkedListNode_1_t539097040 * L_12 = V_0;
		NullCheck(L_12);
		LinkedListNode_1_t539097040 * L_13 = (LinkedListNode_1_t539097040 *)L_12->get_forward_2();
		V_0 = (LinkedListNode_1_t539097040 *)L_13;
		int32_t L_14 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = (int32_t)__this->get_index_2();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_006e;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_17 = V_0;
		__this->set_current_1(L_17);
	}

IL_008c:
	{
		return;
	}
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m1124073047_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_gshared (Enumerator_t4014815678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1124073047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t539097040 * L_2 = (LinkedListNode_1_t539097040 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t539097040 * L_4 = (LinkedListNode_1_t539097040 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t539097040 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((LinkedListNode_1_t539097040 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_5;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_MoveNext_m2358966120_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m2358966120_gshared (Enumerator_t4014815678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2358966120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2577235966 * L_3 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t539097040 * L_6 = (LinkedListNode_1_t539097040 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2577235966 * L_7 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t539097040 * L_8 = (LinkedListNode_1_t539097040 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t539097040 * L_9 = (LinkedListNode_1_t539097040 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t539097040 * L_10 = (LinkedListNode_1_t539097040 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t539097040 * L_11 = (LinkedListNode_1_t539097040 *)__this->get_current_1();
		LinkedList_1_t2577235966 * L_12 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t539097040 * L_13 = (LinkedListNode_1_t539097040 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t539097040 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t539097040 * L_14 = (LinkedListNode_1_t539097040 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m272587367_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m272587367_gshared (Enumerator_t4014815678 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m272587367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t539097040 *)NULL);
		__this->set_list_0((LinkedList_1_t2577235966 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2815666514_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2815666514_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2815666514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t2549405407 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m3250722177_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m3250722177_gshared (LinkedList_1_t292577037 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3250722177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  void (*) (LinkedList_1_t292577037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((LinkedList_1_t292577037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SerializationInfo_t2995724695 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m626514495_gshared (LinkedList_1_t292577037 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  LinkedListNode_1_t2549405407 * (*) (LinkedList_1_t292577037 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((LinkedList_1_t292577037 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m1204564228_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m1204564228_gshared (LinkedList_1_t292577037 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m1204564228_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1809983122* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Int32U5BU5D_t1809983122* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		Int32U5BU5D_t1809983122* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t292577037 *)__this);
		VirtActionInvoker2< Int32U5BU5D_t1809983122*, int32_t >::Invoke(15 /* System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32) */, (LinkedList_1_t292577037 *)__this, (Int32U5BU5D_t1809983122*)L_3, (int32_t)L_4);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2862642506_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t292577037 *)__this);
		Enumerator_t1730156749  L_0 = ((  Enumerator_t1730156749  (*) (LinkedList_1_t292577037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((LinkedList_1_t292577037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t1730156749  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2334691967_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t292577037 *)__this);
		Enumerator_t1730156749  L_0 = ((  Enumerator_t1730156749  (*) (LinkedList_1_t292577037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((LinkedList_1_t292577037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t1730156749  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1831363171_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m1431399256_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m4084278763_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m4084278763_gshared (LinkedList_1_t292577037 * __this, LinkedListNode_1_t2549405407 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m4084278763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t2549405407 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3386882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t2549405407 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t2549405407 *)L_2);
		LinkedList_1_t292577037 * L_3 = ((  LinkedList_1_t292577037 * (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((LinkedListNode_1_t2549405407 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t292577037 *)L_3) == ((Il2CppObject*)(LinkedList_1_t292577037 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C"  LinkedListNode_1_t2549405407 * LinkedList_1_AddLast_m2458274559_gshared (LinkedList_1_t292577037 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1 = ___value0;
		LinkedListNode_1_t2549405407 * L_2 = (LinkedListNode_1_t2549405407 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t2549405407 *, LinkedList_1_t292577037 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_2, (LinkedList_1_t292577037 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t2549405407 *)L_2;
		LinkedListNode_1_t2549405407 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		int32_t L_4 = ___value0;
		LinkedListNode_1_t2549405407 * L_5 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t2549405407 * L_6 = (LinkedListNode_1_t2549405407 *)L_5->get_back_3();
		LinkedListNode_1_t2549405407 * L_7 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		LinkedListNode_1_t2549405407 * L_8 = (LinkedListNode_1_t2549405407 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t2549405407 *, LinkedList_1_t292577037 *, int32_t, LinkedListNode_1_t2549405407 *, LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_8, (LinkedList_1_t292577037 *)__this, (int32_t)L_4, (LinkedListNode_1_t2549405407 *)L_6, (LinkedListNode_1_t2549405407 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t2549405407 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t2549405407 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C"  void LinkedList_1_Clear_m1056829547_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  void (*) (LinkedList_1_t292577037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((LinkedList_1_t292577037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m2583562993_gshared (LinkedList_1_t292577037 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t2549405407 *)L_0;
		LinkedListNode_1_t2549405407 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t2549405407 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t2549405407 *)L_2);
		int32_t L_3 = ((  int32_t (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2549405407 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), &L_4);
		bool L_6 = Int32_Equals_m4061110258((int32_t*)(&___value0), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t2549405407 * L_7 = V_0;
		NullCheck(L_7);
		LinkedListNode_1_t2549405407 * L_8 = (LinkedListNode_1_t2549405407 *)L_7->get_forward_2();
		V_0 = (LinkedListNode_1_t2549405407 *)L_8;
		LinkedListNode_1_t2549405407 * L_9 = V_0;
		LinkedListNode_1_t2549405407 * L_10 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_9) == ((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_10))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral279574791;
extern Il2CppCodeGenString* _stringLiteral768919341;
extern const uint32_t LinkedList_1_CopyTo_m1139882607_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m1139882607_gshared (LinkedList_1_t292577037 * __this, Int32U5BU5D_t1809983122* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m1139882607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		Int32U5BU5D_t1809983122* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		Int32U5BU5D_t1809983122* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_5 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		Int32U5BU5D_t1809983122* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m1671008509((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t124305799 * L_8 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)_stringLiteral93090393, (String_t*)_stringLiteral279574791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		Int32U5BU5D_t1809983122* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		Int32U5BU5D_t1809983122* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t124305799 * L_14 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_14, (String_t*)_stringLiteral768919341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t2549405407 * L_15 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t2549405407 *)L_15;
		LinkedListNode_1_t2549405407 * L_16 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		Int32U5BU5D_t1809983122* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t2549405407 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t2549405407 *)L_19);
		int32_t L_20 = ((  int32_t (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2549405407 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (int32_t)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t2549405407 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t2549405407 * L_23 = (LinkedListNode_1_t2549405407 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t2549405407 *)L_23;
		LinkedListNode_1_t2549405407 * L_24 = V_0;
		LinkedListNode_1_t2549405407 * L_25 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C"  LinkedListNode_1_t2549405407 * LinkedList_1_Find_m29770003_gshared (LinkedList_1_t292577037 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t2549405407 *)L_0;
		LinkedListNode_1_t2549405407 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t2549405407 *)NULL;
	}

IL_000f:
	{
		goto IL_002a;
	}
	{
		LinkedListNode_1_t2549405407 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t2549405407 *)L_3);
		int32_t L_4 = ((  int32_t (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2549405407 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
	}

IL_002a:
	{
	}
	{
		LinkedListNode_1_t2549405407 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t2549405407 *)L_6);
		int32_t L_7 = ((  int32_t (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t2549405407 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12), &L_8);
		bool L_10 = Int32_Equals_m4061110258((int32_t*)(&___value0), (Il2CppObject *)L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t2549405407 * L_11 = V_0;
		return L_11;
	}

IL_0054:
	{
		LinkedListNode_1_t2549405407 * L_12 = V_0;
		NullCheck(L_12);
		LinkedListNode_1_t2549405407 * L_13 = (LinkedListNode_1_t2549405407 *)L_12->get_forward_2();
		V_0 = (LinkedListNode_1_t2549405407 *)L_13;
		LinkedListNode_1_t2549405407 * L_14 = V_0;
		LinkedListNode_1_t2549405407 * L_15 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_14) == ((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_15))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t2549405407 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1730156749  LinkedList_1_GetEnumerator_m3435228161_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1730156749  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t1730156749 *, LinkedList_1_t292577037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(&L_0, (LinkedList_1_t292577037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_GetObjectData_m3013233630_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m3013233630_gshared (LinkedList_1_t292577037 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m3013233630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1809983122* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		Int32U5BU5D_t1809983122* L_1 = V_0;
		NullCheck((LinkedList_1_t292577037 *)__this);
		VirtActionInvoker2< Int32U5BU5D_t1809983122*, int32_t >::Invoke(15 /* System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32) */, (LinkedList_1_t292577037 *)__this, (Int32U5BU5D_t1809983122*)L_1, (int32_t)0);
		SerializationInfo_t2995724695 * L_2 = ___info0;
		Int32U5BU5D_t1809983122* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_2);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2995724695 *)L_2, (String_t*)_stringLiteral1612836015, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t2995724695 *)L_5);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2995724695 *)L_5, (String_t*)_stringLiteral351608024, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_OnDeserialization_m734131526_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m734131526_gshared (LinkedList_1_t292577037 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m734131526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1809983122* V_0 = NULL;
	int32_t V_1 = 0;
	Int32U5BU5D_t1809983122* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t2995724695 * L_0 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t2995724695 * L_1 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2995724695 *)L_1, (String_t*)_stringLiteral1612836015, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Int32U5BU5D_t1809983122* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_5 = V_0;
		V_2 = (Int32U5BU5D_t1809983122*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		Int32U5BU5D_t1809983122* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		int32_t L_9 = V_1;
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  LinkedListNode_1_t2549405407 * (*) (LinkedList_1_t292577037 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((LinkedList_1_t292577037 *)__this, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_3;
		Int32U5BU5D_t1809983122* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t2995724695 * L_13 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2995724695 *)L_13);
		uint32_t L_14 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2995724695 *)L_13, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_1(L_14);
		__this->set_si_4((SerializationInfo_t2995724695 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m3899314988_gshared (LinkedList_1_t292577037 * __this, int32_t ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		int32_t L_0 = ___value0;
		NullCheck((LinkedList_1_t292577037 *)__this);
		LinkedListNode_1_t2549405407 * L_1 = ((  LinkedListNode_1_t2549405407 * (*) (LinkedList_1_t292577037 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((LinkedList_1_t292577037 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t2549405407 *)L_1;
		LinkedListNode_1_t2549405407 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t2549405407 * L_3 = V_0;
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  void (*) (LinkedList_1_t292577037 *, LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((LinkedList_1_t292577037 *)__this, (LinkedListNode_1_t2549405407 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m1790686448_gshared (LinkedList_1_t292577037 * __this, LinkedListNode_1_t2549405407 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2549405407 * L_0 = ___node0;
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  void (*) (LinkedList_1_t292577037 *, LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((LinkedList_1_t292577037 *)__this, (LinkedListNode_1_t2549405407 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t2549405407 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t2549405407 * L_3 = ___node0;
		LinkedListNode_1_t2549405407 * L_4 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t2549405407 * L_5 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t2549405407 * L_6 = (LinkedListNode_1_t2549405407 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t2549405407 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t2549405407 *)L_8);
		((  void (*) (LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((LinkedListNode_1_t2549405407 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m1940347262_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t2549405407 * L_1 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t2549405407 * L_2 = (LinkedListNode_1_t2549405407 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t292577037 *)__this);
		((  void (*) (LinkedList_1_t292577037 *, LinkedListNode_1_t2549405407 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((LinkedList_1_t292577037 *)__this, (LinkedListNode_1_t2549405407 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m263874676_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C"  LinkedListNode_1_t2549405407 * LinkedList_1_get_First_m4117623486_gshared (LinkedList_1_t292577037 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2955457271_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2955457271_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2955457271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t539097040 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m3369579448_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m3369579448_gshared (LinkedList_1_t2577235966 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3369579448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  void (*) (LinkedList_1_t2577235966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((LinkedList_1_t2577235966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SerializationInfo_t2995724695 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared (LinkedList_1_t2577235966 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  LinkedListNode_1_t539097040 * (*) (LinkedList_1_t2577235966 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((LinkedList_1_t2577235966 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared (LinkedList_1_t2577235966 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		ObjectU5BU5D_t11523773* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t11523773* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(15 /* System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32) */, (LinkedList_1_t2577235966 *)__this, (ObjectU5BU5D_t11523773*)L_3, (int32_t)L_4);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2577235966 *)__this);
		Enumerator_t4014815678  L_0 = ((  Enumerator_t4014815678  (*) (LinkedList_1_t2577235966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((LinkedList_1_t2577235966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t4014815678  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2577235966 *)__this);
		Enumerator_t4014815678  L_0 = ((  Enumerator_t4014815678  (*) (LinkedList_1_t2577235966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((LinkedList_1_t2577235966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t4014815678  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m3939775124_gshared (LinkedList_1_t2577235966 * __this, LinkedListNode_1_t539097040 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t539097040 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3386882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t539097040 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t539097040 *)L_2);
		LinkedList_1_t2577235966 * L_3 = ((  LinkedList_1_t2577235966 * (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((LinkedListNode_1_t539097040 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2577235966 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2577235966 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_4 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t539097040 * LinkedList_1_AddLast_m4070107716_gshared (LinkedList_1_t2577235966 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t539097040 * L_2 = (LinkedListNode_1_t539097040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t539097040 *, LinkedList_1_t2577235966 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_2, (LinkedList_1_t2577235966 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t539097040 *)L_2;
		LinkedListNode_1_t539097040 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t539097040 * L_5 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t539097040 * L_6 = (LinkedListNode_1_t539097040 *)L_5->get_back_3();
		LinkedListNode_1_t539097040 * L_7 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		LinkedListNode_1_t539097040 * L_8 = (LinkedListNode_1_t539097040 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t539097040 *, LinkedList_1_t2577235966 *, Il2CppObject *, LinkedListNode_1_t539097040 *, LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_8, (LinkedList_1_t2577235966 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t539097040 *)L_6, (LinkedListNode_1_t539097040 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t539097040 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t539097040 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m361590562_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  void (*) (LinkedList_1_t2577235966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((LinkedList_1_t2577235966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3484410556_gshared (LinkedList_1_t2577235966 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t539097040 *)L_0;
		LinkedListNode_1_t539097040 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t539097040 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t539097040 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t539097040 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t539097040 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t539097040 * L_6 = (LinkedListNode_1_t539097040 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t539097040 *)L_6;
		LinkedListNode_1_t539097040 * L_7 = V_0;
		LinkedListNode_1_t539097040 * L_8 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral279574791;
extern Il2CppCodeGenString* _stringLiteral768919341;
extern const uint32_t LinkedList_1_CopyTo_m3470139544_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3470139544_gshared (LinkedList_1_t2577235966 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3470139544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t11523773* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_5 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t11523773* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m1671008509((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t124305799 * L_8 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)_stringLiteral93090393, (String_t*)_stringLiteral279574791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t11523773* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t11523773* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t124305799 * L_14 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_14, (String_t*)_stringLiteral768919341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t539097040 * L_15 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t539097040 *)L_15;
		LinkedListNode_1_t539097040 * L_16 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t11523773* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t539097040 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t539097040 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t539097040 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t539097040 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t539097040 * L_23 = (LinkedListNode_1_t539097040 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t539097040 *)L_23;
		LinkedListNode_1_t539097040 * L_24 = V_0;
		LinkedListNode_1_t539097040 * L_25 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t539097040 * LinkedList_1_Find_m2643247334_gshared (LinkedList_1_t2577235966 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t539097040 *)L_0;
		LinkedListNode_1_t539097040 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t539097040 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t539097040 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t539097040 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t539097040 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((LinkedListNode_1_t539097040 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t539097040 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t539097040 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t539097040 * L_11 = (LinkedListNode_1_t539097040 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t539097040 *)L_11;
		LinkedListNode_1_t539097040 * L_12 = V_0;
		LinkedListNode_1_t539097040 * L_13 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t539097040 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t4014815678  LinkedList_1_GetEnumerator_m3713737734_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4014815678  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t4014815678 *, LinkedList_1_t2577235966 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(&L_0, (LinkedList_1_t2577235966 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_GetObjectData_m3974480661_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m3974480661_gshared (LinkedList_1_t2577235966 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m3974480661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t11523773* L_1 = V_0;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(15 /* System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32) */, (LinkedList_1_t2577235966 *)__this, (ObjectU5BU5D_t11523773*)L_1, (int32_t)0);
		SerializationInfo_t2995724695 * L_2 = ___info0;
		ObjectU5BU5D_t11523773* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_2);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2995724695 *)L_2, (String_t*)_stringLiteral1612836015, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t2995724695 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t2995724695 *)L_5);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2995724695 *)L_5, (String_t*)_stringLiteral351608024, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m3445006959_gshared (LinkedList_1_t2577235966 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t11523773* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t2995724695 * L_0 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t2995724695 * L_1 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2995724695 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2995724695 *)L_1, (String_t*)_stringLiteral1612836015, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		ObjectU5BU5D_t11523773* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t11523773*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t11523773* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_1 = (Il2CppObject *)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		Il2CppObject * L_9 = V_1;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  LinkedListNode_1_t539097040 * (*) (LinkedList_1_t2577235966 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((LinkedList_1_t2577235966 *)__this, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		int32_t L_10 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_11 = V_3;
		ObjectU5BU5D_t11523773* L_12 = V_2;
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t2995724695 * L_13 = (SerializationInfo_t2995724695 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2995724695 *)L_13);
		uint32_t L_14 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2995724695 *)L_13, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_1(L_14);
		__this->set_si_4((SerializationInfo_t2995724695 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m3283493303_gshared (LinkedList_1_t2577235966 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		LinkedListNode_1_t539097040 * L_1 = ((  LinkedListNode_1_t539097040 * (*) (LinkedList_1_t2577235966 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((LinkedList_1_t2577235966 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t539097040 *)L_1;
		LinkedListNode_1_t539097040 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t539097040 * L_3 = V_0;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  void (*) (LinkedList_1_t2577235966 *, LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((LinkedList_1_t2577235966 *)__this, (LinkedListNode_1_t539097040 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m4034790180_gshared (LinkedList_1_t2577235966 * __this, LinkedListNode_1_t539097040 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t539097040 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  void (*) (LinkedList_1_t2577235966 *, LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((LinkedList_1_t2577235966 *)__this, (LinkedListNode_1_t539097040 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t539097040 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t539097040 * L_3 = ___node0;
		LinkedListNode_1_t539097040 * L_4 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_5 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t539097040 * L_6 = (LinkedListNode_1_t539097040 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t539097040 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t539097040 *)L_8);
		((  void (*) (LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((LinkedListNode_1_t539097040 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2573038887_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_1 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t539097040 * L_2 = (LinkedListNode_1_t539097040 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2577235966 *)__this);
		((  void (*) (LinkedList_1_t2577235966 *, LinkedListNode_1_t539097040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((LinkedList_1_t2577235966 *)__this, (LinkedListNode_1_t539097040 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m1368924491_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t539097040 * LinkedList_1_get_First_m3278587786_gshared (LinkedList_1_t2577235966 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m1733287687_gshared (LinkedListNode_1_t2549405407 * __this, LinkedList_1_t292577037 * ___list0, int32_t ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t292577037 * L_0 = ___list0;
		__this->set_container_1(L_0);
		int32_t L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t2549405407 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t2549405407 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m3189609383_gshared (LinkedListNode_1_t2549405407 * __this, LinkedList_1_t292577037 * ___list0, int32_t ___value1, LinkedListNode_1_t2549405407 * ___previousNode2, LinkedListNode_1_t2549405407 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t292577037 * L_0 = ___list0;
		__this->set_container_1(L_0);
		int32_t L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t2549405407 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t2549405407 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t2549405407 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t2549405407 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C"  void LinkedListNode_1_Detach_m2691333945_gshared (LinkedListNode_1_t2549405407 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * V_0 = NULL;
	{
		LinkedListNode_1_t2549405407 * L_0 = (LinkedListNode_1_t2549405407 *)__this->get_back_3();
		LinkedListNode_1_t2549405407 * L_1 = (LinkedListNode_1_t2549405407 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t2549405407 * L_2 = (LinkedListNode_1_t2549405407 *)__this->get_forward_2();
		LinkedListNode_1_t2549405407 * L_3 = (LinkedListNode_1_t2549405407 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t2549405407 *)NULL;
		__this->set_back_3((LinkedListNode_1_t2549405407 *)NULL);
		LinkedListNode_1_t2549405407 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t292577037 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C"  LinkedList_1_t292577037 * LinkedListNode_1_get_List_m751690205_gshared (LinkedListNode_1_t2549405407 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C"  LinkedListNode_1_t2549405407 * LinkedListNode_1_get_Next_m3012446889_gshared (LinkedListNode_1_t2549405407 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t2549405407 * G_B4_0 = NULL;
	{
		LinkedList_1_t292577037 * L_0 = (LinkedList_1_t292577037 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t2549405407 * L_1 = (LinkedListNode_1_t2549405407 *)__this->get_forward_2();
		LinkedList_1_t292577037 * L_2 = (LinkedList_1_t292577037 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t2549405407 * L_3 = (LinkedListNode_1_t2549405407 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t2549405407 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t2549405407 * L_4 = (LinkedListNode_1_t2549405407 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t2549405407 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C"  int32_t LinkedListNode_1_get_Value_m326957826_gshared (LinkedListNode_1_t2549405407 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m648136130_gshared (LinkedListNode_1_t539097040 * __this, LinkedList_1_t2577235966 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2577235966 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t539097040 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t539097040 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m448391458_gshared (LinkedListNode_1_t539097040 * __this, LinkedList_1_t2577235966 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t539097040 * ___previousNode2, LinkedListNode_1_t539097040 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2577235966 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t539097040 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t539097040 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t539097040 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t539097040 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3406254942_gshared (LinkedListNode_1_t539097040 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * V_0 = NULL;
	{
		LinkedListNode_1_t539097040 * L_0 = (LinkedListNode_1_t539097040 *)__this->get_back_3();
		LinkedListNode_1_t539097040 * L_1 = (LinkedListNode_1_t539097040 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t539097040 * L_2 = (LinkedListNode_1_t539097040 *)__this->get_forward_2();
		LinkedListNode_1_t539097040 * L_3 = (LinkedListNode_1_t539097040 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t539097040 *)NULL;
		__this->set_back_3((LinkedListNode_1_t539097040 *)NULL);
		LinkedListNode_1_t539097040 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2577235966 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t2577235966 * LinkedListNode_1_get_List_m3467110818_gshared (LinkedListNode_1_t539097040 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C"  LinkedListNode_1_t539097040 * LinkedListNode_1_get_Next_m1427618777_gshared (LinkedListNode_1_t539097040 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t539097040 * G_B4_0 = NULL;
	{
		LinkedList_1_t2577235966 * L_0 = (LinkedList_1_t2577235966 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_1 = (LinkedListNode_1_t539097040 *)__this->get_forward_2();
		LinkedList_1_t2577235966 * L_2 = (LinkedList_1_t2577235966 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t539097040 * L_3 = (LinkedListNode_1_t539097040 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t539097040 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t539097040 * L_4 = (LinkedListNode_1_t539097040 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t539097040 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m702633824_gshared (LinkedListNode_1_t539097040 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2249551728_gshared (Enumerator_t2195698409 * __this, List_1_t4109915417 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4109915417 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4109915417 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1882046178_gshared (Enumerator_t2195698409 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2195698409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2195698409 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m602451608_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m602451608_gshared (Enumerator_t2195698409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m602451608_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t2195698409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2195698409 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		KeyValuePair_2_t3312956448  L_2 = (KeyValuePair_2_t3312956448 )__this->get_current_3();
		KeyValuePair_2_t3312956448  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void Enumerator_Dispose_m3838892373_gshared (Enumerator_t2195698409 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4109915417 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1553851918_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1553851918_gshared (Enumerator_t2195698409 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1553851918_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4109915417 * L_0 = (List_1_t4109915417 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2195698409  L_1 = (*(Enumerator_t2195698409 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4109915417 * L_7 = (List_1_t4109915417 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1524607506_gshared (Enumerator_t2195698409 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t2195698409 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2195698409 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4109915417 * L_2 = (List_1_t4109915417 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4109915417 * L_4 = (List_1_t4109915417 *)__this->get_l_0();
		NullCheck(L_4);
		KeyValuePair_2U5BU5D_t346249057* L_5 = (KeyValuePair_2U5BU5D_t346249057*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t3312956448  Enumerator_get_Current_m3227842279_gshared (Enumerator_t2195698409 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = (KeyValuePair_2_t3312956448 )__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1242988386_gshared (Enumerator_t1730156748 * __this, List_1_t3644373756 * ___l0, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t3644373756 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_gshared (Enumerator_t1730156748 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1730156748 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1730156748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_gshared (Enumerator_t1730156748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t1730156748 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1730156748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3304555975_gshared (Enumerator_t1730156748 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t3644373756 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m936708480_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m936708480_gshared (Enumerator_t1730156748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m936708480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3644373756 * L_0 = (List_1_t3644373756 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1730156748  L_1 = (*(Enumerator_t1730156748 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t3644373756 * L_7 = (List_1_t3644373756 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1157355384_gshared (Enumerator_t1730156748 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t1730156748 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1730156748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t3644373756 * L_2 = (List_1_t3644373756 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t3644373756 * L_4 = (List_1_t3644373756 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1989858276_gshared (Enumerator_t1730156748 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1029849669_gshared (Enumerator_t4014815677 * __this, List_1_t1634065389 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1634065389 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1634065389 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared (Enumerator_t4014815677 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t4014815677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared (Enumerator_t4014815677 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t4014815677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2904289642_gshared (Enumerator_t4014815677 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1634065389 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1522854819_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1522854819_gshared (Enumerator_t4014815677 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1522854819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4014815677  L_1 = (*(Enumerator_t4014815677 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1634065389 * L_7 = (List_1_t1634065389 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m844464217_gshared (Enumerator_t4014815677 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t4014815677 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t4014815677 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1634065389 * L_2 = (List_1_t1634065389 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1634065389 * L_4 = (List_1_t1634065389 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4198990746_gshared (Enumerator_t4014815677 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m256124610_gshared (Enumerator_t3496444386 * __this, List_1_t1115694098 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1115694098 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1115694098 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_gshared (Enumerator_t3496444386 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3496444386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3496444386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_gshared (Enumerator_t3496444386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t3496444386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3496444386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t318735129  L_2 = (CustomAttributeNamedArgument_t318735129 )__this->get_current_3();
		CustomAttributeNamedArgument_t318735129  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m855442727_gshared (Enumerator_t3496444386 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1115694098 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m508287200_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m508287200_gshared (Enumerator_t3496444386 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m508287200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1115694098 * L_0 = (List_1_t1115694098 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3496444386  L_1 = (*(Enumerator_t3496444386 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1115694098 * L_7 = (List_1_t1115694098 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3090636416_gshared (Enumerator_t3496444386 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t3496444386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3496444386 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1115694098 * L_2 = (List_1_t1115694098 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1115694098 * L_4 = (List_1_t1115694098 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t318735129  Enumerator_get_Current_m473447609_gshared (Enumerator_t3496444386 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = (CustomAttributeNamedArgument_t318735129 )__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m444414259_gshared (Enumerator_t3738124819 * __this, List_1_t1357374531 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1357374531 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1357374531 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_gshared (Enumerator_t3738124819 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3738124819 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3738124819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_gshared (Enumerator_t3738124819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t3738124819 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3738124819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t560415562  L_2 = (CustomAttributeTypedArgument_t560415562 )__this->get_current_3();
		CustomAttributeTypedArgument_t560415562  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3403219928_gshared (Enumerator_t3738124819 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1357374531 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1438062353_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1438062353_gshared (Enumerator_t3738124819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1438062353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1357374531 * L_0 = (List_1_t1357374531 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3738124819  L_1 = (*(Enumerator_t3738124819 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1357374531 * L_7 = (List_1_t1357374531 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m467351023_gshared (Enumerator_t3738124819 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t3738124819 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3738124819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1357374531 * L_2 = (List_1_t1357374531 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1357374531 * L_4 = (List_1_t1357374531 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_5 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t560415562  Enumerator_get_Current_m1403222762_gshared (Enumerator_t3738124819 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = (CustomAttributeTypedArgument_t560415562 )__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m704684707_gshared (Enumerator_t2114956985 * __this, List_1_t4029173993 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4029173993 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4029173993 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2250643407_gshared (Enumerator_t2114956985 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2114956985 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2114956985 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_gshared (Enumerator_t2114956985 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t2114956985 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2114956985 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C"  void Enumerator_Dispose_m1475987784_gshared (Enumerator_t2114956985 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4029173993 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m823633025_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m823633025_gshared (Enumerator_t2114956985 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m823633025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4029173993 * L_0 = (List_1_t4029173993 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2114956985  L_1 = (*(Enumerator_t2114956985 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4029173993 * L_7 = (List_1_t4029173993 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2093101951_gshared (Enumerator_t2114956985 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t2114956985 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2114956985 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4029173993 * L_2 = (List_1_t4029173993 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4029173993 * L_4 = (List_1_t4029173993 *)__this->get_l_0();
		NullCheck(L_4);
		PIXEL_FORMATU5BU5D_t4247347793* L_5 = (PIXEL_FORMATU5BU5D_t4247347793*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2537161434_gshared (Enumerator_t2114956985 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3526812969_gshared (Enumerator_t2788092671 * __this, List_1_t407342383 * ___l0, const MethodInfo* method)
{
	{
		List_1_t407342383 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t407342383 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2248530313_gshared (Enumerator_t2788092671 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2788092671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2788092671 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m541630517_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m541630517_gshared (Enumerator_t2788092671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m541630517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t2788092671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2788092671 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		TargetSearchResult_t3905350710  L_2 = (TargetSearchResult_t3905350710 )__this->get_current_3();
		TargetSearchResult_t3905350710  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C"  void Enumerator_Dispose_m1489800526_gshared (Enumerator_t2788092671 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t407342383 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1128068487_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1128068487_gshared (Enumerator_t2788092671 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1128068487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t407342383 * L_0 = (List_1_t407342383 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2788092671  L_1 = (*(Enumerator_t2788092671 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t973246880 * L_5 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t407342383 * L_7 = (List_1_t407342383 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_9 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m530730869_gshared (Enumerator_t2788092671 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Enumerator_t2788092671 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2788092671 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t407342383 * L_2 = (List_1_t407342383 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t407342383 * L_4 = (List_1_t407342383 *)__this->get_l_0();
		NullCheck(L_4);
		TargetSearchResultU5BU5D_t2556575283* L_5 = (TargetSearchResultU5BU5D_t2556575283*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->set_current_3(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C"  TargetSearchResult_t3905350710  Enumerator_get_Current_m3724049598_gshared (Enumerator_t2788092671 * __this, const MethodInfo* method)
{
	{
		TargetSearchResult_t3905350710  L_0 = (TargetSearchResult_t3905350710 )__this->get_current_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C"  void List_1__ctor_m4086276689_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2U5BU5D_t346249057* L_0 = ((List_1_t4109915417_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m449995150_gshared (List_1_t4109915417 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t4109915417 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2U5BU5D_t346249057* L_3 = ((List_1_t4109915417_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t4109915417 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((KeyValuePair_2U5BU5D_t346249057*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t4109915417 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2368693794_MetadataUsageId;
extern "C"  void List_1__ctor_m2368693794_gshared (List_1_t4109915417 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2368693794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((KeyValuePair_2U5BU5D_t346249057*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.cctor()
extern "C"  void List_1__cctor_m1638429564_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t4109915417_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((KeyValuePair_2U5BU5D_t346249057*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m653598491_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t4109915417 *)__this);
		Enumerator_t2195698409  L_0 = ((  Enumerator_t2195698409  (*) (List_1_t4109915417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t4109915417 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t2195698409  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3259239443_gshared (List_1_t4109915417 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2473143138_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t4109915417 *)__this);
		Enumerator_t2195698409  L_0 = ((  Enumerator_t2195698409  (*) (List_1_t4109915417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t4109915417 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t2195698409  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m979169691_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m979169691_gshared (List_1_t4109915417 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m979169691_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4109915417 *)__this);
			VirtActionInvoker1< KeyValuePair_2_t3312956448  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T) */, (List_1_t4109915417 *)__this, (KeyValuePair_2_t3312956448 )((*(KeyValuePair_2_t3312956448 *)((KeyValuePair_2_t3312956448 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m1387332357_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m1387332357_gshared (List_1_t4109915417 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m1387332357_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4109915417 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, KeyValuePair_2_t3312956448  >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T) */, (List_1_t4109915417 *)__this, (KeyValuePair_2_t3312956448 )((*(KeyValuePair_2_t3312956448 *)((KeyValuePair_2_t3312956448 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m2823086707_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2823086707_gshared (List_1_t4109915417 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m2823086707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4109915417 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, KeyValuePair_2_t3312956448  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T) */, (List_1_t4109915417 *)__this, (KeyValuePair_2_t3312956448 )((*(KeyValuePair_2_t3312956448 *)((KeyValuePair_2_t3312956448 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m3622467686_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3622467686_gshared (List_1_t4109915417 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3622467686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t4109915417 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t4109915417 *)__this);
			VirtActionInvoker2< int32_t, KeyValuePair_2_t3312956448  >::Invoke(26 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T) */, (List_1_t4109915417 *)__this, (int32_t)L_1, (KeyValuePair_2_t3312956448 )((*(KeyValuePair_2_t3312956448 *)((KeyValuePair_2_t3312956448 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m3448734466_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m3448734466_gshared (List_1_t4109915417 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3448734466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4109915417 *)__this);
			VirtFuncInvoker1< bool, KeyValuePair_2_t3312956448  >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T) */, (List_1_t4109915417 *)__this, (KeyValuePair_2_t3312956448 )((*(KeyValuePair_2_t3312956448 *)((KeyValuePair_2_t3312956448 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2678894790_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2689327529_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4234022128_gshared (List_1_t4109915417 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4109915417 *)__this);
		KeyValuePair_2_t3312956448  L_1 = VirtFuncInvoker1< KeyValuePair_2_t3312956448 , int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32) */, (List_1_t4109915417 *)__this, (int32_t)L_0);
		KeyValuePair_2_t3312956448  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m2881888957_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m2881888957_gshared (List_1_t4109915417 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m2881888957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t4109915417 *)__this);
			VirtActionInvoker2< int32_t, KeyValuePair_2_t3312956448  >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T) */, (List_1_t4109915417 *)__this, (int32_t)L_0, (KeyValuePair_2_t3312956448 )((*(KeyValuePair_2_t3312956448 *)((KeyValuePair_2_t3312956448 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T)
extern "C"  void List_1_Add_m953241614_gshared (List_1_t4109915417 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		KeyValuePair_2U5BU5D_t346249057* L_1 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t4109915417 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		KeyValuePair_2U5BU5D_t346249057* L_2 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		KeyValuePair_2_t3312956448  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t3312956448 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3474582345_gshared (List_1_t4109915417 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		KeyValuePair_2U5BU5D_t346249057* L_3 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t4109915417 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t4109915417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t4109915417 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t4109915417 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m1143135815_gshared (List_1_t4109915417 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t4109915417 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		KeyValuePair_2U5BU5D_t346249057* L_5 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< KeyValuePair_2U5BU5D_t346249057*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (KeyValuePair_2U5BU5D_t346249057*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m404108039_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m404108039_gshared (List_1_t4109915417 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m404108039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t3312956448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			KeyValuePair_2_t3312956448  L_3 = InterfaceFuncInvoker0< KeyValuePair_2_t3312956448  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (KeyValuePair_2_t3312956448 )L_3;
			KeyValuePair_2_t3312956448  L_4 = V_0;
			NullCheck((List_1_t4109915417 *)__this);
			VirtActionInvoker1< KeyValuePair_2_t3312956448  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(T) */, (List_1_t4109915417 *)__this, (KeyValuePair_2_t3312956448 )L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m968536880_gshared (List_1_t4109915417 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t4109915417 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t4109915417 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t4109915417 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C"  void List_1_Clear_m1492409980_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		KeyValuePair_2U5BU5D_t346249057* L_1 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Contains(T)
extern "C"  bool List_1_Contains_m417879790_gshared (List_1_t4109915417 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		KeyValuePair_2_t3312956448  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057*, KeyValuePair_2_t3312956448 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t346249057*)L_0, (KeyValuePair_2_t3312956448 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1368690814_gshared (List_1_t4109915417 * __this, KeyValuePair_2U5BU5D_t346249057* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		KeyValuePair_2U5BU5D_t346249057* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C"  Enumerator_t2195698409  List_1_GetEnumerator_m393507115_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2195698409  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t2195698409 *, List_1_t4109915417 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t4109915417 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m4232652234_gshared (List_1_t4109915417 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		KeyValuePair_2_t3312956448  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057*, KeyValuePair_2_t3312956448 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t346249057*)L_0, (KeyValuePair_2_t3312956448 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1644890325_gshared (List_1_t4109915417 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		KeyValuePair_2U5BU5D_t346249057* L_5 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_6 = ___start0;
		KeyValuePair_2U5BU5D_t346249057* L_7 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		KeyValuePair_2U5BU5D_t346249057* L_15 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m1115057998_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m1115057998_gshared (List_1_t4109915417 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m1115057998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m863546997_gshared (List_1_t4109915417 * __this, int32_t ___index0, KeyValuePair_2_t3312956448  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t4109915417 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		KeyValuePair_2U5BU5D_t346249057* L_2 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t4109915417 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t4109915417 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		KeyValuePair_2U5BU5D_t346249057* L_4 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_5 = ___index0;
		KeyValuePair_2_t3312956448  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t3312956448 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m3863526186_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m3863526186_gshared (List_1_t4109915417 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m3863526186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(T)
extern "C"  bool List_1_Remove_m276953705_gshared (List_1_t4109915417 * __this, KeyValuePair_2_t3312956448  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		KeyValuePair_2_t3312956448  L_0 = ___item0;
		NullCheck((List_1_t4109915417 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, KeyValuePair_2_t3312956448  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::IndexOf(T) */, (List_1_t4109915417 *)__this, (KeyValuePair_2_t3312956448 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t4109915417 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32) */, (List_1_t4109915417 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m3032367163_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m3032367163_gshared (List_1_t4109915417 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3032367163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t4109915417 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		KeyValuePair_2U5BU5D_t346249057* L_5 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3834349075_gshared (List_1_t4109915417 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t346249057*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t346249057* List_1_ToArray_m642277418_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	KeyValuePair_2U5BU5D_t346249057* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (KeyValuePair_2U5BU5D_t346249057*)((KeyValuePair_2U5BU5D_t346249057*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		KeyValuePair_2U5BU5D_t346249057* L_1 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		KeyValuePair_2U5BU5D_t346249057* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		KeyValuePair_2U5BU5D_t346249057* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2758617338_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2U5BU5D_t346249057* L_0 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m684771163_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m684771163_gshared (List_1_t4109915417 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m684771163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		KeyValuePair_2U5BU5D_t346249057** L_3 = (KeyValuePair_2U5BU5D_t346249057**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, KeyValuePair_2U5BU5D_t346249057**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (KeyValuePair_2U5BU5D_t346249057**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C"  int32_t List_1_get_Count_m2180507889_gshared (List_1_t4109915417 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m3889419041_MetadataUsageId;
extern "C"  KeyValuePair_2_t3312956448  List_1_get_Item_m3889419041_gshared (List_1_t4109915417 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m3889419041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		KeyValuePair_2U5BU5D_t346249057* L_3 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m2749572620_MetadataUsageId;
extern "C"  void List_1_set_Item_m2749572620_gshared (List_1_t4109915417 * __this, int32_t ___index0, KeyValuePair_2_t3312956448  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m2749572620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4109915417 *)__this);
		((  void (*) (List_1_t4109915417 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t4109915417 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		KeyValuePair_2U5BU5D_t346249057* L_4 = (KeyValuePair_2U5BU5D_t346249057*)__this->get__items_1();
		int32_t L_5 = ___index0;
		KeyValuePair_2_t3312956448  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (KeyValuePair_2_t3312956448 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1394852971_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t1809983122* L_0 = ((List_1_t3644373756_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2669630085_gshared (List_1_t3644373756 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t3644373756 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t1809983122* L_3 = ((List_1_t3644373756_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t3644373756 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t3644373756 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2126972244_MetadataUsageId;
extern "C"  void List_1__ctor_m2126972244_gshared (List_1_t3644373756 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2126972244_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C"  void List_1__cctor_m1113350026_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t3644373756_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1918002125_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3644373756 *)__this);
		Enumerator_t1730156748  L_0 = ((  Enumerator_t1730156748  (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t3644373756 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t1730156748  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1952216609_gshared (List_1_t3644373756 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m868394352_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3644373756 *)__this);
		Enumerator_t1730156748  L_0 = ((  Enumerator_t1730156748  (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t3644373756 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t1730156748  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3146823117_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3146823117_gshared (List_1_t3644373756 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3146823117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3644373756 *)__this);
			VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(T) */, (List_1_t3644373756 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m474280595_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m474280595_gshared (List_1_t3644373756 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m474280595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3644373756 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, int32_t >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T) */, (List_1_t3644373756 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m615988645_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m615988645_gshared (List_1_t3644373756 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m615988645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3644373756 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T) */, (List_1_t3644373756 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m411008408_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m411008408_gshared (List_1_t3644373756 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m411008408_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t3644373756 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t3644373756 *)__this);
			VirtActionInvoker2< int32_t, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T) */, (List_1_t3644373756 *)__this, (int32_t)L_1, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1912599696_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1912599696_gshared (List_1_t3644373756 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1912599696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t3644373756 *)__this);
			VirtFuncInvoker1< bool, int32_t >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T) */, (List_1_t3644373756 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3879536340_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m857080475_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m86227298_gshared (List_1_t3644373756 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3644373756 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, (List_1_t3644373756 *)__this, (int32_t)L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m456041327_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m456041327_gshared (List_1_t3644373756 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m456041327_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t3644373756 *)__this);
			VirtActionInvoker2< int32_t, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T) */, (List_1_t3644373756 *)__this, (int32_t)L_0, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C"  void List_1_Add_m2047666076_gshared (List_1_t3644373756 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3644373756 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1518088151_gshared (List_1_t3644373756 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t3644373756 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t3644373756 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t3644373756 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3810486997_gshared (List_1_t3644373756 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3644373756 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< Int32U5BU5D_t1809983122*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (Int32U5BU5D_t1809983122*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m3071459221_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m3071459221_gshared (List_1_t3644373756 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m3071459221_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (int32_t)L_3;
			int32_t L_4 = V_0;
			NullCheck((List_1_t3644373756 *)__this);
			VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(T) */, (List_1_t3644373756 *)__this, (int32_t)L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3137340898_gshared (List_1_t3644373756 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t3644373756 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t3644373756 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t3644373756 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C"  void List_1_Clear_m1059829934_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C"  bool List_1_Contains_m3160387232_gshared (List_1_t3644373756 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (Int32U5BU5D_t1809983122*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m1849335948_gshared (List_1_t3644373756 * __this, Int32U5BU5D_t1809983122* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		Int32U5BU5D_t1809983122* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t1730156748  List_1_GetEnumerator_m1383295158_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		Enumerator_t1730156748  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t1730156748 *, List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t3644373756 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m779930456_gshared (List_1_t3644373756 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (Int32U5BU5D_t1809983122*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3680447203_gshared (List_1_t3644373756 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_6 = ___start0;
		Int32U5BU5D_t1809983122* L_7 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_15 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m1595703132_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m1595703132_gshared (List_1_t3644373756 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m1595703132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2397006339_gshared (List_1_t3644373756 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t3644373756 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3644373756 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t3644373756 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		Int32U5BU5D_t1809983122* L_4 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m3092536376_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m3092536376_gshared (List_1_t3644373756 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m3092536376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C"  bool List_1_Remove_m418354843_gshared (List_1_t3644373756 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___item0;
		NullCheck((List_1_t3644373756 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T) */, (List_1_t3644373756 *)__this, (int32_t)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3644373756 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32) */, (List_1_t3644373756 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m270859209_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m270859209_gshared (List_1_t3644373756 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m270859209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t3644373756 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3458811461_gshared (List_1_t3644373756 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (Int32U5BU5D_t1809983122*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t1809983122* List_1_ToArray_m1209652252_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	Int32U5BU5D_t1809983122* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		Int32U5BU5D_t1809983122* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		Int32U5BU5D_t1809983122* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2719438728_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m3023244265_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m3023244265_gshared (List_1_t3644373756 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m3023244265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		Int32U5BU5D_t1809983122** L_3 = (Int32U5BU5D_t1809983122**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, Int32U5BU5D_t1809983122**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (Int32U5BU5D_t1809983122**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m2520315171_gshared (List_1_t3644373756 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2194668015_MetadataUsageId;
extern "C"  int32_t List_1_get_Item_m2194668015_gshared (List_1_t3644373756 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2194668015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m3230217754_MetadataUsageId;
extern "C"  void List_1_set_Item_m3230217754_gshared (List_1_t3644373756 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m3230217754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t3644373756 *)__this);
		((  void (*) (List_1_t3644373756 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t3644373756 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		Int32U5BU5D_t1809983122* L_4 = (Int32U5BU5D_t1809983122*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m3048469268_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectU5BU5D_t11523773* L_0 = ((List_1_t1634065389_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m1160795371_gshared (List_1_t1634065389 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1634065389 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectU5BU5D_t11523773* L_3 = ((List_1_t1634065389_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1634065389 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1634065389 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m3643386469_MetadataUsageId;
extern "C"  void List_1__ctor_m3643386469_gshared (List_1_t1634065389 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3643386469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C"  void List_1__cctor_m3826137881_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1634065389_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1634065389 *)__this);
		Enumerator_t4014815677  L_0 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1634065389 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t4014815677  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared (List_1_t1634065389 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1634065389 *)__this);
		Enumerator_t4014815677  L_0 = ((  Enumerator_t4014815677  (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1634065389 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t4014815677  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3794749222_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3794749222_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3794749222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1634065389 *)__this);
			VirtActionInvoker1< Il2CppObject * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(T) */, (List_1_t1634065389 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m2659633254_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m2659633254_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2659633254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1634065389 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T) */, (List_1_t1634065389 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3431692926_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3431692926_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3431692926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1634065389 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T) */, (List_1_t1634065389 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m2067529129_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m2067529129_gshared (List_1_t1634065389 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m2067529129_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1634065389 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t1634065389 *)__this);
			VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(26 /* System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T) */, (List_1_t1634065389 *)__this, (int32_t)L_1, (Il2CppObject *)((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1644145887_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1644145887_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1644145887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1634065389 *)__this);
			VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T) */, (List_1_t1634065389 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3985478825_gshared (List_1_t1634065389 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1634065389 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32) */, (List_1_t1634065389 *)__this, (int32_t)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3234554688_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3234554688_gshared (List_1_t1634065389 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3234554688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t1634065389 *)__this);
			VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T) */, (List_1_t1634065389 *)__this, (int32_t)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C"  void List_1_Add_m642669291_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1634065389 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		Il2CppObject * L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m4122600870_gshared (List_1_t1634065389 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1634065389 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t1634065389 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t1634065389 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2478449828_gshared (List_1_t1634065389 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1634065389 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (ObjectU5BU5D_t11523773*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1739422052_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1739422052_gshared (List_1_t1634065389 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1739422052_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (Il2CppObject *)L_3;
			Il2CppObject * L_4 = V_0;
			NullCheck((List_1_t1634065389 *)__this);
			VirtActionInvoker1< Il2CppObject * >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(T) */, (List_1_t1634065389 *)__this, (Il2CppObject *)L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2229151411_gshared (List_1_t1634065389 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1634065389 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1634065389 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1634065389 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m454602559_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C"  bool List_1_Contains_m4186092781_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (Il2CppObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3988356635_gshared (List_1_t1634065389 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		ObjectU5BU5D_t11523773* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t4014815677  List_1_GetEnumerator_m2326457258_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4014815677  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t4014815677 *, List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t1634065389 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1752303327_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (Il2CppObject *)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3807054194_gshared (List_1_t1634065389 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_6 = ___start0;
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m3734723819_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m3734723819_gshared (List_1_t1634065389 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m3734723819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3427163986_gshared (List_1_t1634065389 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1634065389 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1634065389 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t1634065389 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m2905071175_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2905071175_gshared (List_1_t1634065389 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2905071175_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C"  bool List_1_Remove_m2747911208_gshared (List_1_t1634065389 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((List_1_t1634065389 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T) */, (List_1_t1634065389 *)__this, (Il2CppObject *)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1634065389 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32) */, (List_1_t1634065389 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m1301016856_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m1301016856_gshared (List_1_t1634065389 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m1301016856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t1634065389 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3726677974_gshared (List_1_t1634065389 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t11523773* List_1_ToArray_m238588755_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		ObjectU5BU5D_t11523773* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m543520655_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m1332789688_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m1332789688_gshared (List_1_t1634065389 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m1332789688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		ObjectU5BU5D_t11523773** L_3 = (ObjectU5BU5D_t11523773**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2599103100_gshared (List_1_t1634065389 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2771401372_MetadataUsageId;
extern "C"  Il2CppObject * List_1_get_Item_m2771401372_gshared (List_1_t1634065389 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2771401372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m1074271145_MetadataUsageId;
extern "C"  void List_1_set_Item_m1074271145_gshared (List_1_t1634065389 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m1074271145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1634065389 *)__this);
		((  void (*) (List_1_t1634065389 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1634065389 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		ObjectU5BU5D_t11523773* L_4 = (ObjectU5BU5D_t11523773*)__this->get__items_1();
		int32_t L_5 = ___index0;
		Il2CppObject * L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void List_1__ctor_m3182785955_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = ((List_1_t1115694098_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m2518707964_gshared (List_1_t1115694098 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1115694098 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = ((List_1_t1115694098_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1115694098 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1115694098 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2888355444_MetadataUsageId;
extern "C"  void List_1__ctor_m2888355444_gshared (List_1_t1115694098 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2888355444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C"  void List_1__cctor_m3694987882_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1115694098_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1445350893_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1115694098 *)__this);
		Enumerator_t3496444386  L_0 = ((  Enumerator_t3496444386  (*) (List_1_t1115694098 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1115694098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t3496444386  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m499056897_gshared (List_1_t1115694098 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1886158288_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1115694098 *)__this);
		Enumerator_t3496444386  L_0 = ((  Enumerator_t3496444386  (*) (List_1_t1115694098 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1115694098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t3496444386  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m225941485_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m225941485_gshared (List_1_t1115694098 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m225941485_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1115694098 *)__this);
			VirtActionInvoker1< CustomAttributeNamedArgument_t318735129  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T) */, (List_1_t1115694098 *)__this, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m3549523443_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m3549523443_gshared (List_1_t1115694098 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m3549523443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1115694098 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, CustomAttributeNamedArgument_t318735129  >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, (List_1_t1115694098 *)__this, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m2250248133_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2250248133_gshared (List_1_t1115694098 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m2250248133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1115694098 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, (List_1_t1115694098 *)__this, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m3869877944_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3869877944_gshared (List_1_t1115694098 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3869877944_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1115694098 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t1115694098 *)__this);
			VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(26 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T) */, (List_1_t1115694098 *)__this, (int32_t)L_1, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m686389104_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m686389104_gshared (List_1_t1115694098 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m686389104_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1115694098 *)__this);
			VirtFuncInvoker1< bool, CustomAttributeNamedArgument_t318735129  >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T) */, (List_1_t1115694098 *)__this, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2804861492_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3955324539_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1086436674_gshared (List_1_t1115694098 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1115694098 *)__this);
		CustomAttributeNamedArgument_t318735129  L_1 = VirtFuncInvoker1< CustomAttributeNamedArgument_t318735129 , int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, (List_1_t1115694098 *)__this, (int32_t)L_0);
		CustomAttributeNamedArgument_t318735129  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m124978319_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m124978319_gshared (List_1_t1115694098 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m124978319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t1115694098 *)__this);
			VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T) */, (List_1_t1115694098 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void List_1_Add_m1339738748_gshared (List_1_t1115694098 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1115694098 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeNamedArgument_t318735129  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t318735129 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1448862391_gshared (List_1_t1115694098 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1115694098 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1115694098 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t1115694098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t1115694098 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3229326773_gshared (List_1_t1115694098 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1115694098 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3019176036*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m2490298997_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m2490298997_gshared (List_1_t1115694098 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m2490298997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgument_t318735129  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CustomAttributeNamedArgument_t318735129  L_3 = InterfaceFuncInvoker0< CustomAttributeNamedArgument_t318735129  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CustomAttributeNamedArgument_t318735129 )L_3;
			CustomAttributeNamedArgument_t318735129  L_4 = V_0;
			NullCheck((List_1_t1115694098 *)__this);
			VirtActionInvoker1< CustomAttributeNamedArgument_t318735129  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T) */, (List_1_t1115694098 *)__this, (CustomAttributeNamedArgument_t318735129 )L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1506248450_gshared (List_1_t1115694098 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1115694098 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1115694098 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1115694098 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void List_1_Clear_m588919246_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m3793098560_gshared (List_1_t1115694098 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2854849388_gshared (List_1_t1115694098 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Enumerator_t3496444386  List_1_GetEnumerator_m712489085_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3496444386  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t3496444386 *, List_1_t1115694098 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t1115694098 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m33596728_gshared (List_1_t1115694098 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2460300739_gshared (List_1_t1115694098 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_7 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_15 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2601216572_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2601216572_gshared (List_1_t1115694098 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2601216572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3041627363_gshared (List_1_t1115694098 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1115694098 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_2 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1115694098 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t1115694098 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t318735129  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t318735129 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m2943309592_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m2943309592_gshared (List_1_t1115694098 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m2943309592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m1013425979_gshared (List_1_t1115694098 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t318735129  L_0 = ___item0;
		NullCheck((List_1_t1115694098 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, (List_1_t1115694098 *)__this, (CustomAttributeNamedArgument_t318735129 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1115694098 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32) */, (List_1_t1115694098 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m915480233_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m915480233_gshared (List_1_t1115694098 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m915480233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t1115694098 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_5 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4100988773_gshared (List_1_t1115694098 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* List_1_ToArray_m1013706236_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3019176036* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4200284520_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m2954018505_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m2954018505_gshared (List_1_t1115694098 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m2954018505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036** L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m858806083_gshared (List_1_t1115694098 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m3808740495_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  List_1_get_Item_m3808740495_gshared (List_1_t1115694098 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m3808740495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m4235731194_MetadataUsageId;
extern "C"  void List_1_set_Item_m4235731194_gshared (List_1_t1115694098 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4235731194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1115694098 *)__this);
		((  void (*) (List_1_t1115694098 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1115694098 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_4 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeNamedArgument_t318735129  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeNamedArgument_t318735129 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void List_1__ctor_m1026780308_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = ((List_1_t1357374531_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3629378411_gshared (List_1_t1357374531 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1357374531 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = ((List_1_t1357374531_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1357374531 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1357374531 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m1237246949_MetadataUsageId;
extern "C"  void List_1__ctor_m1237246949_gshared (List_1_t1357374531 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m1237246949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C"  void List_1__cctor_m1283322265_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t1357374531_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3773941406_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1357374531 *)__this);
		Enumerator_t3738124819  L_0 = ((  Enumerator_t3738124819  (*) (List_1_t1357374531 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1357374531 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t3738124819  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1212976944_gshared (List_1_t1357374531 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1995803071_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t1357374531 *)__this);
		Enumerator_t3738124819  L_0 = ((  Enumerator_t3738124819  (*) (List_1_t1357374531 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t1357374531 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t3738124819  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m414231134_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m414231134_gshared (List_1_t1357374531 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m414231134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1357374531 *)__this);
			VirtActionInvoker1< CustomAttributeTypedArgument_t560415562  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T) */, (List_1_t1357374531 *)__this, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m1543977506_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m1543977506_gshared (List_1_t1357374531 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m1543977506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1357374531 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, CustomAttributeTypedArgument_t560415562  >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, (List_1_t1357374531 *)__this, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m1354269110_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1354269110_gshared (List_1_t1357374531 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m1354269110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1357374531 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, (List_1_t1357374531 *)__this, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m3967399721_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m3967399721_gshared (List_1_t1357374531 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m3967399721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1357374531 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t1357374531 *)__this);
			VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(26 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T) */, (List_1_t1357374531 *)__this, (int32_t)L_1, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m796033887_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m796033887_gshared (List_1_t1357374531 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m796033887_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t1357374531 *)__this);
			VirtFuncInvoker1< bool, CustomAttributeTypedArgument_t560415562  >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T) */, (List_1_t1357374531 *)__this, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1976723363_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m218083500_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m190457651_gshared (List_1_t1357374531 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1357374531 *)__this);
		CustomAttributeTypedArgument_t560415562  L_1 = VirtFuncInvoker1< CustomAttributeTypedArgument_t560415562 , int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, (List_1_t1357374531 *)__this, (int32_t)L_0);
		CustomAttributeTypedArgument_t560415562  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3649092800_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3649092800_gshared (List_1_t1357374531 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3649092800_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t1357374531 *)__this);
			VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T) */, (List_1_t1357374531 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void List_1_Add_m1547284843_gshared (List_1_t1357374531 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1357374531 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_2 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		CustomAttributeTypedArgument_t560415562  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t560415562 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3071867942_gshared (List_1_t1357374531 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t1357374531 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t1357374531 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t1357374531 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t1357374531 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2401188644_gshared (List_1_t1357374531 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1357374531 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_5 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t3123668047*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1662160868_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1662160868_gshared (List_1_t1357374531 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1662160868_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgument_t560415562  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			CustomAttributeTypedArgument_t560415562  L_3 = InterfaceFuncInvoker0< CustomAttributeTypedArgument_t560415562  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (CustomAttributeTypedArgument_t560415562 )L_3;
			CustomAttributeTypedArgument_t560415562  L_4 = V_0;
			NullCheck((List_1_t1357374531 *)__this);
			VirtActionInvoker1< CustomAttributeTypedArgument_t560415562  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T) */, (List_1_t1357374531 *)__this, (CustomAttributeTypedArgument_t560415562 )L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1061486643_gshared (List_1_t1357374531 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t1357374531 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t1357374531 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t1357374531 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void List_1_Clear_m2727880895_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool List_1_Contains_m4075630001_gshared (List_1_t1357374531 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2968269979_gshared (List_1_t1357374531 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Enumerator_t3738124819  List_1_GetEnumerator_m873213550_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3738124819  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t3738124819 *, List_1_t1357374531 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t1357374531 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m1705278631_gshared (List_1_t1357374531 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m490684402_gshared (List_1_t1357374531 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_5 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_6 = ___start0;
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_7 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_15 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2714637163_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2714637163_gshared (List_1_t1357374531 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2714637163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m833926610_gshared (List_1_t1357374531 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1357374531 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_2 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t1357374531 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t1357374531 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_4 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t560415562  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t560415562 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m1671517383_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m1671517383_gshared (List_1_t1357374531 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m1671517383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool List_1_Remove_m3561203180_gshared (List_1_t1357374531 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t560415562  L_0 = ___item0;
		NullCheck((List_1_t1357374531 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, (List_1_t1357374531 *)__this, (CustomAttributeTypedArgument_t560415562 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t1357374531 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32) */, (List_1_t1357374531 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m3002746776_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m3002746776_gshared (List_1_t1357374531 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m3002746776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t1357374531 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_5 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3205009750_gshared (List_1_t1357374531 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* List_1_ToArray_m3561483437_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t3123668047* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2958543191_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m282056760_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m282056760_gshared (List_1_t1357374531 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m282056760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047** L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t List_1_get_Count_m1141337524_gshared (List_1_t1357374531 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m1601039742_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  List_1_get_Item_m1601039742_gshared (List_1_t1357374531 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m1601039742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m54184489_MetadataUsageId;
extern "C"  void List_1_set_Item_m54184489_gshared (List_1_t1357374531 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m54184489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t1357374531 *)__this);
		((  void (*) (List_1_t1357374531 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t1357374531 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_4 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get__items_1();
		int32_t L_5 = ___index0;
		CustomAttributeTypedArgument_t560415562  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (CustomAttributeTypedArgument_t560415562 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
extern "C"  void List_1__ctor_m3024762476_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = ((List_1_t4029173993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m733502843_gshared (List_1_t4029173993 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t4029173993 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PIXEL_FORMATU5BU5D_t4247347793* L_3 = ((List_1_t4029173993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t4029173993 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((PIXEL_FORMATU5BU5D_t4247347793*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t4029173993 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m3131339733_MetadataUsageId;
extern "C"  void List_1__ctor_m3131339733_gshared (List_1_t4029173993 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m3131339733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((PIXEL_FORMATU5BU5D_t4247347793*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
extern "C"  void List_1__cctor_m3312646057_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t4029173993_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((PIXEL_FORMATU5BU5D_t4247347793*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2867103886_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t4029173993 *)__this);
		Enumerator_t2114956985  L_0 = ((  Enumerator_t2114956985  (*) (List_1_t4029173993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t4029173993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t2114956985  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m800597312_gshared (List_1_t4029173993 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3640704207_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t4029173993 *)__this);
		Enumerator_t2114956985  L_0 = ((  Enumerator_t2114956985  (*) (List_1_t4029173993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t4029173993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t2114956985  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3003977806_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3003977806_gshared (List_1_t4029173993 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3003977806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4029173993 *)__this);
			VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T) */, (List_1_t4029173993 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m2773269298_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m2773269298_gshared (List_1_t4029173993 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m2773269298_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4029173993 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, int32_t >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T) */, (List_1_t4029173993 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m4186992550_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m4186992550_gshared (List_1_t4029173993 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m4186992550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4029173993 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T) */, (List_1_t4029173993 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m579639065_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m579639065_gshared (List_1_t4029173993 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m579639065_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t4029173993 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t4029173993 *)__this);
			VirtActionInvoker2< int32_t, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T) */, (List_1_t4029173993 *)__this, (int32_t)L_1, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m3733093743_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m3733093743_gshared (List_1_t4029173993 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m3733093743_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t4029173993 *)__this);
			VirtFuncInvoker1< bool, int32_t >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T) */, (List_1_t4029173993 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m959829171_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2249081116_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2064014243_gshared (List_1_t4029173993 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4029173993 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32) */, (List_1_t4029173993 *)__this, (int32_t)L_0);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m3596312752_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m3596312752_gshared (List_1_t4029173993 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m3596312752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t4029173993 *)__this);
			VirtActionInvoker2< int32_t, int32_t >::Invoke(29 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T) */, (List_1_t4029173993 *)__this, (int32_t)L_0, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
extern "C"  void List_1_Add_m1071184763_gshared (List_1_t4029173993 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		PIXEL_FORMATU5BU5D_t4247347793* L_1 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t4029173993 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_2 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2512297526_gshared (List_1_t4029173993 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		PIXEL_FORMATU5BU5D_t4247347793* L_3 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t4029173993 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t4029173993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t4029173993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t4029173993 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4231568692_gshared (List_1_t4029173993 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t4029173993 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		PIXEL_FORMATU5BU5D_t4247347793* L_5 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< PIXEL_FORMATU5BU5D_t4247347793*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (PIXEL_FORMATU5BU5D_t4247347793*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m3492540916_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m3492540916_gshared (List_1_t4029173993 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m3492540916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			int32_t L_3 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (int32_t)L_3;
			int32_t L_4 = V_0;
			NullCheck((List_1_t4029173993 *)__this);
			VirtActionInvoker1< int32_t >::Invoke(19 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T) */, (List_1_t4029173993 *)__this, (int32_t)L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3040515107_gshared (List_1_t4029173993 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t4029173993 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t4029173993 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t4029173993 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
extern "C"  void List_1_Clear_m853680303_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		PIXEL_FORMATU5BU5D_t4247347793* L_1 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
extern "C"  bool List_1_Contains_m2104427169_gshared (List_1_t4029173993 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t4247347793*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2779978411_gshared (List_1_t4029173993 * __this, PIXEL_FORMATU5BU5D_t4247347793* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		PIXEL_FORMATU5BU5D_t4247347793* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
extern "C"  Enumerator_t2114956985  List_1_GetEnumerator_m3833138014_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2114956985  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t2114956985 *, List_1_t4029173993 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t4029173993 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3810515127_gshared (List_1_t4029173993 * __this, int32_t ___item0, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793*, int32_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t4247347793*)L_0, (int32_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2886145538_gshared (List_1_t4029173993 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_5 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_6 = ___start0;
		PIXEL_FORMATU5BU5D_t4247347793* L_7 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_15 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2526345595_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2526345595_gshared (List_1_t4029173993 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2526345595_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m471719906_gshared (List_1_t4029173993 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t4029173993 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		PIXEL_FORMATU5BU5D_t4247347793* L_2 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t4029173993 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t4029173993 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		PIXEL_FORMATU5BU5D_t4247347793* L_4 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m4025119447_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m4025119447_gshared (List_1_t4029173993 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m4025119447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
extern "C"  bool List_1_Remove_m2316695260_gshared (List_1_t4029173993 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___item0;
		NullCheck((List_1_t4029173993 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T) */, (List_1_t4029173993 *)__this, (int32_t)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t4029173993 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32) */, (List_1_t4029173993 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m2640540072_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m2640540072_gshared (List_1_t4029173993 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m2640540072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t4029173993 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		PIXEL_FORMATU5BU5D_t4247347793* L_5 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4059552070_gshared (List_1_t4029173993 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t4247347793*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
extern "C"  PIXEL_FORMATU5BU5D_t4247347793* List_1_ToArray_m3260422429_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	PIXEL_FORMATU5BU5D_t4247347793* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (PIXEL_FORMATU5BU5D_t4247347793*)((PIXEL_FORMATU5BU5D_t4247347793*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		PIXEL_FORMATU5BU5D_t4247347793* L_1 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		PIXEL_FORMATU5BU5D_t4247347793* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		PIXEL_FORMATU5BU5D_t4247347793* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m161914215_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_0 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m4017453640_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m4017453640_gshared (List_1_t4029173993 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m4017453640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		PIXEL_FORMATU5BU5D_t4247347793** L_3 = (PIXEL_FORMATU5BU5D_t4247347793**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, PIXEL_FORMATU5BU5D_t4247347793**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (PIXEL_FORMATU5BU5D_t4247347793**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
extern "C"  int32_t List_1_get_Count_m1979159460_gshared (List_1_t4029173993 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2219422222_MetadataUsageId;
extern "C"  int32_t List_1_get_Item_m2219422222_gshared (List_1_t4029173993 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2219422222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_3 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m4160860217_MetadataUsageId;
extern "C"  void List_1_set_Item_m4160860217_gshared (List_1_t4029173993 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m4160860217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t4029173993 *)__this);
		((  void (*) (List_1_t4029173993 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t4029173993 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		PIXEL_FORMATU5BU5D_t4247347793* L_4 = (PIXEL_FORMATU5BU5D_t4247347793*)__this->get__items_1();
		int32_t L_5 = ___index0;
		int32_t L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)L_6);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C"  void List_1__ctor_m1414933584_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TargetSearchResultU5BU5D_t2556575283* L_0 = ((List_1_t407342383_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_0);
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3957273671_gshared (List_1_t407342383 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t407342383 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TargetSearchResultU5BU5D_t2556575283* L_3 = ((List_1_t407342383_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->get_EmptyArray_4();
		__this->set__items_1(L_3);
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t407342383 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0049;
	}

IL_0031:
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_5);
		__this->set__items_1(((TargetSearchResultU5BU5D_t2556575283*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_6)));
		Il2CppObject* L_7 = V_0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t407342383 *)__this, (Il2CppObject*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0049:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4227142842;
extern const uint32_t List_1__ctor_m2003846793_MetadataUsageId;
extern "C"  void List_1__ctor_m2003846793_gshared (List_1_t407342383 * __this, int32_t ___capacity0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1__ctor_m2003846793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity0;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_1 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, (String_t*)_stringLiteral4227142842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = ___capacity0;
		__this->set__items_1(((TargetSearchResultU5BU5D_t2556575283*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_2)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C"  void List_1__cctor_m2001718645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t407342383_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->set_EmptyArray_4(((TargetSearchResultU5BU5D_t2556575283*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)0)));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2347584394_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t407342383 *)__this);
		Enumerator_t2788092671  L_0 = ((  Enumerator_t2788092671  (*) (List_1_t407342383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t407342383 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t2788092671  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3137939212_gshared (List_1_t407342383 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1153382279_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t407342383 *)__this);
		Enumerator_t2788092671  L_0 = ((  Enumerator_t2788092671  (*) (List_1_t407342383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t407342383 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t2788092671  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Add_m3897426506_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_Add_m3897426506_gshared (List_1_t407342383 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Add_m3897426506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t407342383 *)__this);
			VirtActionInvoker1< TargetSearchResult_t3905350710  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T) */, (List_1_t407342383 *)__this, (TargetSearchResult_t3905350710 )((*(TargetSearchResult_t3905350710 *)((TargetSearchResult_t3905350710 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)__this->get__size_2();
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_0036;
		}

IL_001a:
		{
			; // IL_001a: leave IL_002b
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001f;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0025;
		throw e;
	}

CATCH_001f:
	{ // begin catch(System.NullReferenceException)
		goto IL_002b;
	} // end catch (depth: 1)

CATCH_0025:
	{ // begin catch(System.InvalidCastException)
		goto IL_002b;
	} // end catch (depth: 1)

IL_002b:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0036:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Contains_m263376962_MetadataUsageId;
extern "C"  bool List_1_System_Collections_IList_Contains_m263376962_gshared (List_1_t407342383 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Contains_m263376962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t407342383 *)__this);
			bool L_1 = VirtFuncInvoker1< bool, TargetSearchResult_t3905350710  >::Invoke(21 /* System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T) */, (List_1_t407342383 *)__this, (TargetSearchResult_t3905350710 )((*(TargetSearchResult_t3905350710 *)((TargetSearchResult_t3905350710 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (bool)0;
	}

IL_0025:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_IndexOf_m3771728802_MetadataUsageId;
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3771728802_gshared (List_1_t407342383 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_IndexOf_m3771728802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t407342383 *)__this);
			int32_t L_1 = VirtFuncInvoker1< int32_t, TargetSearchResult_t3905350710  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T) */, (List_1_t407342383 *)__this, (TargetSearchResult_t3905350710 )((*(TargetSearchResult_t3905350710 *)((TargetSearchResult_t3905350710 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0025;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return (-1);
	}

IL_0025:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t List_1_System_Collections_IList_Insert_m1010607565_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Insert_m1010607565_gshared (List_1_t407342383 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Insert_m1010607565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t407342383 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index0;
			Il2CppObject * L_2 = ___item1;
			NullCheck((List_1_t407342383 *)__this);
			VirtActionInvoker2< int32_t, TargetSearchResult_t3905350710  >::Invoke(26 /* System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T) */, (List_1_t407342383 *)__this, (int32_t)L_1, (TargetSearchResult_t3905350710 )((*(TargetSearchResult_t3905350710 *)((TargetSearchResult_t3905350710 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0035;
		}

IL_0019:
		{
			; // IL_0019: leave IL_002a
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001e;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0024;
		throw e;
	}

CATCH_001e:
	{ // begin catch(System.NullReferenceException)
		goto IL_002a;
	} // end catch (depth: 1)

CATCH_0024:
	{ // begin catch(System.InvalidCastException)
		goto IL_002a;
	} // end catch (depth: 1)

IL_002a:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern const uint32_t List_1_System_Collections_IList_Remove_m1172085307_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_Remove_m1172085307_gshared (List_1_t407342383 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_Remove_m1172085307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = ___item0;
			NullCheck((List_1_t407342383 *)__this);
			VirtFuncInvoker1< bool, TargetSearchResult_t3905350710  >::Invoke(23 /* System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T) */, (List_1_t407342383 *)__this, (TargetSearchResult_t3905350710 )((*(TargetSearchResult_t3905350710 *)((TargetSearchResult_t3905350710 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0023;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1829819843_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2957236998_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3996984077_gshared (List_1_t407342383 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t407342383 *)__this);
		TargetSearchResult_t3905350710  L_1 = VirtFuncInvoker1< TargetSearchResult_t3905350710 , int32_t >::Invoke(28 /* T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32) */, (List_1_t407342383 *)__this, (int32_t)L_0);
		TargetSearchResult_t3905350710  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NullReferenceException_t3216235232_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidCastException_t922874574_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral111972721;
extern const uint32_t List_1_System_Collections_IList_set_Item_m1145213540_MetadataUsageId;
extern "C"  void List_1_System_Collections_IList_set_Item_m1145213540_gshared (List_1_t407342383 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_System_Collections_IList_set_Item_m1145213540_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index0;
			Il2CppObject * L_1 = ___value1;
			NullCheck((List_1_t407342383 *)__this);
			VirtActionInvoker2< int32_t, TargetSearchResult_t3905350710  >::Invoke(29 /* System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T) */, (List_1_t407342383 *)__this, (int32_t)L_0, (TargetSearchResult_t3905350710 )((*(TargetSearchResult_t3905350710 *)((TargetSearchResult_t3905350710 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_002e;
		}

IL_0012:
		{
			; // IL_0012: leave IL_0023
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t3216235232_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0017;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t922874574_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001d;
		throw e;
	}

CATCH_0017:
	{ // begin catch(System.NullReferenceException)
		goto IL_0023;
	} // end catch (depth: 1)

CATCH_001d:
	{ // begin catch(System.InvalidCastException)
		goto IL_0023;
	} // end catch (depth: 1)

IL_0023:
	{
		ArgumentException_t124305799 * L_2 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral111972721, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C"  void List_1_Add_m3460879431_gshared (List_1_t407342383 * __this, TargetSearchResult_t3905350710  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		TargetSearchResultU5BU5D_t2556575283* L_1 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_001a;
		}
	}
	{
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t407342383 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001a:
	{
		TargetSearchResultU5BU5D_t2556575283* L_2 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_3 = (int32_t)__this->get__size_2();
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->set__size_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		TargetSearchResult_t3905350710  L_6 = ___item0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TargetSearchResult_t3905350710 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2947946754_gshared (List_1_t407342383 * __this, int32_t ___newCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		int32_t L_1 = ___newCount0;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		TargetSearchResultU5BU5D_t2556575283* L_3 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))))))
		{
			goto IL_0031;
		}
	}
	{
		NullCheck((List_1_t407342383 *)__this);
		int32_t L_4 = ((  int32_t (*) (List_1_t407342383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t407342383 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t407342383 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_0031:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2444355584_gshared (List_1_t407342383 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t407342383 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Il2CppObject* L_4 = ___collection0;
		TargetSearchResultU5BU5D_t2556575283* L_5 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		NullCheck((Il2CppObject*)L_4);
		InterfaceActionInvoker2< TargetSearchResultU5BU5D_t2556575283*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_4, (TargetSearchResultU5BU5D_t2556575283*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)__this->get__size_2();
		int32_t L_8 = V_0;
		__this->set__size_2(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t List_1_AddEnumerable_m1705327808_MetadataUsageId;
extern "C"  void List_1_AddEnumerable_m1705327808_gshared (List_1_t407342383 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_AddEnumerable_m1705327808_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TargetSearchResult_t3905350710  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Il2CppObject* V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___enumerable0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Il2CppObject*)L_0);
		V_1 = (Il2CppObject*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001a;
		}

IL_000c:
		{
			Il2CppObject* L_2 = V_1;
			NullCheck((Il2CppObject*)L_2);
			TargetSearchResult_t3905350710  L_3 = InterfaceFuncInvoker0< TargetSearchResult_t3905350710  >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Il2CppObject*)L_2);
			V_0 = (TargetSearchResult_t3905350710 )L_3;
			TargetSearchResult_t3905350710  L_4 = V_0;
			NullCheck((List_1_t407342383 *)__this);
			VirtActionInvoker1< TargetSearchResult_t3905350710  >::Invoke(19 /* System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T) */, (List_1_t407342383 *)__this, (TargetSearchResult_t3905350710 )L_4);
		}

IL_001a:
		{
			Il2CppObject* L_5 = V_1;
			NullCheck((Il2CppObject *)L_5);
			bool L_6 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			if (L_6)
			{
				goto IL_000c;
			}
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x35, FINALLY_002a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_7 = V_1;
			if (L_7)
			{
				goto IL_002e;
			}
		}

IL_002d:
		{
			IL2CPP_END_FINALLY(42)
		}

IL_002e:
		{
			Il2CppObject* L_8 = V_1;
			NullCheck((Il2CppObject *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			IL2CPP_END_FINALLY(42)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x35, IL_0035)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0035:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2633546199_gshared (List_1_t407342383 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	{
		Il2CppObject* L_0 = ___collection0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t407342383 *)__this, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Il2CppObject* L_1 = ___collection0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t407342383 *)__this, (Il2CppObject*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0027;
	}

IL_0020:
	{
		Il2CppObject* L_4 = ___collection0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t407342383 *)__this, (Il2CppObject*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0027:
	{
		int32_t L_5 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_5+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C"  void List_1_Clear_m1919770979_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		TargetSearchResultU5BU5D_t2556575283* L_1 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		NullCheck(L_1);
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set__size_2(0);
		int32_t L_2 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_2+(int32_t)1)));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C"  bool List_1_Contains_m2557723537_gshared (List_1_t407342383 * __this, TargetSearchResult_t3905350710  ___item0, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		TargetSearchResult_t3905350710  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283*, TargetSearchResult_t3905350710 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t2556575283*)L_0, (TargetSearchResult_t3905350710 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return (bool)((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m3164788855_gshared (List_1_t407342383 * __this, TargetSearchResultU5BU5D_t2556575283* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		TargetSearchResultU5BU5D_t2556575283* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, (int32_t)0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C"  Enumerator_t2788092671  List_1_GetEnumerator_m1131907150_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2788092671  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t2788092671 *, List_1_t407342383 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(&L_0, (List_1_t407342383 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2436220731_gshared (List_1_t407342383 * __this, TargetSearchResult_t3905350710  ___item0, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		TargetSearchResult_t3905350710  L_1 = ___item0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		int32_t L_3 = ((  int32_t (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283*, TargetSearchResult_t3905350710 , int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22)->method)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t2556575283*)L_0, (TargetSearchResult_t3905350710 )L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m275924942_gshared (List_1_t407342383 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta1;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_1 = ___start0;
		int32_t L_2 = ___delta1;
		___start0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000c:
	{
		int32_t L_3 = ___start0;
		int32_t L_4 = (int32_t)__this->get__size_2();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0035;
		}
	}
	{
		TargetSearchResultU5BU5D_t2556575283* L_5 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_6 = ___start0;
		TargetSearchResultU5BU5D_t2556575283* L_7 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_8 = ___start0;
		int32_t L_9 = ___delta1;
		int32_t L_10 = (int32_t)__this->get__size_2();
		int32_t L_11 = ___start0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (Il2CppArray *)(Il2CppArray *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_12 = (int32_t)__this->get__size_2();
		int32_t L_13 = ___delta1;
		__this->set__size_2(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = ___delta1;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		TargetSearchResultU5BU5D_t2556575283* L_15 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_16 = (int32_t)__this->get__size_2();
		int32_t L_17 = ___delta1;
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckIndex(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_CheckIndex_m2911156039_MetadataUsageId;
extern "C"  void List_1_CheckIndex_m2911156039_gshared (List_1_t407342383 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckIndex_m2911156039_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2268766382_gshared (List_1_t407342383 * __this, int32_t ___index0, TargetSearchResult_t3905350710  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t407342383 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)__this->get__size_2();
		TargetSearchResultU5BU5D_t2556575283* L_2 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0021;
		}
	}
	{
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t407342383 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0021:
	{
		int32_t L_3 = ___index0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t407342383 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		TargetSearchResultU5BU5D_t2556575283* L_4 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_5 = ___index0;
		TargetSearchResult_t3905350710  L_6 = ___item1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TargetSearchResult_t3905350710 )L_6);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2553654942;
extern const uint32_t List_1_CheckCollection_m205273763_MetadataUsageId;
extern "C"  void List_1_CheckCollection_m205273763_gshared (List_1_t407342383 * __this, Il2CppObject* ___collection0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_CheckCollection_m205273763_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___collection0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral2553654942, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C"  bool List_1_Remove_m2889233356_gshared (List_1_t407342383 * __this, TargetSearchResult_t3905350710  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		TargetSearchResult_t3905350710  L_0 = ___item0;
		NullCheck((List_1_t407342383 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, TargetSearchResult_t3905350710  >::Invoke(25 /* System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T) */, (List_1_t407342383 *)__this, (TargetSearchResult_t3905350710 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t407342383 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(27 /* System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32) */, (List_1_t407342383 *)__this, (int32_t)L_3);
	}

IL_0016:
	{
		int32_t L_4 = V_0;
		return (bool)((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_RemoveAt_m142619252_MetadataUsageId;
extern "C"  void List_1_RemoveAt_m142619252_gshared (List_1_t407342383 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_RemoveAt_m142619252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}

IL_0013:
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		int32_t L_4 = ___index0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)((List_1_t407342383 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		TargetSearchResultU5BU5D_t2556575283* L_5 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_6 = (int32_t)__this->get__size_2();
		Array_Clear_m2499577033(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_7+(int32_t)1)));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1977701882_gshared (List_1_t407342383 * __this, Il2CppObject* ___comparer0, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_1 = (int32_t)__this->get__size_2();
		Il2CppObject* L_2 = ___comparer0;
		((  void (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283*, int32_t, int32_t, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t2556575283*)L_0, (int32_t)0, (int32_t)L_1, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		int32_t L_3 = (int32_t)__this->get__version_3();
		__this->set__version_3(((int32_t)((int32_t)L_3+(int32_t)1)));
		return;
	}
}
// T[] System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::ToArray()
extern "C"  TargetSearchResultU5BU5D_t2556575283* List_1_ToArray_m302001847_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	TargetSearchResultU5BU5D_t2556575283* V_0 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		V_0 = (TargetSearchResultU5BU5D_t2556575283*)((TargetSearchResultU5BU5D_t2556575283*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (uint32_t)L_0));
		TargetSearchResultU5BU5D_t2556575283* L_1 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		TargetSearchResultU5BU5D_t2556575283* L_2 = V_0;
		int32_t L_3 = (int32_t)__this->get__size_2();
		Array_Copy_m3799309042(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)(Il2CppArray *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		TargetSearchResultU5BU5D_t2556575283* L_4 = V_0;
		return L_4;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3327764971_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		TargetSearchResultU5BU5D_t2556575283* L_0 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Capacity(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern const uint32_t List_1_set_Capacity_m158135572_MetadataUsageId;
extern "C"  void List_1_set_Capacity_m158135572_gshared (List_1_t407342383 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Capacity_m158135572_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m410800215(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0012:
	{
		TargetSearchResultU5BU5D_t2556575283** L_3 = (TargetSearchResultU5BU5D_t2556575283**)__this->get_address_of__items_1();
		int32_t L_4 = ___value0;
		((  void (*) (Il2CppObject * /* static, unused */, TargetSearchResultU5BU5D_t2556575283**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27)->method)(NULL /*static, unused*/, (TargetSearchResultU5BU5D_t2556575283**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 27));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C"  int32_t List_1_get_Count_m2325706144_gshared (List_1_t407342383 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
// T System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_get_Item_m2001746744_MetadataUsageId;
extern "C"  TargetSearchResult_t3905350710  List_1_get_Item_m2001746744_gshared (List_1_t407342383 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_get_Item_m2001746744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		TargetSearchResultU5BU5D_t2556575283* L_3 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t List_1_set_Item_m250703365_MetadataUsageId;
extern "C"  void List_1_set_Item_m250703365_gshared (List_1_t407342383 * __this, int32_t ___index0, TargetSearchResult_t3905350710  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (List_1_set_Item_m250703365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		NullCheck((List_1_t407342383 *)__this);
		((  void (*) (List_1_t407342383 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t407342383 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001e;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_3 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001e:
	{
		TargetSearchResultU5BU5D_t2556575283* L_4 = (TargetSearchResultU5BU5D_t2556575283*)__this->get__items_1();
		int32_t L_5 = ___index0;
		TargetSearchResult_t3905350710  L_6 = ___value1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (TargetSearchResult_t3905350710 )L_6);
		return;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C"  void Enumerator__ctor_m1003414509_gshared (Enumerator_t4014815679 * __this, Stack_1_t3407512455 * ___t0, const MethodInfo* method)
{
	{
		Stack_1_t3407512455 * L_0 = ___t0;
		__this->set_parent_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		Stack_1_t3407512455 * L_1 = ___t0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		__this->set__version_2(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m3054975473_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3054975473_gshared (Enumerator_t4014815679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m3054975473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get__version_2();
		Stack_1_t3407512455 * L_1 = (Stack_1_t3407512455 *)__this->get_parent_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2470832103_gshared (Enumerator_t4014815679 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Enumerator_t4014815679 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t4014815679 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1634653158_gshared (Enumerator_t4014815679 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_MoveNext_m3012756789_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m3012756789_gshared (Enumerator_t4014815679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m3012756789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get__version_2();
		Stack_1_t3407512455 * L_1 = (Stack_1_t3407512455 *)__this->get_parent_0();
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_2();
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_003a;
		}
	}
	{
		Stack_1_t3407512455 * L_5 = (Stack_1_t3407512455 *)__this->get_parent_0();
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get__size_1();
		__this->set_idx_1(L_6);
	}

IL_003a:
	{
		int32_t L_7 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_idx_1();
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8-(int32_t)1));
		V_0 = (int32_t)L_9;
		__this->set_idx_1(L_9);
		int32_t L_10 = V_0;
		G_B7_0 = ((((int32_t)((((int32_t)L_10) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return (bool)G_B7_0;
	}
}
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m2483819640_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m2483819640_gshared (Enumerator_t4014815679 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m2483819640_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Stack_1_t3407512455 * L_2 = (Stack_1_t3407512455 *)__this->get_parent_0();
		NullCheck(L_2);
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)L_2->get__array_0();
		int32_t L_4 = (int32_t)__this->get_idx_1();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m2725689112_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArrayTypeMismatchException_t3670186211_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const uint32_t Stack_1_System_Collections_ICollection_CopyTo_m3277353260_MetadataUsageId;
extern "C"  void Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared (Stack_1_t3407512455 * __this, Il2CppArray * ___dest0, int32_t ___idx1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stack_1_System_Collections_ICollection_CopyTo_m3277353260_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
			if (!L_0)
			{
				goto IL_0025;
			}
		}

IL_000b:
		{
			ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
			Il2CppArray * L_2 = ___dest0;
			int32_t L_3 = ___idx1;
			NullCheck((Il2CppArray *)(Il2CppArray *)L_1);
			VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(8 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_1, (Il2CppArray *)L_2, (int32_t)L_3);
			Il2CppArray * L_4 = ___dest0;
			int32_t L_5 = ___idx1;
			int32_t L_6 = (int32_t)__this->get__size_1();
			Array_Reverse_m3064094494(NULL /*static, unused*/, (Il2CppArray *)L_4, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		}

IL_0025:
		{
			goto IL_0036;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArrayTypeMismatchException_t3670186211_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.ArrayTypeMismatchException)
		{
			ArgumentException_t124305799 * L_7 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
			ArgumentException__ctor_m571182463(L_7, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
		}

IL_0031:
		{
			goto IL_0036;
		}
	} // end catch (depth: 1)

IL_0036:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stack_1_t3407512455 *)__this);
		Enumerator_t4014815679  L_0 = ((  Enumerator_t4014815679  (*) (Stack_1_t3407512455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Stack_1_t3407512455 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Enumerator_t4014815679  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	{
		NullCheck((Stack_1_t3407512455 *)__this);
		Enumerator_t4014815679  L_0 = ((  Enumerator_t4014815679  (*) (Stack_1_t3407512455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Stack_1_t3407512455 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Enumerator_t4014815679  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::Contains(T)
extern "C"  bool Stack_1_Contains_m328948937_gshared (Stack_1_t3407512455 * __this, Il2CppObject * ___t0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		Il2CppObject * L_2 = ___t0;
		int32_t L_3 = (int32_t)__this->get__size_1();
		int32_t L_4 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_1, (Il2CppObject *)L_2, (int32_t)0, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B3_0 = ((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0027;
	}

IL_0026:
	{
		G_B3_0 = 0;
	}

IL_0027:
	{
		return (bool)G_B3_0;
	}
}
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t Stack_1_Pop_m4267009222_MetadataUsageId;
extern "C"  Il2CppObject * Stack_1_Pop_m4267009222_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Stack_1_Pop_m4267009222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = (int32_t)__this->get__version_2();
		__this->set__version_2(((int32_t)((int32_t)L_2+(int32_t)1)));
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		int32_t L_4 = (int32_t)__this->get__size_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_1 = (int32_t)L_5;
		__this->set__size_1(L_5);
		int32_t L_6 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		int32_t L_7 = L_6;
		V_0 = (Il2CppObject *)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
		ObjectU5BU5D_t11523773* L_8 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		int32_t L_9 = (int32_t)__this->get__size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_10 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (Il2CppObject *)L_10);
		Il2CppObject * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C"  void Stack_1_Push_m3350166104_gshared (Stack_1_t3407512455 * __this, Il2CppObject * ___t0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ObjectU5BU5D_t11523773** G_B4_0 = NULL;
	ObjectU5BU5D_t11523773** G_B3_0 = NULL;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t11523773** G_B5_1 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get__size_1();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0043;
		}
	}

IL_001e:
	{
		ObjectU5BU5D_t11523773** L_3 = (ObjectU5BU5D_t11523773**)__this->get_address_of__array_0();
		int32_t L_4 = (int32_t)__this->get__size_1();
		G_B3_0 = L_3;
		if (L_4)
		{
			G_B4_0 = L_3;
			goto IL_0036;
		}
	}
	{
		G_B5_0 = ((int32_t)16);
		G_B5_1 = G_B3_0;
		goto IL_003e;
	}

IL_0036:
	{
		int32_t L_5 = (int32_t)__this->get__size_1();
		G_B5_0 = ((int32_t)((int32_t)2*(int32_t)L_5));
		G_B5_1 = G_B4_0;
	}

IL_003e:
	{
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773**)G_B5_1, (int32_t)G_B5_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0043:
	{
		int32_t L_6 = (int32_t)__this->get__version_2();
		__this->set__version_2(((int32_t)((int32_t)L_6+(int32_t)1)));
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get__array_0();
		int32_t L_8 = (int32_t)__this->get__size_1();
		int32_t L_9 = (int32_t)L_8;
		V_0 = (int32_t)L_9;
		__this->set__size_1(((int32_t)((int32_t)L_9+(int32_t)1)));
		int32_t L_10 = V_0;
		Il2CppObject * L_11 = ___t0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)L_11);
		return;
	}
}
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m3631765324_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		return L_0;
	}
}
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t4014815679  Stack_1_GetEnumerator_m202302354_gshared (Stack_1_t3407512455 * __this, const MethodInfo* method)
{
	{
		Enumerator_t4014815679  L_0;
		memset(&L_0, 0, sizeof(L_0));
		((  void (*) (Enumerator_t4014815679 *, Stack_1_t3407512455 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(&L_0, (Stack_1_t3407512455 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_0;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m1690372513_MetadataUsageId;
extern "C"  void Collection_1__ctor_m1690372513_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m1690372513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1634065389 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t1634065389 * L_0 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1634065389 *)L_0;
		List_1_t1634065389 * L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t1634065389 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m1285013187_MetadataUsageId;
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared (Collection_1_t2806094150 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m1285013187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1708617267_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___value0;
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t2806094150 *)__this, (int32_t)L_2, (Il2CppObject *)L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m504494585_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2187188150_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0, (Il2CppObject *)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2625864114_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_1 = ___value0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(25 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2224513142_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2262756877_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0, (Il2CppObject *)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C"  void Collection_1_Add_m321765054_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___item0;
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t2806094150 *)__this, (int32_t)L_2, (Il2CppObject *)L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C"  void Collection_1_Clear_m3391473100_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems() */, (Collection_1_t2806094150 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2738199222_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C"  bool Collection_1_Contains_m1050871674_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1746187054_gshared (Collection_1_t2806094150 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		ObjectU5BU5D_t11523773* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (ObjectU5BU5D_t11523773*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m625631581_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3101447730_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1208073509_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___item1;
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m714854616_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject *)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C"  bool Collection_1_Remove_m2181520885_gshared (Collection_1_t2806094150 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		NullCheck((Collection_1_t2806094150 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, Il2CppObject * >::Invoke(25 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T) */, (Collection_1_t2806094150 *)__this, (Il2CppObject *)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3376893675_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1099170891_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1472906633_gshared (Collection_1_t2806094150 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_get_Item_m2356360623_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3127068860_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		NullCheck((Collection_1_t2806094150 *)__this);
		VirtActionInvoker2< int32_t, Il2CppObject * >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T) */, (Collection_1_t2806094150 *)__this, (int32_t)L_0, (Il2CppObject *)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m112162877_gshared (Collection_1_t2806094150 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		Il2CppObject * L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (Il2CppObject *)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m1993492338_MetadataUsageId;
extern "C"  bool Collection_1_IsValidItem_m1993492338_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m1993492338_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_2);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t Collection_1_ConvertItem_m1655469326_MetadataUsageId;
extern "C"  Il2CppObject * Collection_1_ConvertItem_m1655469326_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m1655469326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = ___item0;
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)));
	}

IL_0012:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m651250670_MetadataUsageId;
extern "C"  void Collection_1_CheckWritable_m651250670_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m651250670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1374155497 * L_2 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m1235362998_MetadataUsageId;
extern "C"  void Collection_1__ctor_m1235362998_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m1235362998_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1115694098 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t1115694098 * L_0 = (List_1_t1115694098 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1115694098 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1115694098 *)L_0;
		List_1_t1115694098 * L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t1115694098 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m749697729_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m3417958734_MetadataUsageId;
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3417958734_gshared (Collection_1_t2287722859 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m3417958734_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m934847197_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2420983424_gshared (Collection_1_t2287722859 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___value0;
		CustomAttributeNamedArgument_t318735129  L_4 = ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2287722859 *)__this, (int32_t)L_2, (CustomAttributeNamedArgument_t318735129 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m3744332480_gshared (Collection_1_t2287722859 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t318735129  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m847658200_gshared (Collection_1_t2287722859 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m793248331_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeNamedArgument_t318735129  L_2 = ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2287722859 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t318735129 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m979290365_gshared (Collection_1_t2287722859 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_1 = ___value0;
		CustomAttributeNamedArgument_t318735129  L_2 = ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2287722859 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(25 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, (Collection_1_t2287722859 *)__this, (CustomAttributeNamedArgument_t318735129 )L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2287722859 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m81883406_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2446153493_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t318735129  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t318735129 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeNamedArgument_t318735129  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2716387170_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeNamedArgument_t318735129  L_2 = ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2287722859 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t318735129 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void Collection_1_Add_m1692560649_gshared (Collection_1_t2287722859 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		CustomAttributeNamedArgument_t318735129  L_3 = ___item0;
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2287722859 *)__this, (int32_t)L_2, (CustomAttributeNamedArgument_t318735129 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void Collection_1_Clear_m2936463585_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems() */, (Collection_1_t2287722859 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3651347649_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Clear() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool Collection_1_Contains_m2554792531_gshared (Collection_1_t2287722859 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t318735129  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1143243961_gshared (Collection_1_t2287722859 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3019176036*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3784377642_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2242432069_gshared (Collection_1_t2287722859 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3361633648_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgument_t318735129  L_1 = ___item1;
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2287722859 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t318735129 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m1107127203_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeNamedArgument_t318735129  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeNamedArgument_t318735129 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool Collection_1_Remove_m3394257678_gshared (Collection_1_t2287722859 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeNamedArgument_t318735129  L_0 = ___item0;
		NullCheck((Collection_1_t2287722859 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(25 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, (Collection_1_t2287722859 *)__this, (CustomAttributeNamedArgument_t318735129 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2287722859 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1235486518_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2287722859 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m496227798_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m613224918_gshared (Collection_1_t2287722859 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t318735129  Collection_1_get_Item_m447264732_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t318735129  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t318735129 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2524125767_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgument_t318735129  L_1 = ___value1;
		NullCheck((Collection_1_t2287722859 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2287722859 *)__this, (int32_t)L_0, (CustomAttributeNamedArgument_t318735129 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2448017746_gshared (Collection_1_t2287722859 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeNamedArgument_t318735129  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeNamedArgument_t318735129 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m2031032185_MetadataUsageId;
extern "C"  bool Collection_1_IsValidItem_m2031032185_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m2031032185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_2);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t Collection_1_ConvertItem_m4174059707_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  Collection_1_ConvertItem_m4174059707_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m4174059707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = ___item0;
		return ((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m240037625_MetadataUsageId;
extern "C"  void Collection_1_CheckWritable_m240037625_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m240037625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1374155497 * L_2 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1__ctor_m3374324647_MetadataUsageId;
extern "C"  void Collection_1__ctor_m3374324647_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1__ctor_m3374324647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t1357374531 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		List_1_t1357374531 * L_0 = (List_1_t1357374531 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (List_1_t1357374531 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t1357374531 *)L_0;
		List_1_t1357374531 * L_1 = V_0;
		V_1 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_1;
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(1 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_syncRoot_1(L_3);
		List_1_t1357374531 * L_4 = V_0;
		__this->set_list_0(L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4216526896_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_System_Collections_ICollection_CopyTo_m4131878781_MetadataUsageId;
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m4131878781_gshared (Collection_1_t2529403292 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_System_Collections_ICollection_CopyTo_m4131878781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1044491980_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2609273073_gshared (Collection_1_t2529403292 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Il2CppObject * L_3 = ___value0;
		CustomAttributeTypedArgument_t560415562  L_4 = ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2529403292 *)__this, (int32_t)L_2, (CustomAttributeTypedArgument_t560415562 )L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1738786543_gshared (Collection_1_t2529403292 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t560415562  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m4246646473_gshared (Collection_1_t2529403292 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m890770108_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeTypedArgument_t560415562  L_2 = ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2529403292 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t560415562 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1088935148_gshared (Collection_1_t2529403292 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(NULL /*static, unused*/, (Il2CppObject*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Il2CppObject * L_1 = ___value0;
		CustomAttributeTypedArgument_t560415562  L_2 = ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2529403292 *)__this);
		int32_t L_3 = VirtFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(25 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, (Collection_1_t2529403292 *)__this, (CustomAttributeTypedArgument_t560415562 )L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2529403292 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m639609663_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_1();
		return L_0;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1550174470_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t560415562  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t560415562 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeTypedArgument_t560415562  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1945534355_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		Il2CppObject * L_1 = ___value1;
		CustomAttributeTypedArgument_t560415562  L_2 = ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2529403292 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t560415562 )L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void Collection_1_Add_m1900106744_gshared (Collection_1_t2529403292 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		CustomAttributeTypedArgument_t560415562  L_3 = ___item0;
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2529403292 *)__this, (int32_t)L_2, (CustomAttributeTypedArgument_t560415562 )L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void Collection_1_Clear_m780457938_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker0::Invoke(30 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems() */, (Collection_1_t2529403292 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3819887728_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Clear() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool Collection_1_Contains_m2837323972_gshared (Collection_1_t2529403292 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t560415562  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m1256664552_gshared (Collection_1_t2529403292 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t3123668047*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3945102107_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3914113972_gshared (Collection_1_t2529403292 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1153932895_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgument_t560415562  L_1 = ___item1;
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(31 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T) */, (Collection_1_t2529403292 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t560415562 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2730132754_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeTypedArgument_t560415562  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeTypedArgument_t560415562 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool Collection_1_Remove_m1647067583_gshared (Collection_1_t2529403292 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		CustomAttributeTypedArgument_t560415562  L_0 = ___item0;
		NullCheck((Collection_1_t2529403292 *)__this);
		int32_t L_1 = VirtFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(25 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, (Collection_1_t2529403292 *)__this, (CustomAttributeTypedArgument_t560415562 )L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)0;
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2529403292 *)__this, (int32_t)L_3);
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3322753061_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(32 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32) */, (Collection_1_t2529403292 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m609648389_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m895756359_gshared (Collection_1_t2529403292 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t560415562  Collection_1_get_Item_m2534531275_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t560415562  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t560415562 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2637546358_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgument_t560415562  L_1 = ___value1;
		NullCheck((Collection_1_t2529403292 *)__this);
		VirtActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T) */, (Collection_1_t2529403292 *)__this, (int32_t)L_0, (CustomAttributeTypedArgument_t560415562 )L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2728771139_gshared (Collection_1_t2529403292 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		CustomAttributeTypedArgument_t560415562  L_2 = ___item1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9), (Il2CppObject*)L_0, (int32_t)L_1, (CustomAttributeTypedArgument_t560415562 )L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_IsValidItem_m3654037736_MetadataUsageId;
extern "C"  bool Collection_1_IsValidItem_m3654037736_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_IsValidItem_m3654037736_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___item0;
		if (((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))
		{
			goto IL_0028;
		}
	}
	{
		Il2CppObject * L_1 = ___item0;
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_2);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0026;
	}

IL_0025:
	{
		G_B4_0 = 0;
	}

IL_0026:
	{
		G_B6_0 = G_B4_0;
		goto IL_0029;
	}

IL_0028:
	{
		G_B6_0 = 1;
	}

IL_0029:
	{
		return (bool)G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3242771;
extern const uint32_t Collection_1_ConvertItem_m1502097962_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  Collection_1_ConvertItem_m1502097962_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_ConvertItem_m1502097962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___item0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_2 = ___item0;
		return ((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)))));
	}

IL_0012:
	{
		ArgumentException_t124305799 * L_3 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_3, (String_t*)_stringLiteral3242771, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t Collection_1_CheckWritable_m2442447784_MetadataUsageId;
extern "C"  void Collection_1_CheckWritable_m2442447784_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Collection_1_CheckWritable_m2442447784_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = ___list0;
		NullCheck((Il2CppObject*)L_0);
		bool L_1 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Il2CppObject*)L_0);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1374155497 * L_2 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0011:
	{
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ReadOnlyCollection_1__ctor_m1366664402_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1__ctor_m1366664402_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m1366664402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Clear()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.Remove(T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_MetadataUsageId;
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t4000251768 *)__this);
		Il2CppObject * L_1 = VirtFuncInvoker1< Il2CppObject *, int32_t >::Invoke(30 /* T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Item(System.Int32) */, (ReadOnlyCollection_1_t4000251768 *)__this, (int32_t)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_MetadataUsageId;
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Clear()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_2, (Il2CppObject *)((Il2CppObject *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m687553276_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, Il2CppObject * >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m475587820_gshared (ReadOnlyCollection_1_t4000251768 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		ObjectU5BU5D_t11523773* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (ObjectU5BU5D_t11523773*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m809369055_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m817393776_gshared (ReadOnlyCollection_1_t4000251768 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (Il2CppObject *)L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m3681678091_gshared (ReadOnlyCollection_1_t4000251768 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_get_Item_m2421641197_gshared (ReadOnlyCollection_1_t4000251768 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject * L_2 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ReadOnlyCollection_1__ctor_m3466118433_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1__ctor_m3466118433_gshared (ReadOnlyCollection_1_t3481880477 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m3466118433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2455993995_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2455993995_gshared (ReadOnlyCollection_1_t3481880477 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2455993995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m833093535_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m833093535_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m833093535_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1011322802_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1011322802_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1011322802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1374717388_MetadataUsageId;
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1374717388_gshared (ReadOnlyCollection_1_t3481880477 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1374717388_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3180142968_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3180142968_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3180142968_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t318735129  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2735326366_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t3481880477 *)__this);
		CustomAttributeNamedArgument_t318735129  L_1 = VirtFuncInvoker1< CustomAttributeNamedArgument_t318735129 , int32_t >::Invoke(30 /* T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, (ReadOnlyCollection_1_t3481880477 *)__this, (int32_t)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3028200457_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3028200457_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3028200457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3502725699_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m58572112_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m58572112_gshared (ReadOnlyCollection_1_t3481880477 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m58572112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3974072671_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3974072671_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3974072671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2157369662_MetadataUsageId;
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2157369662_gshared (ReadOnlyCollection_1_t3481880477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m2157369662_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Clear()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m1270090846_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1270090846_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m1270090846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4167890114_gshared (ReadOnlyCollection_1_t3481880477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t318735129  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3928768662_gshared (ReadOnlyCollection_1_t3481880477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_2, (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m3096196361_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3096196361_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m3096196361_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m1439234431_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1439234431_gshared (ReadOnlyCollection_1_t3481880477 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m1439234431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3251950105_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3251950105_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3251950105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4281934668_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m1426158035_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t318735129  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t318735129 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeNamedArgument_t318735129  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941286560_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941286560_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m3941286560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3072511761_gshared (ReadOnlyCollection_1_t3481880477 * __this, CustomAttributeNamedArgument_t318735129  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeNamedArgument_t318735129  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2591949499_gshared (ReadOnlyCollection_1_t3481880477 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeNamedArgumentU5BU5D_t3019176036*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2795667688_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2162782663_gshared (ReadOnlyCollection_1_t3481880477 * __this, CustomAttributeNamedArgument_t318735129  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeNamedArgument_t318735129  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2439060628_gshared (ReadOnlyCollection_1_t3481880477 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeNamedArgument_t318735129  ReadOnlyCollection_1_get_Item_m719412574_gshared (ReadOnlyCollection_1_t3481880477 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeNamedArgument_t318735129  L_2 = InterfaceFuncInvoker1< CustomAttributeNamedArgument_t318735129 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3322014;
extern const uint32_t ReadOnlyCollection_1__ctor_m713162960_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1__ctor_m713162960_gshared (ReadOnlyCollection_1_t3723560910 * __this, Il2CppObject* ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1__ctor_m713162960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject* L_0 = ___list0;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3322014, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0017:
	{
		Il2CppObject* L_2 = ___list0;
		__this->set_list_0(L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m450448058_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m450448058_gshared (ReadOnlyCollection_1_t3723560910 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m450448058_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3085678928_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3085678928_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3085678928_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m183184673_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m183184673_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m183184673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1445762877_MetadataUsageId;
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1445762877_gshared (ReadOnlyCollection_1_t3723560910 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m1445762877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2352004839_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2352004839_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2352004839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t560415562  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1907188237_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((ReadOnlyCollection_1_t3723560910 *)__this);
		CustomAttributeTypedArgument_t560415562  L_1 = VirtFuncInvoker1< CustomAttributeTypedArgument_t560415562 , int32_t >::Invoke(30 /* T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, (ReadOnlyCollection_1_t3723560910 *)__this, (int32_t)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1756408248_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1756408248_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1756408248_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2674587570_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ICollection_t3761522009_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m772492159_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m772492159_gshared (ReadOnlyCollection_1_t3723560910 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m772492159_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		Il2CppArray * L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Il2CppArray *, int32_t >::Invoke(2 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t3761522009_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_0, ICollection_t3761522009_il2cpp_TypeInfo_var)), (Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4083717454_MetadataUsageId;
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4083717454_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m4083717454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject *)L_0);
		Il2CppObject * L_1 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, (Il2CppObject *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2345659311_MetadataUsageId;
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2345659311_gshared (ReadOnlyCollection_1_t3723560910 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Add_m2345659311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Clear()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Clear_m3595441805_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m3595441805_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Clear_m3595441805_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2162344177_gshared (ReadOnlyCollection_1_t3723560910 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		bool L_4 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t560415562  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (bool)0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3032789639_gshared (ReadOnlyCollection_1_t3723560910 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		bool L_1 = ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_list_0();
		Il2CppObject * L_3 = ___value0;
		NullCheck((Il2CppObject*)L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_2, (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))));
		return L_4;
	}

IL_001d:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Insert_m3193718138_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3193718138_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Insert_m3193718138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_Remove_m1548879214_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1548879214_gshared (ReadOnlyCollection_1_t3723560910 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_Remove_m1548879214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2355971082_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2355971082_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m2355971082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m544693629_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m530179012_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t560415562  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t560415562 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		CustomAttributeTypedArgument_t560415562  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t ReadOnlyCollection_1_System_Collections_IList_set_Item_m3170433745_MetadataUsageId;
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3170433745_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyCollection_1_System_Collections_IList_set_Item_m3170433745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3355043202_gshared (ReadOnlyCollection_1_t3723560910 * __this, CustomAttributeTypedArgument_t560415562  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		bool L_2 = InterfaceFuncInvoker1< bool, CustomAttributeTypedArgument_t560415562  >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2705370090_gshared (ReadOnlyCollection_1_t3723560910 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppObject*)L_0);
		InterfaceActionInvoker2< CustomAttributeTypedArgumentU5BU5D_t3123668047*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2956392153_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		Il2CppObject* L_1 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), (Il2CppObject*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3834464566_gshared (ReadOnlyCollection_1_t3723560910 * __this, CustomAttributeTypedArgument_t560415562  ___value0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___value0;
		NullCheck((Il2CppObject*)L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, CustomAttributeTypedArgument_t560415562  >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2721592069_gshared (ReadOnlyCollection_1_t3723560910 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		NullCheck((Il2CppObject*)L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Il2CppObject*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C"  CustomAttributeTypedArgument_t560415562  ReadOnlyCollection_1_get_Item_m2806679117_gshared (ReadOnlyCollection_1_t3723560910 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = (Il2CppObject*)__this->get_list_0();
		int32_t L_1 = ___index0;
		NullCheck((Il2CppObject*)L_0);
		CustomAttributeTypedArgument_t560415562  L_2 = InterfaceFuncInvoker1< CustomAttributeTypedArgument_t560415562 , int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Il2CppObject*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m487232819_gshared (Comparison_1_t3540781296 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 System.Comparison`1<System.Object>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1888033133_gshared (Comparison_1_t3540781296 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Comparison_1_Invoke_m1888033133((Comparison_1_t3540781296 *)__this->get_prev_9(),___x0, ___y1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___y1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___x0, ___y1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Comparison`1<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m3177996774_gshared (Comparison_1_t3540781296 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___x0;
	__d_args[1] = ___y1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 System.Comparison`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m651541983_gshared (Comparison_1_t3540781296 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Converter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Converter_2__ctor_m15321797_gshared (Converter_2_t113996300 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C"  Il2CppObject * Converter_2_Invoke_m606895179_gshared (Converter_2_t113996300 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Converter_2_Invoke_m606895179((Converter_2_t113996300 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Converter`2<System.Object,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Converter_2_BeginInvoke_m3132354088_gshared (Converter_2_t113996300 * __this, Il2CppObject * ___input0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TOutput System.Converter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Converter_2_EndInvoke_m3873471959_gshared (Converter_2_t113996300 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m3915480592_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m2518344089_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m1460175968_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_4();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m416466113_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m1980624040_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_3();
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * L_2 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		((  void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 *)L_2;
		U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * L_3 = V_0;
		Il2CppObject * L_4 = (Il2CppObject *)__this->get_U3CU24U3Esource_5();
		NullCheck(L_3);
		L_3->set_source_0(L_4);
		U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
extern Il2CppClass* IEnumerable_t287189635_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1129579948_MetadataUsageId;
extern "C"  bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1129579948_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m1129579948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	Il2CppObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
	}
	{
		goto IL_00af;
	}

IL_0023:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_source_0();
		NullCheck((Il2CppObject *)L_2);
		Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t287189635_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		__this->set_U3CU24s_31U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			if (((int32_t)((int32_t)L_4-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0043:
		{
			goto IL_0078;
		}

IL_0048:
		{
			Il2CppObject * L_5 = (Il2CppObject *)__this->get_U3CU24s_31U3E__0_1();
			NullCheck((Il2CppObject *)L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_5);
			__this->set_U3CelementU3E__1_2(((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))));
			Il2CppObject * L_7 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_4(L_7);
			__this->set_U24PC_3(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xB1, FINALLY_008d);
		}

IL_0078:
		{
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CU24s_31U3E__0_1();
			NullCheck((Il2CppObject *)L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_8);
			if (L_9)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA8, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_10 = V_1;
			if (!L_10)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Il2CppObject * L_11 = (Il2CppObject *)__this->get_U3CU24s_31U3E__0_1();
			V_2 = (Il2CppObject *)((Il2CppObject *)IsInst(L_11, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_12 = V_2;
			if (L_12)
			{
				goto IL_00a1;
			}
		}

IL_00a0:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_00a1:
		{
			Il2CppObject * L_13 = V_2;
			NullCheck((Il2CppObject *)L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_13);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xB1, IL_00b1)
		IL2CPP_JUMP_TBL(0xA8, IL_00a8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a8:
	{
		__this->set_U24PC_3((-1));
	}

IL_00af:
	{
		return (bool)0;
	}

IL_00b1:
	{
		return (bool)1;
	}
	// Dead block : IL_00b3: ldloc.3
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m283780685_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m283780685_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m283780685_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_3();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_003d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_003d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_2 = (Il2CppObject *)__this->get_U3CU24s_31U3E__0_1();
			V_1 = (Il2CppObject *)((Il2CppObject *)IsInst(L_2, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_3 = V_1;
			if (L_3)
			{
				goto IL_0036;
			}
		}

IL_0035:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_0036:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck((Il2CppObject *)L_4);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_4);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3D, IL_003d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003d:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m1561913533_MetadataUsageId;
extern "C"  void U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m1561913533_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3156486586 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateCastIteratorU3Ec__Iterator0_1_Reset_m1561913533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m4008503583_gshared (Nullable_1_t3649900800 * __this, TimeSpan_t763862892  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t763862892  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2797118855_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral78850559;
extern const uint32_t Nullable_1_get_Value_m3338249190_MetadataUsageId;
extern "C"  TimeSpan_t763862892  Nullable_1_get_Value_m3338249190_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3338249190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral78850559, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t763862892  L_2 = (TimeSpan_t763862892 )__this->get_value_0();
		return L_2;
	}
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2158814990_gshared (Nullable_1_t3649900800 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t3649900800 ));
		UnBoxNullable(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), L_4);
		bool L_5 = ((  bool (*) (Nullable_1_t3649900800 *, Nullable_1_t3649900800 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Nullable_1_t3649900800 *)__this, (Nullable_1_t3649900800 )((*(Nullable_1_t3649900800 *)((Nullable_1_t3649900800 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3609411697_gshared (Nullable_1_t3649900800 * __this, Nullable_1_t3649900800  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t763862892 * L_3 = (TimeSpan_t763862892 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t763862892  L_4 = (TimeSpan_t763862892 )__this->get_value_0();
		TimeSpan_t763862892  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m2969422609((TimeSpan_t763862892 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m2957066482_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t763862892 * L_1 = (TimeSpan_t763862892 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m3188156777((TimeSpan_t763862892 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m3059865940_MetadataUsageId;
extern "C"  String_t* Nullable_1_ToString_m3059865940_gshared (Nullable_1_t3649900800 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m3059865940_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t763862892 * L_1 = (TimeSpan_t763862892 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2803989647((TimeSpan_t763862892 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m982040097_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4106178309_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4106178309((Predicate_1_t1408070318 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2038073176_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3970497007_gshared (Predicate_1_t1408070318 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m207645679_gshared (Predicate_1_t3061133318 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m1510075547_gshared (Predicate_1_t3061133318 * __this, TrackableResultData_t2490169420  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m1510075547((Predicate_1_t3061133318 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, TrackableResultData_t2490169420  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, TrackableResultData_t2490169420  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* TrackableResultData_t2490169420_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2760591914_MetadataUsageId;
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2760591914_gshared (Predicate_1_t3061133318 * __this, TrackableResultData_t2490169420  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2760591914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(TrackableResultData_t2490169420_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2591413785_gshared (Predicate_1_t3061133318 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m4236926794_gshared (Getter_2_t539570894 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m410564889_gshared (Getter_2_t539570894 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m410564889((Getter_2_t539570894 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m3146221447_gshared (Getter_2_t539570894 * __this, Il2CppObject * ____this0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m1749574747_gshared (Getter_2_t539570894 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void StaticGetter_1__ctor_m3357261135_gshared (StaticGetter_1_t171826611 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * StaticGetter_1_Invoke_m3410367530_gshared (StaticGetter_1_t171826611 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StaticGetter_1_Invoke_m3410367530((StaticGetter_1_t171826611 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StaticGetter_1_BeginInvoke_m3837643130_gshared (StaticGetter_1_t171826611 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * StaticGetter_1_EndInvoke_m3212189152_gshared (StaticGetter_1_t171826611 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_1__ctor_m4139691420_MetadataUsageId;
extern "C"  void UnityEvent_1__ctor_m4139691420_gshared (UnityEvent_1_t4074528527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_1__ctor_m4139691420_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)1)));
		NullCheck((UnityEventBase_t2174897510 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t2174897510 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_2__ctor_m1950601551_MetadataUsageId;
extern "C"  void UnityEvent_2__ctor_m1950601551_gshared (UnityEvent_2_t3775219180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_2__ctor_m1950601551_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)2)));
		NullCheck((UnityEventBase_t2174897510 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t2174897510 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_3__ctor_m4248091138_MetadataUsageId;
extern "C"  void UnityEvent_3__ctor_m4248091138_gshared (UnityEvent_3_t1749754057 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_3__ctor_m4248091138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)3)));
		NullCheck((UnityEventBase_t2174897510 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t2174897510 *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern const uint32_t UnityEvent_4__ctor_m492943285_MetadataUsageId;
extern "C"  void UnityEvent_4__ctor_m492943285_gshared (UnityEvent_4_t1065917714 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_4__ctor_m492943285_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4)));
		NullCheck((UnityEventBase_t2174897510 *)__this);
		UnityEventBase__ctor_m199506446((UnityEventBase_t2174897510 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
