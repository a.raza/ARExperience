﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Village
struct Village_t2125020300;

#include "codegen/il2cpp-codegen.h"

// System.Void Village::.ctor()
extern "C"  void Village__ctor_m1884115039 (Village_t2125020300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Village::OnGUI()
extern "C"  void Village_OnGUI_m1379513689 (Village_t2125020300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
