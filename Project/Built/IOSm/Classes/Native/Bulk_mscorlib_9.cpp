﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.WindowsConsoleDriver
struct WindowsConsoleDriver_t2866849361;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_WindowsConsoleDriver2866849361.h"
#include "mscorlib_System_WindowsConsoleDriver2866849361MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_ConsoleScreenBufferInfo1169518982.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Handles839097024.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_ConsoleKeyInfo93518347.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal3977632096MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_ConsoleKeyInfo93518347MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_InputRecord4095575088.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_ConsoleKey136131901.h"
#include "mscorlib_System_ConsoleScreenBufferInfo1169518982MethodDeclarations.h"
#include "mscorlib_System_InputRecord4095575088MethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.WindowsConsoleDriver::.ctor()
extern Il2CppClass* ConsoleScreenBufferInfo_t1169518982_il2cpp_TypeInfo_var;
extern const uint32_t WindowsConsoleDriver__ctor_m3567726553_MetadataUsageId;
extern "C"  void WindowsConsoleDriver__ctor_m3567726553 (WindowsConsoleDriver_t2866849361 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WindowsConsoleDriver__ctor_m3567726553_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ConsoleScreenBufferInfo_t1169518982  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = WindowsConsoleDriver_GetStdHandle_m2710701471(NULL /*static, unused*/, ((int32_t)-11), /*hidden argument*/NULL);
		__this->set_outputHandle_1(L_0);
		IntPtr_t L_1 = WindowsConsoleDriver_GetStdHandle_m2710701471(NULL /*static, unused*/, ((int32_t)-10), /*hidden argument*/NULL);
		__this->set_inputHandle_0(L_1);
		Initobj (ConsoleScreenBufferInfo_t1169518982_il2cpp_TypeInfo_var, (&V_0));
		IntPtr_t L_2 = __this->get_outputHandle_1();
		WindowsConsoleDriver_GetConsoleScreenBufferInfo_m4057528648(NULL /*static, unused*/, L_2, (&V_0), /*hidden argument*/NULL);
		int16_t L_3 = (&V_0)->get_Attribute_2();
		__this->set_defaultAttribute_2(L_3);
		return;
	}
}
// System.ConsoleKeyInfo System.WindowsConsoleDriver::ReadKey(System.Boolean)
extern Il2CppClass* InputRecord_t4095575088_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t3977632096_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1114402996;
extern const uint32_t WindowsConsoleDriver_ReadKey_m1393188309_MetadataUsageId;
extern "C"  ConsoleKeyInfo_t93518347  WindowsConsoleDriver_ReadKey_m1393188309 (WindowsConsoleDriver_t2866849361 * __this, bool ___intercept0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WindowsConsoleDriver_ReadKey_m1393188309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	InputRecord_t4095575088  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	{
		Initobj (InputRecord_t4095575088_il2cpp_TypeInfo_var, (&V_1));
	}

IL_0008:
	{
		IntPtr_t L_0 = __this->get_inputHandle_0();
		bool L_1 = WindowsConsoleDriver_ReadConsoleInput_m9651400(NULL /*static, unused*/, L_0, (&V_1), 1, (&V_0), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t3977632096_il2cpp_TypeInfo_var);
		int32_t L_2 = Marshal_GetLastWin32Error_m701978199(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral1114402996, L_4, /*hidden argument*/NULL);
		InvalidOperationException_t2420574324 * L_6 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_6, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0037:
	{
		int16_t L_7 = (&V_1)->get_EventType_0();
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0050;
		}
	}
	{
		bool L_8 = (&V_1)->get_KeyDown_1();
		if (!L_8)
		{
			goto IL_0008;
		}
	}

IL_0050:
	{
		int32_t L_9 = (&V_1)->get_ControlKeyState_6();
		V_2 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_9&(int32_t)3))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_10 = (&V_1)->get_ControlKeyState_6();
		V_3 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)12)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_11 = (&V_1)->get_ControlKeyState_6();
		V_4 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		uint16_t L_12 = (&V_1)->get_Character_5();
		int16_t L_13 = (&V_1)->get_VirtualKeyCode_3();
		bool L_14 = V_4;
		bool L_15 = V_2;
		bool L_16 = V_3;
		ConsoleKeyInfo_t93518347  L_17;
		memset(&L_17, 0, sizeof(L_17));
		ConsoleKeyInfo__ctor_m3416959802(&L_17, L_12, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		return L_17;
	}
}
// System.IntPtr System.WindowsConsoleDriver::GetStdHandle(System.Handles)
extern "C"  IntPtr_t WindowsConsoleDriver_GetStdHandle_m2710701471 (Il2CppObject * __this /* static, unused */, int32_t ___handle0, const MethodInfo* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(int32_t);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("kernel32.dll", "GetStdHandle", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetStdHandle'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	intptr_t _return_value = _il2cpp_pinvoke_func(___handle0);
	IntPtr_t __return_value_unmarshaled;
	__return_value_unmarshaled.set_m_value_0(reinterpret_cast<void*>((intptr_t)_return_value));

	il2cpp_codegen_marshal_store_last_error();
	// Marshaling cleanup of parameter '___handle0' native representation

	return __return_value_unmarshaled;
}
// System.Boolean System.WindowsConsoleDriver::GetConsoleScreenBufferInfo(System.IntPtr,System.ConsoleScreenBufferInfo&)
extern "C"  bool WindowsConsoleDriver_GetConsoleScreenBufferInfo_m4057528648 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, ConsoleScreenBufferInfo_t1169518982 * ___info1, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, ConsoleScreenBufferInfo_t1169518982_marshaled_pinvoke*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(ConsoleScreenBufferInfo_t1169518982_marshaled_pinvoke*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("kernel32.dll", "GetConsoleScreenBufferInfo", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetConsoleScreenBufferInfo'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Marshaling of parameter '___info1' to native representation
	ConsoleScreenBufferInfo_t1169518982_marshaled_pinvoke ____info1_empty = { };
	ConsoleScreenBufferInfo_t1169518982_marshaled_pinvoke* ____info1_marshaled = &____info1_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___handle0.get_m_value_0()), ____info1_marshaled);

	il2cpp_codegen_marshal_store_last_error();
	// Marshaling cleanup of parameter '___handle0' native representation

	// Marshaling of parameter '___info1' back from native representation
	ConsoleScreenBufferInfo_t1169518982  ____info1_result_dereferenced;
	memset(&____info1_result_dereferenced, 0, sizeof(____info1_result_dereferenced));
	ConsoleScreenBufferInfo_t1169518982 * ____info1_result = &____info1_result_dereferenced;
	ConsoleScreenBufferInfo_t1169518982_marshal_pinvoke_back(*____info1_marshaled, *____info1_result);
	*___info1 = *____info1_result;

	// Marshaling cleanup of parameter '___info1' native representation
	ConsoleScreenBufferInfo_t1169518982_marshal_pinvoke_cleanup(*____info1_marshaled);

	return _return_value;
}
// System.Boolean System.WindowsConsoleDriver::ReadConsoleInput(System.IntPtr,System.InputRecord&,System.Int32,System.Int32&)
extern "C"  bool WindowsConsoleDriver_ReadConsoleInput_m9651400 (Il2CppObject * __this /* static, unused */, IntPtr_t ___handle0, InputRecord_t4095575088 * ___record1, int32_t ___length2, int32_t* ___nread3, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, InputRecord_t4095575088_marshaled_pinvoke*, int32_t, int32_t*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = sizeof(IntPtr_t) + sizeof(InputRecord_t4095575088_marshaled_pinvoke*) + sizeof(int32_t) + sizeof(int32_t*);
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("kernel32.dll", "ReadConsoleInput", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ReadConsoleInput'"));
		}
	}

	// Marshaling of parameter '___handle0' to native representation

	// Marshaling of parameter '___record1' to native representation
	InputRecord_t4095575088_marshaled_pinvoke ____record1_empty = { };
	InputRecord_t4095575088_marshaled_pinvoke* ____record1_marshaled = &____record1_empty;

	// Marshaling of parameter '___length2' to native representation

	// Marshaling of parameter '___nread3' to native representation
	int32_t ____nread3_empty = 0;
	int32_t* ____nread3_marshaled = &____nread3_empty;

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(reinterpret_cast<intptr_t>(___handle0.get_m_value_0()), ____record1_marshaled, ___length2, ____nread3_marshaled);

	il2cpp_codegen_marshal_store_last_error();
	// Marshaling cleanup of parameter '___handle0' native representation

	// Marshaling of parameter '___record1' back from native representation
	InputRecord_t4095575088  ____record1_result_dereferenced;
	memset(&____record1_result_dereferenced, 0, sizeof(____record1_result_dereferenced));
	InputRecord_t4095575088 * ____record1_result = &____record1_result_dereferenced;
	InputRecord_t4095575088_marshal_pinvoke_back(*____record1_marshaled, *____record1_result);
	*___record1 = *____record1_result;

	// Marshaling cleanup of parameter '___record1' native representation
	InputRecord_t4095575088_marshal_pinvoke_cleanup(*____record1_marshaled);

	// Marshaling cleanup of parameter '___length2' native representation

	// Marshaling of parameter '___nread3' back from native representation
	int32_t ____nread3_result_dereferenced = 0;
	int32_t* ____nread3_result = &____nread3_result_dereferenced;
	*____nread3_result = *____nread3_marshaled;
	*___nread3 = *____nread3_result;

	// Marshaling cleanup of parameter '___nread3' native representation

	return _return_value;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
