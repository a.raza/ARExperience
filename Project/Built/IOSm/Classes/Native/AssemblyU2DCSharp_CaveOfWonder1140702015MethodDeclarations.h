﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CaveOfWonder
struct CaveOfWonder_t1140702015;

#include "codegen/il2cpp-codegen.h"

// System.Void CaveOfWonder::.ctor()
extern "C"  void CaveOfWonder__ctor_m874475068 (CaveOfWonder_t1140702015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CaveOfWonder::OnGUI()
extern "C"  void CaveOfWonder_OnGUI_m369873718 (CaveOfWonder_t1140702015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
