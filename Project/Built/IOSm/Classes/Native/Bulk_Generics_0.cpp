﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t1669175889;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t1870691353;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3510862208;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2992490917;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t3234171350;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t398457203;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Exception
struct Exception_t1967233988;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4175053208;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3019176036;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1801841577;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t121766345;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3123668047;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2043522010;
// System.Array
struct Il2CppArray;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t2185368519;
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
struct DefaultComparer_t175060152;
// System.Collections.Generic.Comparer`1<System.Int32>
struct Comparer_1_t3302075123;
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t1291766756;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t3338225570;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1327917203;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Reflection.Emit.Label>
struct Dictionary_2_t2225720352;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t2980980203;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen1669175889.h"
#include "mscorlib_System_Action_1_gen1669175889MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainInitia1520723184.h"
#include "mscorlib_System_Action_1_gen1870691353.h"
#include "mscorlib_System_Action_1_gen1870691353MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE1722238648.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3510862208.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3510862208MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen398457203.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2992490917.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2992490917MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4175053208.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3234171350.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3234171350MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen121766345.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen398457203MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4175053208MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen121766345MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen383943926.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen383943926MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexe476453423.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2632522680.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2632522680MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2725032177.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3174019288.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3174019288MethodDeclarations.h"
#include "mscorlib_Mono_Security_Uri_UriScheme3266528785.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen22959084.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen22959084MethodDeclarations.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName115468581.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2709235369.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2709235369MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen118495844.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen118495844MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686184324.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686184324MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686197202.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686197202MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen37517749.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen37517749MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1652646218.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1652646218MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L1745155715.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2734247371.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2734247371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22826756868.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen723939004.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen723939004MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1621742153.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1621742153MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21714251650.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2377002004.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2377002004MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22469511501.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1009843983.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1009843983MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21102353480.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2594345872.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2594345872MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22686855369.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen935788022.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen935788022MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297519.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3220446951.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3220446951MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3369265799.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3369265799MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23461775296.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4228414662.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4228414662MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25956863.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3161303675.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3161303675MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23253813172.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2404181862.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2404181862MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2496691359.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4205037797.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4205037797MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2579998.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot2579998.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246524439.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246524439MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1596047757.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1596047757MethodDeclarations.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen442007117.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen442007117MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905232.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905232MethodDeclarations.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905290.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905290MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905385.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905385MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen584182523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen584182523MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen744596923.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen744596923MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen226225632.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen226225632MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen467906065.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen467906065MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1303237477.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1303237477MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD1395746974.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen228063683.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen228063683MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi320573180.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3630765784.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3630765784MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo3723275281.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1642400072.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1642400072MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_Label1734909569.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1843502757.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1843502757MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_MonoResource1936012254.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3697325377.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3697325377MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS3789834874.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen407693973.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen407693973MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterModifier500203470.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3607348206.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3607348206MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC3699857703.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3982075075.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3982075075MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4074584572.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1645779784.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1645779784MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1738289281.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2762836567.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2762836567MethodDeclarations.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1029642187.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1029642187MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen865699524.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen865699524MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen858999844.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen858999844MethodDeclarations.h"
#include "mscorlib_System_TermInfoStrings951509341.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3633423279.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3633423279MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark3725932776.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen671353395.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen671353395MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415771.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415771MethodDeclarations.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415829.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415829MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415924.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415924MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1MethodDeclarations.h"
#include "System_System_Uri_UriScheme3266528785.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2248895222.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2248895222MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl2341404719.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3785365046.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3785365046MethodDeclarations.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope3877874543.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1495666263.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1495666263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4044574710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4044574710MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2858612868.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2858612868MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2002543010.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2002543010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2498719112.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2498719112MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo2591228609.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224502599.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224502599MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1317012096.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2131168810.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2131168810MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2223678307.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432820291.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432820291MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432820292.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432820292MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1594566981.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1594566981MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice1687076478.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2968712505.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2968712505MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyew3061222002.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3139705527.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3139705527MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3232215024.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen697579894.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen697579894MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData790089391.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3812841213.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3812841213MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3905350710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3272961172.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3272961172MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3365470669.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen602728536.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen602728536MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_695238033.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen171984030.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen171984030MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_264493527.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2397659923.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2397659923MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2490169420.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1030501902.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1030501902MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1123011399.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen25787.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen25787MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_W92535284.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen315747064.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen315747064MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_408256561.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1752564634.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1752564634MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1845074131.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866MethodDeclarations.h"
#include "mscorlib_System_ArraySegment_1_gen860157465.h"
#include "mscorlib_System_ArraySegment_1_gen860157465MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu1376683523.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu1376683523MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu3353018536.h"
#include "mscorlib_System_Collections_Generic_CollectionDebu3353018536MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2185368518.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Def2185368518MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3302075123MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa175060151.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_Defa175060151MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1291766756MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen3302075123.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_Activator690001546MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "mscorlib_System_Collections_Generic_Comparer_1_gen1291766756.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3105253511.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3105253511MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3338225570.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22826756868MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException973246880.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1094945144.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1094945144MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1327917203.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1992748293.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1992748293MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2225720352.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21714251650MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2748008144.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2748008144MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2980980203.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22469511501MethodDeclarations.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"

// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m3345236030_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036* p0, CustomAttributeNamedArgument_t318735129  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m3345236030(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m3345236030_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m858079087_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047* p0, CustomAttributeTypedArgument_t560415562  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m858079087(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m858079087_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t476453423  Array_InternalArray__get_Item_TisTableRange_t476453423_m3354422249_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t476453423_m3354422249(__this, p0, method) ((  TableRange_t476453423  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t476453423_m3354422249_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t2725032177_m115939321_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t2725032177_m115939321(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t2725032177_m115939321_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t3266528785  Array_InternalArray__get_Item_TisUriScheme_t3266528785_m357804435_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t3266528785_m357804435(__this, p0, method) ((  UriScheme_t3266528785  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t3266528785_m357804435_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t115468581  Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273(__this, p0, method) ((  TagName_t115468581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2801744866  Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617(__this, p0, method) ((  ArraySegment_1_t2801744866  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t2778706699_m125306601(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t130027246  Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038(__this, p0, method) ((  DictionaryEntry_t130027246  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t1745155715  Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033(__this, p0, method) ((  Link_t1745155715  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t2826756868  Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353(__this, p0, method) ((  KeyValuePair_2_t2826756868  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t816448501  Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488(__this, p0, method) ((  KeyValuePair_2_t816448501  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern "C"  KeyValuePair_2_t1714251650  Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399(__this, p0, method) ((  KeyValuePair_2_t1714251650  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32)
extern "C"  KeyValuePair_2_t2469511501  Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870(__this, p0, method) ((  KeyValuePair_2_t2469511501  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
extern "C"  KeyValuePair_2_t1102353480  Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577(__this, p0, method) ((  KeyValuePair_2_t1102353480  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t2686855369  Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102(__this, p0, method) ((  KeyValuePair_2_t2686855369  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t1028297519  Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332(__this, p0, method) ((  KeyValuePair_2_t1028297519  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3312956448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653(__this, p0, method) ((  KeyValuePair_2_t3312956448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
extern "C"  KeyValuePair_2_t3461775296  Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301(__this, p0, method) ((  KeyValuePair_2_t3461775296  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
extern "C"  KeyValuePair_2_t25956863  Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917(__this, p0, method) ((  KeyValuePair_2_t25956863  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3253813172  Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513(__this, p0, method) ((  KeyValuePair_2_t3253813172  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2496691359  Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2496691359_m818406549(__this, p0, method) ((  Link_t2496691359  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2579998  Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401(__this, p0, method) ((  Slot_t2579998  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2579999  Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440(__this, p0, method) ((  Slot_t2579999  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t339033936  Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548(__this, p0, method) ((  DateTime_t339033936  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t1688557254  Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984(__this, p0, method) ((  Decimal_t1688557254  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990(__this, p0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053(__this, p0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300(__this, p0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m1819425504(__this, p0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m1537058848(__this, p0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t318735129  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667(__this, p0, method) ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t560415562  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746(__this, p0, method) ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t1395746974  Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434(__this, p0, method) ((  LabelData_t1395746974  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t320573180  Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500(__this, p0, method) ((  LabelFixup_t320573180  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t3723275281  Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117(__this, p0, method) ((  ILTokenInfo_t3723275281  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern "C"  Label_t1734909569  Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877(__this, p0, method) ((  Label_t1734909569  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern "C"  MonoResource_t1936012254  Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598(__this, p0, method) ((  MonoResource_t1936012254  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern "C"  RefEmitPermissionSet_t3789834874  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890(__this, p0, method) ((  RefEmitPermissionSet_t3789834874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t500203470  Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410(__this, p0, method) ((  ParameterModifier_t500203470  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t3699857703  Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445(__this, p0, method) ((  ResourceCacheItem_t3699857703  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t4074584572  Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776(__this, p0, method) ((  ResourceInfo_t4074584572  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230(__this, p0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218(__this, p0, method) ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3725932776_m160824484(__this, p0, method) ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760(__this, p0, method) ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t3266528786  Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123(__this, p0, method) ((  UriScheme_t3266528786  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t2341404719  Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851(__this, p0, method) ((  NsDecl_t2341404719  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t3877874543  Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815(__this, p0, method) ((  NsScope_t3877874543  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t1588175760  Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850(__this, p0, method) ((  Color_t1588175760  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403(__this, p0, method) ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859(__this, p0, method) ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013(__this, p0, method) ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997(__this, p0, method) ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226(__this, p0, method) ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207(__this, p0, method) ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166(__this, p0, method) ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989(__this, p0, method) ((  Vector3_t3525329789  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern "C"  WebCamDevice_t1687076478  Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186(__this, p0, method) ((  WebCamDevice_t1687076478  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32)
extern "C"  EyewearCalibrationReading_t3061222002  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777(__this, p0, method) ((  EyewearCalibrationReading_t3061222002  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern "C"  RectangleData_t790089391  Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777(__this, p0, method) ((  RectangleData_t790089391  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern "C"  TargetSearchResult_t3905350710  Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772(__this, p0, method) ((  TargetSearchResult_t3905350710  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern "C"  PropData_t3365470669  Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881(__this, p0, method) ((  PropData_t3365470669  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern "C"  SmartTerrainRevisionData_t695238033  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413(__this, p0, method) ((  SmartTerrainRevisionData_t695238033  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern "C"  SurfaceData_t264493527  Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851(__this, p0, method) ((  SurfaceData_t264493527  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern "C"  TrackableResultData_t2490169420  Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910(__this, p0, method) ((  TrackableResultData_t2490169420  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern "C"  VirtualButtonData_t1123011399  Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419(__this, p0, method) ((  VirtualButtonData_t1123011399  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern "C"  WordData_t92535284  Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074(__this, p0, method) ((  WordData_t92535284  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern "C"  WordResultData_t408256561  Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869(__this, p0, method) ((  WordResultData_t408256561  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern "C"  ProfileData_t1845074131  Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072(__this, p0, method) ((  ProfileData_t1845074131  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1794196709_gshared (Action_1_t359458046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3594021162_gshared (Action_1_t359458046 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3594021162((Action_1_t359458046 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m647183148_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m647183148_gshared (Action_1_t359458046 * __this, bool ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m647183148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1601629789_gshared (Action_1_t359458046 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m881151526_gshared (Action_1_t985559125 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m663971678_gshared (Action_1_t985559125 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m663971678((Action_1_t985559125 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m917692971_gshared (Action_1_t985559125 * __this, Il2CppObject * ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3562128182_gshared (Action_1_t985559125 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1043807536_gshared (Action_1_t1669175889 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m1477274131_gshared (Action_1_t1669175889 * __this, SmartTerrainInitializationInfo_t1520723184  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1477274131((Action_1_t1669175889 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SmartTerrainInitializationInfo_t1520723184  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, SmartTerrainInitializationInfo_t1520723184  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Vuforia.SmartTerrainInitializationInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* SmartTerrainInitializationInfo_t1520723184_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m2872323433_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m2872323433_gshared (Action_1_t1669175889 * __this, SmartTerrainInitializationInfo_t1520723184  ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m2872323433_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SmartTerrainInitializationInfo_t1520723184_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m546883392_gshared (Action_1_t1669175889 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m626199895_gshared (Action_1_t1870691353 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::Invoke(T)
extern "C"  void Action_1_Invoke_m279777809_gshared (Action_1_t1870691353 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m279777809((Action_1_t1870691353 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Vuforia.VuforiaUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitError_t1722238648_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m3881238443_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m3881238443_gshared (Action_1_t1870691353 * __this, int32_t ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m3881238443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitError_t1722238648_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2374626366_gshared (Action_1_t1870691353 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m4262077373_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2204526468_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4054268681_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4201941769_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t398457203 * L_2 = (ArrayReadOnlyList_1_t398457203 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_2(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t398457203 * L_8 = (ArrayReadOnlyList_1_t398457203 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)L_8->get_array_0();
		NullCheck(L_9);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2650805434_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2799549978_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t318735129  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2027929923_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = (CustomAttributeNamedArgument_t318735129 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1186685398_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = (CustomAttributeNamedArgument_t318735129 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t318735129  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4084754896_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4175053208 * L_2 = (ArrayReadOnlyList_1_t4175053208 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_2(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4175053208 * L_8 = (ArrayReadOnlyList_1_t4175053208 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_9 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_8->get_array_0();
		NullCheck(L_9);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1616284631_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m643544331_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t560415562  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4108562996_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = (CustomAttributeTypedArgument_t560415562 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2728697221_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = (CustomAttributeTypedArgument_t560415562 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t560415562  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1461469503_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t121766345 * L_2 = (ArrayReadOnlyList_1_t121766345 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_2(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t121766345 * L_8 = (ArrayReadOnlyList_1_t121766345 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_9 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_8->get_array_0();
		NullCheck(L_9);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m4164061832_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m240689135_gshared (ArrayReadOnlyList_1_t398457203 * __this, ObjectU5BU5D_t11523773* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2499499206_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t398457203 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(17 /* System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator() */, (ArrayReadOnlyList_1_t398457203 *)__this);
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m535939909_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m2625374704_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m3813449101_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m1486920810_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m976758002_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2221418008_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m158553866_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1244492898_gshared (ArrayReadOnlyList_1_t398457203 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		ObjectU5BU5D_t11523773* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(8 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1770413409_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1098739118_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m738278233_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m12996997_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2907098399_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId;
extern "C"  Exception_t1967233988 * ArrayReadOnlyList_1_ReadOnlyError_m740547916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3527945938_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m932360725_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4175053208 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(17 /* System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, (ArrayReadOnlyList_1_t4175053208 *)__this);
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  ArrayReadOnlyList_1_get_Item_m3165498630_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3031060371_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___value1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2401585746_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2438902353_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3652230485_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m3556686357_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2530929859_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1650178565_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(8 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2319686054_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m3408499785_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2906295740_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m609878398_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m780148610_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId;
extern "C"  Exception_t1967233988 * ArrayReadOnlyList_1_ReadOnlyError_m467076531_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m904660545_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1042005508_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t121766345 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(17 /* System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, (ArrayReadOnlyList_1_t121766345 *)__this);
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  ArrayReadOnlyList_1_get_Item_m957797877_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3144480962_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___value1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2684117187_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m3126393472_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3859776580_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1400680710_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2813461300_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1763599156_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(8 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2480410519_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m785214392_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m698594987_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m3157655599_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2867415153_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId;
extern "C"  Exception_t1967233988 * ArrayReadOnlyList_1_ReadOnlyError_m627800996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2510835690_gshared (InternalEnumerator_1_t383943926 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	{
		TableRange_t476453423  L_0 = ((  TableRange_t476453423  (*) (InternalEnumerator_1_t383943926 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t383943926 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t476453423  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId;
extern "C"  TableRange_t476453423  InternalEnumerator_1_get_Current_m2306301105_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t476453423  L_8 = ((  TableRange_t476453423  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4155127258_gshared (InternalEnumerator_1_t2632522680 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t2632522680 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2632522680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m833446416_gshared (InternalEnumerator_1_t3174019288 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2813262224_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m444897414_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	{
		UriScheme_t3266528785  L_0 = ((  UriScheme_t3266528785  (*) (InternalEnumerator_1_t3174019288 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3174019288 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t3266528785  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m980107239_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3795700544_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1507354425_MetadataUsageId;
extern "C"  UriScheme_t3266528785  InternalEnumerator_1_get_Current_m1507354425_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1507354425_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t3266528785  L_8 = ((  UriScheme_t3266528785  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3894762810_gshared (InternalEnumerator_1_t22959084 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2241088038_gshared (InternalEnumerator_1_t22959084 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2786336284_gshared (InternalEnumerator_1_t22959084 * __this, const MethodInfo* method)
{
	{
		TagName_t115468581  L_0 = ((  TagName_t115468581  (*) (InternalEnumerator_1_t22959084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t22959084 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TagName_t115468581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1537296273_gshared (InternalEnumerator_1_t22959084 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3872827350_gshared (InternalEnumerator_1_t22959084 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Xml2.XmlTextReader/TagName>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId;
extern "C"  TagName_t115468581  InternalEnumerator_1_get_Current_m1405735267_gshared (InternalEnumerator_1_t22959084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1405735267_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TagName_t115468581  L_8 = ((  TagName_t115468581  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1191579514_gshared (InternalEnumerator_1_t2709235369 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1782050790_gshared (InternalEnumerator_1_t2709235369 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3141792466_gshared (InternalEnumerator_1_t2709235369 * __this, const MethodInfo* method)
{
	{
		ArraySegment_1_t2801744866  L_0 = ((  ArraySegment_1_t2801744866  (*) (InternalEnumerator_1_t2709235369 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2709235369 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArraySegment_1_t2801744866  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2416953809_gshared (InternalEnumerator_1_t2709235369 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m552394578_gshared (InternalEnumerator_1_t2709235369 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.ArraySegment`1<System.Byte>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3176144321_MetadataUsageId;
extern "C"  ArraySegment_1_t2801744866  InternalEnumerator_1_get_Current_m3176144321_gshared (InternalEnumerator_1_t2709235369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3176144321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArraySegment_1_t2801744866  L_8 = ((  ArraySegment_1_t2801744866  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3904876858_gshared (InternalEnumerator_1_t118495844 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (InternalEnumerator_1_t118495844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t118495844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2505350033_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2858864786_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m94889217_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m94889217_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m94889217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2342462508_gshared (InternalEnumerator_1_t2686184324 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (InternalEnumerator_1_t2686184324 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2686184324 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1621603587_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3669835172_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4103229525_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3601500154_gshared (InternalEnumerator_1_t2686197202 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = ((  uint16_t (*) (InternalEnumerator_1_t2686197202 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2686197202 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2748899921_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4256283158_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3048220323_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m263261269_gshared (InternalEnumerator_1_t37517749 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t130027246  L_0 = ((  DictionaryEntry_t130027246  (*) (InternalEnumerator_1_t37517749 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t37517749 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t130027246  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1282215020_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4030197783_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId;
extern "C"  DictionaryEntry_t130027246  InternalEnumerator_1_get_Current_m2915343068_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t130027246  L_8 = ((  DictionaryEntry_t130027246  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2684410586_gshared (InternalEnumerator_1_t1652646218 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2628495494_gshared (InternalEnumerator_1_t1652646218 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3135406386_gshared (InternalEnumerator_1_t1652646218 * __this, const MethodInfo* method)
{
	{
		Link_t1745155715  L_0 = ((  Link_t1745155715  (*) (InternalEnumerator_1_t1652646218 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1652646218 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t1745155715  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3733933361_gshared (InternalEnumerator_1_t1652646218 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3124962674_gshared (InternalEnumerator_1_t1652646218 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m13756385_MetadataUsageId;
extern "C"  Link_t1745155715  InternalEnumerator_1_get_Current_m13756385_gshared (InternalEnumerator_1_t1652646218 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m13756385_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t1745155715  L_8 = ((  Link_t1745155715  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2135273650_gshared (InternalEnumerator_1_t2734247371 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1725010862_gshared (InternalEnumerator_1_t2734247371 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3923248602_gshared (InternalEnumerator_1_t2734247371 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2826756868  L_0 = ((  KeyValuePair_2_t2826756868  (*) (InternalEnumerator_1_t2734247371 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2734247371 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2826756868  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1660227849_gshared (InternalEnumerator_1_t2734247371 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m47820698_gshared (InternalEnumerator_1_t2734247371 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2170878265_MetadataUsageId;
extern "C"  KeyValuePair_2_t2826756868  InternalEnumerator_1_get_Current_m2170878265_gshared (InternalEnumerator_1_t2734247371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2170878265_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2826756868  L_8 = ((  KeyValuePair_2_t2826756868  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m664150035_gshared (InternalEnumerator_1_t723939004 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t816448501  L_0 = ((  KeyValuePair_2_t816448501  (*) (InternalEnumerator_1_t723939004 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t723939004 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t816448501  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m233182634_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m44493661_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m818645564_MetadataUsageId;
extern "C"  KeyValuePair_2_t816448501  InternalEnumerator_1_get_Current_m818645564_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m818645564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t816448501  L_8 = ((  KeyValuePair_2_t816448501  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m732772036_gshared (InternalEnumerator_1_t1621742153 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2057396316_gshared (InternalEnumerator_1_t1621742153 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2577031688_gshared (InternalEnumerator_1_t1621742153 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1714251650  L_0 = ((  KeyValuePair_2_t1714251650  (*) (InternalEnumerator_1_t1621742153 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1621742153 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1714251650  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1880597915_gshared (InternalEnumerator_1_t1621742153 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m108163400_gshared (InternalEnumerator_1_t1621742153 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1323439819_MetadataUsageId;
extern "C"  KeyValuePair_2_t1714251650  InternalEnumerator_1_get_Current_m1323439819_gshared (InternalEnumerator_1_t1621742153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1323439819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1714251650  L_8 = ((  KeyValuePair_2_t1714251650  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1007548845_gshared (InternalEnumerator_1_t2377002004 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1157724883_gshared (InternalEnumerator_1_t2377002004 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3690877567_gshared (InternalEnumerator_1_t2377002004 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2469511501  L_0 = ((  KeyValuePair_2_t2469511501  (*) (InternalEnumerator_1_t2377002004 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2377002004 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2469511501  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1899644868_gshared (InternalEnumerator_1_t2377002004 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3682099391_gshared (InternalEnumerator_1_t2377002004 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2369010356_MetadataUsageId;
extern "C"  KeyValuePair_2_t2469511501  InternalEnumerator_1_get_Current_m2369010356_gshared (InternalEnumerator_1_t2377002004 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2369010356_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2469511501  L_8 = ((  KeyValuePair_2_t2469511501  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3279540306_gshared (InternalEnumerator_1_t1009843983 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2352549902_gshared (InternalEnumerator_1_t1009843983 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3425914426_gshared (InternalEnumerator_1_t1009843983 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1102353480  L_0 = ((  KeyValuePair_2_t1102353480  (*) (InternalEnumerator_1_t1009843983 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1009843983 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1102353480  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m138295465_gshared (InternalEnumerator_1_t1009843983 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m494863354_gshared (InternalEnumerator_1_t1009843983 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m696489177_MetadataUsageId;
extern "C"  KeyValuePair_2_t1102353480  InternalEnumerator_1_get_Current_m696489177_gshared (InternalEnumerator_1_t1009843983 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m696489177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1102353480  L_8 = ((  KeyValuePair_2_t1102353480  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2662086813_gshared (InternalEnumerator_1_t2594345872 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2686855369  L_0 = ((  KeyValuePair_2_t2686855369  (*) (InternalEnumerator_1_t2594345872 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2594345872 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2686855369  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1742566068_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3398874899_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId;
extern "C"  KeyValuePair_2_t2686855369  InternalEnumerator_1_get_Current_m1242840582_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2686855369  L_8 = ((  KeyValuePair_2_t2686855369  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2624907895_gshared (InternalEnumerator_1_t935788022 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1028297519  L_0 = ((  KeyValuePair_2_t1028297519  (*) (InternalEnumerator_1_t935788022 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t935788022 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1028297519  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1793576206_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1172054137_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId;
extern "C"  KeyValuePair_2_t1028297519  InternalEnumerator_1_get_Current_m1033564064_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1028297519  L_8 = ((  KeyValuePair_2_t1028297519  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2957909742_gshared (InternalEnumerator_1_t3220446951 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = ((  KeyValuePair_2_t3312956448  (*) (InternalEnumerator_1_t3220446951 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3220446951 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3312956448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m72014405_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m535991902_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId;
extern "C"  KeyValuePair_2_t3312956448  InternalEnumerator_1_get_Current_m4216610997_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3312956448  L_8 = ((  KeyValuePair_2_t3312956448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1257520974_gshared (InternalEnumerator_1_t3369265799 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1542121362_gshared (InternalEnumerator_1_t3369265799 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3457638398_gshared (InternalEnumerator_1_t3369265799 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3461775296  L_0 = ((  KeyValuePair_2_t3461775296  (*) (InternalEnumerator_1_t3369265799 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3369265799 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3461775296  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3719033509_gshared (InternalEnumerator_1_t3369265799 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1924434430_gshared (InternalEnumerator_1_t3369265799 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2477934869_MetadataUsageId;
extern "C"  KeyValuePair_2_t3461775296  InternalEnumerator_1_get_Current_m2477934869_gshared (InternalEnumerator_1_t3369265799 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2477934869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3461775296  L_8 = ((  KeyValuePair_2_t3461775296  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m869685158_gshared (InternalEnumerator_1_t4228414662 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2139324474_gshared (InternalEnumerator_1_t4228414662 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m60667686_gshared (InternalEnumerator_1_t4228414662 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t25956863  L_0 = ((  KeyValuePair_2_t25956863  (*) (InternalEnumerator_1_t4228414662 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4228414662 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t25956863  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3764918525_gshared (InternalEnumerator_1_t4228414662 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1014342566_gshared (InternalEnumerator_1_t4228414662 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m753937901_MetadataUsageId;
extern "C"  KeyValuePair_2_t25956863  InternalEnumerator_1_get_Current_m753937901_gshared (InternalEnumerator_1_t4228414662 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m753937901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t25956863  L_8 = ((  KeyValuePair_2_t25956863  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1975839218_gshared (InternalEnumerator_1_t3161303675 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4213396078_gshared (InternalEnumerator_1_t3161303675 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1549070308_gshared (InternalEnumerator_1_t3161303675 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3253813172  L_0 = ((  KeyValuePair_2_t3253813172  (*) (InternalEnumerator_1_t3161303675 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3161303675 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3253813172  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3274627657_gshared (InternalEnumerator_1_t3161303675 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3836612382_gshared (InternalEnumerator_1_t3161303675 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m589867419_MetadataUsageId;
extern "C"  KeyValuePair_2_t3253813172  InternalEnumerator_1_get_Current_m589867419_gshared (InternalEnumerator_1_t3161303675 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m589867419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3253813172  L_8 = ((  KeyValuePair_2_t3253813172  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1865178318_gshared (InternalEnumerator_1_t2404181862 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	{
		Link_t2496691359  L_0 = ((  Link_t2496691359  (*) (InternalEnumerator_1_t2404181862 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2404181862 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2496691359  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1986195493_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m608833986_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId;
extern "C"  Link_t2496691359  InternalEnumerator_1_get_Current_m1839009783_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2496691359  L_8 = ((  Link_t2496691359  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1735201994_gshared (InternalEnumerator_1_t4205037797 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	{
		Slot_t2579998  L_0 = ((  Slot_t2579998  (*) (InternalEnumerator_1_t4205037797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4205037797 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2579998  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2838377249_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2675725766_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId;
extern "C"  Slot_t2579998  InternalEnumerator_1_get_Current_m3036426419_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2579998  L_8 = ((  Slot_t2579998  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m831950091_gshared (InternalEnumerator_1_t4205037798 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	{
		Slot_t2579999  L_0 = ((  Slot_t2579999  (*) (InternalEnumerator_1_t4205037798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4205037798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2579999  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m857987234_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3764038305_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId;
extern "C"  Slot_t2579999  InternalEnumerator_1_get_Current_m2267962386_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2579999  L_8 = ((  Slot_t2579999  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3756518911_gshared (InternalEnumerator_1_t246524439 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ((  DateTime_t339033936  (*) (InternalEnumerator_1_t246524439 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t246524439 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t339033936  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1192762006_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m556104049_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId;
extern "C"  DateTime_t339033936  InternalEnumerator_1_get_Current_m4103732200_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t339033936  L_8 = ((  DateTime_t339033936  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2868618147_gshared (InternalEnumerator_1_t1596047757 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	{
		Decimal_t1688557254  L_0 = ((  Decimal_t1688557254  (*) (InternalEnumerator_1_t1596047757 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1596047757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t1688557254  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m136301882_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2432816137_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId;
extern "C"  Decimal_t1688557254  InternalEnumerator_1_get_Current_m3602913834_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t1688557254  L_8 = ((  Decimal_t1688557254  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m923076597_gshared (InternalEnumerator_1_t442007117 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	{
		double L_0 = ((  double (*) (InternalEnumerator_1_t442007117 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t442007117 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3854110732_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253414715_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m403053214_MetadataUsageId;
extern "C"  double InternalEnumerator_1_get_Current_m403053214_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m403053214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1585494054_gshared (InternalEnumerator_1_t2754905232 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = ((  int16_t (*) (InternalEnumerator_1_t2754905232 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2754905232 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2583239037_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253274534_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m296300717_MetadataUsageId;
extern "C"  int16_t InternalEnumerator_1_get_Current_m296300717_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m296300717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m767389920_gshared (InternalEnumerator_1_t2754905290 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t2754905290 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2754905290 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2745733815_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3995645356_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1478851815_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3055898623_gshared (InternalEnumerator_1_t2754905385 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = ((  int64_t (*) (InternalEnumerator_1_t2754905385 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2754905385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m642251926_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3212216237_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId;
extern "C"  int64_t InternalEnumerator_1_get_Current_m1194254150_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m152804899_gshared (InternalEnumerator_1_t584182523 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ((  IntPtr_t (*) (InternalEnumerator_1_t584182523 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t584182523 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1686038458_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m467683661_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId;
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m2342284108_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2616641763_gshared (InternalEnumerator_1_t744596923 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (InternalEnumerator_1_t744596923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t744596923 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2760671866_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3716548237_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId;
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m2178852364_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2025782336_gshared (InternalEnumerator_1_t226225632 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = ((  CustomAttributeNamedArgument_t318735129  (*) (InternalEnumerator_1_t226225632 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t226225632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t318735129  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3114194455_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1042509516_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  InternalEnumerator_1_get_Current_m2477961351_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t318735129  L_8 = ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m374673841_gshared (InternalEnumerator_1_t467906065 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = ((  CustomAttributeTypedArgument_t560415562  (*) (InternalEnumerator_1_t467906065 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t467906065 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t560415562  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1367004360_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2714191419_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  InternalEnumerator_1_get_Current_m3407736504_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t560415562  L_8 = ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m238137273_gshared (InternalEnumerator_1_t1303237477 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	{
		LabelData_t1395746974  L_0 = ((  LabelData_t1395746974  (*) (InternalEnumerator_1_t1303237477 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1303237477 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t1395746974  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3252411600_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4284495539_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId;
extern "C"  LabelData_t1395746974  InternalEnumerator_1_get_Current_m2308068992_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t1395746974  L_8 = ((  LabelData_t1395746974  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4080636215_gshared (InternalEnumerator_1_t228063683 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t320573180  L_0 = ((  LabelFixup_t320573180  (*) (InternalEnumerator_1_t228063683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t228063683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t320573180  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2169103310_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1336166329_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId;
extern "C"  LabelFixup_t320573180  InternalEnumerator_1_get_Current_m3571284320_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t320573180  L_8 = ((  LabelFixup_t320573180  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2480959198_gshared (InternalEnumerator_1_t3630765784 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t3723275281  L_0 = ((  ILTokenInfo_t3723275281  (*) (InternalEnumerator_1_t3630765784 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3630765784 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t3723275281  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1771263541_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2010832750_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId;
extern "C"  ILTokenInfo_t3723275281  InternalEnumerator_1_get_Current_m3618560037_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t3723275281  L_8 = ((  ILTokenInfo_t3723275281  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2990531726_gshared (InternalEnumerator_1_t1642400072 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2590030418_gshared (InternalEnumerator_1_t1642400072 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3027429502_gshared (InternalEnumerator_1_t1642400072 * __this, const MethodInfo* method)
{
	{
		Label_t1734909569  L_0 = ((  Label_t1734909569  (*) (InternalEnumerator_1_t1642400072 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1642400072 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Label_t1734909569  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m50931685_gshared (InternalEnumerator_1_t1642400072 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3965684286_gshared (InternalEnumerator_1_t1642400072 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.Label>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3074747669_MetadataUsageId;
extern "C"  Label_t1734909569  InternalEnumerator_1_get_Current_m3074747669_gshared (InternalEnumerator_1_t1642400072 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3074747669_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Label_t1734909569  L_8 = ((  Label_t1734909569  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1127962797_gshared (InternalEnumerator_1_t1843502757 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2604776403_gshared (InternalEnumerator_1_t1843502757 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2999162761_gshared (InternalEnumerator_1_t1843502757 * __this, const MethodInfo* method)
{
	{
		MonoResource_t1936012254  L_0 = ((  MonoResource_t1936012254  (*) (InternalEnumerator_1_t1843502757 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1843502757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		MonoResource_t1936012254  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2428659396_gshared (InternalEnumerator_1_t1843502757 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m777510403_gshared (InternalEnumerator_1_t1843502757 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MonoResource>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1507698326_MetadataUsageId;
extern "C"  MonoResource_t1936012254  InternalEnumerator_1_get_Current_m1507698326_gshared (InternalEnumerator_1_t1843502757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1507698326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		MonoResource_t1936012254  L_8 = ((  MonoResource_t1936012254  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3879268681_gshared (InternalEnumerator_1_t3697325377 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m839632311_gshared (InternalEnumerator_1_t3697325377 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m164159853_gshared (InternalEnumerator_1_t3697325377 * __this, const MethodInfo* method)
{
	{
		RefEmitPermissionSet_t3789834874  L_0 = ((  RefEmitPermissionSet_t3789834874  (*) (InternalEnumerator_1_t3697325377 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3697325377 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RefEmitPermissionSet_t3789834874  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1596772960_gshared (InternalEnumerator_1_t3697325377 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3649651687_gshared (InternalEnumerator_1_t3697325377 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.RefEmitPermissionSet>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3371742002_MetadataUsageId;
extern "C"  RefEmitPermissionSet_t3789834874  InternalEnumerator_1_get_Current_m3371742002_gshared (InternalEnumerator_1_t3697325377 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3371742002_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RefEmitPermissionSet_t3789834874  L_8 = ((  RefEmitPermissionSet_t3789834874  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2078231777_gshared (InternalEnumerator_1_t407693973 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t500203470  L_0 = ((  ParameterModifier_t500203470  (*) (InternalEnumerator_1_t407693973 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t407693973 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t500203470  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2387364856_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3792346959_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m147444170_MetadataUsageId;
extern "C"  ParameterModifier_t500203470  InternalEnumerator_1_get_Current_m147444170_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m147444170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t500203470  L_8 = ((  ParameterModifier_t500203470  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2772733110_gshared (InternalEnumerator_1_t3607348206 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t3699857703  L_0 = ((  ResourceCacheItem_t3699857703  (*) (InternalEnumerator_1_t3607348206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3607348206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t3699857703  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m420635149_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4160471130_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId;
extern "C"  ResourceCacheItem_t3699857703  InternalEnumerator_1_get_Current_m4042729375_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t3699857703  L_8 = ((  ResourceCacheItem_t3699857703  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2865872515_gshared (InternalEnumerator_1_t3982075075 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t4074584572  L_0 = ((  ResourceInfo_t4074584572  (*) (InternalEnumerator_1_t3982075075 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3982075075 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t4074584572  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3639607322_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3402661033_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId;
extern "C"  ResourceInfo_t4074584572  InternalEnumerator_1_get_Current_m3891250378_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t4074584572  L_8 = ((  ResourceInfo_t4074584572  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2400880886_gshared (InternalEnumerator_1_t1645779784 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (InternalEnumerator_1_t1645779784 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1645779784 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m44740173_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2285731670_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m790384317_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m790384317_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m790384317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1925586605_gshared (InternalEnumerator_1_t2762836567 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = ((  int8_t (*) (InternalEnumerator_1_t2762836567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2762836567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2750196932_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134001983_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m92522612_MetadataUsageId;
extern "C"  int8_t InternalEnumerator_1_get_Current_m92522612_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m92522612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3415441081_gshared (InternalEnumerator_1_t1029642187 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t1122151684  L_0 = ((  X509ChainStatus_t1122151684  (*) (InternalEnumerator_1_t1029642187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1029642187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t1122151684  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2305059792_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1811839991_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId;
extern "C"  X509ChainStatus_t1122151684  InternalEnumerator_1_get_Current_m2419281506_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t1122151684  L_8 = ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2203995436_gshared (InternalEnumerator_1_t865699524 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	{
		float L_0 = ((  float (*) (InternalEnumerator_1_t865699524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t865699524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m479600131_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1722801188_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m1563251989_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3583269882_gshared (InternalEnumerator_1_t858999844 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3782824806_gshared (InternalEnumerator_1_t858999844 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.TermInfoStrings>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125924562_gshared (InternalEnumerator_1_t858999844 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t858999844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t858999844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TermInfoStrings>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m289125969_gshared (InternalEnumerator_1_t858999844 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.TermInfoStrings>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4044746706_gshared (InternalEnumerator_1_t858999844 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.TermInfoStrings>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3348656321_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3348656321_gshared (InternalEnumerator_1_t858999844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3348656321_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m207626719_gshared (InternalEnumerator_1_t3633423279 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	{
		Mark_t3725932776  L_0 = ((  Mark_t3725932776  (*) (InternalEnumerator_1_t3633423279 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3633423279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t3725932776  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1252370038_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2967245969_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId;
extern "C"  Mark_t3725932776  InternalEnumerator_1_get_Current_m3956653384_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t3725932776  L_8 = ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m111087899_gshared (InternalEnumerator_1_t671353395 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = ((  TimeSpan_t763862892  (*) (InternalEnumerator_1_t671353395 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t671353395 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t763862892  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m774844594_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m485566165_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId;
extern "C"  TimeSpan_t763862892  InternalEnumerator_1_get_Current_m2948637700_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t763862892  L_8 = ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1037769859_gshared (InternalEnumerator_1_t893415771 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = ((  uint16_t (*) (InternalEnumerator_1_t893415771 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t893415771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1215749658_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3068600045_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m737292716_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m737292716_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m737292716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m219665725_gshared (InternalEnumerator_1_t893415829 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = ((  uint32_t (*) (InternalEnumerator_1_t893415829 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t893415829 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1378244436_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3810970867_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1919843814_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2508174428_gshared (InternalEnumerator_1_t893415924 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = ((  uint64_t (*) (InternalEnumerator_1_t893415924 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t893415924 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3569729843_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3027541748_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1635246149_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2879257728_gshared (InternalEnumerator_1_t3174019289 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136_gshared (InternalEnumerator_1_t3174019289 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868_gshared (InternalEnumerator_1_t3174019289 * __this, const MethodInfo* method)
{
	{
		UriScheme_t3266528786  L_0 = ((  UriScheme_t3266528786  (*) (InternalEnumerator_1_t3174019289 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3174019289 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t3266528786  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1861559895_gshared (InternalEnumerator_1_t3174019289 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m91355148_gshared (InternalEnumerator_1_t3174019289 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId;
extern "C"  UriScheme_t3266528786  InternalEnumerator_1_get_Current_m3881062791_gshared (InternalEnumerator_1_t3174019289 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t3266528786  L_8 = ((  UriScheme_t3266528786  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2145162288_gshared (InternalEnumerator_1_t2248895222 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2235126640_gshared (InternalEnumerator_1_t2248895222 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3086130214_gshared (InternalEnumerator_1_t2248895222 * __this, const MethodInfo* method)
{
	{
		NsDecl_t2341404719  L_0 = ((  NsDecl_t2341404719  (*) (InternalEnumerator_1_t2248895222 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2248895222 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsDecl_t2341404719  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3530332679_gshared (InternalEnumerator_1_t2248895222 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1353284256_gshared (InternalEnumerator_1_t2248895222 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsDecl>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m105405465_MetadataUsageId;
extern "C"  NsDecl_t2341404719  InternalEnumerator_1_get_Current_m105405465_gshared (InternalEnumerator_1_t2248895222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m105405465_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsDecl_t2341404719  L_8 = ((  NsDecl_t2341404719  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3201546884_gshared (InternalEnumerator_1_t3785365046 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1674495132_gshared (InternalEnumerator_1_t3785365046 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2881108360_gshared (InternalEnumerator_1_t3785365046 * __this, const MethodInfo* method)
{
	{
		NsScope_t3877874543  L_0 = ((  NsScope_t3877874543  (*) (InternalEnumerator_1_t3785365046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3785365046 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		NsScope_t3877874543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1025235291_gshared (InternalEnumerator_1_t3785365046 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3063542280_gshared (InternalEnumerator_1_t3785365046 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Xml.XmlNamespaceManager/NsScope>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m860004555_MetadataUsageId;
extern "C"  NsScope_t3877874543  InternalEnumerator_1_get_Current_m860004555_gshared (InternalEnumerator_1_t3785365046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m860004555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		NsScope_t3877874543  L_8 = ((  NsScope_t3877874543  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1839379985_gshared (InternalEnumerator_1_t1495666263 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ((  Color_t1588175760  (*) (InternalEnumerator_1_t1495666263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1495666263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color_t1588175760  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m643330344_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1891900575_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId;
extern "C"  Color_t1588175760  InternalEnumerator_1_get_Current_m3730699578_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color_t1588175760  L_8 = ((  Color_t1588175760  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2826013104_gshared (InternalEnumerator_1_t4044574710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	{
		Color32_t4137084207  L_0 = ((  Color32_t4137084207  (*) (InternalEnumerator_1_t4044574710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4044574710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t4137084207  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4247678855_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1461072288_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId;
extern "C"  Color32_t4137084207  InternalEnumerator_1_get_Current_m3648321497_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t4137084207  L_8 = ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2847645656_gshared (InternalEnumerator_1_t2858612868 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t2951122365  L_0 = ((  ContactPoint_t2951122365  (*) (InternalEnumerator_1_t2858612868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2858612868 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t2951122365  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2261686191_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2398593716_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId;
extern "C"  ContactPoint_t2951122365  InternalEnumerator_1_get_Current_m3523444575_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t2951122365  L_8 = ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2728019190_gshared (InternalEnumerator_1_t2002543010 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3521736938_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3512084502_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	{
		Keyframe_t2095052507  L_0 = ((  Keyframe_t2095052507  (*) (InternalEnumerator_1_t2002543010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2002543010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t2095052507  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1130719821_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3205116630_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3245714045_MetadataUsageId;
extern "C"  Keyframe_t2095052507  InternalEnumerator_1_get_Current_m3245714045_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3245714045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Keyframe_t2095052507  L_8 = ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2952786262_gshared (InternalEnumerator_1_t2498719112 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3508316298_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2891046656_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	{
		HitInfo_t2591228609  L_0 = ((  HitInfo_t2591228609  (*) (InternalEnumerator_1_t2498719112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2498719112 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HitInfo_t2591228609  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m54857389_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m204092218_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4186452479_MetadataUsageId;
extern "C"  HitInfo_t2591228609  InternalEnumerator_1_get_Current_m4186452479_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4186452479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HitInfo_t2591228609  L_8 = ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3202091545_gshared (InternalEnumerator_1_t1224502599 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3642743527_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3542460115_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t1317012096  L_0 = ((  GcAchievementData_t1317012096  (*) (InternalEnumerator_1_t1224502599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1224502599 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t1317012096  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2757865264_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2931622739_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4069864032_MetadataUsageId;
extern "C"  GcAchievementData_t1317012096  InternalEnumerator_1_get_Current_m4069864032_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4069864032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcAchievementData_t1317012096  L_8 = ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m909397884_gshared (InternalEnumerator_1_t2131168810 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3844431012_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3502383888_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t2223678307  L_0 = ((  GcScoreData_t2223678307  (*) (InternalEnumerator_1_t2131168810 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2131168810 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t2223678307  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3662398547_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3456760080_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2404034883_MetadataUsageId;
extern "C"  GcScoreData_t2223678307  InternalEnumerator_1_get_Current_m2404034883_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2404034883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcScoreData_t2223678307  L_8 = ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3463151677_gshared (InternalEnumerator_1_t3432820291 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1422051907_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2550701049_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = ((  Vector2_t3525329788  (*) (InternalEnumerator_1_t3432820291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3432820291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector2_t3525329788  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2902704724_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2716547187_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m630856742_MetadataUsageId;
extern "C"  Vector2_t3525329788  InternalEnumerator_1_get_Current_m630856742_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m630856742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector2_t3525329788  L_8 = ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m412948862_gshared (InternalEnumerator_1_t3432820292 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1546125154_gshared (InternalEnumerator_1_t3432820292 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3520282072_gshared (InternalEnumerator_1_t3432820292 * __this, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = ((  Vector3_t3525329789  (*) (InternalEnumerator_1_t3432820292 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3432820292 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector3_t3525329789  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector3>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2609301717_gshared (InternalEnumerator_1_t3432820292 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2210988562_gshared (InternalEnumerator_1_t3432820292 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector3>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1984166439_MetadataUsageId;
extern "C"  Vector3_t3525329789  InternalEnumerator_1_get_Current_m1984166439_gshared (InternalEnumerator_1_t3432820292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1984166439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector3_t3525329789  L_8 = ((  Vector3_t3525329789  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m746871257_gshared (InternalEnumerator_1_t1594566981 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2768237351_gshared (InternalEnumerator_1_t1594566981 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2003434259_gshared (InternalEnumerator_1_t1594566981 * __this, const MethodInfo* method)
{
	{
		WebCamDevice_t1687076478  L_0 = ((  WebCamDevice_t1687076478  (*) (InternalEnumerator_1_t1594566981 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1594566981 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WebCamDevice_t1687076478  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3782651632_gshared (InternalEnumerator_1_t1594566981 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2303882131_gshared (InternalEnumerator_1_t1594566981 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3764129312_MetadataUsageId;
extern "C"  WebCamDevice_t1687076478  InternalEnumerator_1_get_Current_m3764129312_gshared (InternalEnumerator_1_t1594566981 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3764129312_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WebCamDevice_t1687076478  L_8 = ((  WebCamDevice_t1687076478  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3198491810_gshared (InternalEnumerator_1_t2968712505 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m560284094_gshared (InternalEnumerator_1_t2968712505 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m766867892_gshared (InternalEnumerator_1_t2968712505 * __this, const MethodInfo* method)
{
	{
		EyewearCalibrationReading_t3061222002  L_0 = ((  EyewearCalibrationReading_t3061222002  (*) (InternalEnumerator_1_t2968712505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2968712505 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		EyewearCalibrationReading_t3061222002  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1887283449_gshared (InternalEnumerator_1_t2968712505 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3450023790_gshared (InternalEnumerator_1_t2968712505 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.EyewearDevice/EyewearCalibrationReading>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m362208459_MetadataUsageId;
extern "C"  EyewearCalibrationReading_t3061222002  InternalEnumerator_1_get_Current_m362208459_gshared (InternalEnumerator_1_t2968712505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m362208459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		EyewearCalibrationReading_t3061222002  L_8 = ((  EyewearCalibrationReading_t3061222002  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1352770337_gshared (InternalEnumerator_1_t3139705527 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2506642655_gshared (InternalEnumerator_1_t3139705527 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3984626699_gshared (InternalEnumerator_1_t3139705527 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t3139705527 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3139705527 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3013177912_gshared (InternalEnumerator_1_t3139705527 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2015912395_gshared (InternalEnumerator_1_t3139705527 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1720576424_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1720576424_gshared (InternalEnumerator_1_t3139705527 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1720576424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1067052690_gshared (InternalEnumerator_1_t697579894 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1001245134_gshared (InternalEnumerator_1_t697579894 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3067239940_gshared (InternalEnumerator_1_t697579894 * __this, const MethodInfo* method)
{
	{
		RectangleData_t790089391  L_0 = ((  RectangleData_t790089391  (*) (InternalEnumerator_1_t697579894 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t697579894 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RectangleData_t790089391  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m302196457_gshared (InternalEnumerator_1_t697579894 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.RectangleData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3326922238_gshared (InternalEnumerator_1_t697579894 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.RectangleData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1519159803_MetadataUsageId;
extern "C"  RectangleData_t790089391  InternalEnumerator_1_get_Current_m1519159803_gshared (InternalEnumerator_1_t697579894 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1519159803_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RectangleData_t790089391  L_8 = ((  RectangleData_t790089391  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2236588999_gshared (InternalEnumerator_1_t3812841213 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3370866297_gshared (InternalEnumerator_1_t3812841213 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2751863599_gshared (InternalEnumerator_1_t3812841213 * __this, const MethodInfo* method)
{
	{
		TargetSearchResult_t3905350710  L_0 = ((  TargetSearchResult_t3905350710  (*) (InternalEnumerator_1_t3812841213 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3812841213 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TargetSearchResult_t3905350710  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1674242654_gshared (InternalEnumerator_1_t3812841213 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1108262057_gshared (InternalEnumerator_1_t3812841213 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3508704432_MetadataUsageId;
extern "C"  TargetSearchResult_t3905350710  InternalEnumerator_1_get_Current_m3508704432_gshared (InternalEnumerator_1_t3812841213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3508704432_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TargetSearchResult_t3905350710  L_8 = ((  TargetSearchResult_t3905350710  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1106170858_gshared (InternalEnumerator_1_t3272961172 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3040769398_gshared (InternalEnumerator_1_t3272961172 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1864977132_gshared (InternalEnumerator_1_t3272961172 * __this, const MethodInfo* method)
{
	{
		PropData_t3365470669  L_0 = ((  PropData_t3365470669  (*) (InternalEnumerator_1_t3272961172 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3272961172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PropData_t3365470669  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2883476033_gshared (InternalEnumerator_1_t3272961172 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1991924262_gshared (InternalEnumerator_1_t3272961172 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/PropData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3601437587_MetadataUsageId;
extern "C"  PropData_t3365470669  InternalEnumerator_1_get_Current_m3601437587_gshared (InternalEnumerator_1_t3272961172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3601437587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		PropData_t3365470669  L_8 = ((  PropData_t3365470669  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m543572142_gshared (InternalEnumerator_1_t602728536 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1539287602_gshared (InternalEnumerator_1_t602728536 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3217979560_gshared (InternalEnumerator_1_t602728536 * __this, const MethodInfo* method)
{
	{
		SmartTerrainRevisionData_t695238033  L_0 = ((  SmartTerrainRevisionData_t695238033  (*) (InternalEnumerator_1_t602728536 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t602728536 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SmartTerrainRevisionData_t695238033  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m68037637_gshared (InternalEnumerator_1_t602728536 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1877179618_gshared (InternalEnumerator_1_t602728536 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m480477527_MetadataUsageId;
extern "C"  SmartTerrainRevisionData_t695238033  InternalEnumerator_1_get_Current_m480477527_gshared (InternalEnumerator_1_t602728536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m480477527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		SmartTerrainRevisionData_t695238033  L_8 = ((  SmartTerrainRevisionData_t695238033  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1291721296_gshared (InternalEnumerator_1_t171984030 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3840465232_gshared (InternalEnumerator_1_t171984030 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2681665340_gshared (InternalEnumerator_1_t171984030 * __this, const MethodInfo* method)
{
	{
		SurfaceData_t264493527  L_0 = ((  SurfaceData_t264493527  (*) (InternalEnumerator_1_t171984030 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t171984030 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		SurfaceData_t264493527  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2960738343_gshared (InternalEnumerator_1_t171984030 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2934594748_gshared (InternalEnumerator_1_t171984030 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/SurfaceData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3379808663_MetadataUsageId;
extern "C"  SurfaceData_t264493527  InternalEnumerator_1_get_Current_m3379808663_gshared (InternalEnumerator_1_t171984030 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3379808663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		SurfaceData_t264493527  L_8 = ((  SurfaceData_t264493527  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2602538245_gshared (InternalEnumerator_1_t2397659923 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1589489787_gshared (InternalEnumerator_1_t2397659923 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3333646119_gshared (InternalEnumerator_1_t2397659923 * __this, const MethodInfo* method)
{
	{
		TrackableResultData_t2490169420  L_0 = ((  TrackableResultData_t2490169420  (*) (InternalEnumerator_1_t2397659923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2397659923 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TrackableResultData_t2490169420  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3558715676_gshared (InternalEnumerator_1_t2397659923 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1937765479_gshared (InternalEnumerator_1_t2397659923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1301446924_MetadataUsageId;
extern "C"  TrackableResultData_t2490169420  InternalEnumerator_1_get_Current_m1301446924_gshared (InternalEnumerator_1_t2397659923 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1301446924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TrackableResultData_t2490169420  L_8 = ((  TrackableResultData_t2490169420  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2154227008_gshared (InternalEnumerator_1_t1030501902 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3277899872_gshared (InternalEnumerator_1_t1030501902 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4183358988_gshared (InternalEnumerator_1_t1030501902 * __this, const MethodInfo* method)
{
	{
		VirtualButtonData_t1123011399  L_0 = ((  VirtualButtonData_t1123011399  (*) (InternalEnumerator_1_t1030501902 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1030501902 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		VirtualButtonData_t1123011399  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3982884631_gshared (InternalEnumerator_1_t1030501902 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1159355724_gshared (InternalEnumerator_1_t1030501902 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3273915719_MetadataUsageId;
extern "C"  VirtualButtonData_t1123011399  InternalEnumerator_1_get_Current_m3273915719_gshared (InternalEnumerator_1_t1030501902 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3273915719_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		VirtualButtonData_t1123011399  L_8 = ((  VirtualButtonData_t1123011399  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3982370001_gshared (InternalEnumerator_1_t25787 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3010827567_gshared (InternalEnumerator_1_t25787 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3282959333_gshared (InternalEnumerator_1_t25787 * __this, const MethodInfo* method)
{
	{
		WordData_t92535284  L_0 = ((  WordData_t92535284  (*) (InternalEnumerator_1_t25787 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t25787 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WordData_t92535284  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m504057832_gshared (InternalEnumerator_1_t25787 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1244404063_gshared (InternalEnumerator_1_t25787 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3632618938_MetadataUsageId;
extern "C"  WordData_t92535284  InternalEnumerator_1_get_Current_m3632618938_gshared (InternalEnumerator_1_t25787 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3632618938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WordData_t92535284  L_8 = ((  WordData_t92535284  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3145404878_gshared (InternalEnumerator_1_t315747064 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3026846994_gshared (InternalEnumerator_1_t315747064 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1387047368_gshared (InternalEnumerator_1_t315747064 * __this, const MethodInfo* method)
{
	{
		WordResultData_t408256561  L_0 = ((  WordResultData_t408256561  (*) (InternalEnumerator_1_t315747064 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t315747064 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		WordResultData_t408256561  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4271891749_gshared (InternalEnumerator_1_t315747064 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2156839490_gshared (InternalEnumerator_1_t315747064 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2909589431_MetadataUsageId;
extern "C"  WordResultData_t408256561  InternalEnumerator_1_get_Current_m2909589431_gshared (InternalEnumerator_1_t315747064 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2909589431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		WordResultData_t408256561  L_8 = ((  WordResultData_t408256561  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3079997611_gshared (InternalEnumerator_1_t1752564634 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2724618005_gshared (InternalEnumerator_1_t1752564634 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1568946827_gshared (InternalEnumerator_1_t1752564634 * __this, const MethodInfo* method)
{
	{
		ProfileData_t1845074131  L_0 = ((  ProfileData_t1845074131  (*) (InternalEnumerator_1_t1752564634 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1752564634 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProfileData_t1845074131  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m104488002_gshared (InternalEnumerator_1_t1752564634 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3824505029_gshared (InternalEnumerator_1_t1752564634 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4020035924_MetadataUsageId;
extern "C"  ProfileData_t1845074131  InternalEnumerator_1_get_Current_m4020035924_gshared (InternalEnumerator_1_t1752564634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4020035924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ProfileData_t1845074131  L_8 = ((  ProfileData_t1845074131  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// T[] System.ArraySegment`1<System.Byte>::get_Array()
extern "C"  ByteU5BU5D_t58506160* ArraySegment_1_get_Array_m888360954_gshared (ArraySegment_1_t2801744866 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = (ByteU5BU5D_t58506160*)__this->get_array_0();
		return L_0;
	}
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Offset()
extern "C"  int32_t ArraySegment_1_get_Offset_m1679307891_gshared (ArraySegment_1_t2801744866 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_offset_1();
		return L_0;
	}
}
// System.Int32 System.ArraySegment`1<System.Byte>::get_Count()
extern "C"  int32_t ArraySegment_1_get_Count_m3668785873_gshared (ArraySegment_1_t2801744866 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		return L_0;
	}
}
// System.Boolean System.ArraySegment`1<System.Byte>::Equals(System.Object)
extern "C"  bool ArraySegment_1_Equals_m357209176_gshared (ArraySegment_1_t2801744866 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = ((  bool (*) (ArraySegment_1_t2801744866 *, ArraySegment_1_t2801744866 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ArraySegment_1_t2801744866 *)__this, (ArraySegment_1_t2801744866 )((*(ArraySegment_1_t2801744866 *)((ArraySegment_1_t2801744866 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// System.Boolean System.ArraySegment`1<System.Byte>::Equals(System.ArraySegment`1<T>)
extern "C"  bool ArraySegment_1_Equals_m78779232_gshared (ArraySegment_1_t2801744866 * __this, ArraySegment_1_t2801744866  ___obj0, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = (ByteU5BU5D_t58506160*)__this->get_array_0();
		ByteU5BU5D_t58506160* L_1 = ((  ByteU5BU5D_t58506160* (*) (ArraySegment_1_t2801744866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ArraySegment_1_t2801744866 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if ((!(((Il2CppObject*)(ByteU5BU5D_t58506160*)L_0) == ((Il2CppObject*)(ByteU5BU5D_t58506160*)L_1))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = ((  int32_t (*) (ArraySegment_1_t2801744866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ArraySegment_1_t2801744866 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_count_2();
		int32_t L_5 = ((  int32_t (*) (ArraySegment_1_t2801744866 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ArraySegment_1_t2801744866 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		return (bool)0;
	}
}
// System.Int32 System.ArraySegment`1<System.Byte>::GetHashCode()
extern "C"  int32_t ArraySegment_1_GetHashCode_m664655932_gshared (ArraySegment_1_t2801744866 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t58506160* L_0 = (ByteU5BU5D_t58506160*)__this->get_array_0();
		NullCheck((Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = (int32_t)__this->get_count_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_2))^(int32_t)L_3));
	}
}
// T[] System.ArraySegment`1<System.Object>::get_Array()
extern "C"  ObjectU5BU5D_t11523773* ArraySegment_1_get_Array_m2695477809_gshared (ArraySegment_1_t860157465 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		return L_0;
	}
}
// System.Int32 System.ArraySegment`1<System.Object>::get_Offset()
extern "C"  int32_t ArraySegment_1_get_Offset_m1214541148_gshared (ArraySegment_1_t860157465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_offset_1();
		return L_0;
	}
}
// System.Int32 System.ArraySegment`1<System.Object>::get_Count()
extern "C"  int32_t ArraySegment_1_get_Count_m2129772744_gshared (ArraySegment_1_t860157465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_2();
		return L_0;
	}
}
// System.Boolean System.ArraySegment`1<System.Object>::Equals(System.Object)
extern "C"  bool ArraySegment_1_Equals_m3000222479_gshared (ArraySegment_1_t860157465 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___obj0;
		if (!((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_0018;
		}
	}
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = ((  bool (*) (ArraySegment_1_t860157465 *, ArraySegment_1_t860157465 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ArraySegment_1_t860157465 *)__this, (ArraySegment_1_t860157465 )((*(ArraySegment_1_t860157465 *)((ArraySegment_1_t860157465 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_2;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// System.Boolean System.ArraySegment`1<System.Object>::Equals(System.ArraySegment`1<T>)
extern "C"  bool ArraySegment_1_Equals_m859659337_gshared (ArraySegment_1_t860157465 * __this, ArraySegment_1_t860157465  ___obj0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		ObjectU5BU5D_t11523773* L_1 = ((  ObjectU5BU5D_t11523773* (*) (ArraySegment_1_t860157465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((ArraySegment_1_t860157465 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if ((!(((Il2CppObject*)(ObjectU5BU5D_t11523773*)L_0) == ((Il2CppObject*)(ObjectU5BU5D_t11523773*)L_1))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = ((  int32_t (*) (ArraySegment_1_t860157465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((ArraySegment_1_t860157465 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_count_2();
		int32_t L_5 = ((  int32_t (*) (ArraySegment_1_t860157465 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((ArraySegment_1_t860157465 *)(&___obj0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		return (bool)0;
	}
}
// System.Int32 System.ArraySegment`1<System.Object>::GetHashCode()
extern "C"  int32_t ArraySegment_1_GetHashCode_m3436756083_gshared (ArraySegment_1_t860157465 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		NullCheck((Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Il2CppObject *)(Il2CppObject *)L_0);
		int32_t L_2 = (int32_t)__this->get_offset_1();
		int32_t L_3 = (int32_t)__this->get_count_2();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)L_2))^(int32_t)L_3));
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C"  void DefaultComparer__ctor_m925764245_gshared (DefaultComparer_t2185368519 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t3302075123 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Comparer_1_t3302075123 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Comparer_1_t3302075123 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Int32>::Compare(T,T)
extern Il2CppClass* IComparable_t1596950936_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2403916996;
extern const uint32_t DefaultComparer_Compare_m470059266_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m470059266_gshared (DefaultComparer_t2185368519 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m470059266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		goto IL_001e;
	}
	{
		goto IL_001c;
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		goto IL_002b;
	}
	{
		return 1;
	}

IL_002b:
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_4);
		if (!((Il2CppObject*)IsInst(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = ___x0;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_7);
		int32_t L_9 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.Int32>::CompareTo(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (int32_t)L_9);
		return L_10;
	}

IL_004d:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_12);
		if (!((Il2CppObject *)IsInst(L_13, IComparable_t1596950936_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = ___x0;
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_15);
		int32_t L_17 = ___y1;
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_18);
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1596950936_il2cpp_TypeInfo_var)));
		int32_t L_20 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1596950936_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_16, IComparable_t1596950936_il2cpp_TypeInfo_var)), (Il2CppObject *)L_19);
		return L_20;
	}

IL_0074:
	{
		ArgumentException_t124305799 * L_21 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_21, (String_t*)_stringLiteral2403916996, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21);
	}
}
// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::.ctor()
extern "C"  void DefaultComparer__ctor_m86943554_gshared (DefaultComparer_t175060152 * __this, const MethodInfo* method)
{
	{
		NullCheck((Comparer_1_t1291766756 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		((  void (*) (Comparer_1_t1291766756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Comparer_1_t1291766756 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>::Compare(T,T)
extern Il2CppClass* IComparable_t1596950936_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2403916996;
extern const uint32_t DefaultComparer_Compare_m341753389_MetadataUsageId;
extern "C"  int32_t DefaultComparer_Compare_m341753389_gshared (DefaultComparer_t175060152 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DefaultComparer_Compare_m341753389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_001d;
	}

IL_001c:
	{
		G_B4_0 = (-1);
	}

IL_001d:
	{
		return G_B4_0;
	}

IL_001e:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		return 1;
	}

IL_002b:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject*)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))))
		{
			goto IL_004d;
		}
	}
	{
		Il2CppObject * L_4 = ___x0;
		Il2CppObject * L_5 = ___y1;
		NullCheck((Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
		int32_t L_6 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable`1<System.Object>::CompareTo(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Il2CppObject*)((Il2CppObject*)Castclass(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))), (Il2CppObject *)L_5);
		return L_6;
	}

IL_004d:
	{
		Il2CppObject * L_7 = ___x0;
		if (!((Il2CppObject *)IsInst(L_7, IComparable_t1596950936_il2cpp_TypeInfo_var)))
		{
			goto IL_0074;
		}
	}
	{
		Il2CppObject * L_8 = ___x0;
		Il2CppObject * L_9 = ___y1;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t1596950936_il2cpp_TypeInfo_var)));
		int32_t L_10 = InterfaceFuncInvoker1< int32_t, Il2CppObject * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t1596950936_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_8, IComparable_t1596950936_il2cpp_TypeInfo_var)), (Il2CppObject *)L_9);
		return L_10;
	}

IL_0074:
	{
		ArgumentException_t124305799 * L_11 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_11, (String_t*)_stringLiteral2403916996, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11);
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.ctor()
extern "C"  void Comparer_1__ctor_m1768876756_gshared (Comparer_1_t3302075123 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Int32>::.cctor()
extern const Il2CppType* GenericComparer_1_t3795808858_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m2813475673_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m2813475673_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m2813475673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(GenericComparer_1_t3795808858_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(83 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3431720054*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3302075123_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t3302075123 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t2185368519 * L_8 = (DefaultComparer_t2185368519 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t2185368519 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3302075123_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m2019036153_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m2019036153_gshared (Comparer_1_t3302075123 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m2019036153_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t3302075123 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Int32>::Compare(T,T) */, (Comparer_1_t3302075123 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (int32_t)((*(int32_t*)((int32_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t124305799 * L_8 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Int32>::get_Default()
extern "C"  Comparer_1_t3302075123 * Comparer_1_get_Default_m3403755004_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3302075123 * L_0 = ((Comparer_1_t3302075123_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.ctor()
extern "C"  void Comparer_1__ctor_m453627619_gshared (Comparer_1_t1291766756 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.Object>::.cctor()
extern const Il2CppType* GenericComparer_1_t3795808858_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1__cctor_m695458090_MetadataUsageId;
extern "C"  void Comparer_1__cctor_m695458090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1__cctor_m695458090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = VirtFuncInvoker1< bool, Type_t * >::Invoke(41 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(GenericComparer_1_t3795808858_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t3431720054* L_4 = (TypeU5BU5D_t3431720054*)((TypeU5BU5D_t3431720054*)SZArrayNew(TypeU5BU5D_t3431720054_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_5);
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3431720054* >::Invoke(83 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t3431720054*)L_4);
		Il2CppObject * L_7 = Activator_CreateInstance_m1399154923(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t1291766756_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(((Comparer_1_t1291766756 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		goto IL_005e;
	}

IL_0054:
	{
		DefaultComparer_t175060152 * L_8 = (DefaultComparer_t175060152 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (DefaultComparer_t175060152 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t1291766756_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->set__default_0(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.Object>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern const uint32_t Comparer_1_System_Collections_IComparer_Compare_m1794290832_MetadataUsageId;
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m1794290832_gshared (Comparer_1_t1291766756 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Comparer_1_System_Collections_IComparer_Compare_m1794290832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Il2CppObject * L_0 = ___x0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Il2CppObject * L_1 = ___y1;
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0013;
	}

IL_0012:
	{
		G_B4_0 = (-1);
	}

IL_0013:
	{
		return G_B4_0;
	}

IL_0014:
	{
		Il2CppObject * L_2 = ___y1;
		if (L_2)
		{
			goto IL_001c;
		}
	}
	{
		return 1;
	}

IL_001c:
	{
		Il2CppObject * L_3 = ___x0;
		if (!((Il2CppObject *)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_4 = ___y1;
		if (!((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0045;
		}
	}
	{
		Il2CppObject * L_5 = ___x0;
		Il2CppObject * L_6 = ___y1;
		NullCheck((Comparer_1_t1291766756 *)__this);
		int32_t L_7 = VirtFuncInvoker2< int32_t, Il2CppObject *, Il2CppObject * >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.Object>::Compare(T,T) */, (Comparer_1_t1291766756 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))), (Il2CppObject *)((Il2CppObject *)Castclass(L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))));
		return L_7;
	}

IL_0045:
	{
		ArgumentException_t124305799 * L_8 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m571182463(L_8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}
}
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::get_Default()
extern "C"  Comparer_1_t1291766756 * Comparer_1_get_Default_m2088913959_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t1291766756 * L_0 = ((Comparer_1_t1291766756_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->get__default_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1504349661_gshared (Enumerator_t3105253512 * __this, Dictionary_2_t3338225570 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t3338225570 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t3338225570 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2674652452_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2826756868  L_0 = (KeyValuePair_2_t2826756868 )__this->get_current_3();
		KeyValuePair_2_t2826756868  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m744059576_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1893697921_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2826756868 * L_0 = (KeyValuePair_2_t2826756868 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t2826756868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t2826756868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t2826756868 * L_4 = (KeyValuePair_2_t2826756868 *)__this->get_address_of_current_3();
		int32_t L_5 = ((  int32_t (*) (KeyValuePair_2_t2826756868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2826756868 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t130027246  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2600671860(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3083847424_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1098376594_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2667991844_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3338225570 * L_4 = (Dictionary_2_t3338225570 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t1417492998* L_5 = (LinkU5BU5D_t1417492998*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t3338225570 * L_8 = (Dictionary_2_t3338225570 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t3338225570 * L_12 = (Dictionary_2_t3338225570 *)__this->get_dictionary_0();
		NullCheck(L_12);
		Int32U5BU5D_t1809983122* L_13 = (Int32U5BU5D_t1809983122*)L_12->get_valueSlots_7();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t2826756868  L_16;
		memset(&L_16, 0, sizeof(L_16));
		((  void (*) (KeyValuePair_2_t2826756868 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(&L_16, (int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (int32_t)((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_16);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)__this->get_next_1();
		Dictionary_2_t3338225570 * L_18 = (Dictionary_2_t3338225570 *)__this->get_dictionary_0();
		NullCheck(L_18);
		int32_t L_19 = (int32_t)L_18->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t2826756868  Enumerator_get_Current_m760986380_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2826756868  L_0 = (KeyValuePair_2_t2826756868 )__this->get_current_3();
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3073711217_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2826756868 * L_0 = (KeyValuePair_2_t2826756868 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t2826756868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t2826756868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m236733781_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2826756868 * L_0 = (KeyValuePair_2_t2826756868 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t2826756868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2826756868 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m619818159_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4290586738;
extern const uint32_t Enumerator_VerifyState_m4151737720_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m4151737720_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m4151737720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3338225570 * L_0 = (Dictionary_2_t3338225570 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t3338225570 * L_2 = (Dictionary_2_t3338225570 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral4290586738, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1609931872;
extern const uint32_t Enumerator_VerifyCurrent_m2540202720_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2540202720_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2540202720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t3105253512 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t3105253512 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral1609931872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1168225727_gshared (Enumerator_t3105253512 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t3338225570 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2377115088_gshared (Enumerator_t1094945145 * __this, Dictionary_2_t1327917203 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t1327917203 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t1327917203 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t816448501  L_0 = (KeyValuePair_2_t816448501 )__this->get_current_3();
		KeyValuePair_2_t816448501  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t816448501 * L_0 = (KeyValuePair_2_t816448501 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t816448501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t816448501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t816448501 * L_4 = (KeyValuePair_2_t816448501 *)__this->get_address_of_current_3();
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (KeyValuePair_2_t816448501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t816448501 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		DictionaryEntry_t130027246  L_6;
		memset(&L_6, 0, sizeof(L_6));
		DictionaryEntry__ctor_m2600671860(&L_6, (Il2CppObject *)L_3, (Il2CppObject *)L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		return L_0;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1213995029_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t1327917203 * L_4 = (Dictionary_2_t1327917203 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t1417492998* L_5 = (LinkU5BU5D_t1417492998*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t1327917203 * L_8 = (Dictionary_2_t1327917203 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t1327917203 * L_12 = (Dictionary_2_t1327917203 *)__this->get_dictionary_0();
		NullCheck(L_12);
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)L_12->get_valueSlots_7();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t816448501  L_16;
		memset(&L_16, 0, sizeof(L_16));
		((  void (*) (KeyValuePair_2_t816448501 *, int32_t, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(&L_16, (int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Il2CppObject *)((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_16);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)__this->get_next_1();
		Dictionary_2_t1327917203 * L_18 = (Dictionary_2_t1327917203 *)__this->get_dictionary_0();
		NullCheck(L_18);
		int32_t L_19 = (int32_t)L_18->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t816448501  Enumerator_get_Current_m1399860359_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t816448501  L_0 = (KeyValuePair_2_t816448501 )__this->get_current_3();
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1767398110_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t816448501 * L_0 = (KeyValuePair_2_t816448501 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t816448501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t816448501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3384846750_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t816448501 * L_0 = (KeyValuePair_2_t816448501 *)__this->get_address_of_current_3();
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (KeyValuePair_2_t816448501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t816448501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1080084514_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4290586738;
extern const uint32_t Enumerator_VerifyState_m2404513451_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m2404513451_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2404513451_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1327917203 * L_0 = (Dictionary_2_t1327917203 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t1327917203 * L_2 = (Dictionary_2_t1327917203 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral4290586738, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1609931872;
extern const uint32_t Enumerator_VerifyCurrent_m2789892947_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m2789892947_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m2789892947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t1094945145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1094945145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral1609931872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1102561394_gshared (Enumerator_t1094945145 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t1327917203 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3672648587_gshared (Enumerator_t1992748294 * __this, Dictionary_2_t2225720352 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2225720352 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2225720352 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2775595830_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1714251650  L_0 = (KeyValuePair_2_t1714251650 )__this->get_current_3();
		KeyValuePair_2_t1714251650  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2099307594_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m195133843_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1714251650 * L_0 = (KeyValuePair_2_t1714251650 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t1714251650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1714251650 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t1714251650 * L_4 = (KeyValuePair_2_t1714251650 *)__this->get_address_of_current_3();
		Label_t1734909569  L_5 = ((  Label_t1734909569  (*) (KeyValuePair_2_t1714251650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1714251650 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Label_t1734909569  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t130027246  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2600671860(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3610752914_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m648411556_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		Label_t1734909569  L_0 = ((  Label_t1734909569  (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		Label_t1734909569  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1073952694_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2225720352 * L_4 = (Dictionary_2_t2225720352 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t1417492998* L_5 = (LinkU5BU5D_t1417492998*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2225720352 * L_8 = (Dictionary_2_t2225720352 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t2225720352 * L_12 = (Dictionary_2_t2225720352 *)__this->get_dictionary_0();
		NullCheck(L_12);
		LabelU5BU5D_t2428975708* L_13 = (LabelU5BU5D_t2428975708*)L_12->get_valueSlots_7();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t1714251650  L_16;
		memset(&L_16, 0, sizeof(L_16));
		((  void (*) (KeyValuePair_2_t1714251650 *, int32_t, Label_t1734909569 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(&L_16, (int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (Label_t1734909569 )((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_16);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)__this->get_next_1();
		Dictionary_2_t2225720352 * L_18 = (Dictionary_2_t2225720352 *)__this->get_dictionary_0();
		NullCheck(L_18);
		int32_t L_19 = (int32_t)L_18->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_Current()
extern "C"  KeyValuePair_2_t1714251650  Enumerator_get_Current_m751583546_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1714251650  L_0 = (KeyValuePair_2_t1714251650 )__this->get_current_3();
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m931825539_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1714251650 * L_0 = (KeyValuePair_2_t1714251650 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t1714251650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t1714251650 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::get_CurrentValue()
extern "C"  Label_t1734909569  Enumerator_get_CurrentValue_m304074727_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1714251650 * L_0 = (KeyValuePair_2_t1714251650 *)__this->get_address_of_current_3();
		Label_t1734909569  L_1 = ((  Label_t1734909569  (*) (KeyValuePair_2_t1714251650 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t1714251650 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Reset()
extern "C"  void Enumerator_Reset_m4132873565_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4290586738;
extern const uint32_t Enumerator_VerifyState_m733854630_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m733854630_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m733854630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2225720352 * L_0 = (Dictionary_2_t2225720352 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2225720352 * L_2 = (Dictionary_2_t2225720352 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral4290586738, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1609931872;
extern const uint32_t Enumerator_VerifyCurrent_m3604534670_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3604534670_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3604534670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t1992748294 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t1992748294 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral1609931872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Reflection.Emit.Label>::Dispose()
extern "C"  void Enumerator_Dispose_m1370176237_gshared (Enumerator_t1992748294 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2225720352 *)NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1492166402_gshared (Enumerator_t2748008145 * __this, Dictionary_2_t2980980203 * ___dictionary0, const MethodInfo* method)
{
	{
		Dictionary_2_t2980980203 * L_0 = ___dictionary0;
		__this->set_dictionary_0(L_0);
		Dictionary_2_t2980980203 * L_1 = ___dictionary0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get_generation_14();
		__this->set_stamp_2(L_2);
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3090578399_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2469511501  L_0 = (KeyValuePair_2_t2469511501 )__this->get_current_3();
		KeyValuePair_2_t2469511501  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2553564275_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2092521916_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2469511501 * L_0 = (KeyValuePair_2_t2469511501 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t2469511501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_2);
		KeyValuePair_2_t2469511501 * L_4 = (KeyValuePair_2_t2469511501 *)__this->get_address_of_current_3();
		TrackableResultData_t2490169420  L_5 = ((  TrackableResultData_t2490169420  (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2469511501 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		TrackableResultData_t2490169420  L_6 = L_5;
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_6);
		DictionaryEntry_t130027246  L_8;
		memset(&L_8, 0, sizeof(L_8));
		DictionaryEntry__ctor_m2600671860(&L_8, (Il2CppObject *)L_3, (Il2CppObject *)L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3091928443_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), &L_1);
		return L_2;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m274301261_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		TrackableResultData_t2490169420  L_0 = ((  TrackableResultData_t2490169420  (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		TrackableResultData_t2490169420  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3494917087_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		goto IL_007b;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->set_next_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t2980980203 * L_4 = (Dictionary_2_t2980980203 *)__this->get_dictionary_0();
		NullCheck(L_4);
		LinkU5BU5D_t1417492998* L_5 = (LinkU5BU5D_t1417492998*)L_4->get_linkSlots_5();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))->get_HashCode_0();
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648LL))))
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t2980980203 * L_8 = (Dictionary_2_t2980980203 *)__this->get_dictionary_0();
		NullCheck(L_8);
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)L_8->get_keySlots_6();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t2980980203 * L_12 = (Dictionary_2_t2980980203 *)__this->get_dictionary_0();
		NullCheck(L_12);
		TrackableResultDataU5BU5D_t3096477957* L_13 = (TrackableResultDataU5BU5D_t3096477957*)L_12->get_valueSlots_7();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t2469511501  L_16;
		memset(&L_16, 0, sizeof(L_16));
		((  void (*) (KeyValuePair_2_t2469511501 *, int32_t, TrackableResultData_t2490169420 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(&L_16, (int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), (TrackableResultData_t2490169420 )((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_15))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		__this->set_current_3(L_16);
		return (bool)1;
	}

IL_007b:
	{
		int32_t L_17 = (int32_t)__this->get_next_1();
		Dictionary_2_t2980980203 * L_18 = (Dictionary_2_t2980980203 *)__this->get_dictionary_0();
		NullCheck(L_18);
		int32_t L_19 = (int32_t)L_18->get_touchedSlots_8();
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0019;
		}
	}
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Current()
extern "C"  KeyValuePair_2_t2469511501  Enumerator_get_Current_m2970416625_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2469511501  L_0 = (KeyValuePair_2_t2469511501 )__this->get_current_3();
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2595226732_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2469511501 * L_0 = (KeyValuePair_2_t2469511501 *)__this->get_address_of_current_3();
		int32_t L_1 = ((  int32_t (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((KeyValuePair_2_t2469511501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C"  TrackableResultData_t2490169420  Enumerator_get_CurrentValue_m4294047248_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2469511501 * L_0 = (KeyValuePair_2_t2469511501 *)__this->get_address_of_current_3();
		TrackableResultData_t2490169420  L_1 = ((  TrackableResultData_t2490169420  (*) (KeyValuePair_2_t2469511501 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t2469511501 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Reset()
extern "C"  void Enumerator_Reset_m2909396564_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->set_next_1(0);
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t973246880_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4290586738;
extern const uint32_t Enumerator_VerifyState_m1873420125_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1873420125_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1873420125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2980980203 * L_0 = (Dictionary_2_t2980980203 *)__this->get_dictionary_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t973246880 * L_1 = (ObjectDisposedException_t973246880 *)il2cpp_codegen_object_new(ObjectDisposedException_t973246880_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		Dictionary_2_t2980980203 * L_2 = (Dictionary_2_t2980980203 *)__this->get_dictionary_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get_generation_14();
		int32_t L_4 = (int32_t)__this->get_stamp_2();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_5 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral4290586738, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::VerifyCurrent()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1609931872;
extern const uint32_t Enumerator_VerifyCurrent_m3510314885_MetadataUsageId;
extern "C"  void Enumerator_VerifyCurrent_m3510314885_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyCurrent_m3510314885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Enumerator_t2748008145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Enumerator_t2748008145 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral1609931872, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Dispose()
extern "C"  void Enumerator_Dispose_m2429817380_gshared (Enumerator_t2748008145 * __this, const MethodInfo* method)
{
	{
		__this->set_dictionary_0((Dictionary_2_t2980980203 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
