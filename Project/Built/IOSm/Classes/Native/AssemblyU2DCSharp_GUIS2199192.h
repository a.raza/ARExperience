﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Component[]
struct ComponentU5BU5D_t552366831;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIS
struct  GUIS_t2199192  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Component[] GUIS::m_buttons
	ComponentU5BU5D_t552366831* ___m_buttons_2;

public:
	inline static int32_t get_offset_of_m_buttons_2() { return static_cast<int32_t>(offsetof(GUIS_t2199192, ___m_buttons_2)); }
	inline ComponentU5BU5D_t552366831* get_m_buttons_2() const { return ___m_buttons_2; }
	inline ComponentU5BU5D_t552366831** get_address_of_m_buttons_2() { return &___m_buttons_2; }
	inline void set_m_buttons_2(ComponentU5BU5D_t552366831* value)
	{
		___m_buttons_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_buttons_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
