﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Component[]
struct ComponentU5BU5D_t552366831;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t2427445838;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t346125962  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Component[] Vuforia.DefaultTrackableEventHandler::m_buttons
	ComponentU5BU5D_t552366831* ___m_buttons_2;
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t2427445838 * ___mTrackableBehaviour_3;

public:
	inline static int32_t get_offset_of_m_buttons_2() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t346125962, ___m_buttons_2)); }
	inline ComponentU5BU5D_t552366831* get_m_buttons_2() const { return ___m_buttons_2; }
	inline ComponentU5BU5D_t552366831** get_address_of_m_buttons_2() { return &___m_buttons_2; }
	inline void set_m_buttons_2(ComponentU5BU5D_t552366831* value)
	{
		___m_buttons_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_buttons_2, value);
	}

	inline static int32_t get_offset_of_mTrackableBehaviour_3() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t346125962, ___mTrackableBehaviour_3)); }
	inline TrackableBehaviour_t2427445838 * get_mTrackableBehaviour_3() const { return ___mTrackableBehaviour_3; }
	inline TrackableBehaviour_t2427445838 ** get_address_of_mTrackableBehaviour_3() { return &___mTrackableBehaviour_3; }
	inline void set_mTrackableBehaviour_3(TrackableBehaviour_t2427445838 * value)
	{
		___mTrackableBehaviour_3 = value;
		Il2CppCodeGenWriteBarrier(&___mTrackableBehaviour_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
