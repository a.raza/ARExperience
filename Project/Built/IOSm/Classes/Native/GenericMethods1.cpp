﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// UnityEngine.Component
struct Component_t2126946602;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct NullPremiumObjectFactory_t2159315963;
// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t2654402693;
// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t2598600267;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Predicate`1<System.Object>
struct Predicate_1_t1408070318;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3019176036;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3123668047;
// UnityEngine.Object[]
struct ObjectU5BU5D_t3051965477;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t113996300;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3709260776;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName115468581.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_ArraySegment_1_gen2801744866.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L1745155715.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22826756868.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21714251650.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22469511501.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21102353480.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22686855369.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297519.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23461775296.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25956863.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23253813172.h"
#include "mscorlib_System_Collections_Generic_Link2496691359.h"
#include "mscorlib_System_Collections_Hashtable_Slot2579998.h"
#include "mscorlib_System_Collections_SortedList_Slot2579998.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD1395746974.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi320573180.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo3723275281.h"
#include "mscorlib_System_Reflection_Emit_Label1734909569.h"
#include "mscorlib_System_Reflection_Emit_MonoResource1936012254.h"
#include "mscorlib_System_Reflection_Emit_RefEmitPermissionS3789834874.h"
#include "mscorlib_System_Reflection_ParameterModifier500203470.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC3699857703.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4074584572.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1738289281.h"
#include "mscorlib_System_SByte2855346064.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684.h"
#include "mscorlib_System_Single958209021.h"
#include "mscorlib_System_TermInfoStrings951509341.h"
#include "System_System_Text_RegularExpressions_Mark3725932776.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_UInt64985925421.h"
#include "System_System_Uri_UriScheme3266528785.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsDecl2341404719.h"
#include "System_Xml_System_Xml_XmlNamespaceManager_NsScope3877874543.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo2591228609.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1317012096.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2223678307.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_WebCamDevice1687076478.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyew3061222002.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3232215024.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData790089391.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3905350710.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3365470669.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_695238033.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_264493527.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl2490169420.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1123011399.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_W92535284.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_408256561.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1845074131.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen4244616972.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFacto2159315963.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder590090693.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilde2654402693.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3703831335MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom3639963509MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_PremiumObjectFacto1638041834MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionFrom3639963509.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager3856558731.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl2598600267.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker3275326447.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTracker2552664724.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker2495541825.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke3586718050.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTracker1348055288.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1666739050MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TypeMapping1672605994MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl3096135087MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl492206548MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl1378695937MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1655490978MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode1421621208MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr1614950671MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl3096135087.h"
#include "Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl492206548.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl1378695937.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1655490978.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode1421621208.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr1614950671.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Predicate_1_gen1408070318.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData2584644259.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "mscorlib_System_Converter_2_gen113996300.h"
#include "mscorlib_System_Converter_2_gen113996300MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable4285654829.h"
#include "System_Core_System_Linq_Check3277941805MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// T System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern "C"  TagName_t115468581  Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273(__this, ___index0, method) ((  TagName_t115468581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Mono.Xml2.XmlTextReader/TagName>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTagName_t115468581_m2315266206_gshared (Il2CppArray * __this, int32_t p0, TagName_t115468581 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTagName_t115468581_m2315266206(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TagName_t115468581 *, const MethodInfo*))Array_GetGenericValueImpl_TisTagName_t115468581_m2315266206_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern "C"  ArraySegment_1_t2801744866  Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617(__this, ___index0, method) ((  ArraySegment_1_t2801744866  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.ArraySegment`1<System.Byte>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisArraySegment_1_t2801744866_m3070707360_gshared (Il2CppArray * __this, int32_t p0, ArraySegment_1_t2801744866 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisArraySegment_1_t2801744866_m3070707360(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ArraySegment_1_t2801744866 *, const MethodInfo*))Array_GetGenericValueImpl_TisArraySegment_1_t2801744866_m3070707360_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257(__this, ___index0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Boolean>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisBoolean_t211005341_m1385618144_gshared (Il2CppArray * __this, int32_t p0, bool* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisBoolean_t211005341_m1385618144(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, bool*, const MethodInfo*))Array_GetGenericValueImpl_TisBoolean_t211005341_m1385618144_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127(__this, ___index0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Byte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisByte_t2778693821_m4168978832_gshared (Il2CppArray * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisByte_t2778693821_m4168978832(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisByte_t2778693821_m4168978832_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t2778706699_m125306601(__this, ___index0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Char>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisChar_t2778706699_m2936770782_gshared (Il2CppArray * __this, int32_t p0, uint16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisChar_t2778706699_m2936770782(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisChar_t2778706699_m2936770782_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t130027246  Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038(__this, ___index0, method) ((  DictionaryEntry_t130027246  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.DictionaryEntry>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDictionaryEntry_t130027246_m812760635_gshared (Il2CppArray * __this, int32_t p0, DictionaryEntry_t130027246 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDictionaryEntry_t130027246_m812760635(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, DictionaryEntry_t130027246 *, const MethodInfo*))Array_GetGenericValueImpl_TisDictionaryEntry_t130027246_m812760635_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t1745155715  Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033(__this, ___index0, method) ((  Link_t1745155715  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLink_t1745155715_m3370671936_gshared (Il2CppArray * __this, int32_t p0, Link_t1745155715 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLink_t1745155715_m3370671936(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Link_t1745155715 *, const MethodInfo*))Array_GetGenericValueImpl_TisLink_t1745155715_m3370671936_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t2826756868  Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353(__this, ___index0, method) ((  KeyValuePair_2_t2826756868  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2826756868_m3681184152_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2826756868 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2826756868_m3681184152(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2826756868 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2826756868_m3681184152_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t816448501  Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488(__this, ___index0, method) ((  KeyValuePair_2_t816448501  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t816448501_m3704726135_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t816448501 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t816448501_m3704726135(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t816448501 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t816448501_m3704726135_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern "C"  KeyValuePair_2_t1714251650  Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399(__this, ___index0, method) ((  KeyValuePair_2_t1714251650  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1714251650_m336808490_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1714251650 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1714251650_m336808490(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1714251650 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1714251650_m336808490_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32)
extern "C"  KeyValuePair_2_t2469511501  Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870(__this, ___index0, method) ((  KeyValuePair_2_t2469511501  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2469511501_m2558417171_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2469511501 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2469511501_m2558417171(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2469511501 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2469511501_m2558417171_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
extern "C"  KeyValuePair_2_t1102353480  Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577(__this, ___index0, method) ((  KeyValuePair_2_t1102353480  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1102353480_m2186014008_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1102353480 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1102353480_m2186014008(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1102353480 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1102353480_m2186014008_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t2686855369  Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102(__this, ___index0, method) ((  KeyValuePair_2_t2686855369  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t2686855369_m3796936129_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t2686855369 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t2686855369_m3796936129(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t2686855369 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t2686855369_m3796936129_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t1028297519  Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332(__this, ___index0, method) ((  KeyValuePair_2_t1028297519  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t1028297519_m2129114587_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t1028297519 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t1028297519_m2129114587(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t1028297519 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t1028297519_m2129114587_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3312956448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653(__this, ___index0, method) ((  KeyValuePair_2_t3312956448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3312956448_m2835209876_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3312956448 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3312956448_m2835209876(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3312956448 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3312956448_m2835209876_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
extern "C"  KeyValuePair_2_t3461775296  Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301(__this, ___index0, method) ((  KeyValuePair_2_t3461775296  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3461775296_m2507183860_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3461775296 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3461775296_m2507183860(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3461775296 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3461775296_m2507183860_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
extern "C"  KeyValuePair_2_t25956863  Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917(__this, ___index0, method) ((  KeyValuePair_2_t25956863  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t25956863_m624812748_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t25956863 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t25956863_m624812748(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t25956863 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t25956863_m624812748_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3253813172  Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513(__this, ___index0, method) ((  KeyValuePair_2_t3253813172  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyValuePair_2_t3253813172_m2618112470_gshared (Il2CppArray * __this, int32_t p0, KeyValuePair_2_t3253813172 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyValuePair_2_t3253813172_m2618112470(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, KeyValuePair_2_t3253813172 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyValuePair_2_t3253813172_m2618112470_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2496691359  Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2496691359_m818406549(__this, ___index0, method) ((  Link_t2496691359  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Generic.Link>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLink_t2496691359_m4096392498_gshared (Il2CppArray * __this, int32_t p0, Link_t2496691359 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLink_t2496691359_m4096392498(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Link_t2496691359 *, const MethodInfo*))Array_GetGenericValueImpl_TisLink_t2496691359_m4096392498_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2579998  Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401(__this, ___index0, method) ((  Slot_t2579998  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.Hashtable/Slot>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSlot_t2579998_m2736456814_gshared (Il2CppArray * __this, int32_t p0, Slot_t2579998 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t2579998_m2736456814(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Slot_t2579998 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t2579998_m2736456814_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2579999  Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440(__this, ___index0, method) ((  Slot_t2579999  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Collections.SortedList/Slot>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSlot_t2579999_m3490444401_gshared (Il2CppArray * __this, int32_t p0, Slot_t2579999 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSlot_t2579999_m3490444401(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Slot_t2579999 *, const MethodInfo*))Array_GetGenericValueImpl_TisSlot_t2579999_m3490444401_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t339033936  Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548(__this, ___index0, method) ((  DateTime_t339033936  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDateTime_t339033936_m2360861859_gshared (Il2CppArray * __this, int32_t p0, DateTime_t339033936 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDateTime_t339033936_m2360861859(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, DateTime_t339033936 *, const MethodInfo*))Array_GetGenericValueImpl_TisDateTime_t339033936_m2360861859_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t1688557254  Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984(__this, ___index0, method) ((  Decimal_t1688557254  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDecimal_t1688557254_m3301856137_gshared (Il2CppArray * __this, int32_t p0, Decimal_t1688557254 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDecimal_t1688557254_m3301856137(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Decimal_t1688557254 *, const MethodInfo*))Array_GetGenericValueImpl_TisDecimal_t1688557254_m3301856137_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990(__this, ___index0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Double>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDouble_t534516614_m3240686553_gshared (Il2CppArray * __this, int32_t p0, double* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDouble_t534516614_m3240686553(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, double*, const MethodInfo*))Array_GetGenericValueImpl_TisDouble_t534516614_m3240686553_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053(__this, ___index0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Int16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisInt16_t2847414729_m2409481484_gshared (Il2CppArray * __this, int32_t p0, int16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt16_t2847414729_m2409481484(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt16_t2847414729_m2409481484_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Int32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisInt32_t2847414787_m1782932550_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt32_t2847414787_m1782932550(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt32_t2847414787_m1782932550_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300(__this, ___index0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Int64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisInt64_t2847414882_m386432805_gshared (Il2CppArray * __this, int32_t p0, int64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisInt64_t2847414882_m386432805(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisInt64_t2847414882_m386432805_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m1819425504(__this, ___index0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.IntPtr>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisIntPtr_t_m3937090951_gshared (Il2CppArray * __this, int32_t p0, IntPtr_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisIntPtr_t_m3937090951(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, IntPtr_t*, const MethodInfo*))Array_GetGenericValueImpl_TisIntPtr_t_m3937090951_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m1537058848(__this, ___index0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Object>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisIl2CppObject_m735189063_gshared (Il2CppArray * __this, int32_t p0, Il2CppObject ** p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisIl2CppObject_m735189063(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Il2CppObject **, const MethodInfo*))Array_GetGenericValueImpl_TisIl2CppObject_m735189063_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t318735129  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667(__this, ___index0, method) ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeNamedArgument>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t318735129_m3008899942_gshared (Il2CppArray * __this, int32_t p0, CustomAttributeNamedArgument_t318735129 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t318735129_m3008899942(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, CustomAttributeNamedArgument_t318735129 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeNamedArgument_t318735129_m3008899942_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t560415562  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746(__this, ___index0, method) ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.CustomAttributeTypedArgument>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t560415562_m2846288151_gshared (Il2CppArray * __this, int32_t p0, CustomAttributeTypedArgument_t560415562 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t560415562_m2846288151(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, CustomAttributeTypedArgument_t560415562 *, const MethodInfo*))Array_GetGenericValueImpl_TisCustomAttributeTypedArgument_t560415562_m2846288151_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t1395746974  Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434(__this, ___index0, method) ((  LabelData_t1395746974  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabelData_t1395746974_m4249583967_gshared (Il2CppArray * __this, int32_t p0, LabelData_t1395746974 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelData_t1395746974_m4249583967(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, LabelData_t1395746974 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelData_t1395746974_m4249583967_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t320573180  Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500(__this, ___index0, method) ((  LabelFixup_t320573180  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabelFixup_t320573180_m482634139_gshared (Il2CppArray * __this, int32_t p0, LabelFixup_t320573180 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabelFixup_t320573180_m482634139(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, LabelFixup_t320573180 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabelFixup_t320573180_m482634139_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t3723275281  Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117(__this, ___index0, method) ((  ILTokenInfo_t3723275281  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.ILTokenInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisILTokenInfo_t3723275281_m3036294916_gshared (Il2CppArray * __this, int32_t p0, ILTokenInfo_t3723275281 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisILTokenInfo_t3723275281_m3036294916(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ILTokenInfo_t3723275281 *, const MethodInfo*))Array_GetGenericValueImpl_TisILTokenInfo_t3723275281_m3036294916_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern "C"  Label_t1734909569  Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877(__this, ___index0, method) ((  Label_t1734909569  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.Label>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLabel_t1734909569_m3421628788_gshared (Il2CppArray * __this, int32_t p0, Label_t1734909569 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLabel_t1734909569_m3421628788(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Label_t1734909569 *, const MethodInfo*))Array_GetGenericValueImpl_TisLabel_t1734909569_m3421628788_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern "C"  MonoResource_t1936012254  Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598(__this, ___index0, method) ((  MonoResource_t1936012254  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.MonoResource>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMonoResource_t1936012254_m2129286481_gshared (Il2CppArray * __this, int32_t p0, MonoResource_t1936012254 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMonoResource_t1936012254_m2129286481(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, MonoResource_t1936012254 *, const MethodInfo*))Array_GetGenericValueImpl_TisMonoResource_t1936012254_m2129286481_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern "C"  RefEmitPermissionSet_t3789834874  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890(__this, ___index0, method) ((  RefEmitPermissionSet_t3789834874  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRefEmitPermissionSet_t3789834874_m2461304301_gshared (Il2CppArray * __this, int32_t p0, RefEmitPermissionSet_t3789834874 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRefEmitPermissionSet_t3789834874_m2461304301(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RefEmitPermissionSet_t3789834874 *, const MethodInfo*))Array_GetGenericValueImpl_TisRefEmitPermissionSet_t3789834874_m2461304301_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t500203470  Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410(__this, ___index0, method) ((  ParameterModifier_t500203470  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Reflection.ParameterModifier>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisParameterModifier_t500203470_m950388357_gshared (Il2CppArray * __this, int32_t p0, ParameterModifier_t500203470 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisParameterModifier_t500203470_m950388357(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ParameterModifier_t500203470 *, const MethodInfo*))Array_GetGenericValueImpl_TisParameterModifier_t500203470_m950388357_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t3699857703  Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445(__this, ___index0, method) ((  ResourceCacheItem_t3699857703  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResourceCacheItem_t3699857703_m911034714_gshared (Il2CppArray * __this, int32_t p0, ResourceCacheItem_t3699857703 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceCacheItem_t3699857703_m911034714(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ResourceCacheItem_t3699857703 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceCacheItem_t3699857703_m911034714_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t4074584572  Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776(__this, ___index0, method) ((  ResourceInfo_t4074584572  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Resources.ResourceReader/ResourceInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisResourceInfo_t4074584572_m4149525161_gshared (Il2CppArray * __this, int32_t p0, ResourceInfo_t4074584572 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisResourceInfo_t4074584572_m4149525161(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ResourceInfo_t4074584572 *, const MethodInfo*))Array_GetGenericValueImpl_TisResourceInfo_t4074584572_m4149525161_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397(__this, ___index0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTypeTag_t1738289281_m1311091868_gshared (Il2CppArray * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTypeTag_t1738289281_m1311091868(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTypeTag_t1738289281_m1311091868_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230(__this, ___index0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611_gshared (Il2CppArray * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218(__this, ___index0, method) ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045_gshared (Il2CppArray * __this, int32_t p0, X509ChainStatus_t1122151684 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, X509ChainStatus_t1122151684 *, const MethodInfo*))Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775(__this, ___index0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Single>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912_gshared (Il2CppArray * __this, int32_t p0, float* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, float*, const MethodInfo*))Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TermInfoStrings>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTermInfoStrings_t951509341_m2810016928_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTermInfoStrings_t951509341_m2810016928(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTermInfoStrings_t951509341_m2810016928_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3725932776_m160824484(__this, ___index0, method) ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Text.RegularExpressions.Mark>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835_gshared (Il2CppArray * __this, int32_t p0, Mark_t3725932776 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Mark_t3725932776 *, const MethodInfo*))Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760(__this, ___index0, method) ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767_gshared (Il2CppArray * __this, int32_t p0, TimeSpan_t763862892 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TimeSpan_t763862892 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256(__this, ___index0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575_gshared (Il2CppArray * __this, int32_t p0, uint16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062(__this, ___index0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641_gshared (Il2CppArray * __this, int32_t p0, uint32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503(__this, ___index0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192_gshared (Il2CppArray * __this, int32_t p0, uint64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t3266528786  Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123(__this, ___index0, method) ((  UriScheme_t3266528786  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Uri/UriScheme>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUriScheme_t3266528786_m4227749606_gshared (Il2CppArray * __this, int32_t p0, UriScheme_t3266528786 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUriScheme_t3266528786_m4227749606(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UriScheme_t3266528786 *, const MethodInfo*))Array_GetGenericValueImpl_TisUriScheme_t3266528786_m4227749606_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern "C"  NsDecl_t2341404719  Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851(__this, ___index0, method) ((  NsDecl_t2341404719  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNsDecl_t2341404719_m806024660_gshared (Il2CppArray * __this, int32_t p0, NsDecl_t2341404719 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNsDecl_t2341404719_m806024660(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NsDecl_t2341404719 *, const MethodInfo*))Array_GetGenericValueImpl_TisNsDecl_t2341404719_m806024660_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern "C"  NsScope_t3877874543  Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815(__this, ___index0, method) ((  NsScope_t3877874543  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Xml.XmlNamespaceManager/NsScope>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisNsScope_t3877874543_m2130078634_gshared (Il2CppArray * __this, int32_t p0, NsScope_t3877874543 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisNsScope_t3877874543_m2130078634(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, NsScope_t3877874543 *, const MethodInfo*))Array_GetGenericValueImpl_TisNsScope_t3877874543_m2130078634_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t1588175760  Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850(__this, ___index0, method) ((  Color_t1588175760  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor_t1588175760_m855411061_gshared (Il2CppArray * __this, int32_t p0, Color_t1588175760 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor_t1588175760_m855411061(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color_t1588175760 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor_t1588175760_m855411061_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403(__this, ___index0, method) ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100_gshared (Il2CppArray * __this, int32_t p0, Color32_t4137084207 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color32_t4137084207 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859(__this, ___index0, method) ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686_gshared (Il2CppArray * __this, int32_t p0, ContactPoint_t2951122365 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint_t2951122365 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013(__this, ___index0, method) ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Keyframe>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980_gshared (Il2CppArray * __this, int32_t p0, Keyframe_t2095052507 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Keyframe_t2095052507 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997(__this, ___index0, method) ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SendMouseEvents/HitInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378_gshared (Il2CppArray * __this, int32_t p0, HitInfo_t2591228609 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, HitInfo_t2591228609 *, const MethodInfo*))Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226(__this, ___index0, method) ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367_gshared (Il2CppArray * __this, int32_t p0, GcAchievementData_t1317012096 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcAchievementData_t1317012096 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207(__this, ___index0, method) ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434_gshared (Il2CppArray * __this, int32_t p0, GcScoreData_t2223678307 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcScoreData_t2223678307 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166(__this, ___index0, method) ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector2>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537_gshared (Il2CppArray * __this, int32_t p0, Vector2_t3525329788 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector2_t3525329788 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989(__this, ___index0, method) ((  Vector3_t3525329789  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector3>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450_gshared (Il2CppArray * __this, int32_t p0, Vector3_t3525329789 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector3_t3525329789 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern "C"  WebCamDevice_t1687076478  Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186(__this, ___index0, method) ((  WebCamDevice_t1687076478  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.WebCamDevice>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisWebCamDevice_t1687076478_m2650379775_gshared (Il2CppArray * __this, int32_t p0, WebCamDevice_t1687076478 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisWebCamDevice_t1687076478_m2650379775(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, WebCamDevice_t1687076478 *, const MethodInfo*))Array_GetGenericValueImpl_TisWebCamDevice_t1687076478_m2650379775_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32)
extern "C"  EyewearCalibrationReading_t3061222002  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777(__this, ___index0, method) ((  EyewearCalibrationReading_t3061222002  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisEyewearCalibrationReading_t3061222002_m4085703686_gshared (Il2CppArray * __this, int32_t p0, EyewearCalibrationReading_t3061222002 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisEyewearCalibrationReading_t3061222002_m4085703686(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, EyewearCalibrationReading_t3061222002 *, const MethodInfo*))Array_GetGenericValueImpl_TisEyewearCalibrationReading_t3061222002_m4085703686_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.Image/PIXEL_FORMAT>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPIXEL_FORMAT_t3232215024_m4075936007_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPIXEL_FORMAT_t3232215024_m4075936007(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisPIXEL_FORMAT_t3232215024_m4075936007_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern "C"  RectangleData_t790089391  Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777(__this, ___index0, method) ((  RectangleData_t790089391  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.RectangleData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRectangleData_t790089391_m2238103222_gshared (Il2CppArray * __this, int32_t p0, RectangleData_t790089391 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRectangleData_t790089391_m2238103222(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RectangleData_t790089391 *, const MethodInfo*))Array_GetGenericValueImpl_TisRectangleData_t790089391_m2238103222_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern "C"  TargetSearchResult_t3905350710  Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772(__this, ___index0, method) ((  TargetSearchResult_t3905350710  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.TargetFinder/TargetSearchResult>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTargetSearchResult_t3905350710_m4164177771_gshared (Il2CppArray * __this, int32_t p0, TargetSearchResult_t3905350710 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTargetSearchResult_t3905350710_m4164177771(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TargetSearchResult_t3905350710 *, const MethodInfo*))Array_GetGenericValueImpl_TisTargetSearchResult_t3905350710_m4164177771_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern "C"  PropData_t3365470669  Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881(__this, ___index0, method) ((  PropData_t3365470669  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/PropData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisPropData_t3365470669_m2848816078_gshared (Il2CppArray * __this, int32_t p0, PropData_t3365470669 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisPropData_t3365470669_m2848816078(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, PropData_t3365470669 *, const MethodInfo*))Array_GetGenericValueImpl_TisPropData_t3365470669_m2848816078_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern "C"  SmartTerrainRevisionData_t695238033  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413(__this, ___index0, method) ((  SmartTerrainRevisionData_t695238033  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSmartTerrainRevisionData_t695238033_m400460690_gshared (Il2CppArray * __this, int32_t p0, SmartTerrainRevisionData_t695238033 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSmartTerrainRevisionData_t695238033_m400460690(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, SmartTerrainRevisionData_t695238033 *, const MethodInfo*))Array_GetGenericValueImpl_TisSmartTerrainRevisionData_t695238033_m400460690_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern "C"  SurfaceData_t264493527  Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851(__this, ___index0, method) ((  SurfaceData_t264493527  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSurfaceData_t264493527_m3549318774_gshared (Il2CppArray * __this, int32_t p0, SurfaceData_t264493527 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSurfaceData_t264493527_m3549318774(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, SurfaceData_t264493527 *, const MethodInfo*))Array_GetGenericValueImpl_TisSurfaceData_t264493527_m3549318774_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern "C"  TrackableResultData_t2490169420  Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910(__this, ___index0, method) ((  TrackableResultData_t2490169420  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTrackableResultData_t2490169420_m3760337259_gshared (Il2CppArray * __this, int32_t p0, TrackableResultData_t2490169420 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTrackableResultData_t2490169420_m3760337259(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TrackableResultData_t2490169420 *, const MethodInfo*))Array_GetGenericValueImpl_TisTrackableResultData_t2490169420_m3760337259_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern "C"  VirtualButtonData_t1123011399  Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419(__this, ___index0, method) ((  VirtualButtonData_t1123011399  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVirtualButtonData_t1123011399_m881749158_gshared (Il2CppArray * __this, int32_t p0, VirtualButtonData_t1123011399 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVirtualButtonData_t1123011399_m881749158(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, VirtualButtonData_t1123011399 *, const MethodInfo*))Array_GetGenericValueImpl_TisVirtualButtonData_t1123011399_m881749158_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern "C"  WordData_t92535284  Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074(__this, ___index0, method) ((  WordData_t92535284  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/WordData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisWordData_t92535284_m1725553269_gshared (Il2CppArray * __this, int32_t p0, WordData_t92535284 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisWordData_t92535284_m1725553269(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, WordData_t92535284 *, const MethodInfo*))Array_GetGenericValueImpl_TisWordData_t92535284_m1725553269_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern "C"  WordResultData_t408256561  Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869(__this, ___index0, method) ((  WordResultData_t408256561  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisWordResultData_t408256561_m4066890610_gshared (Il2CppArray * __this, int32_t p0, WordResultData_t408256561 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisWordResultData_t408256561_m4066890610(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, WordResultData_t408256561 *, const MethodInfo*))Array_GetGenericValueImpl_TisWordResultData_t408256561_m4066890610_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern "C"  ProfileData_t1845074131  Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072(__this, ___index0, method) ((  ProfileData_t1845074131  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<Vuforia.WebCamProfile/ProfileData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisProfileData_t1845074131_m1187019919_gshared (Il2CppArray * __this, int32_t p0, ProfileData_t1845074131 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisProfileData_t1845074131_m1187019919(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ProfileData_t1845074131 *, const MethodInfo*))Array_GetGenericValueImpl_TisProfileData_t1845074131_m1187019919_gshared)(__this, p0, p1, method)
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m807709032_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m807709032(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m807709032_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m833851745(__this, p0, method) ((  Il2CppObject * (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared)(__this, p0, method)
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m95508767_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m95508767(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m95508767_gshared)(__this, method)
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4179409533_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m4179409533(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m4179409533_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m1994270962_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m1994270962(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m1994270962_gshared)(__this, method)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m2192232654_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m2192232654(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m2192232654_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411(__this, p0, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared)(__this, p0, method)
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m150646178_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m150646178(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m150646178_gshared)(__this /* static, unused */, method)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m2029006109_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m2029006109(__this /* static, unused */, ___original0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m2029006109_gshared)(__this /* static, unused */, ___original0, method)
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m512360883(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared)(__this /* static, unused */, method)
// T Vuforia.IPremiumObjectFactory::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * IPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2644247684_gshared (Il2CppObject * __this, const MethodInfo* method);
#define IPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2644247684(__this, method) ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m2644247684_gshared)(__this, method)
// T Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m4049841707_gshared (NullPremiumObjectFactory_t2159315963 * __this, const MethodInfo* method);
#define NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m4049841707(__this, method) ((  Il2CppObject * (*) (NullPremiumObjectFactory_t2159315963 *, const MethodInfo*))NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m4049841707_gshared)(__this, method)
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * SmartTerrainBuilder_CreateReconstruction_TisIl2CppObject_m1046386226_gshared (SmartTerrainBuilder_t590090693 * __this, const MethodInfo* method);
#define SmartTerrainBuilder_CreateReconstruction_TisIl2CppObject_m1046386226(__this, method) ((  Il2CppObject * (*) (SmartTerrainBuilder_t590090693 *, const MethodInfo*))SmartTerrainBuilder_CreateReconstruction_TisIl2CppObject_m1046386226_gshared)(__this, method)
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<System.Object>()
extern "C"  Il2CppObject * SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1245037938_gshared (SmartTerrainBuilderImpl_t2654402693 * __this, const MethodInfo* method);
#define SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1245037938(__this, method) ((  Il2CppObject * (*) (SmartTerrainBuilderImpl_t2654402693 *, const MethodInfo*))SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1245037938_gshared)(__this, method)
// T Vuforia.TrackerManager::GetTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManager_GetTracker_TisIl2CppObject_m1924442114_gshared (TrackerManager_t3856558731 * __this, const MethodInfo* method);
#define TrackerManager_GetTracker_TisIl2CppObject_m1924442114(__this, method) ((  Il2CppObject * (*) (TrackerManager_t3856558731 *, const MethodInfo*))TrackerManager_GetTracker_TisIl2CppObject_m1924442114_gshared)(__this, method)
// T Vuforia.TrackerManager::InitTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManager_InitTracker_TisIl2CppObject_m3447984590_gshared (TrackerManager_t3856558731 * __this, const MethodInfo* method);
#define TrackerManager_InitTracker_TisIl2CppObject_m3447984590(__this, method) ((  Il2CppObject * (*) (TrackerManager_t3856558731 *, const MethodInfo*))TrackerManager_InitTracker_TisIl2CppObject_m3447984590_gshared)(__this, method)
// T Vuforia.TrackerManagerImpl::GetTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManagerImpl_GetTracker_TisIl2CppObject_m2058534466_gshared (TrackerManagerImpl_t2598600267 * __this, const MethodInfo* method);
#define TrackerManagerImpl_GetTracker_TisIl2CppObject_m2058534466(__this, method) ((  Il2CppObject * (*) (TrackerManagerImpl_t2598600267 *, const MethodInfo*))TrackerManagerImpl_GetTracker_TisIl2CppObject_m2058534466_gshared)(__this, method)
// T Vuforia.TrackerManagerImpl::InitTracker<System.Object>()
extern "C"  Il2CppObject * TrackerManagerImpl_InitTracker_TisIl2CppObject_m3309880206_gshared (TrackerManagerImpl_t2598600267 * __this, const MethodInfo* method);
#define TrackerManagerImpl_InitTracker_TisIl2CppObject_m3309880206(__this, method) ((  Il2CppObject * (*) (TrackerManagerImpl_t2598600267 *, const MethodInfo*))TrackerManagerImpl_InitTracker_TisIl2CppObject_m3309880206_gshared)(__this, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C"  ObjectU5BU5D_t11523773* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Predicate_1_t1408070318 * ___match1, const MethodInfo* method);
#define Array_FindAll_TisIl2CppObject_m3670613038(__this /* static, unused */, ___array0, ___match1, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Predicate_1_t1408070318 *, const MethodInfo*))Array_FindAll_TisIl2CppObject_m3670613038_gshared)(__this /* static, unused */, ___array0, ___match1, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m4097160425_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m4097160425(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m4097160425_gshared)(__this /* static, unused */, p0, p1, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t11523773* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542(__this /* static, unused */, ___values0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489(__this /* static, unused */, ___values0, method) ((  CustomAttributeNamedArgumentU5BU5D_t3019176036* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954(__this /* static, unused */, ___values0, method) ((  CustomAttributeTypedArgumentU5BU5D_t3123668047* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared)(__this /* static, unused */, ___values0, method)
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m1562339739_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m1562339739(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m1562339739_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m2453515573_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m2453515573(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m2453515573_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m3436092793_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m3436092793(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3436092793_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m1469303600_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m1469303600(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m1469303600_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m2311584134_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m2311584134(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m2311584134_gshared)(__this, p0, method)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1228840236(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1225053603(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared)(__this, p0, method)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m1021901391_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m1021901391(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m1021901391_gshared)(__this, method)
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m774141441_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m774141441(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m774141441_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t11523773* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3051965477* p0, const MethodInfo* method);
#define Resources_ConvertObjects_TisIl2CppObject_m1537961554(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3051965477*, const MethodInfo*))Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared)(__this /* static, unused */, p0, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t11523773* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Converter_2_t113996300 * ___converter1, const MethodInfo* method);
#define Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410(__this /* static, unused */, ___array0, ___converter1, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Converter_2_t113996300 *, const MethodInfo*))Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared)(__this /* static, unused */, ___array0, ___converter1, method)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3984298619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m3984298619(__this /* static, unused */, ___source0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m3984298619_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t1809983122* Enumerable_ToArray_TisInt32_t2847414787_m4185692666_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisInt32_t2847414787_m4185692666(__this /* static, unused */, ___source0, method) ((  Int32U5BU5D_t1809983122* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisInt32_t2847414787_m4185692666_gshared)(__this /* static, unused */, ___source0, method)
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m3764797323_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3764797323(__this /* static, unused */, ___source0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3764797323_gshared)(__this /* static, unused */, ___source0, method)
// T System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
// T System.Array::InternalArray__get_Item<Mono.Xml2.XmlTextReader/TagName>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_MetadataUsageId;
extern "C"  TagName_t115468581  Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTagName_t115468581_m2189903273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TagName_t115468581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TagName_t115468581 *)(&V_0));
		TagName_t115468581  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.ArraySegment`1<System.Byte>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_MetadataUsageId;
extern "C"  ArraySegment_1_t2801744866  Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisArraySegment_1_t2801744866_m3907926617_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ArraySegment_1_t2801744866  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ArraySegment_1_t2801744866 *)(&V_0));
		ArraySegment_1_t2801744866  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_MetadataUsageId;
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (bool*)(&V_0));
		bool L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_MetadataUsageId;
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Char>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_MetadataUsageId;
extern "C"  uint16_t Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_MetadataUsageId;
extern "C"  DictionaryEntry_t130027246  Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DictionaryEntry_t130027246  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DictionaryEntry_t130027246 *)(&V_0));
		DictionaryEntry_t130027246  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_MetadataUsageId;
extern "C"  Link_t1745155715  Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t1745155715_m1741943033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Link_t1745155715  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t1745155715 *)(&V_0));
		Link_t1745155715  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_MetadataUsageId;
extern "C"  KeyValuePair_2_t2826756868  Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2826756868_m876792353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2826756868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2826756868 *)(&V_0));
		KeyValuePair_2_t2826756868  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_MetadataUsageId;
extern "C"  KeyValuePair_2_t816448501  Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t816448501  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t816448501 *)(&V_0));
		KeyValuePair_2_t816448501  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Reflection.Emit.Label>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_MetadataUsageId;
extern "C"  KeyValuePair_2_t1714251650  Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1714251650_m2620728399_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1714251650  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1714251650 *)(&V_0));
		KeyValuePair_2_t1714251650  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_MetadataUsageId;
extern "C"  KeyValuePair_2_t2469511501  Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2469511501_m63444870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2469511501  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2469511501 *)(&V_0));
		KeyValuePair_2_t2469511501  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_MetadataUsageId;
extern "C"  KeyValuePair_2_t1102353480  Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1102353480_m299048577_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1102353480  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1102353480 *)(&V_0));
		KeyValuePair_2_t1102353480  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_MetadataUsageId;
extern "C"  KeyValuePair_2_t2686855369  Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t2686855369  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t2686855369 *)(&V_0));
		KeyValuePair_2_t2686855369  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_MetadataUsageId;
extern "C"  KeyValuePair_2_t1028297519  Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t1028297519  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t1028297519 *)(&V_0));
		KeyValuePair_2_t1028297519  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_MetadataUsageId;
extern "C"  KeyValuePair_2_t3312956448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t3312956448  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3312956448 *)(&V_0));
		KeyValuePair_2_t3312956448  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_MetadataUsageId;
extern "C"  KeyValuePair_2_t3461775296  Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3461775296_m3874903301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t3461775296  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3461775296 *)(&V_0));
		KeyValuePair_2_t3461775296  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_MetadataUsageId;
extern "C"  KeyValuePair_2_t25956863  Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t25956863_m3707519917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t25956863  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t25956863 *)(&V_0));
		KeyValuePair_2_t25956863  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_MetadataUsageId;
extern "C"  KeyValuePair_2_t3253813172  Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyValuePair_2_t3253813172_m3696534513_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t3253813172  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (KeyValuePair_2_t3253813172 *)(&V_0));
		KeyValuePair_2_t3253813172  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_MetadataUsageId;
extern "C"  Link_t2496691359  Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Link_t2496691359  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Link_t2496691359 *)(&V_0));
		Link_t2496691359  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_MetadataUsageId;
extern "C"  Slot_t2579998  Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t2579998  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Slot_t2579998 *)(&V_0));
		Slot_t2579998  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_MetadataUsageId;
extern "C"  Slot_t2579999  Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Slot_t2579999  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Slot_t2579999 *)(&V_0));
		Slot_t2579999  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
// T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_MetadataUsageId;
extern "C"  DateTime_t339033936  Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t339033936  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DateTime_t339033936 *)(&V_0));
		DateTime_t339033936  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_MetadataUsageId;
extern "C"  Decimal_t1688557254  Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Decimal_t1688557254  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Decimal_t1688557254 *)(&V_0));
		Decimal_t1688557254  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Double>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_MetadataUsageId;
extern "C"  double Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	double V_0 = 0.0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (double*)(&V_0));
		double L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_MetadataUsageId;
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int16_t*)(&V_0));
		int16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_MetadataUsageId;
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int64_t*)(&V_0));
		int64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
// T System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_MetadataUsageId;
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (IntPtr_t*)(&V_0));
		IntPtr_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_MetadataUsageId;
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Il2CppObject **)(&V_0));
		Il2CppObject * L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeNamedArgument_t318735129  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeNamedArgument_t318735129 *)(&V_0));
		CustomAttributeNamedArgument_t318735129  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CustomAttributeTypedArgument_t560415562  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (CustomAttributeTypedArgument_t560415562 *)(&V_0));
		CustomAttributeTypedArgument_t560415562  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_MetadataUsageId;
extern "C"  LabelData_t1395746974  Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LabelData_t1395746974  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelData_t1395746974 *)(&V_0));
		LabelData_t1395746974  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_MetadataUsageId;
extern "C"  LabelFixup_t320573180  Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LabelFixup_t320573180  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (LabelFixup_t320573180 *)(&V_0));
		LabelFixup_t320573180  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_MetadataUsageId;
extern "C"  ILTokenInfo_t3723275281  Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ILTokenInfo_t3723275281  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ILTokenInfo_t3723275281 *)(&V_0));
		ILTokenInfo_t3723275281  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.Label>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_MetadataUsageId;
extern "C"  Label_t1734909569  Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLabel_t1734909569_m1753959877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Label_t1734909569  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Label_t1734909569 *)(&V_0));
		Label_t1734909569  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.MonoResource>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_MetadataUsageId;
extern "C"  MonoResource_t1936012254  Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMonoResource_t1936012254_m358920598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	MonoResource_t1936012254  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (MonoResource_t1936012254 *)(&V_0));
		MonoResource_t1936012254  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.Emit.RefEmitPermissionSet>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_MetadataUsageId;
extern "C"  RefEmitPermissionSet_t3789834874  Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRefEmitPermissionSet_t3789834874_m113952890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RefEmitPermissionSet_t3789834874  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RefEmitPermissionSet_t3789834874 *)(&V_0));
		RefEmitPermissionSet_t3789834874  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_MetadataUsageId;
extern "C"  ParameterModifier_t500203470  Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ParameterModifier_t500203470  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ParameterModifier_t500203470 *)(&V_0));
		ParameterModifier_t500203470  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_MetadataUsageId;
extern "C"  ResourceCacheItem_t3699857703  Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResourceCacheItem_t3699857703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceCacheItem_t3699857703 *)(&V_0));
		ResourceCacheItem_t3699857703  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_MetadataUsageId;
extern "C"  ResourceInfo_t4074584572  Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ResourceInfo_t4074584572  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ResourceInfo_t4074584572 *)(&V_0));
		ResourceInfo_t4074584572  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_MetadataUsageId;
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_MetadataUsageId;
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_MetadataUsageId;
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	X509ChainStatus_t1122151684  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (X509ChainStatus_t1122151684 *)(&V_0));
		X509ChainStatus_t1122151684  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_MetadataUsageId;
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (float*)(&V_0));
		float L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TermInfoStrings>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTermInfoStrings_t951509341_m1248113113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_MetadataUsageId;
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mark_t3725932776  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Mark_t3725932776 *)(&V_0));
		Mark_t3725932776  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_MetadataUsageId;
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeSpan_t763862892 *)(&V_0));
		TimeSpan_t763862892  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_MetadataUsageId;
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_MetadataUsageId;
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint32_t*)(&V_0));
		uint32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_MetadataUsageId;
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint64_t*)(&V_0));
		uint64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_MetadataUsageId;
extern "C"  UriScheme_t3266528786  Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t3266528786_m2328943123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UriScheme_t3266528786  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t3266528786 *)(&V_0));
		UriScheme_t3266528786  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsDecl>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_MetadataUsageId;
extern "C"  NsDecl_t2341404719  Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsDecl_t2341404719_m1899540851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NsDecl_t2341404719  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsDecl_t2341404719 *)(&V_0));
		NsDecl_t2341404719  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Xml.XmlNamespaceManager/NsScope>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_MetadataUsageId;
extern "C"  NsScope_t3877874543  Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisNsScope_t3877874543_m3910748815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	NsScope_t3877874543  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (NsScope_t3877874543 *)(&V_0));
		NsScope_t3877874543  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_MetadataUsageId;
extern "C"  Color_t1588175760  Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color_t1588175760 *)(&V_0));
		Color_t1588175760  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_MetadataUsageId;
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color32_t4137084207  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color32_t4137084207 *)(&V_0));
		Color32_t4137084207  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_MetadataUsageId;
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint_t2951122365  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint_t2951122365 *)(&V_0));
		ContactPoint_t2951122365  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_MetadataUsageId;
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Keyframe_t2095052507  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Keyframe_t2095052507 *)(&V_0));
		Keyframe_t2095052507  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_MetadataUsageId;
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t2591228609  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (HitInfo_t2591228609 *)(&V_0));
		HitInfo_t2591228609  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_MetadataUsageId;
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcAchievementData_t1317012096  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcAchievementData_t1317012096 *)(&V_0));
		GcAchievementData_t1317012096  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_MetadataUsageId;
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcScoreData_t2223678307  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcScoreData_t2223678307 *)(&V_0));
		GcScoreData_t2223678307  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_MetadataUsageId;
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector2_t3525329788 *)(&V_0));
		Vector2_t3525329788  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_MetadataUsageId;
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3_t3525329789 *)(&V_0));
		Vector3_t3525329789  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.WebCamDevice>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_MetadataUsageId;
extern "C"  WebCamDevice_t1687076478  Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWebCamDevice_t1687076478_m3923092186_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WebCamDevice_t1687076478  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WebCamDevice_t1687076478 *)(&V_0));
		WebCamDevice_t1687076478  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.EyewearDevice/EyewearCalibrationReading>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_MetadataUsageId;
extern "C"  EyewearCalibrationReading_t3061222002  Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisEyewearCalibrationReading_t3061222002_m2025683777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EyewearCalibrationReading_t3061222002  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (EyewearCalibrationReading_t3061222002 *)(&V_0));
		EyewearCalibrationReading_t3061222002  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.Image/PIXEL_FORMAT>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPIXEL_FORMAT_t3232215024_m2801720466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.RectangleData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_MetadataUsageId;
extern "C"  RectangleData_t790089391  Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRectangleData_t790089391_m28733777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RectangleData_t790089391  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RectangleData_t790089391 *)(&V_0));
		RectangleData_t790089391  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.TargetFinder/TargetSearchResult>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_MetadataUsageId;
extern "C"  TargetSearchResult_t3905350710  Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTargetSearchResult_t3905350710_m610339772_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TargetSearchResult_t3905350710  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TargetSearchResult_t3905350710 *)(&V_0));
		TargetSearchResult_t3905350710  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/PropData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_MetadataUsageId;
extern "C"  PropData_t3365470669  Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisPropData_t3365470669_m3474501881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	PropData_t3365470669  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (PropData_t3365470669 *)(&V_0));
		PropData_t3365470669  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SmartTerrainRevisionData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_MetadataUsageId;
extern "C"  SmartTerrainRevisionData_t695238033  Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t695238033_m2436319413_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainRevisionData_t695238033  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SmartTerrainRevisionData_t695238033 *)(&V_0));
		SmartTerrainRevisionData_t695238033  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/SurfaceData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_MetadataUsageId;
extern "C"  SurfaceData_t264493527  Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSurfaceData_t264493527_m251254851_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SurfaceData_t264493527  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (SurfaceData_t264493527 *)(&V_0));
		SurfaceData_t264493527  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/TrackableResultData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_MetadataUsageId;
extern "C"  TrackableResultData_t2490169420  Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTrackableResultData_t2490169420_m1372804910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TrackableResultData_t2490169420  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TrackableResultData_t2490169420 *)(&V_0));
		TrackableResultData_t2490169420  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/VirtualButtonData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_MetadataUsageId;
extern "C"  VirtualButtonData_t1123011399  Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVirtualButtonData_t1123011399_m3281838419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	VirtualButtonData_t1123011399  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (VirtualButtonData_t1123011399 *)(&V_0));
		VirtualButtonData_t1123011399  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_MetadataUsageId;
extern "C"  WordData_t92535284  Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWordData_t92535284_m2367129074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WordData_t92535284  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WordData_t92535284 *)(&V_0));
		WordData_t92535284  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.VuforiaManagerImpl/WordResultData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_MetadataUsageId;
extern "C"  WordResultData_t408256561  Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisWordResultData_t408256561_m4093502869_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WordResultData_t408256561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (WordResultData_t408256561 *)(&V_0));
		WordResultData_t408256561  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
// T System.Array::InternalArray__get_Item<Vuforia.WebCamProfile/ProfileData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_MetadataUsageId;
extern "C"  ProfileData_t1845074131  Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisProfileData_t1845074131_m2854602072_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ProfileData_t1845074131  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ProfileData_t1845074131 *)(&V_0));
		ProfileData_t1845074131  L_4 = V_0;
		return L_4;
	}
}
// T UnityEngine.Component::GetComponent<System.Object>()
// T UnityEngine.Component::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t4244616972_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t4244616972  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t4244616972_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)__this);
		Component_GetComponentFastPath_m1455568887((Component_t2126946602 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m807709032_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((Component_t2126946602 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInChildren_TisIl2CppObject_m833851745_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared (Component_t2126946602 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInChildren_TisIl2CppObject_m833851745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((Component_t2126946602 *)__this);
		Component_t2126946602 * L_2 = Component_GetComponentInChildren_m1899663946((Component_t2126946602 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Component::GetComponentInParent<System.Object>()
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInParent_TisIl2CppObject_m95508767_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m95508767_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInParent_TisIl2CppObject_m95508767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)__this);
		Component_t2126946602 * L_1 = Component_GetComponentInParent_m1953645192((Component_t2126946602 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.GameObject::AddComponent<System.Object>()
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_AddComponent_TisIl2CppObject_m4179409533_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m4179409533_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_AddComponent_TisIl2CppObject_m4179409533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		Component_t2126946602 * L_1 = GameObject_AddComponent_m2208780168((GameObject_t4012695102 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.GameObject::GetComponent<System.Object>()
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t4244616972_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponent_TisIl2CppObject_m1994270962_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m1994270962_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponent_TisIl2CppObject_m1994270962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t4244616972  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t4244616972_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		GameObject_GetComponentFastPath_m2905716663((GameObject_t4012695102 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m2192232654_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t4012695102 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)__this);
		Component_t2126946602 * L_2 = GameObject_GetComponentInChildren_m1490154500((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::FindObjectOfType<System.Object>()
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_TisIl2CppObject_m150646178_MetadataUsageId;
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m150646178_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_TisIl2CppObject_m150646178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m2029006109_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m2029006109_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m2029006109_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3473406, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___original0;
		Object_t3878351788 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, (Object_t3878351788 *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_MetadataUsageId;
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ScriptableObject_t184905905 * L_1 = ScriptableObject_CreateInstance_m3255479417(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T Vuforia.IPremiumObjectFactory::CreateReconstruction<System.Object>()
// T Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::CreateReconstruction<System.Object>()
// T Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::CreateReconstruction<System.Object>()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m4049841707_MetadataUsageId;
extern "C"  Il2CppObject * NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m4049841707_gshared (NullPremiumObjectFactory_t2159315963 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NullPremiumObjectFactory_CreateReconstruction_TisIl2CppObject_m4049841707_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<System.Object>()
// T Vuforia.SmartTerrainBuilderImpl::CreateReconstruction<System.Object>()
extern const Il2CppType* ReconstructionFromTarget_t1377534133_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3703831335_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2085963696_il2cpp_TypeInfo_var;
extern Il2CppClass* ReconstructionFromTargetImpl_t3639963509_il2cpp_TypeInfo_var;
extern const uint32_t SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1245037938_MetadataUsageId;
extern "C"  Il2CppObject * SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1245037938_gshared (SmartTerrainBuilderImpl_t2654402693 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmartTerrainBuilderImpl_CreateReconstruction_TisIl2CppObject_m1245037938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(ReconstructionFromTarget_t1377534133_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3703831335_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m1518294522(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_2);
		IntPtr_t L_3 = InterfaceFuncInvoker0< IntPtr_t >::Invoke(75 /* System.IntPtr Vuforia.IVuforiaWrapper::SmartTerrainBuilderCreateReconstructionFromTarget() */, IVuforiaWrapper_t2085963696_il2cpp_TypeInfo_var, (Il2CppObject *)L_2);
		ReconstructionFromTargetImpl_t3639963509 * L_4 = (ReconstructionFromTargetImpl_t3639963509 *)il2cpp_codegen_object_new(ReconstructionFromTargetImpl_t3639963509_il2cpp_TypeInfo_var);
		ReconstructionFromTargetImpl__ctor_m298934325(L_4, (IntPtr_t)L_3, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_4, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0030:
	{
		Il2CppObject * L_5 = PremiumObjectFactory_get_Instance_m2007919130(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_5);
		Il2CppObject * L_6 = GenericInterfaceFuncInvoker0< Il2CppObject * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2), (Il2CppObject *)L_5);
		return L_6;
	}
}
// T Vuforia.TrackerManager::GetTracker<System.Object>()
// T Vuforia.TrackerManager::InitTracker<System.Object>()
// T Vuforia.TrackerManagerImpl::GetTracker<System.Object>()
// T Vuforia.TrackerManagerImpl::GetTracker<System.Object>()
extern const Il2CppType* ObjectTracker_t3275326447_0_0_0_var;
extern const Il2CppType* MarkerTracker_t2552664724_0_0_0_var;
extern const Il2CppType* TextTracker_t2495541825_0_0_0_var;
extern const Il2CppType* SmartTerrainTracker_t3586718050_0_0_0_var;
extern const Il2CppType* DeviceTracker_t1348055288_0_0_0_var;
extern const Il2CppType* RotationalDeviceTracker_t4008517455_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2887163856;
extern const uint32_t TrackerManagerImpl_GetTracker_TisIl2CppObject_m2058534466_MetadataUsageId;
extern "C"  Il2CppObject * TrackerManagerImpl_GetTracker_TisIl2CppObject_m2058534466_gshared (TrackerManagerImpl_t2598600267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackerManagerImpl_GetTracker_TisIl2CppObject_m2058534466_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(ObjectTracker_t3275326447_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_0) == ((Il2CppObject*)(Type_t *)L_1))))
		{
			goto IL_0027;
		}
	}
	{
		ObjectTracker_t3275326447 * L_2 = (ObjectTracker_t3275326447 *)__this->get_mObjectTracker_1();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0027:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(MarkerTracker_t2552664724_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4))))
		{
			goto IL_004e;
		}
	}
	{
		MarkerTracker_t2552664724 * L_5 = (MarkerTracker_t2552664724 *)__this->get_mMarkerTracker_2();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_5, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_7 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(TextTracker_t2495541825_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_0075;
		}
	}
	{
		TextTracker_t2495541825 * L_8 = (TextTracker_t2495541825 *)__this->get_mTextTracker_3();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_8, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_10 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(SmartTerrainTracker_t3586718050_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_9) == ((Il2CppObject*)(Type_t *)L_10))))
		{
			goto IL_009c;
		}
	}
	{
		SmartTerrainTracker_t3586718050 * L_11 = (SmartTerrainTracker_t3586718050 *)__this->get_mSmartTerrainTracker_4();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_11, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_009c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(DeviceTracker_t1348055288_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13)))
		{
			goto IL_00c8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_15 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(RotationalDeviceTracker_t4008517455_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_14) == ((Il2CppObject*)(Type_t *)L_15))))
		{
			goto IL_00d9;
		}
	}

IL_00c8:
	{
		DeviceTracker_t1348055288 * L_16 = (DeviceTracker_t1348055288 *)__this->get_mDeviceTracker_5();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_16, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2887163856, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_0));
		Il2CppObject * L_17 = V_0;
		return L_17;
	}
}
// T Vuforia.TrackerManagerImpl::InitTracker<System.Object>()
// T Vuforia.TrackerManagerImpl::InitTracker<System.Object>()
extern const Il2CppType* DeviceTracker_t1348055288_0_0_0_var;
extern const Il2CppType* RotationalDeviceTracker_t4008517455_0_0_0_var;
extern const Il2CppType* ObjectTracker_t3275326447_0_0_0_var;
extern const Il2CppType* MarkerTracker_t2552664724_0_0_0_var;
extern const Il2CppType* TextTracker_t2495541825_0_0_0_var;
extern const Il2CppType* SmartTerrainTracker_t3586718050_0_0_0_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t1666739050_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3703831335_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeMapping_t1672605994_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2085963696_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectTrackerImpl_t3096135087_il2cpp_TypeInfo_var;
extern Il2CppClass* MarkerTrackerImpl_t492206548_il2cpp_TypeInfo_var;
extern Il2CppClass* TextTrackerImpl_t1378695937_il2cpp_TypeInfo_var;
extern Il2CppClass* SmartTerrainTrackerImpl_t1655490978_il2cpp_TypeInfo_var;
extern Il2CppClass* RotationalPlayModeDeviceTrackerImpl_t1421621208_il2cpp_TypeInfo_var;
extern Il2CppClass* RotationalDeviceTrackerImpl_t1614950671_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2444913673;
extern Il2CppCodeGenString* _stringLiteral1568033648;
extern const uint32_t TrackerManagerImpl_InitTracker_TisIl2CppObject_m3309880206_MetadataUsageId;
extern "C"  Il2CppObject * TrackerManagerImpl_InitTracker_TisIl2CppObject_m3309880206_gshared (TrackerManagerImpl_t2598600267 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TrackerManagerImpl_InitTracker_TisIl2CppObject_m3309880206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1666739050_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m3553018023(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_1 = V_1;
		return L_1;
	}

IL_0011:
	{
		V_0 = (bool)1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1666739050_il2cpp_TypeInfo_var);
		bool L_2 = VuforiaRuntimeUtilities_IsPlayMode_m908661311(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(DeviceTracker_t1348055288_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_3) == ((Il2CppObject*)(Type_t *)L_4)))
		{
			goto IL_0046;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(RotationalDeviceTracker_t4008517455_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_5) == ((Il2CppObject*)(Type_t *)L_6))))
		{
			goto IL_0048;
		}
	}

IL_0046:
	{
		V_0 = (bool)0;
	}

IL_0048:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3703831335_il2cpp_TypeInfo_var);
		Il2CppObject * L_8 = VuforiaWrapper_get_Instance_m1518294522(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t1672605994_il2cpp_TypeInfo_var);
		uint16_t L_10 = TypeMapping_GetTypeID_m66495655(NULL /*static, unused*/, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck((Il2CppObject *)L_8);
		int32_t L_11 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(118 /* System.Int32 Vuforia.IVuforiaWrapper::TrackerManagerInitTracker(System.Int32) */, IVuforiaWrapper_t2085963696_il2cpp_TypeInfo_var, (Il2CppObject *)L_8, (int32_t)L_10);
		if (L_11)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral2444913673, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_12 = V_2;
		return L_12;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_14 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(ObjectTracker_t3275326447_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_13) == ((Il2CppObject*)(Type_t *)L_14))))
		{
			goto IL_00b4;
		}
	}
	{
		ObjectTracker_t3275326447 * L_15 = (ObjectTracker_t3275326447 *)__this->get_mObjectTracker_1();
		if (L_15)
		{
			goto IL_00a3;
		}
	}
	{
		ObjectTrackerImpl_t3096135087 * L_16 = (ObjectTrackerImpl_t3096135087 *)il2cpp_codegen_object_new(ObjectTrackerImpl_t3096135087_il2cpp_TypeInfo_var);
		ObjectTrackerImpl__ctor_m1310261615(L_16, /*hidden argument*/NULL);
		__this->set_mObjectTracker_1(L_16);
	}

IL_00a3:
	{
		ObjectTracker_t3275326447 * L_17 = (ObjectTracker_t3275326447 *)__this->get_mObjectTracker_1();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_17, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_19 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(MarkerTracker_t2552664724_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_18) == ((Il2CppObject*)(Type_t *)L_19))))
		{
			goto IL_00ee;
		}
	}
	{
		MarkerTracker_t2552664724 * L_20 = (MarkerTracker_t2552664724 *)__this->get_mMarkerTracker_2();
		if (L_20)
		{
			goto IL_00dd;
		}
	}
	{
		MarkerTrackerImpl_t492206548 * L_21 = (MarkerTrackerImpl_t492206548 *)il2cpp_codegen_object_new(MarkerTrackerImpl_t492206548_il2cpp_TypeInfo_var);
		MarkerTrackerImpl__ctor_m1464182506(L_21, /*hidden argument*/NULL);
		__this->set_mMarkerTracker_2(L_21);
	}

IL_00dd:
	{
		MarkerTracker_t2552664724 * L_22 = (MarkerTracker_t2552664724 *)__this->get_mMarkerTracker_2();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_22, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_24 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(TextTracker_t2495541825_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_23) == ((Il2CppObject*)(Type_t *)L_24))))
		{
			goto IL_0128;
		}
	}
	{
		TextTracker_t2495541825 * L_25 = (TextTracker_t2495541825 *)__this->get_mTextTracker_3();
		if (L_25)
		{
			goto IL_0117;
		}
	}
	{
		TextTrackerImpl_t1378695937 * L_26 = (TextTrackerImpl_t1378695937 *)il2cpp_codegen_object_new(TextTrackerImpl_t1378695937_il2cpp_TypeInfo_var);
		TextTrackerImpl__ctor_m3872193949(L_26, /*hidden argument*/NULL);
		__this->set_mTextTracker_3(L_26);
	}

IL_0117:
	{
		TextTracker_t2495541825 * L_27 = (TextTracker_t2495541825 *)__this->get_mTextTracker_3();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_27, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0128:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_28 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_29 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(SmartTerrainTracker_t3586718050_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_28) == ((Il2CppObject*)(Type_t *)L_29))))
		{
			goto IL_0162;
		}
	}
	{
		SmartTerrainTracker_t3586718050 * L_30 = (SmartTerrainTracker_t3586718050 *)__this->get_mSmartTerrainTracker_4();
		if (L_30)
		{
			goto IL_0151;
		}
	}
	{
		SmartTerrainTrackerImpl_t1655490978 * L_31 = (SmartTerrainTrackerImpl_t1655490978 *)il2cpp_codegen_object_new(SmartTerrainTrackerImpl_t1655490978_il2cpp_TypeInfo_var);
		SmartTerrainTrackerImpl__ctor_m4231978780(L_31, /*hidden argument*/NULL);
		__this->set_mSmartTerrainTracker_4(L_31);
	}

IL_0151:
	{
		SmartTerrainTracker_t3586718050 * L_32 = (SmartTerrainTracker_t3586718050 *)__this->get_mSmartTerrainTracker_4();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_32, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_0162:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_34 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(DeviceTracker_t1348055288_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_33) == ((Il2CppObject*)(Type_t *)L_34)))
		{
			goto IL_018e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_35 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_36 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(RotationalDeviceTracker_t4008517455_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_35) == ((Il2CppObject*)(Type_t *)L_36))))
		{
			goto IL_01c6;
		}
	}

IL_018e:
	{
		DeviceTracker_t1348055288 * L_37 = (DeviceTracker_t1348055288 *)__this->get_mDeviceTracker_5();
		if (L_37)
		{
			goto IL_01b5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t1666739050_il2cpp_TypeInfo_var);
		bool L_38 = VuforiaRuntimeUtilities_IsPlayMode_m908661311(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_01aa;
		}
	}
	{
		RotationalPlayModeDeviceTrackerImpl_t1421621208 * L_39 = (RotationalPlayModeDeviceTrackerImpl_t1421621208 *)il2cpp_codegen_object_new(RotationalPlayModeDeviceTrackerImpl_t1421621208_il2cpp_TypeInfo_var);
		RotationalPlayModeDeviceTrackerImpl__ctor_m4100767270(L_39, /*hidden argument*/NULL);
		__this->set_mDeviceTracker_5(L_39);
		goto IL_01b5;
	}

IL_01aa:
	{
		RotationalDeviceTrackerImpl_t1614950671 * L_40 = (RotationalDeviceTrackerImpl_t1614950671 *)il2cpp_codegen_object_new(RotationalDeviceTrackerImpl_t1614950671_il2cpp_TypeInfo_var);
		RotationalDeviceTrackerImpl__ctor_m1445799887(L_40, /*hidden argument*/NULL);
		__this->set_mDeviceTracker_5(L_40);
	}

IL_01b5:
	{
		DeviceTracker_t1348055288 * L_41 = (DeviceTracker_t1348055288 *)__this->get_mDeviceTracker_5();
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_41, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_01c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1568033648, /*hidden argument*/NULL);
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_42 = V_3;
		return L_42;
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral103668165;
extern const uint32_t Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Predicate_1_t1408070318 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t1408070318 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral103668165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t11523773* L_4 = ___array0;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ObjectU5BU5D_t11523773* L_5 = ___array0;
		V_3 = (ObjectU5BU5D_t11523773*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t11523773* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (Il2CppObject *)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		Predicate_1_t1408070318 * L_9 = ___match1;
		Il2CppObject * L_10 = V_2;
		NullCheck((Predicate_1_t1408070318 *)L_9);
		bool L_11 = ((  bool (*) (Predicate_1_t1408070318 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Predicate_1_t1408070318 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_12 = V_1;
		int32_t L_13 = V_0;
		int32_t L_14 = (int32_t)L_13;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		Il2CppObject * L_15 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
	}

IL_0058:
	{
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_4;
		ObjectU5BU5D_t11523773* L_18 = V_3;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_19 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773**)(&V_1), (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t11523773* L_20 = V_1;
		return L_20;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t11523773* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t11523773* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)((Il2CppObject *)Castclass(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = V_0;
		return L_9;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3019176036* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_9 = V_0;
		return L_9;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t3123668047* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_9 = V_0;
		return L_9;
	}
}
// T[] UnityEngine.Component::GetComponents<System.Object>()
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m1562339739_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_1 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m3436092793_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m1469303600_gshared (Component_t2126946602 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t2126946602 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m2453515573_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m2453515573_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m2453515573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m1021901391_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	{
		NullCheck((GameObject_t4012695102 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m2311584134_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m2311584134_gshared (GameObject_t4012695102 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m2311584134_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t4012695102 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m774141441_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m774141441_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m774141441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ObjectU5BU5D_t3051965477* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3051965477*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t3051965477*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t11523773* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3051965477* ___rawObjects0, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3051965477* L_0 = ___rawObjects0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t11523773*)NULL;
	}

IL_0008:
	{
		ObjectU5BU5D_t3051965477* L_1 = ___rawObjects0;
		NullCheck(L_1);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_002b;
	}

IL_0018:
	{
		ObjectU5BU5D_t11523773* L_2 = V_0;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t3051965477* L_4 = ___rawObjects0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t11523773* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_10 = V_0;
		return L_10;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral3945236896;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Converter_2_t113996300 * ___converter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t113996300 * L_2 = ___converter1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral3945236896, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t11523773* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t11523773* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t113996300 * L_7 = ___converter1;
		ObjectU5BU5D_t11523773* L_8 = ___array0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((Converter_2_t113996300 *)L_7);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Converter_2_t113996300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Converter_2_t113996300 *)L_7, (Il2CppObject *)((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_11);
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_1;
		ObjectU5BU5D_t11523773* L_14 = ___array0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_15 = V_0;
		return L_15;
	}
}
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t Enumerable_First_TisIl2CppObject_m3984298619_MetadataUsageId;
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m3984298619_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerable_First_TisIl2CppObject_m3984298619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject* V_0 = NULL;
	Il2CppObject* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (Il2CppObject*)L_3);
		if (!L_4)
		{
			goto IL_0026;
		}
	}
	{
		Il2CppObject* L_5 = V_0;
		NullCheck((Il2CppObject*)L_5);
		Il2CppObject * L_6 = InterfaceFuncInvoker1< Il2CppObject *, int32_t >::Invoke(3 /* !0 System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (int32_t)0);
		return L_6;
	}

IL_0026:
	{
		InvalidOperationException_t2420574324 * L_7 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_002c:
	{
		Il2CppObject* L_8 = ___source0;
		NullCheck((Il2CppObject*)L_8);
		Il2CppObject* L_9 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 2), (Il2CppObject*)L_8);
		V_1 = (Il2CppObject*)L_9;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject* L_10 = V_1;
			NullCheck((Il2CppObject *)L_10);
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
			if (!L_11)
			{
				goto IL_004a;
			}
		}

IL_003e:
		{
			Il2CppObject* L_12 = V_1;
			NullCheck((Il2CppObject*)L_12);
			Il2CppObject * L_13 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 3), (Il2CppObject*)L_12);
			V_2 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x62, FINALLY_004f);
		}

IL_004a:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_004f;
	}

FINALLY_004f:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_14 = V_1;
			if (!L_14)
			{
				goto IL_005b;
			}
		}

IL_0055:
		{
			Il2CppObject* L_15 = V_1;
			NullCheck((Il2CppObject *)L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
		}

IL_005b:
		{
			IL2CPP_END_FINALLY(79)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(79)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_005c:
	{
		InvalidOperationException_t2420574324 * L_16 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_0062:
	{
		Il2CppObject * L_17 = V_2;
		return L_17;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  Int32U5BU5D_t1809983122* Enumerable_ToArray_TisInt32_t2847414787_m4185692666_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	Int32U5BU5D_t1809983122* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		Int32U5BU5D_t1809983122* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< Int32U5BU5D_t1809983122*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (Int32U5BU5D_t1809983122*)L_6, (int32_t)0);
		Int32U5BU5D_t1809983122* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t3644373756 * L_9 = (List_1_t3644373756 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t3644373756 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t3644373756 *)L_9);
		Int32U5BU5D_t1809983122* L_10 = ((  Int32U5BU5D_t1809983122* (*) (List_1_t3644373756 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->method)((List_1_t3644373756 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
// TSource[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
extern "C"  ObjectU5BU5D_t11523773* Enumerable_ToArray_TisIl2CppObject_m3764797323_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, const MethodInfo* method)
{
	Il2CppObject* V_0 = NULL;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	{
		Il2CppObject* L_0 = ___source0;
		Check_Source_m228347543(NULL /*static, unused*/, (Il2CppObject *)L_0, /*hidden argument*/NULL);
		Il2CppObject* L_1 = ___source0;
		V_0 = (Il2CppObject*)((Il2CppObject*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
		Il2CppObject* L_2 = V_0;
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Il2CppObject* L_3 = V_0;
		NullCheck((Il2CppObject*)L_3);
		int32_t L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count() */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_3);
		V_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 1), (uint32_t)L_4));
		Il2CppObject* L_5 = V_0;
		ObjectU5BU5D_t11523773* L_6 = V_1;
		NullCheck((Il2CppObject*)L_5);
		InterfaceActionInvoker2< ObjectU5BU5D_t11523773*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(!0[],System.Int32) */, IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (Il2CppObject*)L_5, (ObjectU5BU5D_t11523773*)L_6, (int32_t)0);
		ObjectU5BU5D_t11523773* L_7 = V_1;
		return L_7;
	}

IL_0029:
	{
		Il2CppObject* L_8 = ___source0;
		List_1_t1634065389 * L_9 = (List_1_t1634065389 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->rgctx_data, 2));
		((  void (*) (List_1_t1634065389 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)(L_9, (Il2CppObject*)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		NullCheck((List_1_t1634065389 *)L_9);
		ObjectU5BU5D_t11523773* L_10 = ((  ObjectU5BU5D_t11523773* (*) (List_1_t1634065389 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4)->method)((List_1_t1634065389 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 4));
		return L_10;
	}
}
