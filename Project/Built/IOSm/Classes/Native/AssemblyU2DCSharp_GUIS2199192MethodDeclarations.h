﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GUIS
struct GUIS_t2199192;

#include "codegen/il2cpp-codegen.h"

// System.Void GUIS::.ctor()
extern "C"  void GUIS__ctor_m1524164355 (GUIS_t2199192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIS::OnGUI()
extern "C"  void GUIS_OnGUI_m1019563005 (GUIS_t2199192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GUIS::WindowFunction(System.Int32)
extern "C"  void GUIS_WindowFunction_m4070079386 (GUIS_t2199192 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
